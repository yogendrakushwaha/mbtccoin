/*-------------------Get Setting Details------------------------*/

function setting_value(id)
{
	var csrf = $("input[name='csrf_token']").val();
	$.ajax(
		{
			type: "POST",
			url: baseUrl+"get_details/get_setting_details",
			dataType: 'json',
			data:{'id': id, csrf_token: csrf},
			beforeSend : function(){
				$.blockUI(
					{
						message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
					});
				setTimeout($.unblockUI, 1000);
			},
			success: function(msg) {
				$("input[name='csrf_token']").val(msg.csrf);
				$("#txtname").empty();
				$("#txtval").empty();
				$("#txtdesc").empty();
				$.each(msg.rec,function(i,item)
					   {
					$("#txtname").val(item.m00_name);
					$("#txtval").val(item.m00_value);
					$("#txtdesc").val(item.m00_desc);
				});
				$("#signupForm").attr("action","update_setting/"+id)
			}
		});
}

/*-----------------Reset Password------------------------*/

function reset_pwd()
{
	var txtuserid =$('#txtuserid').val();
	$.ajax(
		{
			type: "POST",
			url:baseUrl+"auth/resetpassword",
			data: "txtuserid="+txtuserid,
			beforeSend : function(){
				$.blockUI(
					{
						message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
					});
				setTimeout($.unblockUI, 1000);
			},
			success: function(msg) {
				alert("A Link is send on your Registered Email Id For Reset Your password !");
				window.location.href = baseUrl;
			}
		});	
}

/*-----------------SELECT SINGLE USER IN CHECK BOX------------------------*/

var quid="";
function chbchecksin()
{
	quid="";
	var collection=$("#userid");
	var inputs=collection.find("input[type=checkbox]");
	for(var x=0;x<inputs.length;x++)
	{
		var id=inputs[x].id;
		if(document.getElementById(id).checked)
		{ 
			quid=id+","+quid;
		}
	}
	$("#txtquid").val(quid);
}

/*-----------------SELECT MULTI USER IN CHECK BOX------------------------*/

function chbcheckall()
{
	quid="";
	var collection=$("#userid");
	var inputs=collection.find("input[type=checkbox]");
	for(var x=0;x<inputs.length;x++)
	{
		var id=inputs[x].id;
		if(document.getElementById('checkAll').checked == true)
		{
			if(document.getElementById(id).checked == false)
			{
				document.getElementById(id).checked=true;
				quid=id+","+quid;
			}
			else
			{
				quid=id+","+quid;
			}
		}
		else
		{
			document.getElementById(id).checked=false;
			quid="";
		}
	}
	$("#txtquid").val(quid);
}

/*-----------------Change Password------------------------*/

function change_pwd(userid,type)
{
	if(type == 1)
	{
		$("#headtxt").html("Change Password");
		$("#chg_name").html("Change Password");
		$("#signupForm1").attr("action","update_password/"+userid);
	}
	else
	{
		$("#headtxt").html("Change Transaction Password");
		$("#chg_name").html("Change Transaction Password");
		$("#signupForm1").attr("action","update_pin_password/"+userid);
	}
}

/*-------------------Get Tree Details------------------------*/

function get_details(id,regid)
{
	var total = 0;
	$("#"+id).empty();
	$("#"+id).removeAttr('style');
	$("#"+id).css("margin-left","50%");
	var csrf = $("input[name='csrf_token']").val();

	$.ajax({
		type: "POST",
		url: baseUrl+"get_details/get_tree_tooltips",
		dataType: 'json',
		data:{'regid': regid, csrf_token: csrf},
		success: function(msg) {
			$("input[name='csrf_token']").val(msg.csrf);
			$("#"+id).empty();
			$.each(msg.json_result,function(i,item)
				   {
				$("#"+id).append("<table border='1' style='z-index:1000; position:absolute; background-color:rgb(146, 139, 139); width:230px;'><tr><td colspan='2'>UserId</td><td colspan='2'>"+item.USERID+"</td></tr><tr><td nowrap colspan='2'>User Name</td><td colspan='2'>"+item.USERNAME+"</td></tr><tr><td colspan='2'>Introducer Userid</td><td colspan='2'>"+item.Intro_userid+"</td></tr><tr><td colspan='2'>Introducer Name</td><td colspan='2'>"+item.INTRONAME+"</td></tr><tr><td>DOJ</td><td nowrap width='100px'>"+item.DOJ+"</td><td nowrap>Topup Date</td><td nowrap width='100px'>"+item.TOPUP_DATE+"</td></tr><tr><td nowrap>Total Left</td><td>"+item.TOTLEFT+"</td><td nowrap>Total Right</td><td>"+item.TOTRIGHT+"</td></tr><tr><td nowrap>Left Topup</td><td>"+item.TOPUPLEFT+"</td><td nowrap>Right Topup</td><td>"+item.TOPUPRIGHT+"</td></tr><tr><td nowrap>Left BV</td><td>"+item.TOPUPLEFT+"</td><td nowrap>Right BV</td><td>"+item.TOPUPRIGHT+"</td></tr><tr><td nowrap>Non Topup</td><td>"+(item.TOTLEFT-item.TOPUPLEFT)+"</td><td nowrap>Non Topup</td><td>"+(item.TOTRIGHT-item.TOPUPRIGHT)+"</td></tr></table>");
			});
		}
	});
}

function get_details_hide(id)
{
	$("#"+id).css("visibility","hidden");
}


/*--------------------------Update Bank Details---------------------------------*/
function get_bank_details(bankid,branch,ifsc,acc,pan,userid,adhar)
{
	var bankid = bankid.trim();
	var branch = branch.trim();
	var ifsc = ifsc.trim();
	var acc = acc.trim();
	var pan = pan.trim();
	var adhar = adhar.trim();

	$("#txtbank").val(bankid);
	$("#txtbranch").val(branch);
	$("#txtifsc").val(ifsc);
	$("#txtacc").val(acc);
	$("#txtpancard").val(pan);
	$("#txtadhar").val(adhar);

	$("#signupForm").attr("action","update_bank_details/"+userid);
}

/*--------------------------Get Ticket Details------------------------------*/
function get_ticket_details(id)
{
	$("#divtitle").empty();
	$("#divdesc").empty();
	$("#divreply").empty();
	$("#txtid").val('');
	var csrf = $("input[name='csrf_token']").val();
	$.ajax(
		{
			type : "POST",
			url : baseUrl+"get_details/get_ticket_details",
			dataType : 'json',
			data : {'id':id, csrf_token: csrf},
			beforeSend : function(){
				$.blockUI(
					{
						message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
					});
				setTimeout($.unblockUI, 1000);
			},
			success : function(msg) {
				$("input[name='csrf_token']").val(msg.csrf);
				$.each(msg.rec,function(i,item)
					   {
					$("#divtitle").append(item.TICKET_TITLE);
					$("#divdesc").append(item.TICKET_DESC);
					$("#divreply").append(item.TICKET_REPLY);
					$("#txtid").val(id);
				});
			}
		});
}

/*--------------------------Get Change Plan Amount------------------------------*/
function change_plan()
{
	var ddpack = $("#ddpack").val();
	if(ddpack == 1)
		$("#txtamt").val('5750');
	if(ddpack == 2)
		$("#txtamt").val('11500');
}


/*-----------------Validate User Details------------------------*/

function chaech_topup()
{
	var txtuserid =$('#txtuserid').val();
	var csrf = $("input[name='csrf_token']").val();
	$.ajax(
		{
			type: "POST",
			url:baseUrl+"get_details/view_check_topup",
			dataType : 'json',
			data: {'txtintuserid':txtuserid, csrf_token: csrf},
			success: function(msg) {
				$("input[name='csrf_token']").val(msg.csrf);
				if(msg.vali == '0')
				{
					validate_user();
				}
				else
				{
					$("#divtxtuserid").html('This Is already topuped');
					$("#txtuserid").val('');
				}
			}
		});	
}

/*-----------------Validate User Details------------------------*/

function validate_user()
{
	var txtuserid =$('#txtuserid').val();
	var csrf = $("input[name='csrf_token']").val();
	$.ajax(
		{
			type: "POST",
			url:baseUrl+"get_details/get_member_name",
			dataType : 'json',
			data: {'txtintuserid':txtuserid, csrf_token: csrf},
			success: function(msg) {
				$("input[name='csrf_token']").val(msg.csrf);
				if(msg.name!='' && msg.name!='false')
				{
					$("#divtxtuserid").html(msg.name);
				}
				else
				{
					$("#divtxtuserid").html('This Is Not Validate id');
					$("#txtuserid").val('');
				}
			}
		});	
}

/*-----------------Direct Date------------------------*/

function get_direct_date()
{
	window.location.href = baseUrl+"admin_closing/insert_direct_date";
}

/*-----------------Closing Date------------------------*/

function get_closing_date()
{
	window.location.href = baseUrl+"admin_closing/insert_closing_date";
}

/*-----------------Payout Date------------------------*/

function get_payout_date()
{
	window.location.href = baseUrl+"admin_closing/insert_payout_date";
}

/*-----------------Payment Release--------------------*/

function approve_payment()
{
	var txtquid =$('#txtquid').val();
	var csrf = $("input[name='csrf_token']").val();
	if(txtquid != '')
	{
		$.ajax({
			type: "POST",
			url:baseUrl+"admin_closing/insert_payment_release",
			data: {'txtquid':txtquid, csrf_token: csrf},
			success: function(msg) {
				alert("Payment Approve Successfully.");
				window.location.reload();
			}
		});	
	}
	else
	{
		alert("Plaese Select First");
	}
}

// sms campagining
function send_sms_user()
{ 
	var txtquid = $("#txtquid").val();
	var txtdesc = $("#txtdesc").val();
	var csrf = $("input[name='csrf_token']").val();
	$("#b_send").hide();
	$.ajax(
		{
			type:"POST",
			url:baseUrl+"master/send_sms_hid",
			data:{'txtdesc': txtdesc,'txtquid': txtquid,csrf_token: csrf},
			success: function(msg) {

				alert("Message send Successfully.");
				window.location.href = baseUrl+"index.php/master/view_send_sms";
			}
		});

}

//------------------Make Branch Update----------------------

function make_branch(id)
{
	$("#signupForm1").attr("action", baseUrl+"user_action/insert_make_branch/"+id);
}