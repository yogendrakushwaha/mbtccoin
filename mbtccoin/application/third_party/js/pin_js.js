//////////////////////////////////////////////////////////////////////////
//////////         MODEL SCHEDULING SCRIPT        //////////
/////////////////////////////////////////////////////////////////////


/*----------------------Validate User--------------------------*/

function validate_user()
{
	var login_id = $('#login_id').val();
	var csrf = $("input[name='csrf_token']").val();
	$("#btnsubmit").hide();
	$.ajax({
		type: "POST",
		url:baseUrl+"get_details/get_member_name/",
		dataType: 'json',
		data: {'txtintuserid':login_id, csrf_token: csrf},
		success: function(msg){
			$("input[name='csrf_token']").val(msg.csrf);
			if(msg.name!="false")
			{
				$("#validate_user").empty();
				$("#validate_user").append("<div style='color:red'>"+msg.name+"</div>");
				$("#btnsubmit").show();
			}
			else
			{
				$("#validate_user").empty();
				$("#validate_user").append("<div style='color:red'>This User Id is Not Valid</div>");
			}
		}
	});
}

/*----------------------Fill Point--------------------------*/

function fill_points()
{
	var plan=$("#pln_nm").val();
	var csrf = $("input[name='csrf_token']").val();
	
	$.ajax({
		type: "POST",
		url:baseUrl+"pin/select_points/",
		dataType: 'json',
		data: {'plan':plan, csrf_token: csrf},
		success: function(msg){
			$("input[name='csrf_token']").val(msg.csrf);
			$("#point").val(msg.m_pack_fee);
		}
	});
}

/*-----------------------Show Selected Pin-------------------------*/

function show_pin()
{
	var quant=$("#quantity").val();
	var id=$("#txtowner").val();
	var pintype=$("#pintype").val();
	var plan=$("#pln_nm").val();
	var csrf = $("input[name='csrf_token']").val();
	
	if (id == null || id == "")
	{
		alert("Please Enter User Id");
	}
	else
	{
		if (quant == null || quant == "")
		{
			alert("Please Enter Pin Quantity Correctly");
		}
		else
		{
			$("#pin").load(baseUrl+"pin/select_pin_transfer/"+quant+"/"+id+"/"+pintype+"/"+plan+"/");
		}
	}
}

/*----------------------View Active Pin--------------------------*/

function get_active_pin()
{
	var txtfrom=$('#txtfrom').val(),
	txtto=$('#txtto').val(),
	txtuser=$('#txtuser').val(),
	buisnespln=$('#buisnespln').val();
	var csrf = $("input[name='csrf_token']").val();
	
	$.ajax(
	{
		type : "POST",
		url : baseUrl+"pin/getcancel_pin_report/",
		dataType : 'json',
		data : {'txtfrom': txtfrom,'txtto': txtto,'txtuser': txtuser,'buisnespln': buisnespln, csrf_token: csrf},
		success: function(data) {
			
			$("#pin > tbody").empty();
			
			$.each(data,function(i,item)
			{
				$('#pin').append("<tr><td>"+item.SN+"</td><td>"+item.NAME+"</td><td>"+item.USER_ID+"</td><td>"+item.PIN_NO+"</td><td>"+item.PACKAGE+"</td><td>"+item.FEES+"</td><td>"+item.TYPE+"</td><td><a href="+baseUrl+"pin/update_cancel_pin/"+item.PIN_ID+" title='Cancel'><span class='glyphicon glyphicon-trash'></span></a></td></tr>");
			});  
			
		}
	});
}