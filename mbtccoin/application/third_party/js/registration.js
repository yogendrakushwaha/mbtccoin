//////////////////////////////////////////////////////////////////////////
//////////         MODEL SCHEDULING SCRIPT        //////////
/////////////////////////////////////////////////////////////////////

/*-----------------Get Intro Name From User id------------------------*/


function get_intro_detail()
{
	var introducer = $("#txtintroducer_id").val();
	if($("#txtintroducer_id").val()!="")
	{
		get_member_name(introducer,1);
		get_upliner_details();
	}
	else
	{
		var mssg="";
		$('#txtintroducer_name').val(mssg);
	}
}

/*-----------------Get Name From User id------------------------*/

function get_member_name(name,id)
{
	var intoducer = name;
	var csrf = $("input[name='csrf_token']").val();
	if($("#txtintroducer_id").val()!="")
	{
		$.ajax({	
			type:"POST",
			url:baseUrl+"get_details/get_member_name",
			dataType: 'json',
			data:{"txtintuserid":intoducer,csrf_token:csrf},
			beforeSend : function(){
				$.blockUI(
					{
						message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
					});
				setTimeout($.unblockUI, 1000);
			},
			success:function(msg) 
			{
				$("input[name='csrf_token']").val(msg.csrf);
				if(msg.name!="" && msg.name!="0" && msg.name!="false")
				{
					if(id==1)
					{
						$('#txtintroducer_name').val(msg.name);
					}
					if(id==2)
					{
						$('#txtparent_name').val(msg.name);
					}
				}
				else
				{
					$('#txtintroducer_id').val('');
					$('#txtintroducer_name').val('');
				}
			}
		});
	}
}

$(function() {
	get_upliner_details();
});
/*--------------Get Upliner Details In Level Plan-------------------*/

function get_upliner_details()
{
	var name = $("#txtintroducer_id").val();
	if(name!="")
	{
		$.ajax({	
			type:"POST",
			url:baseUrl+"get_details/get_upliner_details",
			dataType: 'json',
			data:{"txtintuserid":name},
			beforeSend : function(){
				$.blockUI(
					{
						message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
					});
				setTimeout($.unblockUI, 1000);
			},
			success:function(msg) 
			{
				if(msg.rec!="" && msg.rec!="0" && msg.rec!="false")
				{
					$('#txtparent_id').val(msg.rec);
				}
				else
				{
					$('#txtintroducer_id').val('');
					$('#txtintroducer_name').val('');
				}
			}
		});
	}
}


/*-----------------Get Paraent id From Leg------------------------*/

function check_leg()
{
	var collection=$("#leg");
	var mark=0;
	var obtainmark=0;
	var inputs=collection.find("input[type=checkbox],input[type=radio]");
	for(var x=0;x<inputs.length;x++)
	{
		var id=inputs[x].id;
		var name=inputs[x].name;
		if($("#"+id+"").is(':checked'))
		{
			if(id=="rbjoin_leg1")
			{
				$("#txtjoin_leg").val('L');
				get_parent_detail();

			}
			if(id=="rbjoin_leg2")
			{
				$("#txtjoin_leg").val('R');
				get_parent_detail();
			}
		}
	}
}

/*-----------------Get Paraent id------------------------*/

function get_parent_detail()
{
	var introducer = $("#txtintroducer_id").val();
	var csrf = $("input[name='csrf_token']").val();
	if($("#txtintroducer_id").val()!="")
	{
		if($("#txtjoin_leg").val()!="" && $("#txtjoin_leg").val()!=0)
		{
			var leg=$("#txtjoin_leg").val();
			$.ajax({	
				type:"POST",
				url:baseUrl+"get_details/get_parent_detail",
				dataType: 'json',
				data:{"txtintuserid":introducer, "leg":leg, csrf_token:csrf},
				beforeSend : function(){
					$.blockUI(
						{
							message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
						});
					setTimeout($.unblockUI, 1000);
				},
				success:function(msg) 
				{
					$("input[name='csrf_token']").val(msg.csrf);
					if(msg.rec!="" && msg.rec!="0" && msg.rec!="This id is not registered")
					{	
						$('#txtparent_id').attr('value',msg.rec);
						var parentid = msg.rec;
						get_member_name(parentid,2);
					}
					else
					{
						$('#txtintroducer_name').attr('value','');
						$('#txtparent_name').attr('value','');
						$('#txtparent_id').attr('value','');
					}
				}
			});
		}
		else
		{
			alert('Please Select Leg.');
		}
	}
	else
	{
		var mssg="";
		$('#txtintroducer_name').attr('value',mssg);
		$('#txtparent_name').attr('value',mssg);
		$('#txtparent_id').attr('value',mssg);
	}
}

/*---------------------Get Pin From Pack-------------------------*/

function fill_pin()
{
	var txtlogin_id=$("#txtlogin_id").val();
	var product_id=$("#ddpackid").val();
	var csrf = $("input[name='csrf_token']").val();
	if(txtlogin_id)
	{
		$.ajax({	
			type : "POST",
			url : baseUrl+"pin/select_pin",
			dataType : 'json',
			data : {"txtintuserid":txtlogin_id, "product_id":product_id, csrf_token:csrf},
			beforeSend : function(){
				$.blockUI(
					{
						message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
					});
				setTimeout($.unblockUI, 1000);
			},
			success:function(msg) 
			{
				$("input[name='csrf_token']").val(msg.csrf);
				$("#ddpin").empty();
				$("#ddpin").append("<option value=-1>Select Pin</option>");
				$.each(msg.pin,function(i,item)
					   {
					$('#ddpin').append("<option value="+item.m_pin_id+">"+item.m_pin+"</option>");
				});
			}
		});
	}
	else
	{
		alert("Plaese Fill Introducer Id!");
	}
}

/*---------------------Get Pin From Pack-------------------------*/

function fill_pin_id()
{
	var txtuserid=$("#txtuserid").val();
	var product_id=$("#ddpackid").val();
	var csrf = $("input[name='csrf_token']").val();
	if(txtuserid!='')
	{
		if(product_id!='-1')
		{
			$.ajax({	
				type : "POST",
				url : baseUrl+"pin/select_topup_pin",
				dataType : 'json',
				data : {"txtuserid":txtuserid, "product_id":product_id, csrf_token:csrf},
				beforeSend : function(){
					$.blockUI(
						{
							message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
						});
					setTimeout($.unblockUI, 1000);
				},
				success:function(msg) 
				{
					$("input[name='csrf_token']").val(msg.csrf);
					$("#ddpin").empty();
					$("#ddpin").append("<option value=-1>Select Pin</option>");
					$.each(msg.pin,function(i,item)
						   {
						$('#ddpin').append("<option value="+item.m_pin_id+">"+item.m_pin+"</option>");
					});
				}
			});
		}
		else
		{
			alert('Select Packid.');
		}
	}
	else
	{
		alert("Plaese Fill Userid!");
	}
}

function get_city()
{
	var ddstate=$("#ddstate").val();
	var ocity=$("#hdocity").val();
	var csrf = $("input[name='csrf_token']").val();
	if(ddstate!="-1")
	{
		$.ajax(
			{
				type:"POST",
				url:baseUrl+"get_details/get_city",
				dataType: 'json',
				data: {'ddstate': ddstate,  csrf_token: csrf},
				beforeSend : function(){
					$.blockUI(
						{
							message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
						});
					setTimeout($.unblockUI, 1000);
				},
				success: function(msg) {
					$("input[name='csrf_token']").val(msg.csrf);
					$("#ddcity").empty();
					$("#ddcity").append("<option value=-1>Select City</option>");
					$.each(msg.rec,function(i,item)
						   {
						if(ocity!="")
						{
							if(item.m_loc_id==ocity)
							{
								$('#ddcity').append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
							}
							else
							{
								$('#ddcity').append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
							}
						}
						else
						{
							$('#ddcity').append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
						}
					});
				}
			});
	}
}

/*-----------------Mobile NO Validate------------------------*/

function validate_mobile()
{
	var txtmobile=$("#txtmobile").val();
	var csrf = $("input[name='csrf_token']").val();
	$.post(baseUrl+"get_details/validate_mobile",
		   {
		phone: txtmobile, csrf_token: csrf
	},
		   function(data){
		var obj = jQuery.parseJSON(data);
		$("input[name='csrf_token']").val(obj.csrf);
		if(obj.mob>2)
		{
			$("#txtmobile").val('');
			$("#divtxtmobile").html("This Mobile No is Already Used");
		}
		else
		{
			$("#divtxtmobile").html("");
		}
	});
}

/*-----------------Login Validate------------------------*/

function validate_login_id()
{
	var txtloginid=$("#txtloginid").val();
	$.post(baseUrl+"get_details/validate_login",
		   {
		txtloginid: txtloginid
	},
		   function(data){
		if(data>0)
		{
			$("#function").hide();
			$("#txtloginid").val('');
			alert("This Login Id is Already Used");
		}
		else
		{
			$("#function").show();
			$("txtloginid").html("");
		}
	});
}

/*-----------------Email Validate------------------------*/

function validate_email()
{
	var txtemail=$("#txtemail").val();
	$.post(baseUrl+"get_details/validate_email",
		   {
		txtemail: txtemail
	},
		   function(data){
		if(data>0)
		{
			$("#sbsubmit").hide();
			$("#txtemail").val('');
			alert("This Email Id is Already Used");
		}
		else
		{
			$("#sbsubmit").show();
		}
	});
}


/*-----------------Mobile NO Validate------------------------*/

function validate_pancard()
{
	var txtpancard=$("#txtpancard").val();
	var csrf = $("input[name='csrf_token']").val();
	$.post(baseUrl+"get_details/validate_pancard",
		   {
		txtpancard: txtpancard, csrf_token: csrf
	},
		   function(data){
		var obj = jQuery.parseJSON(data);
		$("input[name='csrf_token']").val(obj.csrf);
		if(obj.pan>2)
		{
			$("#txtpancard").val('');
			$("#divtxtpancard").html("This Pancard No is Already Used");
		}
		else
		{
			$("#divtxtpancard").html("");
		}
	});
}



/*--------------------Get Member Topup------------------------*/

function get_member_topup(get_id)
{
	if(check(get_id))
	{
		if($("#txtuser_id").val()!="")
		{
			$("#btnsubmit").hide();
			$.ajax({	
				type:"POST",
				url:baseUrl+"User_action/insert_member_topup",
				data: $('#'+get_id).serialize(),
				beforeSend : function(){
					$.blockUI(
						{
							message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
						});
					setTimeout($.unblockUI, 50000);
				},
				success:function(msg)
				{
					alert(msg.trim());
					window.location.reload();
				}
			});
		}			
	}
}


/*--------------------Get Member Registration------------------------*/

function registration(get_id)
{
	var urlpost = baseUrl+txtclass+"/register_candidate";

	if(check(get_id))
	{
		$("#function").html("<div>Associates Registration has been processed.<img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/img/loading-spinner-blue.gif' /></div>");
		$.ajax(
			{
				type: "POST",
				url : urlpost,
				data: $('#'+get_id).serialize(),
				success: function(msg) {
					if(msg!="")
					{
						$("#"+get_id).html("<center><h2>Associate Registered Successfully</h2><p>"+msg+"</p></center>")
						Delayer();
					}
					else
					{
						alert("Some Error on this page");
					}
				}
			});
	}
}

function Delayer()
{  
	setTimeout('Redirection()', 5000);  
}  
function Redirection()  
{  
	window.location.href = window.location.href;
}