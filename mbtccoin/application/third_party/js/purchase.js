/*-------------------Get Vender Details------------------------*/

function get_vender(id)
{
    $.ajax({
            type: "POST",
            url: baseUrl+"purchase/edit_vendor",
            dataType: 'json',
            data:{'id': id},
            beforeSend : function(){
                $.blockUI(
                    {
                        message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
                    });
                setTimeout($.unblockUI, 1000);
            },
            success: function(msg) {
                $("#txtvendor").val(msg.m_vendor_name);
                $("#txtvendorurl").val(msg.m_vendor_url);
                $("#txtdesc").val(msg.m_vendor_desc);
                $("#divimg").hide();
                $("#signupForm").attr("action","update_vendor/"+id)
            }
        });
}

/*-------------------Get Store Details------------------------*/

function get_store(id)
{
    $.ajax({
            type: "POST",
            url: baseUrl+"purchase/view_edit_store",
            dataType: 'json',
            data:{'id': id},
            beforeSend : function(){
                $.blockUI(
                    {
                        message: '<h1><img src="'+baseUrl+'application/libraries/loading36.gif" /> Please Wait ...</h1>'
                    });
                setTimeout($.unblockUI, 1000);
            },
            success: function(msg) {
                $("#txtname").val(msg.m_store_name);
                $("#txtemail").val(msg.m_store_email);
                $("#txtuserid").val(msg.m_store_userid);
                $("#txtstate").val(msg.m_store_state);
                $("#txtpass").val(msg.m_store_pass);
                $("#txtpinpass").val(msg.m_store_pinpass);
                $("#txtdesc").val(msg.m_store_description);                
                $("#signupForm").attr("action","update_store/"+id)
            }
        });
}