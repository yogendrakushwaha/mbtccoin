<?php
	
	if(!defined('BASEPATH')) exit('No direct script access allowed');
	include('system/helpers/file_helper.php');
	
	
	function _print_r($array)
	{
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
	
	function GetAgoTime($timestamp)
	{
		// echo $timestamp.'##';
		//type cast, current time, difference in timestamps
		if($timestamp != '')
		{
			$timestamp = strtotime($timestamp);
			$timestamp      = (int) $timestamp;
			$current_time   = time();
			$diff           = $current_time - $timestamp;
			// echo date('y-m-d h: i : s',$current_time).'###<br/>';
			//intervals in seconds
			$intervals      = array (
			'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
			);
			
			//now we just find the difference
			if ($diff < 5)
			{
				return 'Just now';
			}
			if ($diff < 10)
			{
				return 'Few seconds ago';
			}
			
			if ($diff < 59)
			{
				return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
			}
			
			if ($diff >= 60 && $diff < $intervals['hour'])
			{
				$diff = floor($diff/$intervals['minute']);
				return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
			}
			
			if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
			{
				$diff = floor($diff/$intervals['hour']);
				return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
			}
			
			if ($diff >= $intervals['day'] && $diff < $intervals['week'])
			{
				$diff = floor($diff/$intervals['day']);
				return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
			}
			
			if ($diff >= $intervals['week'] && $diff < $intervals['month'])
			{
				$diff = floor($diff/$intervals['week']);
				return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
			}
			
			if ($diff >= $intervals['month'] && $diff < $intervals['year'])
			{
				$diff = floor($diff/$intervals['month']);
				return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
			}
			
			if ($diff >= $intervals['year'])
			{
				$diff = floor($diff/$intervals['year']);
				// return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
				return 'Posted '. date('M d,Y', ($timestamp) ) ;
			}
		}
	}
	
	function _imageCrop($src,$dest,$width='',$height='')
	{
		
		list($w,$h) = getimagesize($src);
		if(empty($height))
		$height = $h;
		
		if(empty($width))
		$width = $w;
		
		if($width > $height)
		{
			exec("convert ".$src." -crop x".$width." -quality 100 ".$dest);
		}
		else
		{
			exec("convert ".$src." -crop".$height." -quality 100 ".$dest);
		}
		exec("convert ".$dest." -gravity Center -crop ".$width."x".$height."+0+0 ".$dest);
	}
	
	// This function will proportionally resize image
	function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType)
	{
		//Check Image size is not 0
		if($CurWidth <= 0 || $CurHeight <= 0)
		{
			return false;
		}
		
		//Construct a proportional size of new image
		$ImageScale      	= min($MaxSize/$CurWidth, $MaxSize/$CurHeight);
		$NewWidth  			= ceil($ImageScale*$CurWidth);
		$NewHeight 			= ceil($ImageScale*$CurHeight);
		$NewCanves 			= imagecreatetruecolor($NewWidth, $NewHeight);
		
		// Resize Image
		
		imagealphablending($NewCanves, false);
		imagesavealpha($NewCanves,true);
		$transparent = imagecolorallocatealpha($NewCanves, 255, 255, 255, 127);
		imagefilledrectangle($NewCanves, 0, 0, $CurWidth, $CurHeight, $transparent);
		
		if(@imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight))
		{
			switch(strtolower($ImageType))
			{
				case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
				case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;
				case 'image/jpeg':
				case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
				default:
				return false;
			}
			//Destroy image, frees memory
			if(is_resource($NewCanves)) {imagedestroy($NewCanves);}
			return true;
		}
		
	}
	
	//This function corps image to create exact square images, no matter what its original size!
	function _cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
	{
		//Check Image size is not 0
		if($CurWidth <= 0 || $CurHeight <= 0)
		{
			return false;
		}
		
		//abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
		if($CurWidth>$CurHeight)
		{
			$y_offset = 0;
			$x_offset = ($CurWidth - $CurHeight) / 2;
			$square_size 	= $CurWidth - ($x_offset * 2);
			}else{
			$x_offset = 0;
			$y_offset = ($CurHeight - $CurWidth) / 2;
			$square_size = $CurHeight - ($y_offset * 2);
		}
		
		$NewCanves 	= imagecreatetruecolor($iSize, $iSize);
		imagealphablending($NewCanves, false);
        imagesavealpha($NewCanves,true);
		$transparent = imagecolorallocatealpha($NewCanves, 255, 255, 255, 127);
		imagefilledrectangle($NewCanves, 0, 0, $iSize, $iSize, $transparent);
		
		if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
		{
			switch(strtolower($ImageType))
			{
				case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
				case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;
				case 'image/jpeg':
				case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
				default:
				return false;
			}
			//Destroy image, frees memory
			if(is_resource($NewCanves)) {imagedestroy($NewCanves);}
			return true;
			
		}
		
	}
	
	/*
		* Method name: _getProfileImagePath
		*
		* @description :  Returns user profile image path
		* @param:  (string) $imgName
		* @param:  (string) $type (mid/thunb)
		* @return:  (string) image url
	*/
	
	
	function _getProfileImagePath($imgName, $type="thumb"){
		
		if(empty($imgName)){
			return false;
		}
		$typ = "";
		if($type == 'thumb'){
			$typ = 'thumb_';
		}
		
		if(strstr($imgName,'http://') || strstr($imgName,'https://') ){
			return $imgName;
		}
		
		if(file_exists(FCPATH.'uploads/users_profile/'.$typ.$imgName))
		return base_url().'uploads/users_profile/'.$typ.$imgName;
		else
		return base_url().'lib/images/no_user_image.png';
	}
	
	/**
		* @param $string     - string to replace
		* @param $startPos   - Position to start replace (Optional)
		* @param $endPos     - Position to end replace (Optional)
		* @param $glue       - Charecter to replace (Optional)
	**/
	function _replaceBetween($string,$startPos = 0,$endPos = NULL, $glue = "*")
	{
		
		if(empty($string))
		return 0;
		
		$startPos = (string)$startPos;
		$endPos   = (string)$endPos;
		
		$strLen   = strlen($string);
		
		if($endPos == NULL)
		$endPos =  $strLen - 5;
		
		$newstring = "";
		for($i=0; $i <= strlen($string); $i++){
			
			if($i >=$startPos && $i<= $endPos){
				if($i%4 == 0 && $i != 0)
				$newstring .= "-";
				
				$newstring .= $glue;
				}else{
				if($i%4 == 0 && $i != strlen($string))
				$newstring .= "-";
				
				$newstring .= @$string[$i];
			}
		}
		return $newstring;
	}
	
	
	function convertTime($dec)
	{
		// start by converting to seconds
		$seconds = (int)($dec * 3600);
		// we're given hours, so let's get those the easy way
		$hours = floor($dec);
		// since we've "calculated" hours, let's remove them from the seconds variable
		$seconds -= $hours * 3600;
		// calculate minutes left
		$minutes = floor($seconds / 60);
		// remove those from seconds as well
		$seconds -= $minutes * 60;
		// return the time formatted HH:MM:SS
		return lz($hours).":".lz($minutes).":".lz($seconds);
	}
	
	// lz = leading zero
	function lz($num)
	{
		return (strlen($num) < 2) ? "0{$num}" : $num;
	}
	
	
	/*
		*Replace all null or empty values of an array with empty string
	*/
	function replacer(& $item, $key) 
	{
		if ($item === null || $item === "") {
			$item = '';
			}else{
			$item = stripslashes($item);
		}
	}
	
	function nullreplacer(& $item, $key) 
	{
		if ($item === null || $item === "") {
			$item = '';
		}
	}
	
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
	function _valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}
	function _getMenusArray($prevMenu = null)
	{
		
		$menuFile = read_file('lib/menu.xml');
		$menu_array = array();
		$menu = new SimpleXMLElement($menuFile);
		foreach($menu->menu as $menus){
			$replaceto = array(';',' ',':','\'','!','@','#','$','%','^','*');
			$ndx = str_replace($replaceto,"-",$menus->title);
			$ndx = strtolower($ndx);
			if($prevMenu != null)
			{
				if(!in_array($ndx,$prevMenu))
				$menu_array["$ndx"] = ucwords($menus->title);
			}
			else
			{
				$menu_array["$ndx"] = ucwords($menus->title);
			}
		}
		return $menu_array;
	}
	
	function form_ckeditor($data)
	{
		$script =  '  <script type="text/javascript">
		$(function() {
		var editor =  CKEDITOR.replace(\''.$data['id'].'\',{
		fullPage: false,
		extraPlugins: \'wysiwygarea\',
		uiColor: \'#357CA5\'
		});
		CKFinder.setupCKEditor( editor, \''.base_url().'lib/admin/js/plugins/ckfinder\' ) ;
		
		});
        </script>';
		return $script;
	}
	
	// preloadImages(["url1.jpg", "url2.jpg", "url3.jpg"]);
	function preloadImages($imgJsn)
	{
		
		$script = '<script type="text/javascript">
		function preloadImages(array) {
		alert(array);
		if (!preloadImages.list) {
        preloadImages.list = [];
		}
		
		for (var i = 0; i < array.length; i++) {
        var img = new Image();
        img.onload = function() {
		var index = preloadImages.list.indexOf(this);
		if (index !== -1) {
		// remove this one from the array once it\'s loaded
		// for memory consumption reasons
		preloadImages.splice(index, 1);
		}
        }
        preloadImages.list.push(img);
        img.src = array[i];
		}
		}
		</script>';
		return $script;
	}
	
	
	function _isValidPhone($phone) 
	{
		/*
			/^01[0269]\\d{7}$/
			/^(([0-9]|[ ]|\(|\+|00)+)?([\(]?[0-9]{2,15}|[\)]){1,5}$/
			
		**/
		if(!preg_match("/^(6|9|8)[0-9]\\d{6}/",trim($phone)))
		{
			return false;
		}
		return true;
	}
	
	
	function _redirectJs($tourl = '') 
	{
		
		$script = '<script type="text/javascript">
		
		var local = location.pathname.split("/");
		local.pop(); // remove the filename
		local.push(\''.$tourl.'\');
		local = location.protocol+"//"+location.hostname+""+local.join("/");
		
		top.location.href = local;
		
		</script>
		';
		echo $script;
	}
	function _getYearListBox($Current_year, $Past_counter, $Future_counter)
	{
		$Selected = '';
		$Previous        = $Past_counter;
		if($Future_counter>0)
		$Next        = $Future_counter;
		else
		$Next        = 1;
		
		$Startyear = $Current_year - $Previous;
		$Endyear = $Current_year + $Next;
		$String = '';
		$yearArr = array();
		for($i=$Startyear; $i<=$Endyear; $i++)
		{
			
			//$Selected .= "<option value='".$i."' ".$sel.">".$i."</option>";
			$yearArr[$i] = $i;
		}
		
		//$String = "<select name=\"".$fieldName."\" class=\"form-control\">".$Selected."</select>";
		return $yearArr;
	}
	
	function _getTimeSlots($index='')
	{
		$timeSlotArr = array(
		'7am - 9am',
		'9am - 11am',
		'11am - 1pm',
		'1PM - 3PM',
		'3Pm - 5pm',
		'5pm - 7pm',
		'7pm - 9pm',
		'9PM - 11PM'
		);
		if($index){
			return $timeSlotArr[$index];
		}
		return $timeSlotArr;
	}
	function _getMonthListBox()
	{
		$Selected = '';
		$months = array();
		$currentMonth = 1;//(int)date('m');
		
		for ($x = $currentMonth; $x < $currentMonth + 12; $x++) {
		    $months[date('n', mktime(0, 0, 0, $x, 1))] = date('F', mktime(0, 0, 0, $x, 1));
		}
		
		
		return $months;
	}
	
	function dateDifference($date1, $date2)
	{
		$date1 = str_replace("/","-",$date1);
		$date2 = str_replace("/","-",$date2);
		$d1 = (is_string($date1) ? strtotime($date1) : $date1);
		$d2 = (is_string($date2) ? strtotime($date2) : $date2);
		
		$diff_secs = abs($d1 - $d2);
		$base_year = min(date("Y", $d1), date("Y", $d2));
		
		$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
		
		return array
		(
		"years" => abs(substr(date('Ymd', $d1) - date('Ymd', $d2), 0, -4)),
		"months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,
		"months" => date("n", $diff) - 1,
		"days_total" => floor($diff_secs / (3600 * 24)),
		"days" => date("j", $diff) - 1,
		"hours_total" => floor($diff_secs / 3600),
		"hours" => date("G", $diff),
		"minutes_total" => floor($diff_secs / 60),
		"minutes" => (int) date("i", $diff),
		"seconds_total" => $diff_secs,
		"seconds" => (int) date("s", $diff)
		);
	}
	function _getSexualOrientation($soID = "")
	{
		//1=> Straight Females, 2=> Gay Males, 3=> Straight Males, 4=> Lesbian Females
		$sexualOrientation = array(1=> 'Straight Females', 2=> 'Gay Males', 3=> 'Straight Males', 4=> 'Lesbian Females');
		if($soID)
	    return $sexualOrientation[$soID];
		
		return $sexualOrientation;
	}
	function _get_couponCode($code_len = '5')
    {
	    $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $code = "";
	    for ($i = 0; $i < $code_len; $i++) {
		    $code .= $chars[mt_rand(0, strlen($chars)-1)];
		}
	    return $code;
	}
	function _datetime($date='')
	{
		$date = date('Y-m-d H:i:s',strtotime($date));
		$date_regex = '/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01]) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/';
		if (!preg_match($date_regex, $date)) {
			return $date;
			} else {
			return date(DATETIME_FORMAT,strtotime($date));
		}
		
	}
	function _date($date='')
	{
        $date = date('Y-m-d',strtotime($date));
		$date_regex = '/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/';
		if (!preg_match($date_regex, $date)) {
			return $date;
			} else {
			return date(DATE_FORMAT,strtotime($date));
		}
		
	}
	
	// get country code by IP address
	function _getCountryCodeByIp()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		try{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://www.geoplugin.net/json.gp?ip=" . $ip);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$contryData  = json_decode(curl_exec($ch), true);
			curl_close($ch);
		}
		catch(Exception $e){
			echo $e->get_message();
		}
		
		return $contryData['geoplugin_countryCode'];
	}
	
	/**
		* Summary
		* Method : _getGEOLocationByIp
		* @return @$geoData (array) which is return json
		* {
		"geoplugin_request":"xxx.xxx.xxx.xx",
		"geoplugin_status":200,
		"geoplugin_credit":"Some of the returned data includes GeoLite data created by MaxMind, available from <a href=\\'http:\/\/www.maxmind.com\\'>http:\/\/www.maxmind.com<\/a>.",
		"geoplugin_city":"City",
		"geoplugin_region":"Region",
		"geoplugin_areaCode":"0",
		"geoplugin_dmaCode":"0",
		"geoplugin_countryCode":"CountryCode",
		"geoplugin_countryName":"countryName",
		"geoplugin_continentCode":"continentCode",
		"geoplugin_latitude":"latitude",
		"geoplugin_longitude":"longitude",
		"geoplugin_regionCode":"regionCode",
		"geoplugin_regionName":"regionName",
		"geoplugin_currencyCode":"currencyCode",
		"geoplugin_currencySymbol":"currencySymbol",
		"geoplugin_currencySymbol_UTF8":"currencySymbol_UTF8",
		"geoplugin_currencyConverter":currencyConverter
		}
	*/
	function _getGEOLocationByIp()
	{
        $ip = $_SERVER['REMOTE_ADDR'];
		try{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://www.geoplugin.net/json.gp?ip=" . $ip);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$geoData  = json_decode(curl_exec($ch), true);
			curl_close($ch);
		}
		catch(Exception $e){
			echo $e->get_message();
		}
		
		return $geoData;
	}
	function _getGEOLocationByAddress($address)
	{
		$prepAddr = str_replace(' ','+',$address);
		$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
		$output= json_decode($geocode);
		$lat = $output->results[0]->geometry->location->lat;
		$long = $output->results[0]->geometry->location->lng;
		$geoData['lat'] = $lat;
		$geoData['lng'] = $long;
		
		return $geoData;
	}
	function uploadImage($original_name, $tmp_name, $folder_name, $thumbSize ='')
	{
		if (!empty($original_name))
		{
			//$tmp_path = @$_FILES['image']['tmp_name'];
			$targetPath =  'uploads/'.$folder_name.'/';
			$imgName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y"))."_".@$original_name;
			$targetFile = str_replace('//','/',$targetPath).$imgName;
			//$image_name = move_uploaded_file($tmp_name, $targetFile);
			
			if(!file_exists($targetPath))
			mkdir(str_replace('//','/',$targetPath), 0777, true);
			
			move_uploaded_file($tmp_name, $targetFile);
			
			$arr = explode('/',$targetPath);
			$arr = array_reverse($arr);
			
			$info = pathinfo($targetPath.$imgName);
			$getimg = getimagesize($targetPath.$imgName);
			
			//list($width, $height, $type, $attr) = getimagesize($targetPath.$imgName);
			$width 		= $getimg[0];
			$height 	= $getimg[1];
			$type 		= $getimg[2];
			$attr 		= $getimg[3];
			
			if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG')
			$img = imagecreatefromjpeg( "{$targetFile}" );
			if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
			$img = imagecreatefromgif( "{$targetFile}" );
			if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
			$img = imagecreatefrompng( "{$targetFile}" );
			
			if($thumbSize)
			$thumbWidth = $thumbSize;
			else
			$thumbWidth = 60;
			
			if( $thumbWidth )
			{
				############## code for thumb ################
				if($width < $thumbWidth)
				$thumbWidth = $width;
				
				$width = imagesx( $img );
				$height = imagesy( $img );
				$new_height = floor( $height * ( $thumbWidth / $width ) );
				$new_width = $thumbWidth;
				
				$tmp_img = imagecreatetruecolor( $new_width, $new_height );
				imagealphablending($tmp_img, false);
				imagesavealpha($tmp_img,true);
				$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
				imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
				$targetFile1 =  str_replace('//','/',$targetPath). 'thumb_' . $imgName;
				
				if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
				imagejpeg( $tmp_img, "{$targetFile1}" );
				if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
				imagegif( $tmp_img, "{$targetFile1}" );
				if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
				imagepng( $tmp_img, "{$targetFile1}" );
			}
			return $imgName;
		}
		return false;
	}
	/**
		*This function is used to validate the phone number
		*@param string $str
		*@return Boolean
	*/
	function _valid_website_url($str)
	{
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$str)) {
			return false;
		}
		else
		{
			return true;
		}
	}
	/**
		*This function is used to validate the phone number
		*@param string $str
		*@return Boolean
	*/
	function _valid_phone_number($str)
	{
		$regex = "/^[+(0-9)0-9-0-9 . ]+$/";
		if(preg_match($regex,$str)){
			return true;
		}
		else
		{
			return FALSE;
		}
	}
	/**
		*This function is used to load the view file
	*/
	function _getLoadViewAdmin($template_name, $data = array())
	{
		$CI =& get_instance();
		$CI->load->view('templates/admin_header',$data);
		$CI->load->view('admin/'.$template_name, $data);
		$CI->load->view('templates/admin_footer',$data);
	}
	function _getLoadAjaxAdmin($template_name, $data = array())
	{
		$CI =& get_instance();
		
		$CI->load->view('admin/'.$template_name, $data);
		
	}
	
	//get extension of image
	function _getExtension($filename) {
		$file_extension ="";
		if($filename){
			$info = pathinfo($filename);
			$file_extension = strtolower($info['extension']);
		}
		return $file_extension;
	}
	
	function getPaginationOnData($page_no,$data,$recordsPerPage=10)
	{
		$page = $page_no;   // get the value of the page from your url
		$array=array_filter($data);//from your service
		$index=($page-1)* $recordsPerPage;
		$pagedata = array_slice($array,$index,$recordsPerPage);
		return $pagedata;
	}
	
	///////////////////////////////////////////  frontend function
	function checkShopOwnerLogin()
	{
		// check login
		$CI =& get_instance();
		$sessData = $CI->session->userdata;
		$user_data=$CI->pages->_getUser_by_id($sessData['user_data']['Userid']);
		if($sessData['user_data']['Userid'] == '')
		{
			$CI->session->sess_destroy();
			redirect(base_url());
		}
		else if($user_data['account_type']!="shopowner")
		{
			redirect(base_url("shopnow"));
		}
	}
	function checkLogin()
	{
		$CI =& get_instance();
		$sessData = $CI->session->userdata;
		if($sessData['user_data']['Userid'] == '')
		{
			$CI->session->sess_destroy();
			redirect(base_url());
		}
	}
	
	function _showMessage($message,$status)
	{
		$CI =& get_instance();
		if($status == 'warning')
		{
			return $CI->session->set_flashdata('msg', '<div class="alert alert-warning alert-dismissable">
			
			<b>Warning! </b>'.$message.'</div>');
		}
		if($status == 'success')
		{
			return $CI->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
			
			<b>Success! </b>'.$message.'</div>');
		}
		if($status == 'error')
		{
			return $CI->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
			
			<b>Error! </b>'.$message.'</div>');
		}
	}
	
	function showRating($rate)
	{
		for ($i=1; $i<=5;$i++) {
			//code
			if ($i <= $rate) {
				//code
				echo '<img src="'.base_url().'lib/images/assets/star-on.png">';
			}
			else {
				
				echo '<img src="'.base_url().'lib/images/assets/star-off.png">';
			}
		}
	}
	
	function returnRating($rate)
	{
		$html ="";
		for ($i=1; $i<=5;$i++) {
			//code
			if ($i <= $rate) {
				//code
				$html .='<img src="'.base_url().'lib/images/assets/star-on.png">';
			}
			else {
				
				$html .= '<img src="'.base_url().'lib/images/assets/star-off.png">';
			}
		}
		return $html;
	}
	
	function loggedInuserData()
	{
		$CI =& get_instance();
		$sessData = $CI->session->userdata;
		$user_data=$CI->pages->_getUser_by_id($sessData['user_data']['Userid']);
		
		$follower_count=$CI->dashboard->countShopFollowers($sessData['user_data']['Userid'],$sessData['user_data']['account_type']);
		if($sessData['user_data']['account_type'] == "shopowner"){
			$following_count=$CI->dashboard->countShopFollowers($sessData['user_data']['Userid'],"consumer");
		}
		
		$res['user_data']=$user_data;
		$res['follower_count']=$follower_count;
		$res['following_count']=$following_count ? $following_count :0;
		if($user_data['id']>0)
		return $res;
		else
		redirect(site_url());
		
	}
	function userSessionData()
	{
		$CI =& get_instance();
		$sessData = $CI->session->userdata;
		return $sessData['user_data'];
		
	}
	
	function currentDateTime()
	{
		// current date time
		return date("Y-m-d H:i:s");
	}
	
	function showShopRating($shop_id)
	{
		$CI =& get_instance();
		$avrating=$CI->dashboard->shopAverageRating($shop_id);
		
		showRating($avrating);
	}
	
	function showImage($image_url,$default_image="")
	{
		//echo $image_url; die;
		if (getimagesize($image_url) !== false)
		{
			return 	$image_url;
		}
		else
		{
			$default_image_name=$default_image!=""?$default_image:"product_demo.jpg";
			return 	base_url().'lib/images/'.$default_image_name;
		}
	}
	function showUserImage($image_name,$default_image="")
	{
		if(!empty($image_name))
		{
			if (stripos($image_name, "http://graph.facebook.com") !== false) {
				$image_url = $image_name."&height=200&width=200";
				
			}
			else{
				if(file_exists('uploads/users_profile/'.$image_name)) {
					$image_url = base_url().'uploads/users_profile/'.$image_name;
				}
				else{
					
					$default_image_name =$default_image!=""?$default_image:"no_user_image.png";
					
					$image_url = base_url().'lib/images/'.$default_image_name;
				}
			}
		}
		else{
			$default_image_name =$default_image!=""?$default_image:"no_user_image.png";
			
			$image_url = base_url().'lib/images/'.$default_image_name;
			
		}
		return $image_url;
	}
	
	function showNotificationMessage($n)
	{
		
		
		$name 	= $n['name'];
		$type 	= $n['type'];
		$detail_id	= $n['detail_id'];
		$from_id = $n['from_id'];
		$rank = $n['text'];
		$product_name  = ($n['product_name']) ? $n['product_name'] : '' ;
		
		if($n['time_in_hr'] > 1)
		$hourText = 'hours';
		else
		$hourText = 'hour';
		
		//echo "<PRE>";
		//print_r($n);
		//echo "</PRE>";
		
		switch($type)
		{
			case "new_reply":
			
			if($n['account_type']=="shopowner")
			{
				$profile_url = base_url().'shopnow/view/'.base64_encode($from_id);
			}
			else{
				$profile_url = base_url().'profile/'.base64_encode($from_id);
			}
			$str ='<h3><a class="color-black" href="'.$profile_url.'">'.ucfirst($n['name']).'</a>
			<span class="font-normal"> added comment on product </span>
			<a class="color-black" href="'.base_url().'shop/product/detail/'.base64_encode($detail_id).'">'. ucfirst($n['request_title']).'</a></h3>';
			break;
			
			default:
			$str ='';
			break;
		}
		
		return $str;
	}
	
	
	function agoTimePost($session_time,$time_zone)
	{
		//$time_difference = time() - $session_time ;
		$time_difference = strtotime(date('Y-m-d H:i:s')) - strtotime($session_time);
		
		$seconds    = $time_difference ;
		$minutes    = round($time_difference / 60 );
		$hours      = round($time_difference / 3600 );
		$days       = round($time_difference / 86400 );
		$weeks      = round($time_difference / 604800 );
		$months     = round($time_difference / 2419200 );
		$years      = round($time_difference / 29030400 );
		// Seconds
		$string = '';
		/*if($seconds <= 60)
			{
			$string = "$seconds seconds ago";
			} else if($minutes <=60) {
			if($minutes==1){
			$string =  "one minute ago";
			} else {
			$string =  "$minutes minutes ago";
			}
			}
			//Hours
			else if($hours <=24)
			{
			
			if($hours==1){
			$string =  "one hour ago";
			} else {
			$string =  "$hours hours ago";
			}
			
		}*/
		
		//Days
		if($hours <=24)
		{
			
			
			if(!empty($time_zone))
			{
				$string = date("d-m-Y",(strtotime(getTimeZoneDate(date_default_timezone_get(),$time_zone,$session_time))))." at ".date("H:i",(strtotime(getTimeZoneDate(date_default_timezone_get(),$time_zone,$session_time))));
			}
			else{
				$string =  date("d-m-Y", strtotime($session_time))." at ".date("H:i", strtotime($session_time));
			}
		}
		else if($days <= 7)
		{
			
			if($days==1) {
				$string =  "one day ago";
				} else {
				$string =  "$days days ago";
			}
			
		}
		//Weeks
		else if($weeks <= 4)
		{
			
			if($weeks==1){
				$string =  "one week ago";
				} else {
				$string =  "$weeks weeks ago";
			}
			
		}
		//Months
		else if($months <=12)
		{
			if($months==1)
			{
				$string =  "one month ago";
			}
			else
			{
				$string =  "$months months ago";
			}
			
		}
		//Years
		else
		{
			
			if($years==1)
			{
				$string =  "one year ago";
			}
			else
			{
				$string = "$years years ago";
			}
			
		}
		return $string;
	}
	
	function agoTime($session_time)
	{
		//$time_difference = time() - $session_time ;
		$time_difference = strtotime(date('Y-m-d H:i:s')) - strtotime($session_time);
		
		$seconds    = $time_difference ;
		$minutes    = round($time_difference / 60 );
		$hours      = round($time_difference / 3600 );
		$days       = round($time_difference / 86400 );
		$weeks      = round($time_difference / 604800 );
		$months     = round($time_difference / 2419200 );
		$years      = round($time_difference / 29030400 );
		// Seconds
		$string = '';
		if($seconds <= 60)
		{
			$string = "$seconds seconds ago";
			} else if($minutes <=60) {
			if($minutes==1){
				$string =  "one minute ago";
				} else {
				$string =  "$minutes minutes ago";
			}
		}
		//Hours
		else if($hours <=24)
		{
			
			if($hours==1){
				$string =  "one hour ago";
				} else {
				$string =  "$hours hours ago";
			}
			
		}
		else if($days <= 7)
		{
			
			if($days==1) {
				$string =  "one day ago";
				} else {
				$string =  "$days days ago";
			}
			
		}
		//Weeks
		else if($weeks <= 4)
		{
			
			if($weeks==1){
				$string =  "one week ago";
				} else {
				$string =  "$weeks weeks ago";
			}
			
		}
		//Months
		else if($months <=12)
		{
			if($months==1)
			{
				$string =  "one month ago";
			}
			else
			{
				$string =  "$months months ago";
			}
			
		}
		//Years
		else
		{
			
			if($years==1)
			{
				$string =  "one year ago";
			}
			else
			{
				$string = "$years years ago";
			}
			
		}
		return $string;
	}
	
	
	function getTimeZoneDate($timezone1, $timezone2, $date, $type='both')
	{
		$offset = get_timezone_offset($timezone1,$timezone2);
		
		$time=get_timestamp($date);
		
		$offset_time = $time + $offset;
		
		if($type=='date'){
			$offset_time = date('Y-m-d',$offset_time);
			}elseif($type=='time'){
			$offset_time = date('H:i:s',$offset_time);
			}else{
			$offset_time = date('Y-m-d H:i:s',$offset_time);
		}
		
		return $offset_time;
	}
	
	function get_timezone_offset($remote_tz, $origin_tz = null) {
		if($origin_tz === null) {
			if(!is_string($origin_tz = date_default_timezone_get())) {
				return false; // A UTC timestamp was returned -- bail out!
			}
		}
		$origin_dtz = new DateTimeZone($origin_tz);
		$remote_dtz = new DateTimeZone($remote_tz);
		$origin_dt = new DateTime("now", $origin_dtz);
		$remote_dt = new DateTime("now", $remote_dtz);
		$offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
		return $offset;
	}
	function get_timestamp($date) {
		
		$dt = explode(' ', $date);
		list($y, $m, $d) = explode('-',$dt[0]);
		
		if($dt[1]){
			list($h, $i, $s) = explode(':', $dt[1]);
		}
		else{
			$h=0;$i=0;$s=0;
		}
		
		return mktime($h, $i, $s, $m, $d, $y);
	}
	
	function read_more($string, $nb_caracs = 100, $separator = '...'){
		$string = strip_tags(html_entity_decode($string));
		if( strlen($string) <= $nb_caracs ){
			$final_string = $string;
			} else {
			$final_string = "";
			$words = explode(" ", $string);
			foreach( $words as $value ){
				if( strlen($final_string . " " . $value) < $nb_caracs ){
					if( !empty($final_string) ) $final_string .= " ";
					$final_string .= $value;
					} else {
					break;
				}
			}
			$final_string .= $separator;
		}
		return $final_string;
	}
	
	function in_array_r($needle, $haystack, $strict = false)
	{
		foreach ($haystack as $item) {
			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
				return true;
			}
		}
		
		return false;
	}
	
	
	
	
	/*  Time Zone */
	
	function __getTimeZone(){
		
		return '';
		
		$uLcData = getTimZoneDetails();
		$locArr = explode(',',$uLcData['loc']);
		$lat = $locArr[0];
		$lang = $locArr[1];
		$url = "http://api.geonames.org/timezoneJSON?lat=".$lat."&lng=".$lang."&username=sunilgautam";
		
		try{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$geoData  = json_decode(curl_exec($ch), true);
			curl_close($ch);
			//_print_r($geoData);
		}
		catch(Exception $e){
			return 'Asia/Singapore';
		}
		return @($geoData['timezoneId'])?str_replace('Kolkata','Calcutta',$geoData['timezoneId']):'Asia/Singapore';
		
	}
	function getTimZoneDetails(){
		$ip = get_client_ip();
		
		//$ip = $_SERVER['SERVER_NAME'];
		//$ip = '182.71.125.14';//get_client_ip();
		// echo "http://getcitydetails.geobytes.com/GetCityDetails?fqcn=" . $ip;
		try{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://www.ipgeni.com/api/" . $ip);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$geoData  = json_decode(curl_exec($ch), true);
			
			curl_close($ch);
		}
		catch(Exception $e){
			echo $e->get_message();
		}
		
		return $geoData;
	}
	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
		else
		$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	function getUserLocalDateTime($datetime,$date=1,$time=1,$ltz='Asia/Kolkata',$dt_format="")
	{
		$localdate="";
		if(!empty($datetime))
		{
			if(empty($dt_format))
			{
				if($date==1)
				$format = "Y-m-d";
				if($time==1)
				$format .= " h:i A";
				
				$format = $format!=""?$format:"Y-m-d h:i A";
			}
			else{
				$format = $dt_format;
			}
			/*
				$CI =& get_instance();
				$sess_user = $CI->session->userdata('user_data');
				if(empty($sess_user['timeZone']))
				{
				$time_zone = __getTimeZone();
				$time_zone = $time_zone!=""?$time_zone:"Asia/Singapore";
				}
				else{
				$ltz = $sess_user['timeZone']?$sess_user['timeZone']:'Asia/Singapore';
				}
			*/
			
			//$localdate =date($format,(strtotime(getTimeZoneDate(date_default_timezone_get(),$ltz,$datetime))+(60*1)));
			$localdate =date($format,(strtotime(getTimeZoneDate(date_default_timezone_get(),$ltz,$datetime))));
			return $localdate;
		}
		else{
			return $localdate;
		}
		
	}
	
	function getUserLocalTimeZone($userid=0)
	{
		if(empty($userid))
		{
			$time_zone = __getTimeZone();
			$time_zone = $time_zone!=""?$time_zone:"Asia/Singapore";
		}
		else{
			$CI =& get_instance();
			$user_data=$CI->pages->_getUser_by_id($userid);
			$time_zone = $user_data['time_zone']!=""?$user_data['time_zone']:"Asia/Singapore";
		}
		return $time_zone;
	}
	
	
	/*
		* uploadImage() to upload & crop in Width and Height wise
		* $byHor_W = 'H' then Please give $thumbHeight & $midHeight
		* $byHor_W = 'W' then Please give $thumbWidth & $midWidth
	*/
	function uploadResizeImage($tempFile, $fileName, $midSize='', $thumbSize='', $folder='', $byHor_W='W')
	{
		$folder = $folder.'/';
		$fileName = preg_replace('/[^A-Za-z0-9\.]/','_', $fileName);
		$fileName = str_replace(" ","-",$fileName);
		$fileName = str_replace("_","",$fileName);
		$fileName = str_replace("%","",$fileName);
		$fileName = str_replace(",","",$fileName);
		$fileName = str_replace("'","",$fileName);
		
		
		if (!empty($fileName))
		{
			$targetPath = FCPATH.$folder;
			
			$imgName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y"))."_".$fileName;
			//$targetFile =  str_replace('//','/',$targetPath). 'full_' .$imgName;
			
			$targetFile =  str_replace('//','/',$targetPath).$imgName;
			
			echo $targetFile; exit;
			
			// Uncomment the following line if you want to make the directory if it doesn't exist
			if(!file_exists($targetPath))
			mkdir(str_replace('//','/',$targetPath), 0777, true);
			
			if(move_uploaded_file($tempFile, $targetFile))
			{
				
			}
			else{
				
			}
			
			$arr = explode('/',$targetPath);
			$arr =array_reverse($arr);
			
			$info = pathinfo($targetPath.$imgName);
			$getimg = getimagesize($targetPath.$imgName);
			
			//list($width, $height, $type, $attr) = getimagesize($targetPath.$imgName);
			$width 	= $getimg[0];
			$height 	= $getimg[1];
			$type 		= $getimg[2];
			$attr 		= $getimg[3];
			
			if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG')
			$img = imagecreatefromjpeg( "{$targetFile}" );
			if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
			$img = imagecreatefromgif( "{$targetFile}" );
			if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
			
			$img = imagecreatefrompng( "{$targetFile}" );
			
			if( $byHor_W == 'W' )
			{
				if($thumbSize)
				$thumbWidth =$thumbSize;
				else
				$thumbWidth = 60;
				
				if( $thumbWidth )
				{
					############## code for thumb ################
					if($width < $thumbWidth)
					$thumbWidth = $width;
					
					$width = imagesx( $img );
					$height = imagesy( $img );
					$new_height = floor( $height * ( $thumbWidth / $width ) );
					$new_width = $thumbWidth;
					
					$tmp_img = imagecreatetruecolor( $new_width, $new_height );
					imagealphablending($tmp_img, false);
					imagesavealpha($tmp_img,true);
					$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
					imagefilledrectangle($tmp_img, 0, 0, $new_width, $new_height, $transparent);
					imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					
					
					//imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
					$targetFile1 =  str_replace('//','/',$targetPath). 'thumb_' . $imgName;
					
					if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
					imagejpeg( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
					imagegif( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
					{
						
						echo  imagepng( $tmp_img, "{$targetFile1}" );
						echo 'fsdfds';
					}
				}
				########## mid size image #############
				if( $midSize)
				{
					$midWidth =$midSize;
					
					if($width < $midWidth)
					$midWidth = $width;
					
					$new_height =  floor( $height * ( $midWidth / $width ) );
					$new_width  = $midWidth;
					
					$tmp_img = imagecreatetruecolor( $new_width, $new_height );
					//imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
					imagealphablending($tmp_img, false);
					imagesavealpha($tmp_img,true);
					$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
					imagefilledrectangle($tmp_img, 0, 0, $new_width, $new_height, $transparent);
					imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					
					$targetFile1 =  str_replace('//','/',$targetPath). 'mid_' . $imgName;
					
					if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
					imagejpeg( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
					imagegif( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
					imagepng( $tmp_img, "{$targetFile1}" );
				}
			}
			if( $byHor_W == 'H' )
			{
				
				############## code for thumb ################
				if($thumbSize)
				$thumbHeight =$thumbSize;
				else
				$thumbHeight = 60;
				
				if( $thumbHeight )
				{
					if($height < $thumbHeight)
					$thumbHeight = $height;
					
					$width = imagesx( $img );
					$height = imagesy( $img );
					$new_width = floor( $width * ( $thumbHeight / $height ) );
					$new_height = $thumbHeight;
					
					$tmp_img = imagecreatetruecolor( $new_width, $new_height );
					
					//imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
					imagealphablending($tmp_img, false);
					imagesavealpha($tmp_img,true);
					$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
					imagefilledrectangle($tmp_img, 0, 0, $new_width, $new_height, $transparent);
					imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					
					$targetFile1 =  str_replace('//','/',$targetPath). 'thumb_' . $imgName;
					
					if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
					imagejpeg( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
					imagegif( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
					imagepng( $tmp_img, "{$targetFile1}" );
					
				}
				########## mid size image #############
				if( $midSize)
				{
					$midHeight = $midSize;
					
					if($height < $midHeight)
					$midHeight = $height;
					
					$new_width = floor( $width * ( $midHeight / $height ) );
					$new_height = $midHeight;
					
					$tmp_img = imagecreatetruecolor( $new_width, $new_height );
					//imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
					imagealphablending($tmp_img, false);
					imagesavealpha($tmp_img,true);
					$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
					imagefilledrectangle($tmp_img, 0, 0, $new_width, $new_height, $transparent);
					imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
					
					
					$targetFile1 =  str_replace('//','/',$targetPath). 'mid_' . $imgName;
					
					if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
					imagejpeg( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
					imagegif( $tmp_img, "{$targetFile1}" );
					if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
					imagepng( $tmp_img, "{$targetFile1}" );
				}
			}
			return $imgName;
		}
		return false;
	}
	
	function checkProductPrice($price)
	{
		if(!preg_match('/^\d{0,6}(\.\d{1,2})?$/', $price))
		{
			return FALSE;
		}
		return TRUE;
	}
	
	function get_user_membership($user_id)
	{
		$CI =& get_instance();
		$addon_data = $CI->webservice_model->get_user_addon_detail($user_id);
		$addon_service= $addon_data['no_of_service'];
		$addon_portfolio= $addon_data['no_of_portfolio'];
		$member_data = $CI->webservice_model->getusermemberDetails($user_id);
		$member_id= $member_data['membership_id'];
		$service= $member_data['service'];
		$portfolio= $member_data['portfolio'];
		$total_portfolio=$addon_portfolio+$portfolio;
		$total_service=$addon_service+$service;
		$data = array(
		"portfolio"=>$portfolio,
		"services"=>$service,
		"total_portfolio"=>$total_portfolio,
		"total_service"=>$total_service);
		return $data;
		
	}
	
	
	function get_promotion_by_service_id($service_id)
	{
		$CI =& get_instance();
		$date=date("Y-m-d");
		$CI->db->select('*');
		$CI->db->from('service_promotion');
		$CI->db->where('service_id',$service_id);
		$CI->db->where('status !=','Inactive');
		$CI->db->where("'$date' BETWEEN valid_from AND valid_until");
		$query= $CI->db->get();
		$serviceData = $query->row_array();
		if(!empty($serviceData))
		{
			$s_data = $CI->webservice_model->get_service_by_id($service_id);
			$old_price=$s_data['price'];
			$dis=$serviceData['discount'];
			$discont=$old_price*$dis/100;
			$new_price=$old_price-$discont;
			
			$data = array(
			"new_price"=>$new_price,
			"old_price"=>$old_price,
			"discount"=>$discont,
			);
			
		}
		else
		{
			$data = array(
			
			"old_price"=>$old_price,
			
			);
		}
		
		return $data;
		
	}
	
	function send_email($email,$mailShortCode="",$email_data)
	{
		$CI = & get_instance();
		
		$CI->load->library('phpmailer');
		$CI->load->library('smtp');
		
		switch($mailShortCode)
		{
			case "admin_forgot_password":
			$adminEmail = $CI->admin_model->_getConfigurationByKey('signup_email');
			$msg = $CI->pages_model->_getMessage('admin_forgot_password');
			
			$message  	= html_entity_decode($msg->content);
			$subject 	= html_entity_decode($msg->subject);
			
			$patternFind1[0] 	= '/{name}/';
			$patternFind1[1] 	= '/{link}/';
			
			$replaceFind1[0] 	= $name;
			$replaceFind1[1] 	= $email_data['link'];
			
			$txtdesc_contact	= stripslashes($message);
			$contact_sub      = stripslashes($subject);
			$contact_sub      = preg_replace($patternFind1, $replaceFind1, $contact_sub);
			$ebody_contact 	= preg_replace($patternFind1, $replaceFind1, $txtdesc_contact);
			
			break;
			case "activate_user":
			$adminEmail = $CI->admin_model->_getConfigurationByKey('signup_email');
			$msg = $CI->pages_model->_getMessage('activate_user');
			
			$message  	= html_entity_decode($msg->content);
			$subject 	= html_entity_decode($msg->subject);
			
			$patternFind1[0] 	= '/{name}/';
			
			$replaceFind1[0] 	= $email_data['name'];
			
			$txtdesc_contact	= stripslashes($message);
			$contact_sub      = stripslashes($subject);
			$contact_sub      = preg_replace($patternFind1, $replaceFind1, $contact_sub);
			$ebody_contact 	= preg_replace($patternFind1, $replaceFind1, $txtdesc_contact);
			
			break;
			case "new_sign_up":
			$adminEmail = $CI->admin_model->_getConfigurationByKey('admin_email');
			$msg = $CI->pages_model->_getMessage('new_sign_up');
			
			$message  	= html_entity_decode($msg->content);
			$subject 	= html_entity_decode($msg->subject);
			
			$patternFind1[0] 	= '/{admin}/';
			
			$replaceFind1[0] 	= $email_data['admin'];
			
			$txtdesc_contact	= stripslashes($message);
			$contact_sub      = stripslashes($subject);
			$contact_sub      = preg_replace($patternFind1, $replaceFind1, $contact_sub);
			$ebody_contact 	= preg_replace($patternFind1, $replaceFind1, $txtdesc_contact);
			
			break;
			case "payment_request":
			$adminEmail = $CI->admin_model->_getConfigurationByKey('admin_email');
			$msg = $CI->pages_model->_getMessage('payment_request');
			
			$message  	= html_entity_decode($msg->content);
			$subject 	= html_entity_decode($msg->subject);
			
			$patternFind1[0] 	= '/{name}/';
			$patternFind1[1] 	= '/{service}/';
			$patternFind1[2] 	= '/{price}/';
			
			$replaceFind1[0] 	= $email_data['name'];
			$replaceFind1[1] 	= $email_data['service'];
			$replaceFind1[2] 	= $email_data['price'];
			
			$txtdesc_contact	= stripslashes($message);
			$contact_sub      = stripslashes($subject);
			$contact_sub      = preg_replace($patternFind1, $replaceFind1, $contact_sub);
			$ebody_contact 	= preg_replace($patternFind1, $replaceFind1, $txtdesc_contact);
			
			break;
			default:
			break;
		}
		
		
		$CI->phpmailer->isSMTP();                                      // Set mailer to use SMTP
		//$CI->phpmailer->SMTPDebug = 1;
		$CI->phpmailer->SMTPAuth = true;                               // Enable SMTP authentication
		
		//$CI->phpmailer->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		
		
		$CI->phpmailer->Host = $CI->admin_model->_getConfigurationByKey('smtp_server_host'); // sets GMAIL as the SMTP server
		$CI->phpmailer->Port = $CI->admin_model->_getConfigurationByKey('smtp_port_number'); // set the SMTP port
		$CI->phpmailer->Username = $CI->admin_model->_getConfigurationByKey('smtp_uName'); // GMAIL username
		$CI->phpmailer->Password = $CI->admin_model->_getConfigurationByKey('smtp_uPass');
		
		$mail_from = $CI->admin_model->_getConfigurationByKey('signup_email');
		
		$CI->phpmailer->setFrom($mail_from, SITE_NAME);
		$CI->phpmailer->addAddress($email);     // Add a recipient
		
		$CI->phpmailer->isHTML(true);                                  // Set email format to HTML
		
		$CI->phpmailer->Subject = $contact_sub;
		$CI->phpmailer->Body    = $ebody_contact;
		//$this->phpmailer->AltBody = 'This is the body in plain text for non-HTML mail clients';
		
		if(!$CI->phpmailer->send()) {
			return 0;
			echo 'Mailer Error: ' . $CI->ErrorInfo;
			} else {
			return 1;
			echo 'Message has been sent 1';
		}
		
		//==========================================================
	}
	
	
	function notifylog($notificationRec, $type)
	{
		//  print_r($notificationRec);
		//echo '$type'.$type;
		//exit;
		
		$db =& DB();
		
		if(count($notificationRec) > 0 && $type){
			
			$senderName = '';
			if($notificationRec['sender_id'] > 0)
			{
				$sql1='SELECT name, device_type, device_id FROM tfcs_users WHERE id="'.$notificationRec['sender_id'].'"';
				$query = $db->query($sql1);
				$fromUData = $query->row_array();
				$senderName = $fromUData['first_name'] . ' ' . $fromUData['last_name'];
			}
			
			
			switch($type)
			{
				case 'checkin':
				
				$insertArr = array(
				'from_userid' 			=> $notificationRec['sender_id'],
				'to_userid'				=> $notificationRec['receiver_id'],
				'notification_msg' 		=> $notificationRec['message'],
				'notification_type' 	=> $notificationRec['type'],
				'sender_name' 			=> $senderName,
				'notification_type_id' 	=> $notificationRec['message_id'],
				'addedon'				=> CURRENT_TIME,
				);
				
				$msg_payload = array (
				'name' 			=> $senderName,
				'message' 		=> $notificationRec['message'],
				'alert' 			=> $notificationRec['message'],
				'item_id'			=> (string)$notificationRec['message_id'],
				"action" 			=> $notificationRec['type'],
				);
				
				break;
				
				default:
				
				break;
			}
			// echo 'here';
			// print_r($insertArr);
			$db->insert('notification', $insertArr);
			
			$API_ACCESS_KEY = API_ACCESS_KEY;
			
			$notification = @new PushNotifications($API_ACCESS_KEY);
			
			$sql2 = 'SELECT * FROM tfcs_users WHERE id="'.$notificationRec['receiver_id'].'"';
			$query = $db->query($sql2);
			$toUData = $query->row_array();
			
			
			//$sql3='SELECT * FROM tfcs_user_device WHERE id_user="'.$notificationRec['receiver_id'].'"';
			//$query = $db->query($sql3);
			//$deviceInfo = $query->result_array();
			//$deviceInfo=$toUData;
			// echo '$deviceInfo<br/>';
			//print_r($deviceInfo);
			
			if($toUData['notification'] == 'On')
			{
				$msg_payload['silent'] = 'no';
			}
			else
			{
				$msg_payload['silent'] = 'yes';
			}
			
			$gcm_registerId = array();
			$count=0;
			foreach($toUData as $d)
			{
				//echo 'count<br/>'.$count;
				//$count++;
				//echo $d['device_type'];
				if($d['device_type']=='android')
				{//Android
					$gcm_registerId[] = $d['device_token'];
				}
				elseif($d['device_type']=='iOS')
				{//iOS
					$notification->mode = 'dev';
					$regId = isset($d['device_token']) ? $d['device_token'] : '';
					
					$res = $notification->iOS($msg_payload, $regId);
					
				}
			}
			
			if(count($gcm_registerId) > 0)
			{
				$res = $notification->android($msg_payload, $gcm_registerId);
			}
		}
	}
	
	function rrmdir($dir) { 
		if (is_dir($dir)) { 
			$objects = scandir($dir); 
			foreach ($objects as $object) { 
				if ($object != "." && $object != "..") { 
					if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
				} 
			} 
			reset($objects); 
			rmdir($dir); 
		} 
	}
	
?>
