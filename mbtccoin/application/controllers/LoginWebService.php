<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class LoginWebService extends CI_Controller
	{
		/**
			* Index Page for this controller.
			*
			* Maps to the following URL
			* 		http://example.com/index.php/LoginWebService
			*	- or -
			* 		http://example.com/index.php/LoginWebService/index
			**	- or -
			* Since this controller is set as the default controller in
			* config/routes.php, it's displayed at http://example.com/
			*
			* So any other public methods not prefixed with an underscore will
			* map to /index.php/LoginWebService/<method_name>
			*
			*
			*@category 	controller
			*@package 	application_controllers
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
		
		var $status = "failed";
		var $message = "Invalid Operation";
		private $error_code = 0;
		private $isError = FALSE;
		private $record_per_page =10;
		private $userDisbled = 0;
		
		
		
		/**
			*Class default constructor
		*/
		public function __construct()
		{
			parent::__construct();
			$this->load->model('LoginWebService_model','Login');
			$this->load->model('SignupWebService_model','Signup');
			$this->load->helper('functions');
			$this->load->library('error_handler');
			$this->load->library('JWT');
		}
		
		/**
			* Method name: is_logged_in
			* @description :  used for check session is enabled or not 
			* @param :  Request
			* @return:  string
		**/
		public function is_logged_in() 
		{ 
			$is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in!="" && $is_logged_in!="0" ) 
			{
				echo $is_logged_in ;
			}
			else
			{
				   
			}
		}
		
		
		/**
			* Method name: _debugMode
			* @description :  used for debug the posted data
			* @param:  Request data
			* @return:  array
		*/
		public function _debugMode()
		{
			$DataArr = array();
			if(isset($_POST['mode']) && $_POST['mode'] == "debug")
			{
				$this->status = "debug";
				$this->message = "Debug Mode";
				$DataArr = $_POST;
				return $DataArr;
			}
			else{
				return array();
			}
		}
		
		/**
			* Method name: email_login
			* @description :  Used to login into mobile app
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		
		public function email_login()
		{
			$Username='';
			$Password='';
			$TOKEN="";
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			//$postData = $this->input->post();
			
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				if($this->isError === FALSE)
				{
					if(trim($Username) == ''){
						$this->message = $this->error_handler->_getStatusMessage('empty_email');
						$this->isError = TRUE;
					}
					else if(!_valid_email($Username)){
						$this->message = $this->error_handler->_getStatusMessage('invalid_email');
						$this->isError = TRUE;
					}
					else if(trim($Password) == ''){
						$this->message = $this->error_handler->_getStatusMessage('empty_password');
						$this->isError = TRUE;
					}
					else if(!$this->Login->_isValid_user_webser($Username,$Password))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_credential');
						$this->isError = TRUE;
					}
					else
					{
						$DataArr = $this->Login->_getUser_by_email($Username);	
						if($DataArr->MemberStatus == 'INACTIVE'){
							$this->message = $this->error_handler->_getStatusMessage('invalid_activate');
							$this->isError = TRUE;
							$DataArr =array();
						}
						elseif($DataArr->MemberStatus == 'DELETE'){
							$this->message = "User Deleted By Admin";
							$this->isError = TRUE;
							$DataArr =array();
						}
						else if($this->isError == FALSE  && $DataArr->MemberStatus == 'BLOCKED'){
							$this->message = $this->error_handler->_getStatusMessage('block_admin');
							$this->isError = TRUE;
							$DataArr =array();
						}
					}
				}
				if($this->isError === FALSE)
				{
					$query1=$this->db->query("UPDATE `app_mbtc`.`tr04_login` SET `tr04_login_status` =1 WHERE `m11_user_email` ='".trim($Username)."'");
					$query2=$this->db->query("UPDATE `app_mbtc`.`m11_user` SET `m11_user_islogin` =1 WHERE `m11_user`.`m11_user_email`='".trim($Username)."'");
					if(isset($Platform) &&  $Platform=="Android" && $Platform!="")
					{
						$query3=$this->db->query("UPDATE `app_mbtc`.`m11_user` SET `m11_user_device_type` ='".$Platform."' WHERE `m11_user`.`m11_user_email`='".trim($Username)."'");
						if(isset($Latitude) && $Latitude!="" )
						{
							$query4=$this->db->query("UPDATE `app_mbtc`.`m11_user` SET `m11_user_latitude` ='".$Latitude."' WHERE `m11_user`.`m11_user_email`='".trim($Username)."'");
						}
						if(isset($Longitude) && $Longitude!="" )
						{
							$query5=$this->db->query("UPDATE `app_mbtc`.`m11_user` SET `m11_user_longitude` ='".$Longitude."' WHERE `m11_user`.`m11_user_email`='".trim($Username)."'");
						}
						if(isset($MemberDeviceToken) && $MemberDeviceToken!='' )
						{
							$query6=$this->db->query("UPDATE `app_mbtc`.`m11_user` SET `m11_user_device_token` ='".$MemberDeviceToken."' WHERE `m11_user`.`m11_user_email`='".trim($Username)."'");
						}
					}
					
					//$DataArr = $this->Login->_getUser_by_email($Username);
					$sessiondata=array(
						'CONTACTNO'=>$DataArr->MemberContactNo,
						'REGID'=>$DataArr->RegId,
						'EMAIL'  => $DataArr->MemberEmail,
						'IWT'=>time(),
						'EXP'=>time()+20
						
					);
					//$this->session->set_userdata($sessiondata);
					$TOKEN = $this->jwt->encode($sessiondata,KEY);

					$this->message = $this->error_handler->_getStatusMessage('success_login');
					$this->status = "Success";
				}
			}
		
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $TOKEN, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
			$this->template('output',$data);
			
			//return $data;
		}
		/**
			* Method name: Admin_login
			* @description :  Used to login into mobile app
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		
		public function admin_login()
		{
			$Username='';
			$Password='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			//$postData = $this->input->post();
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				if($this->isError === FALSE)
				{
					if(trim($Username) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_email');
						$this->isError = TRUE;
					}
					else if(!_valid_email($Username))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_email');
						$this->isError = TRUE;
					}
					else if(trim($Password) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_password');
						$this->isError = TRUE;
					}
					else if(!$this->Login->_isValid_user_adminser($Username,$Password))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_credential');
						$this->isError = TRUE;
					}
				}
				if($this->isError === FALSE)
				{
					$message=rand(10000,999999);
					$var = array('{code}');
					$val = array($message);
					$query=$this->db->query("UPDATE `app_mbtc`.`m00_admin_login` SET `m00_access_token` =".$message." WHERE `m00_login_id` = 1;");
					$mail_send = $this->sendEmail(EMAIL, $var, $val,'admin_loginotp');
					//$this->send_sms('8188802777', $message);
					
					$this->message = $this->error_handler->_getStatusMessage('verify_otp');
					$this->status = "Success";
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
			$this->template('output',$data);
			
			//return $data;
		}
		
		
		function verify_otp()
		{
			$userid='';
			$insertArr = array();
			$DataArr = array();
			$DataArr = $this->_debugMode();
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				if(!empty($postData['otp']))
				{
					$token=$this->db->query("SELECT `m00_access_token` as Token FROM `app_mbtc`.`m00_admin_login` WHERE `m00_login_id` =1 ")->result();
					if($token!="" && $token!="NULL")
					{
						if($postData['otp']==$token[0]->Token)
						{
					  $query=$this->db->query("UPDATE `app_mbtc`.`m00_admin_login` SET `m00_access_token` ='',`m00_isloggedin` = 1 WHERE `m00_login_id` = 1;");
							$sessiondata=array(
							'user_id'=>USERID,
							'profile_id'=>-1,
							'e_email'  =>EMAIL,
							'name'     =>ucwords(SITE_NAME),
							'designation'=>'-1',
							'affid' =>0,
							'logged_in' => "TRUE",
							'user_image'=>'',
							'user_type'=>-1
							);
							$this->session->set_userdata($sessiondata);
							$this->message = $this->error_handler->_getStatusMessage('success_login');
							$this->status = 'success';
							
						}
						else
						{
					        $this->isError = TRUE;
							$sessiondata="";
							$this->message = $this->error_handler->_getStatusMessage('invalid_otp');
							$this->status = 'failed';
						}
					}
				}
			}	 
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>$sessiondata, 'message' => $this->message));
			$this->template('output',$data);
			
		}
		
		
		
		
		
		public function template($template_name, $data = array())
		{
			$this->load->view('webservice/'.$template_name, $data);
		}
		/**
			* Method name: facebook_login
			* @description :  Used to signup user account via facebook
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		public function facebook_login()
		{
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			//$postData = $this->input->post();
			$data['showHtml'] = @$postData['showHtml'];
			
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			
			if($this->input->server('REQUEST_METHOD') === "POST" &&  @$_POST['mode'] != "debug")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				
				if($this->isError === FALSE)
				{
					$user_details_by_fb = $this->webservice->get_fb_id_exist($fb_id, $email);
					if(trim($fb_id) == ''){
						$this->message = $this->error_handler->_getStatusMessage('empty_fbid');
						$this->isError = TRUE;
						}elseif(!valid_email($email) && $email){
						$this->message = $this->error_handler->_getStatusMessage('invalid_email');
						$this->isError = TRUE;
						}else if(!$this->webservice->sg_fb_id_exist($fb_id, $email)){
						$this->message = $this->error_handler->_getStatusMessage('invalid_fb_id');
						$this->error_code = 1;
						$this->isError = TRUE;
					}
					elseif($this->webservice->sg_fb_id_exist($fb_id, $email) && count($user_details_by_fb)>0)
					{
						if($user_details_by_fb['status'] == 'Pending'){
							$this->message = $this->error_handler->_getStatusMessage('invalid_activate');
							$this->isError = TRUE;
						}
						if($user_details_by_fb['status'] == 'Deleted'){
							$this->message = "User deleted by admin";
							$this->isError = TRUE;
						}
						if($this->isError == FALSE  && $user_details_by_fb['status'] == 'Inactive'){
							$this->message = $this->error_handler->_getStatusMessage('block_admin');
							$this->isError = TRUE;
						}
						
						//$this->message = $this->error_handler->_getStatusMessage('not_active');
						//$this->isError = TRUE;
					}
				}
				if($this->isError === FALSE)
				{
					$DataArr = $this->webservice->get_fb_id_exist($fb_id, $email);
					$last_login=$DataArr['new_login_user'];
					if($last_login == 'Yes' || $DataArr['phone_number'] == '')
					{
						$DataArr['new_user']='true';
					}
					else
					{
						$DataArr['new_user']='false';
					}
					
					$time_zone = __getTimeZone();
					if(!empty($DataArr))
					{
						$updatArray = array(
						'fb_id'	=> $fb_id,
						//'email'	=> $email,
						'device_token' => $device_token,
						'device_type' => $device_type,
						'app_version' => $app_version,
						'is_login' => 1,
						'time_zone' => $time_zone,
					    'new_login_user' => 'No',
						'image_name'	=> $DataArr['image_name'] ? $DataArr['image_name'] : $image_name,
						);
						//update login history
						$this->pages->updateUserLoginHistory($DataArr['id']);
						
						$updateID = $this->webservice->update_user($DataArr['id'], $updatArray);
						if(!empty($device_token) && !empty($device_type) || !empty($app_version))
						{
							$device = $this->webservice_model->check_device_token($device_token);
							
							if(!empty($device))
							{
								$delete_device = $this->webservice->delete_device_tokens($device_token);
								
								
							}
							$insertArray = array(
							'user_id'   => $DataArr['id'],
						    'device_token' => $device_token,
							'device_type' => $device_type,
							'app_version' => $app_version,
							
							);
							$insert_device = $this->webservice->insert_device_token($insertArray);
						}
						$this->message = $this->error_handler->_getStatusMessage('success_login');
						$this->status = "success";
					}
				}
			}
			$data['jsonData'] = json_encode(array('error_code' => $this->error_code, 'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
			$this->template('output',$data);
		}
		
		function _stripTags_trim($string)
		{
			return  strip_tags(trim($string));
		}
		
		
		
		//*** Destroy Session*****/
		public function destroy_session()
		{
            $Username='';
			$insertArr = array();
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			//$postData = $this->input->post();
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				if($this->isError === FALSE)
				{
					if(trim($UserContact) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_contactno');
						$this->isError = TRUE;
					}
					if(trim($UserContact)=="9999999999")
					{
						$query=$this->db->query("UPDATE `app_mbtc`.`m00_admin_login` SET `m00_access_token` ='',`m00_isloggedin` = 2 WHERE `m00_login_id` = 1;");
					}
					else
{
					$query=$this->db->query("UPDATE `app_mbtc`.`tr04_login` SET `tr04_login_status` = 2 WHERE `m11_user_contactno` =".trim($UserContact)."");
					$query2=$this->db->query("UPDATE `app_mbtc`.`m11_user` SET `m11_user_islogin` =2 WHERE `m11_user`.`m11_user_contactno`=".trim($UserContact)."");
}					
							$this->isError = FALSE;
							$this->message = $this->error_handler->_getStatusMessage('user_logout');
							$this->status = 'success';
				}
			}
					$this->session->unset_userdata('profile_id');
					$this->session->unset_userdata('user_id');
					$this->session->unset_userdata('e_email');
					$this->session->unset_userdata('name');
					$this->session->unset_userdata('designation');
					$this->session->unset_userdata('affid');
					$this->session->unset_userdata('logged_in');
					$this->session->unset_userdata('user_image');
					$this->session->unset_userdata('user_type');
					$this->session->sess_destroy();
					$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data);
		}
		
		
		// reset_password here
		public function forget_password()
		{
			$insertArr = array();
			$DataArr = array();
			$DataArr = $this->_debugMode();
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{


				/*foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}*/
				/*if(trim($email) == " "  && $mobileno==" ")
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_email');
					$this->isError = TRUE;
				}
				else if(!valid_email($email) )
                {	
					$this->message = $this->error_handler->_getStatusMessage('invalid_email');
					$this->isError = TRUE;
				}
				else if(!$this->Signup->sg_email_exist($email,0,'Pending') && $mobileno=="")
				{
					$this->message = $this->error_handler->_getStatusMessage('email_not_exists');
					$this->isError = TRUE;		 
					}else{
					
					$user=$this->db->query("CALL `enrolluser`(4,'".$email."',@msg)")->result();
					mysqli_next_result( $this->db->conn_id );
					if(isset($user))
					{	
						$link = base_url().'#/access/reset-password/oauth_token='.base64_encode('securelogin&oauthid='.rand(1111,9999).$user[0]->RegId.'&securitykey='.rand(11111,99999)); 
						$link = '<a href="'.$link.'" title="Change your password">Click here to change your password</a>';
						$var = array('{name}', '{link}');
						$val = array($user[0]->FirstName,$link);
						$mail_send = $this->sendEmail($email, $var, $val,'forgot_password');
						if($mail_send)
						{
							$this->message = $this->error_handler->_getStatusMessage('forget_email_send');
							$this->status = 'success';
							$this->isError = FALSE;
							}else{
							$this->message = $this->error_handler->_getStatusMessage('forget_email_not_send');	
							$this->isError = TRUE;
							$this->status = 'error';
						}
					}	 
				}*/ 
               if(trim($postData['mobileno']) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_contactno');
					$this->isError = TRUE;
				}
				
				 if($this->isError===FALSE)
				{
					$message=rand(10000,999999);
					$msg="Welcome to UNIQUE e BAZAAR,Please submit this code ".$message." to verify your account.";
					$query=$this->db->query("UPDATE `app_mbtc`.`tr04_login` SET `tr04_otp` =".$message." WHERE `m11_user_contactno`=".$postData['mobileno']." ;");
				    $this->send_sms($postData['mobileno'], $msg);
					$this->message = $this->error_handler->_getStatusMessage('verify_otp');
					$this->status = "Success";
				}
			}
			
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data);
			
			//return $data;
		}  

			function verify_userotp()
			{
			$userid='';
			$insertArr = array();
			$DataArr = array();
			$DataArr = $this->_debugMode();
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				if(!empty($postData['otp']))
				{
					$token=$this->db->query("SELECT `tr04_otp` as Token FROM `app_mbtc`.`tr04_login` WHERE `m11_user_contactno` =".$postData['MobileNo']." ")->result();
					if($token!="" && $token!="NULL")
					{
						if($postData['otp']==$token[0]->Token)
						{
                            $message=rand(10000,999999);
							$msg="Welcome to UNIQUE e BAZAAR,You account password has been reset successfully. Please login to your account with the provided credentials.Login MobileNo :".$postData['MobileNo']." Login password : ".$message ." .";
							$query=$this->db->query("UPDATE `app_mbtc`.`tr04_login` SET `tr04_login_pwd` =md5(".$message."),`tr04_pin_pwd` =md5(".$message.") WHERE `m11_user_contactno`=".$postData['MobileNo']." ;");
				            $this->send_sms($postData['MobileNo'], $msg);
							$this->message = "Password Reset Successfully";
							$this->status = 'success';
							
						}
						else
						{
					        $this->isError = TRUE;
							$sessiondata="";
							$this->message = $this->error_handler->_getStatusMessage('invalid_otp');
							$this->status = 'failed';
						}
					}
				}
			}	 
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>$sessiondata, 'message' => $this->message));
			$this->template('output',$data);
			
		}
		// reset_password here
		public function reset_password()
		{
			

			$insertArr = array();
			$DataArr = array();
			$DataArr = $this->_debugMode();
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				$user=$this->db->query("CALL `enrolluser`(3,'".$postData['oauthID']."',@msg)");
				mysqli_next_result( $this->db->conn_id );
				
				if($user->num_rows()){
					if(trim($newpassword) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_new_password');
						$this->isError = TRUE;
						
						}elseif(trim($confirm)==''){
						
						$this->message = $this->error_handler->_getStatusMessage('empty_confirm_password');
						$this->isError = TRUE;
						}elseif(trim($newpassword) != trim($confirm)){
						
						$this->message = $this->error_handler->_getStatusMessage('password_cpassword_no_match');
						$this->isError = TRUE;
						}else{
						
						$updateArr = array(
						'tr04_login_pwd' =>md5($newpassword) 
						);
						
						$updated = $this->db->update('tr04_login',$updateArr, array('m11_user_id'=>$user->row()->RegId));
						
						if($updated){
							$this->message = $this->error_handler->_getStatusMessage('password_updated');  
							$this->isError = FALSE;	
							}else{
							$this->message = $this->error_handler->_getStatusMessage('password_no_updated');  
							$this->isError = TRUE;	
						}
					}
					}else{
					$this->message = $this->error_handler->_getStatusMessage('no_user_found');  
					$this->isError = TRUE;
				}
				
			}  
			
			
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data);
			
			//return $data;
		}
		
		function authenticate_forgetpass_user()
        {
			$userid='';
			$insertArr = array();
			$DataArr = array();
			$DataArr = $this->_debugMode();
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				if(!empty($postData['oauthToken'])){
					
					$authArr = explode('=',$postData['oauthToken']); 
					if(isset($authArr[1]) && !empty($authArr[1])){
						
						$oauth = explode('&',base64_decode($authArr[1]));
						$a=explode('=',$oauth['1']);
						$oauthuserID = substr($a['1'],4);
						if($oauthuserID){
							$user=$this->db->query("CALL `enrolluser`(3,'".$oauthuserID."',@msg)")->num_rows();
							mysqli_next_result( $this->db->conn_id );
							if(isset($user)){
								$userid= $oauthuserID;
								$this->isError = FALSE;
								$this->status = 'success';
								}else{
								$this->isError = TRUE;	
								$this->status = 'failed';
							} 				
							}else{
							$this->isError = TRUE;	
							$this->status = 'failed';	
						}
					}
				}
				
			}	 
			
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>array('oauthID'=>$userid), 'message' => $this->message));
			$this->template('output',$data);
			
		}
		
		private function sendEmail($mailTo, $var, $val, $mailShortCode)
		{
			//=======================
			$this->load->library('phpmailer');
			$this->load->library('smtp');
			
			//$this->phpmailer->IsSMTP();
			$this->phpmailer->SMTPAuth = TRUE; // enable SMTP authentication
			$this->phpmailer->SMTPSecure = "ssl"; // sets the prefix to the servier
			$this->phpmailer->Host = $this->Signup->_getConfigurationByKey('smtp_server_host'); // sets GMAIL as the SMTP server
			$this->phpmailer->Port = $this->Signup->_getConfigurationByKey('smtp_port_number'); // set the SMTP port
			
			$this->phpmailer->Username = $this->Signup->_getConfigurationByKey('smtp_uName'); // GMAIL username
			$this->phpmailer->Password = $this->Signup->_getConfigurationByKey('smtp_uPass'); // GMAIL password
			
			$this->phpmailer->From = $this->Signup->_getConfigurationByKey('smtp_user');
			$this->phpmailer->FromName = $this->Signup->_getConfigurationByKey('smtp_user');
			//$this->phpmailer->setFrom($mail_from, SITE_NAME);
			$mail_from = $this->Signup->_getConfigurationByKey('signup_email');
			$this->phpmailer->setFrom($mail_from, 'UniqueeBazaar');
			//=========
			$mail_content = $this->Signup->_getMessage($mailShortCode);
			
			$message  	= html_entity_decode($mail_content->m09_msg_content);
			$subject 	= html_entity_decode($mail_content->m09_msg_subject);
			
			$mail_content = str_replace($var, $val, $message);
			$mail_body = html_entity_decode($mail_content);
			
			// $sign = $this->Signup->_getConfigurationByKey('EMAIL_SIGN');
			
			//$mail_body .= '<br />' . $sign;
			//==================
			
			
			$this->phpmailer->Subject = $subject;
			$this->phpmailer->Body = $mail_body; //HTML Body
			
			$this->phpmailer->clearAddresses();
			$this->phpmailer->AddAddress(strtolower($mailTo));
			$this->phpmailer->IsHTML(true); // send as HTML
			
			if(!$this->phpmailer->Send()) {
				//  echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
				//echo "\n\n";
				return false;
				exit;
				} else {
				echo " Message has been sent\n\n";
				return true;
			}
			//==========================================================
		}
		public function send_sms($mob,$msg)
		{
					$url="http://w2s.in/SendSMSAPI.aspx";
					$params = array (
					'mo'=>urlencode($mob),
					'ms'=>$msg,
					'r'=>urlencode(1),
					's'=>urlencode('PROMOT'),
					'u'=>urlencode('20039'),
					'p'=>urlencode('angel')
					);
					$options = array(
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0
					);
					
					$defaults = array(
					CURLOPT_URL => $url. (strpos($url, '?') 
					=== FALSE ? '?' : ''). http_build_query($params),
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_TIMEOUT =>56
					);
					
					$ch = curl_init();
					curl_setopt_array($ch, ($options + $defaults));
					$result = curl_exec($ch);
					if(!$result)
					{
						trigger_error(curl_error($ch));
						$flag=0;
					}
					else
					{	                
						$flag=1;
					}
					curl_close($ch);
		}
		
		public function check_sms()
		{
			
			$msg="Thank You for getting connected with our service. Your UID:9721789285,Pass:123,Tran.Pass:123";
			$mob='7080552897';
			$this->send_sms($mob,$msg);
		}
	}
?>