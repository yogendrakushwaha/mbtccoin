<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ActionWebService extends CI_Controller
{
    /**
			* Index Page for this controller.
			*
			* Maps to the following URL
			* 		http://example.com/index.php/MasterWebService
			*	- or -
			* 		http://example.com/index.php/MasterWebService/index
			*	- or -
			* Since this controller is set as the default controller in
			* config/routes.php, it's displayed at http://example.com/
			*
			* So any other public methods not prefixed with an underscore will
			* map to /index.php/MasterWebService/<method_name>
			*
			*
			*@category 	controller
			*@package 	application_controllers
			*@author 	AppWorks Technologies Pvt. Ltd. (00001) <info@appworkstech.com>
			*@version 	0.0.1
			*dated  	2017-03-31
		*/

    /**
			*Default class constructor
		*/

    var $status = "failed";
    var $message = "Invalid Operation";
    private $error_code = 0;
    private $isError = FALSE;
    private $record_per_page =10;
    private $userDisbled = 0;


    public function __construct()
    {
        //load parent class constructor
        parent::__construct();
        $this->load->model('MasterWebService_model','master_model');
        $this->load->helper('functions');
        $this->load->library('error_handler');
        $this->load->library('JWT');
        $this->headers=apache_request_headers();
        if(isset($this->headers["Authorization"]))
        {
            $token=explode(" ", $this->headers["Authorization"]);
            $user= $this->jwt->decode(trim($token[1],'"'),KEY);
            if($this->Userprofile->sg_email_exist($user->EMAIL,0,'Active'))
            {
                $sessiondata=array(
                    'CONTACTNO'=>$user->CONTACTNO,
                    'EMAIL'  => $user->EMAIL,
                    'IWT'=>time(),
                    'EXP'=>time()+20

                );
                $this->TOKEN = $this->jwt->encode($sessiondata,KEY);

            } 
            else
            {

                $this->isError=TRUE;
                $this->status='failed';
                $data['jsonData'] = json_encode(
                    array(
                        'error' => $this->isError,
                        'status' => $this->status,
                        'userDATA'=>"Unauthorized Access",
                        'token'=>$this->TOKEN ,
                        'message' => $this->message));
                $this->template('output',$data);
                die($data['jsonData']);	

            }
        }
        else
        {

            $this->isError=TRUE;
            $this->status='failed';
            $data['jsonData'] = json_encode(
                array(
                    'error' => $this->isError,
                    'status' => $this->status,
                    'userDATA'=>"Unauthorized Access",
                    'token'=>$this->TOKEN ,
                    'message' => $this->message));
            $this->template('output',$data);
            die($data['jsonData']);	
        }

    }

    /**
			* Method name: _debugMode
			* @description :  used for debug the posted data
			* @param:  Request data
			* @return:  array
		*/
    public function _debugMode()
    {
        $DataArr = array();
        if(isset($_POST['mode']) && $_POST['mode'] == "debug")
        {
            $this->status = "debug";
            $this->message = "Debug Mode";
            $DataArr = $_POST;
            return $DataArr;
        }
        else{
            return array();
        }
    }

    function _stripTags_trim($string)
    {
        return strip_tags(trim($string));
    }

    /**
			*Default action for the admin controller or landing function
			*dated : 2016-12-08
		*/
    public function index()
    {
    }
    

    public function userOrder()
    {
        $data['showHtml'] = @$postData['showHtml'];
        $json = file_get_contents('php://input');
        $postData = (array)json_decode($json);

        $data['result'] = $this->db->get("view_user_token")->result();
        $this->isError = FALSE;
        $this->status = 'success';
        $this->message='Buy Token Receive';
        $data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$data['result'] , 'message' =>$this->message));
        $this->template('output',$data); 
    }






}/*Class ENDS*/




/* End of file MasterWebService.php */
/* Location: ./application/controllers/MasterWebService.php */
?>