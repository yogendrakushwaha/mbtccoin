<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class SignupWebService extends CI_Controller
	{
		/**
			* Index Page for this controller.
			*
			* Maps to the following URL
			* 		http://example.com/index.php/SignupWebService
			*	- or -
			* 		http://example.com/index.php/SignupWebService/index
			**	- or -
			* Since this controller is set as the default controller in
			* config/routes.php, it's displayed at http://example.com/
			*
			* So any other public methods not prefixed with an underscore will
			* map to /index.php/LoginWebService/<method_name>
			*
			*
			*@category 	controller
			*@package 	application_controllers
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
		var $status = "failed";
		var $message = "Invalid Operation";
		private $error_code = 0;
		private $TOKEN="";
		private $isError = FALSE;
		private $record_per_page =10;
		private $userDisbled = 0;
		/**
			*Class default constructor
		*/
		public function __construct()
		{
			parent::__construct();
			$this->load->model('SignupWebService_model','Signup');
			$this->load->model('UserprofileWebService_model','Userprofile');
			$this->load->helper('functions');
			$this->load->library('error_handler');
			$this->load->library('JWT');
			$this->headers=apache_request_headers();
		if(isset($this->headers["Authorization"]))
		{
						$token=explode(" ", $this->headers["Authorization"]);
						$user= $this->jwt->decode(trim($token[1],'"'),KEY);
					    if($this->Userprofile->sg_email_exist($user->EMAIL,0,'Pending'))
						{
									$sessiondata=array(
										'CONTACTNO'=>$user->CONTACTNO,
										'EMAIL'  => $user->EMAIL,
										'IWT'=>time(),
										'EXP'=>time()+20
										
									);
									$this->TOKEN = $this->jwt->encode($sessiondata,KEY);
						
						} 
						else
						{
						
						$this->isError=TRUE;
						$this->status='failed';
							$data['jsonData'] = json_encode(
								array(
									'error' => $this->isError,
									'status' => $this->status,
									'userDATA'=>"Unauthorized Access",
									'token'=>$this->TOKEN ,
								    'message' => $this->message));
						    $this->template('output',$data);
							die($data['jsonData']);	

						}
	}
	
			
		}
		/**
			* Method name: _debugMode
			* @description :  used for debug the posted data
			* @param:  Request data
			* @return:  array
		*/
		public function _debugMode()
		{
			$DataArr = array();
			if(isset($_POST['mode']) && $_POST['mode'] == "debug")
			{
				$this->status = "debug";
				$this->message = "Debug Mode";
				$DataArr = $_POST;
				return $DataArr;
			}
			else{
				return array();
			}
		}
		
		function _stripTags_trim($string)
		{
			return  strip_tags(trim($string));
			}
		
		public function template($template_name, $data = array())
		{
			$this->load->view('webservice/'.$template_name, $data);
		}
		/**
			* Method name: user_signup
			* @description :  Used to signup user account
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		public function user_signup()
		{
			$insertArr = array();
			$DataArr = array();
				$TOKEN="";
			$image = $image_name = '';
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			//$postData = $this->input->post();
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				if($this->isError === FALSE)
				{
					if(trim($FirstName) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_name');
						$this->isError = TRUE;
					}
					elseif(!preg_match ("/^[a-zA-Z ]+$/",$FirstName))
					{
						$this->message = "Please enter only alphabets";
						$this->isError = TRUE;
					}
					elseif(!preg_match ("/^[a-zA-Z ]+$/",$LastName))
					{
						$this->message = "Please enter only alphabets";
						$this->isError = TRUE;
					}
					else if(strlen($FirstName) > 110)
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_name_length');
						$this->isError = TRUE;
					}
					else if(trim($Email) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_email');
						$this->isError = TRUE;
					}
					else if(!valid_email($Email))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_email');
						$this->isError = TRUE;
					}
					else if($this->Signup->sg_email_exist($Email,0,'Pending'))
					{
						$this->message = $this->error_handler->_getStatusMessage('email_exists');
						$this->isError = TRUE;
					} 
					else if(trim($Mobile) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_contactno');
						$this->isError = TRUE;
					}
					else if(!_valid_phone_number($Mobile))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_contactno');
						$this->isError = TRUE;
					}
					else if($this->Signup->sg_contact_exist($Mobile,0,'Pending'))
					{
						$this->message = $this->error_handler->_getStatusMessage('contactno_exists');
						$this->isError = TRUE;
					} 
					if(!isset($fb_id))
					{
						if(trim($Password) == '')
						{
							$this->message = $this->error_handler->_getStatusMessage('empty_password');
							$this->isError = TRUE;
						}
						else if(strlen($Password) < 6)
						{
							$this->message = $this->error_handler->_getStatusMessage('minlen_password');
							$this->isError = TRUE;
						}
						else if($Password != $C_Password)
						{
							$this->message = $this->error_handler->_getStatusMessage('confirm_password');
							$this->isError = TRUE;
						}
					}
					
					else if(trim($IsProf) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_acctype');
						$this->isError = TRUE;
					}
					/* elseif(isset($_FILES['image_name']) && ($_FILES['image_name']['name'] =="") && $_FILES['image_name']['error'] >0)
						{
						$this->message = $this->error_handler->_getStatusMessage('empty_image');
						$this->isError = TRUE;
						}
						elseif(isset($_FILES['image_name']) && ($_FILES['image_name']['name'] !="") && ($this->_fileUploadValidation($_FILES['image_name']['name']) == FALSE))
						{
						$this->message = $this->error_handler->_getStatusMessage('invalid_filetype');
						$this->isError = TRUE;
					} */
				}
				if($this->isError === FALSE)
				{
					unset($insertArr['Passwordrepeat']);
					/* if(isset($_FILES['image_name']) && $_FILES['image_name']['name'] !="" && $_FILES['image_name']['error'] ==0)
						{
						$image = $_FILES['image_name']['name'];
						$tmp_path = $_FILES['image_name']['tmp_name'];
						$image_name = uploadImage($image, $tmp_path,'users_profile');
						//create thumbnail
						$this->load->library('upload');
						$this->load->library('image_lib');
						
						$source_imagepath='./uploads/users_profile/'.$image_name;
						
						$thumbimage_name="thumb_".$image_name;
						$dest_imagepath='./uploads/users_profile/'.$thumbimage_name;
						$config1['image_library'] = 'gd2';
						$config1['source_image']	= $source_imagepath;
						$user_image_size=$this->config->item('user_thumbimage_size');
						$user_image_size=$user_image_size[0];
						$config1['maintain_ratio'] = TRUE;
						$config1['width']	= $user_image_size[0];
						//$config1['height']	= $user_image_size[1];
						$config1['new_image']   = $dest_imagepath;
						
						$this->image_lib->initialize($config1);
						$this->image_lib->resize();
						$this->image_lib->clear();
					} */
					$insertArr['Image_name'] = $image_name ;
					$insertArr['City'] = "N/A" ;
					if(!isset($fb_id))
					{
						$insertArr['Password'] 	= md5($insertArr['Password']);
					}
					$insertArr['Created_on'] 		= date('Y-m-d H:i:s');
					$insertArr['Membership_id'] 	= 1;
					$insertArr['New_login_user'] 	= 'Yes';
					//$insertArr['gender'] = $gender;
					if($IsProf == 'Yes')
					{
						$insertArr['User_type'] 			 = 'Service Center';
						$insertArr['Subscription']			 = 'Active';
						$insertArr['Cancellation_policy']	 = '1';
					}
					else
					{
						$insertArr['User_type'] 			= 'Consumer';
						$insertArr['Subscription'] 		= 'None';
						$insertArr['Cancellation_policy']	 = '0' ;
					}
					
					if(isset($Is_verified) && $Is_verified == 'Yes')
					{
						$insertArr['Status'] = 'Active';
						// $link 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';	
					}
					else
					{
						if(trim($Status)!= '' && trim($Status)!= 'Pending' && trim($Status)!= 'InActive')
						{
							$insertArr['Status'] = trim($Status);
						}
						else
						{
							$insertArr['Status'] = 'Pending';
						}
					}
					unset($insertArr['Is_verified']);

					$insertID 			= $this->Signup->create_user($insertArr);
					$insertkyc 			= array (
											    'm19_uk_user_id' =>$insertID,
												'm19_uk_onadd'   =>date('Y-m-d H:i:s'),
												);
					$kyc = $this->Userprofile->user_kyc($insertkyc);
					
					$generated_code = rand(10000,999999);
					$updateArray 	= array(
					'm11_user_activate_code' => $generated_code,
					'm11_user_role'=>''
					);
					$DataArr1 = $this->Signup->update_user($insertID,$updateArray);
					//$link = site_url('activateaccount/'.base64_encode($DataArr['id'].'/'.$generated_code));
					//$link 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';
					//$var = array('{name}', '{link}');
					//$val = array($postData['name'], $link);
					
					//if($DataArr['Status'] != 'Active')
					// {
					//$mail_send = $this->sendEmail($postData['Email'], $var, $val,'sign_up');
					//}
					//elseif($DataArr['Status'] == 'Active')
					//{
					// $adminEmail =  $this->admin_model->_getConfigurationByKey('admin_email');
					// $mail_send = $this->sendEmail($adminEmail,'new_sign_up');
					//}
					//$mail_send = $this->sendEmail($postData['Email'], $var, $val,'sign_up');
					$msg="Welcome to ".SITE_NAME.",Please submit this code ".$generated_code." to verify your account.";
					//$this->send_sms($postData['Contactno'], $msg);			
					$var = array('{name}', '{code}');
					$val = array($FirstName, $generated_code);
					$mail_send = $this->sendEmail($Email, $var, $val,'email_verify');
					$insertArr['Image_name'] = $image_name;
					$insertArr['Password'] = md5($insertArr['Password']);
					if($insertID)
					{
						if($insertArr['Status'] != 'Active')
						{
							//$adminEmail =  $this->admin_model->_getConfigurationByKey('admin_email');
							//$email_data['admin'] = 'Admin';
							//$mail_send = $this->sendEmail($adminEmail, $email_data, $val,'new_sign_up');
							//$var=array('{admin}');
							//$val='Admin';
							
							//$adminmailsend = $this->sendEmail($adminEmail,$var, $val,'new_sign_up');
							
							$this->message = $this->error_handler->_getStatusMessage('success');
						}
						else
						{
							$this->message = 'Login successfully';
						}
						$this->status = "success";
					} 
				}

					$sessiondata=array(
						'IWT'=>time(),
						'EXP'=>time()+20,
						'CONTACTNO'=>$Mobile,
						'EMAIL'  =>$Email
						
					);
					
					$TOKEN = $this->jwt->encode($sessiondata,KEY);
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $TOKEN, 'message' => $this->message));
			$this->template('output',$data);
		}
		
		/**
			* Method name: email_verify
			* @description :  Used to User Email/Mobile Verification
			* @param:  Request data
			* @return:  User detail array with success message
		*/
		public function verification()
		{
			$insertArr = array();
			$DataArr = array();
			$TOKEN="";
			$image = $image_name = '';
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);

			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
                      if(!empty($insertArr['otp']))
				{
					$token=$this->db->query("SELECT `m11_user_activate_code` as Token FROM `m11_user` WHERE `m11_user_email` ='".$insertArr['Email']."' ")->result();
					if($token!="" && $token!="NULL")
					{
						if($insertArr['otp']==$token[0]->Token)
						{
						$insertID=$this->db->query("SELECT `m11_user_id` as InsertiD ,`m11_first_name` AS FIRST_NAME ,`m11_last_name` AS LAST_NAME ,'m11_user_role' as Role FROM `m11_user` WHERE `m11_user_email` ='".$insertArr['Email']."' ")->result();
							$updateArray 	= array(
							'm11_user_status' => 1,
							'm11_user_role'=>'B2CUSER'
								);
							$DataArr1 = $this->Signup->update_user($insertID[0]->InsertiD,$updateArray);
							 $var = array('{name}', '{useremail}','{password}');
							 $val = array($insertID[0]->FIRST_NAME.' '.$insertID[0]->LAST_NAME, $insertArr['Email'],'123456789');
							 $mail_send 		= $this->sendEmail($insertArr['Email'], $var, $val,'account_creation');
							 $adminmailsend 	= $this->sendEmail(EMAIL,$var, $val,'account_creation');
                             $msg="Welcome to ".SITE_NAME ." ,You account  has been create successfully. Please login to your account with the provided credentials.LoginID :".$insertArr['Email']." Login password : 123456789.";
							//$this->send_sms($insertArr['Email'], $msg);
							$this->message 	= "Your Email has been verified.Your account has been activated please Login";
							$this->status = 'success';
							$sessiondata=array(
						'IWT'=>time(),
						'EXP'=>time()+20,
						'CONTACTNO'=>$insertArr['MobileNo'],
						'EMAIL'  => $insertArr['Email']
						
					);
					//$this->session->set_userdata($sessiondata);
					$TOKEN = $this->jwt->encode($sessiondata,KEY);

						}
						else
						{
					        $this->isError = TRUE;
							$sessiondata="";
							$this->message = $this->error_handler->_getStatusMessage('invalid_otp');
							$this->status = 'failed';
						}
					}
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $TOKEN, 'message' => $this->message));
			$this->template('output',$data);
		
		}
		/**
			* Method name: user_signup
			* @description :  Used to signup user account
			* @param:  Request data
			* @return:  user detail array with success message
		*/
	private function sendEmail($mailTo, $var, $val, $mailShortCode)
		{
			//=======================
			$this->load->library('phpmailer');
			$this->load->library('smtp');
			
			//$this->phpmailer->IsSMTP();
			$this->phpmailer->SMTPAuth = TRUE; // enable SMTP authentication
			$this->phpmailer->SMTPSecure = "ssl"; // sets the prefix to the servier
			$this->phpmailer->Host = $this->Signup->_getConfigurationByKey('smtp_server_host'); // sets GMAIL as the SMTP server
			$this->phpmailer->Port = $this->Signup->_getConfigurationByKey('smtp_port_number'); // set the SMTP port
			
			$this->phpmailer->Username = $this->Signup->_getConfigurationByKey('smtp_uName'); // GMAIL username
			$this->phpmailer->Password = $this->Signup->_getConfigurationByKey('smtp_uPass'); // GMAIL password
			
			$this->phpmailer->From = $this->Signup->_getConfigurationByKey('smtp_user');
			$this->phpmailer->FromName = $this->Signup->_getConfigurationByKey('smtp_user');
			//$this->phpmailer->setFrom($mail_from, SITE_NAME);
			$mail_from = $this->Signup->_getConfigurationByKey('signup_email');
			$this->phpmailer->setFrom($mail_from, 'UniqueeBazaar');
			//=========
			$mail_content = $this->Signup->_getMessage($mailShortCode);
			
			$message  	= html_entity_decode($mail_content->m09_msg_content);
			$subject 	= html_entity_decode($mail_content->m09_msg_subject);
			
			$mail_content = str_replace($var, $val, $message);
			$mail_body = html_entity_decode($mail_content);
			
			// $sign = $this->Signup->_getConfigurationByKey('EMAIL_SIGN');
			
			//$mail_body .= '<br />' . $sign;
			//==================
			
			
			$this->phpmailer->Subject = $subject;
			$this->phpmailer->Body = $mail_body; //HTML Body
			
			$this->phpmailer->clearAddresses();
			$this->phpmailer->AddAddress(strtolower($mailTo));
			$this->phpmailer->IsHTML(true); // send as HTML
			
			if(!$this->phpmailer->Send()) {
				//  echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
				//echo "\n\n";
				return false;
				exit;
				} else {
				//echo " Message has been sent\n\n";
				return true;
			}
			//==========================================================
		}
		public function send_sms($mob,$msg)
		{
					$url="http://w2s.in/SendSMSAPI.aspx";
					$params = array (
					'mo'=>urlencode($mob),
					'ms'=>$msg,
					'r'=>urlencode(1),
					's'=>urlencode('PROMOT'),
					'u'=>urlencode('20039'),
					'p'=>urlencode('angel')
					);
					$options = array(
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0
					);
					
					$defaults = array(
					CURLOPT_URL => $url. (strpos($url, '?') 
					=== FALSE ? '?' : ''). http_build_query($params),
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_TIMEOUT =>56
					);
					
					$ch = curl_init();
					curl_setopt_array($ch, ($options + $defaults));
					$result = curl_exec($ch);
					if(!$result)
					{
						trigger_error(curl_error($ch));
						$flag=0;
					}
					else
					{	                
						$flag=1;
					}
					curl_close($ch);
		}
		
		public function check_sms()
		{
			
			$msg="Thank You for getting connected with our service. Your UID:9721789285,Pass:123,Tran.Pass:123";
			$mob='7080552897';
			$this->send_sms($mob,$msg);
		}


   public function send_Email()
    {
       $this->load->library('phpmailer');
       $this->load->library('smtp');

       //$mail=new PHPMailer();

        //$this->phpmailer->IsSMTP();
        $this->phpmailer->SMTPAuth = TRUE; // enable SMTP authentication
        $this->phpmailer->SMTPSecure = "ssl"; // sets the prefix to the servier
        //$mail->phpmailer->SMTPSecure = 'tls';
        $this->phpmailer->Host = "smtp.uniqueebazaar.com"; // sets GMAIL as the SMTP server
        $this->phpmailer->Port = 25; // set the SMTP port

        $this->phpmailer->Username = "noreply@uniqueebazaar.com"; // GMAIL username
        $this->phpmailer->Password = "123456789@#"; // GMAIL password

        $this->phpmailer->From = "noreply@uniqueebazaar.com";
        $this->phpmailer->FromName = "Vaishanvi Group";
        $this->phpmailer->Subject = "Subject";
        $this->phpmailer->Body = "Message<br>"; //HTML Body
        $this->phpmailer->AltBody = "Message"; //Text Body

        $this->phpmailer->WordWrap = 50; // set word wrap

        $this->phpmailer->AddAddress("appworks1302@gmail.com");
        $this->phpmailer->AddReplyTo("noreply@uniqueebazaar.com","Webmaster");
        #$mail->AddAttachment("/path/to/file.zip"); // attachment
        #$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment
        $this->phpmailer->IsHTML(true); // send as HTML

        if(!$this->phpmailer->Send()) {
        echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
        echo "\n\n";
        exit;
        } else {
        echo "Gmail Message has been sent\n\n";
        }
    }
	}
	
?>