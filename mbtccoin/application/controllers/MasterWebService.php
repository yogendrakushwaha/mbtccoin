<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class MasterWebService extends CI_Controller
	{
		/**
			* Index Page for this controller.
			*
			* Maps to the following URL
			* 		http://example.com/index.php/MasterWebService
			*	- or -
			* 		http://example.com/index.php/MasterWebService/index
			*	- or -
			* Since this controller is set as the default controller in
			* config/routes.php, it's displayed at http://example.com/
			*
			* So any other public methods not prefixed with an underscore will
			* map to /index.php/MasterWebService/<method_name>
			*
			*
			*@category 	controller
			*@package 	application_controllers
			*@author 	AppWorks Technologies Pvt. Ltd. (00001) <info@appworkstech.com>
			*@version 	0.0.1
			*dated  	2017-03-31
		*/
		
		/**
			*Default class constructor
		*/
		
		var $status = "failed";
		var $message = "Invalid Operation";
		private $error_code = 0;
		private $isError = FALSE;
		private $record_per_page =10;
		private $userDisbled = 0;
		
		
		public function __construct()
		{
			//load parent class constructor
			parent::__construct();
			$this->load->model('MasterWebService_model','master_model');
			$this->load->helper('functions');
			$this->load->library('error_handler');
			$this->load->library('JWT');
			$this->headers=apache_request_headers();
			if(isset($this->headers["Authorization"]))
			{
				$token=explode(" ", $this->headers["Authorization"]);
				$user= $this->jwt->decode(trim($token[1],'"'),KEY);
				if($this->Userprofile->sg_email_exist($user->EMAIL,0,'Active'))
				{
					$sessiondata=array(
					'CONTACTNO'=>$user->CONTACTNO,
					'EMAIL'  => $user->EMAIL,
					'IWT'=>time(),
					'EXP'=>time()+20
					
					);
					$this->TOKEN = $this->jwt->encode($sessiondata,KEY);
					
				} 
				else
				{
					
					$this->isError=TRUE;
					$this->status='failed';
					$data['jsonData'] = json_encode(
					array(
					'error' => $this->isError,
					'status' => $this->status,
					'userDATA'=>"Unauthorized Access",
					'token'=>$this->TOKEN ,
					'message' => $this->message));
					$this->template('output',$data);
					die($data['jsonData']);	
					
				}
			}
			else
			{
				
				$this->isError=TRUE;
				$this->status='failed';
				$data['jsonData'] = json_encode(
				array(
				'error' => $this->isError,
				'status' => $this->status,
				'userDATA'=>"Unauthorized Access",
				'token'=>$this->TOKEN ,
				'message' => $this->message));
				$this->template('output',$data);
				die($data['jsonData']);	
			}
			
		}
		
		/**
			* Method name: _debugMode
			* @description :  used for debug the posted data
			* @param:  Request data
			* @return:  array
		*/
		public function _debugMode()
		{
			$DataArr = array();
			if(isset($_POST['mode']) && $_POST['mode'] == "debug")
			{
				$this->status = "debug";
				$this->message = "Debug Mode";
				$DataArr = $_POST;
				return $DataArr;
			}
			else{
				return array();
			}
		}
		
		function _stripTags_trim($string)
		{
			return strip_tags(trim($string));
		}
		
		/**
			*Default action for the admin controller or landing function
			*dated : 2016-12-08
		*/
		public function index()
		{
		}
		/**
			*This function is used to reset the password using forget password link
			*@param string $email
			*@return loaded view
		*/
		public function template($template_name, $data = array())
		{
			$this->load->view('webservice/'.$template_name, $data);
		}
		////////////// Add Role /Designation Here //////////////////////////
		
		public function role()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$insertArr = array();
			$json_data = array();
			$data['title'] ="Role Management";
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST" )
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
				$whereClause="";
				$limit=array(10,0);
				$this->db->select(array("role.*"));
				$this->db->from('role');
				if($insertArr['id']!="-1")
				{
					$this->db->where('Role_id',$insertArr['id']);
				}
				if(is_array($whereClause) && count ($whereClause)){
					$this->db->where($whereClause);
				}
				if(is_array($limit) && count ($limit) && !empty($limit[0]) ){
					$this->db->limit($limit[0],$limit[1]);
				}
				//$this->db->order_by('Role_id','DESC');
				$this->db->where('Role_parent',0);
				$query=$this->db->get();
				$json_data=$query->result_array();
				$this->status=='success';
				$this->message = "Role fetch successfully!";
				$this->isError = FALSE;
				
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'userDATA'=> $json_data, 'message' => $this->message));
			$this->template('output',$data);
		}
		
		
		/**
			*This function is used to add the Role
			*@param array requested data
			*@return loaded view
		*/
		public function add_role()
		{
		    $insertArr = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['Category']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'data'=> '', 'message' => $this->message));
			$this->template('output',$data);
		}
		
		
		/**
			*This function is used to edit the Role
			*@param array requested data
			*@return loaded view
		*/
		public function edit_role()
		{
		    $insertArr = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['Category']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'data'=> '', 'message' => $this->message));
			$this->template('output',$data);
		}
		
		
		////////////// Add Services for APP Here //////////////////////////
		
		public function service()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$insertArr = array();
			$json_data = array();
			$data['title'] ="Service Management";
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST" )
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
				$whereClause="";
				$limit=array(10,0);
				$this->db->select(array("service.*"));
				$this->db->from('service');
				if($insertArr['id']!="-1")
				{
					$this->db->where('ServiceId',$insertArr['id']);
				}
				if(is_array($whereClause) && count ($whereClause)){
					$this->db->where($whereClause);
				}
				if(is_array($limit) && count ($limit) && !empty($limit[0]) ){
					$this->db->limit($limit[0],$limit[1]);
				}
				//$this->db->order_by('ServiceTypeId','DESC');
				$query=$this->db->get();
				$json_data=$query->result_array();
				$this->status=='success';
				$this->message = "Service's fetch successfully!";
				$this->isError = FALSE;
				
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'userDATA'=> $json_data, 'message' => $this->message));
			$this->template('output',$data);
		}
		
		////////////// Add Services for APP Here //////////////////////////
		
		public function servicetype()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$insertArr = array();
			$json_data = array();
			$data['title'] ="Service Type Management";
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST" )
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
				$whereClause="";
				$limit=array(10,0);
				$this->db->select(array("service_type.*"));
				$this->db->from('service_type');
				if($insertArr['id']!="-1")
				{
					$this->db->where('ServiceTypeId',$insertArr['id']);
				}
				if(is_array($whereClause) && count ($whereClause)){
					$this->db->where($whereClause);
				}
				if(is_array($limit) && count ($limit) && !empty($limit[0]) ){
					$this->db->limit($limit[0],$limit[1]);
				}
				//$this->db->order_by('ServiceTypeId','DESC');
				$query=$this->db->get();
				$json_data=$query->result_array();
				$this->status=='success';
				$this->message = "Service's fetch successfully!";
				$this->isError = FALSE;
				
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'userDATA'=> $json_data, 'message' => $this->message));
			$this->template('output',$data);
		}
		
		/**
			*This function is used to add the Service Type
			*@param array requested data
			*@return loaded view
		*/
		public function add_servicetype()
		{
		    $insertArr = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['ServiceType']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				$servicetypedata['m03_service_type']= $insertArr['service'];
				$servicetypedata['m03_service_name']= $insertArr['serviceName'];
				$servicetypedata['m03_sr_type_status']= $insertArr['status'];
				$servicetypedata['m03_sr_type_onadd']= date('Y-m-d H:i:s');
				$servicetypedata['m03_sr_type_onupdate']= date('Y-m-d H:i:s');
				$this->db->insert('m03_service_type', $servicetypedata);
				$category_id = $this->db->insert_id();
				return $category_id;
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'data'=> '', 'message' => $this->message));
			$this->template('output',$data);
		}	
		
		////////////// Add Brand for Telecom/Utility/Booking Service Here //////////////////////////
		
		public function operator()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$is_edit = false;
			$insertArr = array();
			$json_data = array();
			$data['title'] ="Service Management";
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($this->input->server('REQUEST_METHOD') === "POST" )
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
				$whereClause="";
				$limit=array(100,0);
				$this->db->select(array("telecom_brand.*"));
				$this->db->from('telecom_brand');
				if($insertArr['id']!="-1")
				{
					$this->db->where('TelecomBrandId',$insertArr['id']);
					$is_edit = true;
				}
				if(is_array($whereClause) && count ($whereClause)){
					$this->db->where($whereClause);
				}
				if(is_array($limit) && count ($limit) && !empty($limit[0]) ){
					$this->db->limit($limit[0],$limit[1]);
				}
				//$this->db->order_by('ServiceTypeId','DESC');
				$query=$this->db->get();
				$json_data=$query->result_array();
				$this->status=='success';
				$this->message = "Brand For Service's fetch successfully!";
				$this->isError = FALSE;
				
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'userDATA'=> $json_data, 'message' => $this->message,'is_edit'=>$is_edit));
			$this->template('output',$data);
		}
		
		/**
			*This function is used to add Operator
			*@param array requested data
			*@return loaded view
		*/
		public function add_operator()
		{
			
		    $insertArr = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['Operator']);
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				
				$BrandID =!empty($postData['id']) ? $postData['id'] : '';
				
				if($postData['operatorName'] ==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_operatorname');
					$this->isError = TRUE; 
					}elseif($postData['operatorCode'] ==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_operatorCode');
					$this->isError = TRUE; 	
					}else{
					
					$DataArr = array(
					'm03_sr_type_id' =>$postData['service'],
					'm04_tc_brand_name' =>$postData['operatorName'],
					'm04_tc_brand_code' =>$postData['operatorCode'],
					'm04_tc_brand_status' =>$postData['status']
					);
					
				}
				
				
				if (isset($_FILES['img']) && $_FILES['img']['name'] !='') 
				{
					$folder_name="operator_images";
					$imagePath = "uploads/".$folder_name."/";
					if(!is_dir($imagePath))
					{
						if(!mkdir($imagePath,0777,true))
						{
							$response = Array(
							"error"=>'error',
							"status"=>'failed',
							"message"=>'Permission denied to create folder'
							);
							print json_encode($response);
							return;
						}
					}
					
					$maxSize = "2097152";
					$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
					$temp = explode(".", $_FILES["img"]["name"][0]);
					$extension = end($temp);
					$mimeType = array("image/png","image/jpeg","image/gif");
					$fileMime = trim($_FILES['img']['type'][0]);
					
					//Check write Access to Directory
					
					if(!is_writable($imagePath))
					{
						$response = Array(
						"error" => 'error',
						"status" => 'failed',
						"message" => 'Can`t upload File; no write Access'
						);
						print json_encode($response);
						return;
					}
					
					if ( in_array($extension, $allowedExts))
					{
						$check = getimagesize($_FILES["img"]["tmp_name"][0]);
						$imagewidth = $check[0];
						$imageheight = $check[1];
						
						if ($_FILES["img"]["error"][0] > 0)
						{
							$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'ERROR Return Code: '. $_FILES["img"]["error"][0],
							);
						}
						elseif($_FILES["img"]["size"][0] <= 0 || $_FILES["img"]["size"][0] > $maxSize)
						{
							$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Maybe '.$_FILES["img"]["name"][0]. ' exceeds max 2 MB size',
							);
							}elseif(!in_array($fileMime, $mimeType)){
							$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Image type not supported',
							);
						}
						else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
						{
							$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
							);
						}
						else
						{
							$temp_filename = $_FILES["img"]["tmp_name"][0];
							list($width, $height) = getimagesize( $temp_filename );
							
							$fileName = $_FILES["img"]["name"][0];
							$fileName = str_replace(" ","-",$fileName);
							$fileName = str_replace("%","",$fileName);
							$fileName = str_replace(",","",$fileName);
							$fileName = str_replace("'","",$fileName);
							$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).'_'.$fileName;
							
							move_uploaded_file($temp_filename,  $imagePath .$fileName );
							$targetFile = str_replace('//','/',$imagePath).$fileName;
							$info = pathinfo( $imagePath .$fileName);
							$getimg = getimagesize( $imagePath .$fileName);
							//list($width, $height, $type, $attr) = getimagesize($targetPath.$imgName);
							$width 		= $getimg[0];
							$height 	= $getimg[1];
							$type 		= $getimg[2];
							$attr 		= $getimg[3];
							if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG')
							$img = imagecreatefromjpeg( "{$targetFile}" );
							if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
							$img = imagecreatefromgif( "{$targetFile}" );
							if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
							$img = imagecreatefrompng( "{$targetFile}" );
							if($thumbSize)
							$thumbWidth = 140;
							else
							$thumbWidth = 60;
							if( $thumbWidth )
							{
								############## code for thumb ################
								if($width < $thumbWidth)
								$thumbWidth = $width;
								
								$width = imagesx( $img );
								$height = imagesy( $img );
								$new_height = floor( $height * ( $thumbWidth / $width ) );
								$new_width = $thumbWidth;
								
								$tmp_img = imagecreatetruecolor( $new_width, $new_height );
								imagealphablending($tmp_img, false);
								imagesavealpha($tmp_img,true);
								$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
								imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
								$targetFile1 =  str_replace('//','/',$imagePath). 'thumb_' . $fileName;
								
								if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
								imagejpeg( $tmp_img, "{$targetFile1}" );
								if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
								imagegif( $tmp_img, "{$targetFile1}" );
								if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
								imagepng( $tmp_img, "{$targetFile1}" );
							}
							$response = array(
							"error" => 'success',
							"status" => 'success',
							"url" => base_url().$imagePath.$fileName,
							"image"=>$fileName,
							"width" => $width,
							"height" => $height
							);
							
						}
					}
					else
					{
						$response = array(
						"error" => 'error',
						"status" => 'failed',
						"message" => 'Only jpg,jpeg,png and gif image is allowed.',
						);
					}
					$DataArr['m04_tc_brand_image'] = $fileName;
				} 	
				
			}
			$id = $this->master_model->add_operator($DataArr,$BrandID); 	
			if($id){
				$this->isError= FALSE;
				$this->status = 'success';
				$this->message = ($BrandID) ? $this->error_handler->_getStatusMessage('update_operator') : $this->error_handler->_getStatusMessage('add_operator') ;
				}else{
				$this->isError= TRUE;
				$this->status = 'error';	
			}
			$data['jsonData'] = json_encode(array("error" => $this->isError,'status' => $this->status, 'data'=> '', 'message' => $this->message,'is_edit'=>$BrandID));
			$this->template('output',$data);
		}
		
		/**
			*This function is used to edit Operator
			*@param array requested data
			*@return loaded view
		*/
		public function edit_operator()
		{
		    $insertArr = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['Operator']);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				$servicetypedata['m03_sr_type_id']= $insertArr['service'];
				$servicetypedata['m04_tc_brand_name']= $insertArr['operatorName'];
				$servicetypedata['m04_tc_brand_code']= $insertArr['operatorCode'];
				$servicetypedata['m04_tc_brand_image']='';
				$servicetypedata['m04_tc_brand_status']= $insertArr['status'];
				$servicetypedata['m04_tc_brand_onadd']= date('Y-m-d H:i:s');
				$servicetypedata['m04_tc_brand_onupdate']= date('Y-m-d H:i:s');
				$this->db->where('m04_telecom_brand', $id);
				$this->db->update('m04_telecom_brand', $servicetypedata);
				$category_id = $this->db->insert_id();
				return $category_id;
			}
			if($this->isError==TRUE)
			$error='error';
			if($this->isError==FALSE)
			$error='success';
			$data['jsonData'] = json_encode(array("error" => $error,'status' => $this->status, 'data'=> '', 'message' => $this->message));
			$this->template('output',$data);
		}
		
		
		
		public function show_sub_category(){
			
			$data=$this->master_model->select_sub_category();
			echo $data;
			
		}
		
		/**
			*This function is used to add Category
			*@param array requested data
			*@return loaded view
		*/
		
		
		public function add_category()
		{
			
		    $insertArr = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml'];
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['categoryData']);
			
			
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				
				// $categoryID =!empty($postData['id']) ? $postData['id'] : '';
				
				
				
				
				if($postData['categoryName'] ==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_categoryName');
					$this->isError = TRUE;
					}elseif($postData['categoryIdentifire'] ==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_categoryIdentifire');
					$this->isError = TRUE;
					}elseif($postData['categoryDescription'] ==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_categoryDescreption');
					$this->isError = TRUE;
					}elseif($postData['categoryStatus'] ==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_categoryStatus');
					$this->isError = TRUE;
					}else{
					
					
					
					
					
					if (isset($_FILES['img']) && $_FILES['img']['name'] !='') 
					{
						
						$fileName=date('Y-m-d').time().$_FILES['img']['name'];
						$fileTemp=$_FILES['img']['tmp_name'];
						move_uploaded_file($fileTemp, 'uploads/category_images/'.$fileName);
						
						
					}
					$img_name=array('categoryImage'=>$fileName);
					$data=array_merge($postData, $img_name);
					$id = $this->master_model->add_category($data);
					if($id){
						$this->isError= FALSE;
						$this->status = 'success';
						//$this->message = ($categoryID) ? $this->error_handler->_getStatusMessage('update_category') : $this->error_handler->_getStatusMessage('add_category') ;
						}else{
						$this->isError= TRUE;
						$this->status = 'error';	
					}
				}
			}
			//$data['jsonData'] = json_encode(array("error" => $this->isError,'status' => $this->status, 'data'=> '', 'message' => $this->message,'is_edit'=>$categoryID));
			//$this->template('output',$data);
		}
		
		/////////////////////////// Cancellation Policy ////////////////////////////////////////
		public function cancellation_rates()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$is_edit=false;
			$data['showHtml'] = @$postData['showHtml'];
			$data['title'] ="Cancellation Rates";
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
			}
			
			$whereClause="";
			
			if(!empty($insertArr) && $insertArr['id'] !='' && $insertArr['id'] !=-1){
				$whereClause = array('CPolicyId'=>$insertArr['id']);
				$is_edit = true;
			}
			
			$limit='All';
			$this->db->select(array("cancellation_policy.*"));
			$this->db->from('cancellation_policy');
			if(is_array($whereClause) && count ($whereClause))
			{
				$this->db->where($whereClause);
			}
			if(is_array($limit) && count ($limit) && !empty($limit[0]) )
			{
				$this->db->limit($limit[0],$limit[1]);
			}
			$this->db->order_by('CPolicyId','ASC');
			$query=$this->db->get();
			$json_data=$query->result_array();
			$data['jsonData'] = json_encode(array('error' => 'success','status' => 'success','userDATA'=>$json_data,'is_edit'=>$is_edit));
			$this->template('output',$data);
		}
		///////////////////////////////Mesaages /////////////////////////////////////////////////////	
		public function messages()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$is_edit = false;
			$data['showHtml'] = @$postData['showHtml'];
			$data['title'] ="User Management";
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
			}
			
			$whereClause="";
			
			if(!empty($insertArr) && $insertArr['id'] !='' && $insertArr['id'] !=-1){
				$whereClause = array('MessageId'=>$insertArr['id']);
				$is_edit = true;
			}
			
			$limit='All';
			$this->db->select(array("messages.*"));
			$this->db->from('messages');
			if(is_array($whereClause) && count ($whereClause))
			{
				$this->db->where($whereClause);
			}
			if(is_array($limit) && count ($limit) && !empty($limit[0]) )
			{
				$this->db->limit($limit[0],$limit[1]);
			}
			$this->db->order_by('MessageId','ASC');
			$query=$this->db->get();
			$json_data=$query->result_array();
			$data['jsonData'] = json_encode(array('error' => 'success','status' => 'success','userDATA'=>$json_data,'is_edit'=>$is_edit));
			$this->template('output',$data);
		}
		
		public function add_api(){
			$id = '';
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$postData = $postData['ApiData'];
			if(!empty($postData->Id)) $id = $postData->Id;
			if($postData->providerName==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_providerName');
				$this->isError = TRUE; 
				
				}elseif($postData->apiurl==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_apiurl');
				$this->isError = TRUE; 
				
				}else{
				
				$apidata = array(
				'm12_api_name' => $postData->providerName,
				'm12_api_url' => $postData->apiurl,
				'm12_api_status' => $postData->status,
				);
				
				$API_id = $this->master_model->add_api($apidata,$id); 
				if($API_id){
					$this->isError= FALSE;
					$this->status = 'success';
					$this->message = ($id !='') ? $this->error_handler->_getStatusMessage('update_apiurl') : $this->error_handler->_getStatusMessage('add_apiurl');
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'id'=>$id,'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		public function getApi($API_id=NULL){
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$STATUS = '';
			if(!empty($postData)){
				$API_id = $postData['API_id'];
				$STATUS = $postData['STATUS'];
			}
			$APIS = $this->master_model->get_api($API_id,$STATUS); 
			if(!empty($APIS)){
				$this->isError = FALSE;	
				$this->status= 'success';	
				$this->message = $this->error_handler->_getStatusMessage('api_loaded');
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'id'=>$API_id,'status' => $this->status, 'data'=>$APIS, 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		
		public function add_apiuri(){
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$postData = $postData['ApiUriData'];
			$id = '';
			
			if(!empty($postData->id)) $id = $postData->id;
			
			if($postData->apiName==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_apiName');
				$this->isError = TRUE; 
				
				}elseif($postData->apiuri==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_apiurl');
				$this->isError = TRUE; 
				
				}else{
				
				$apiuridata = array(
				'm12_api_id' => $postData->selectapi,
				'm14_api_url_name' => $postData->apiName,
				'm14_api_url_address' => $postData->apiuri,
				'm14_api_url_for' => $postData->type,
				'm14_api_url_status' => $postData->status,
				'm14_api_url_onadd' =>date('Y-m-d H:i:s'),
				);
				
				$API_id = $this->master_model->add_api_Uri($apiuridata,$id); 
				if($API_id){
					$this->isError= FALSE;
					$this->status = 'success';
					$this->message = $id ? $this->error_handler->_getStatusMessage('update_apiuri') : $this->error_handler->_getStatusMessage('add_apiuri');
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'id'=>$id, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		public function getAPIServicesType($service_type_id=NULL){
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			if(!empty($postData)){
				$service_type_id = $postData['SERVICE_TYPE_ID'];
			}
			
			$service = $this->master_model->get_service_type($service_type_id); 
			if(!empty($service)){
				$this->isError = FALSE;	
				$this->status= 'success';	
				$this->message = $this->error_handler->_getStatusMessage('service_loaded');
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>$service, 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		public function apioperator($o_id=NULL){
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$SERVICE_TYPE_ID='';
			if(!empty($postData)){
				$SERVICE_TYPE_ID = $postData['SERVICE_TYPE_ID'];
			}
			
			$operators = $this->master_model->get_operators($o_id=NULL,$SERVICE_TYPE_ID); 
			$this->isError = FALSE;	
			$this->status= 'success';	
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>$operators, 'message' =>'Operator loaded'));
			$this->template('output',$data); 
		}
		
		
		public function add_apioperator($id=null){
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$postData = $postData['ApiOperatorData'];
			
			if(!empty($postData->id)) $id = $postData->id;
			
			
			if($postData->operatorCode==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_operatorCode');
				$this->isError = TRUE; 
				}else{
				
				$AddArray = array(
				'm12_api_id' => $postData->api_id,
				'm07_service_id' => $postData->service_id,
				'm04_tc_brand_id' => $postData->brand_id,
				'm15_sp_code' => $postData->operatorCode,
				'm15_sp_status' =>$postData->status,
				'm15_sp_onadd' =>date('Y-m-d H:i:s'),
				);
				
				$action = $this->master_model->add_apioperator($AddArray,$id); 
				
				if($action){
					$this->isError= FALSE;
					$this->status = 'success';
					$this->message = $id ? $this->error_handler->_getStatusMessage('update_apioperator') : $this->error_handler->_getStatusMessage('added_apioperator');
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
			
		}
		
		
		public function add_apisms(){
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$postData = $postData['ApiSmsData'];
			if($postData->apiName==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_apiName');
				$this->isError = TRUE; 
				
				}elseif($postData->apiurl==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_apiurl');
				$this->isError = TRUE; 
				
				}else{
				
				$Api = array(
				'm_api_id' => $postData->selectapi,
				'm_api_url_name' => $postData->apiName,
				'm_api_url_address' => $postData->apiurl,
				'm_api_url_for' => $postData->type,
				'm_api_url_status' => $postData->status,
				'm_api_url_on_created' =>date('Y-m-d H:i:s'),
				);
				
				$API_id = $this->master_model->add_api_Uri($Api); 
				if($API_id){
					$this->isError= FALSE;
					$this->status = 'success';
					$this->message = $this->error_handler->_getStatusMessage('add_smsapi');
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'id'=>$id, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		public function apiuri($URL_ID=NULL){
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$STATUS= '';
			if(!empty($postData)){
				$URL_ID = $postData['URL_ID'];
				$STATUS = $postData['STATUS'];
			}
			
			$APIS = $this->master_model->get_api_url($URL_ID,$STATUS); 
			if(!empty($APIS)){
				$this->isError = FALSE;	
				$this->status= 'success';	
				$this->message = $this->error_handler->_getStatusMessage('api_loaded');
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'id'=>$URL_ID,'status' => $this->status, 'data'=>$APIS, 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		
		public function get_service_provider($OPERATOR_ID=NULL , $status=NULL){
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$STATUS = '';
			if(!empty($postData)){
				$OPERATOR_ID = $postData['OPERATOR_ID'];
				$STATUS = $postData['STATUS'];
			}
			$Op = $this->master_model->get_service_provider($OPERATOR_ID,$STATUS); 
			if(!empty($Op)){
				$this->isError = FALSE;	
				$this->status= 'success';	
				$this->message = $this->error_handler->_getStatusMessage('service_loaded');
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'id'=>$OPERATOR_ID,'status' => $this->status, 'data'=>$Op, 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		
		
		public function edit_cancellation_policy(){
			$id = '';
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
			}
			if($name==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_policyName');
				$this->isError = TRUE; 
				
				}elseif($value==''){
				
				$this->message = $this->error_handler->_getStatusMessage('empty_policyvalue');
				$this->isError = TRUE; 
				
				}else{
				
				$CPolicy = array(
				'm08_cpolicy_name' =>$name,
				'm08_cpolicy_percent' =>$value,
				'm08_cpolicy_status' =>$status,
				);
				$CPolicyPositive = $this->master_model->cancellation_policy($CPolicy,$CPolicyId); 
				if($CPolicyPositive){
					$this->isError= FALSE;
					$this->status = 'success';
					$this->message = $this->error_handler->_getStatusMessage('update_cpolicy');
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		public function updateMessage(){
			$id = '';
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				
				if($title==''){
					$this->message = $this->error_handler->_getStatusMessage('empty_msg_title');
					$this->isError = TRUE; 
					
					}elseif($subject==''){
					
					$this->message = $this->error_handler->_getStatusMessage('empty_msg_subject');
					$this->isError = TRUE; 
					
					}elseif($content==''){
					
					$this->message = $this->error_handler->_getStatusMessage('empty_msg_content');
					$this->isError = TRUE; 
					
					}else{
					
					$Umessage = array(
					'm09_msg_title' =>$title,
					'm09_msg_subject' =>$subject,
					'm09_msg_content' =>$content,
					'm09_msg_status' =>$status,
					);
					$msgPositive = $this->master_model->updateMessage($Umessage,$MessageId); 
					if($msgPositive){
						$this->isError= FALSE;
						$this->status = 'success';
						$this->message = $this->error_handler->_getStatusMessage('update_message');
					}
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		public function getSiteConfigurationByID(){
			$id = '';
			$sitedata = '';
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				} 
				$this->db->select('m00_id AS configID, m00_name AS configName, m00_value AS configValue, m00_desc AS configDesc');
				$this->db->where('m00_id',$id);
				$query = $this->db->get('m00_setconfig');
				if($query->num_rows()) {
					$sitedata = $query->row();
					$this->isError = FALSE;
					$this->status = 'success';
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'data'=>$sitedata, 'message' => $this->message));
			$this->template('output',$data); 
		}
		
		
		public function get_site_configuration(){
			$data='';
			if($this->input->server('REQUEST_METHOD') === "GET")
			{
				$this->db->select('m00_id AS configID, m00_name AS configName, m00_value AS configValue, m00_desc AS configDesc');
				$query = $this->db->get('m00_setconfig');
				if($query->num_rows()) {
					$data = $query->result();
					$this->isError = FALSE;
					$this->status = 'success';
				}
			}
			echo json_encode(array('error'=>$this->isError,'status' => $this->status,'data'=>$data, 'message' => $this->message));
			exit(); 
		}
		
		public function edit_siteConfiguration()
		{
			$data['showHtml'] = @$postData['showHtml']; 
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$postData = $postData['editSiteConfig'];
			
			if(!empty($postData->siteId))
			{
				if($postData->sitevalue=='')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_sitevalue');
					$this->isError = TRUE; 
				}
				else if($postData->sitedesc=='')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_sitedesc');
					$this->isError = TRUE;
				}
				else
				{
					$siteData = array(
					'm00_value' => $postData->sitevalue,
					'm00_desc' => $postData->sitedesc
					);
					$query = $this->db->update('m00_setconfig',$siteData, array('m00_id'=>$postData->siteId));
					if($query)
					{
						$this->isError= FALSE;
						$this->status = 'success';
						$this->message = $this->error_handler->_getStatusMessage('update_siteconfig');
					}
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status,'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
		}
		public function updateConfigSiteData()
		{    
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$json_data=file_get_contents('php://input');
				$get_data=(array)json_decode($json_data);
				
				if(!empty($get_data->sitename))
				{
					if(!empty($get_data->sitevalue))
					{
						if(!empty($get_data->sitediscription))
						{
							//Database query will be write here..........
						}
						else{
							$this->message = $this->error_handler->_getStatusMessage('empty_sitedesc');
							$this->isError = TRUE;
						}
					}
					else{
						$this->message = $this->error_handler->_getStatusMessage('empty_sitevalue');
						$this->isError = TRUE;
					}
				}
				else{
                    $this->message = $this->error_handler->_getStatusMessage('empty_sitename');
				    $this->isError = TRUE; 
				}
			}
		}
		public function get_qrcode_data()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$get_qrcode_data=$this->db->query("select m12_id as qrcodeId, m12_qrcode_name as qrcodeName, (select m11_user_name from m11_user where `m11_user`.`m11_user_id` = `m12_qrcode_info`.`m12_assign_to`) as qrcodeAssignTo, m12_assign_type as qrcodeAssignType, m12_date_created as qrcodeDateCreated from `app_mbtc`.`m12_qrcode_info`")->result();
			$data['jsonData']=json_encode($get_qrcode_data);
			$this->template('output',$data);
			
		}
		public function generateqrcode(){
			
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{
				
				$json = file_get_contents('php://input');//Getting data from angular service in json form...
				$postData = (array)json_decode($json);  //decoding data from json..
				$data['showHtml'] = @$postData['showHtml'];
				unset($postData['showHtml']);
				unset($postData['submit']);
				unset($postData['mode']);
				
				$Pattern=strtoupper($postData['qrcodePattern']);  ///Converting pattern name in uppercase..
				$Number=$postData['qrcodeNumber'];              ///getting number of assign to generate..
				
				$start_sequence=$this->db->query("select `m12_id` from `m12_qrcode_info` ORDER BY `m12_id` DESC limit 1")->row()->m12_id;   // Getting last id of assign from table m011_assign_info for 
				//generating sequence...
				$num_repeat=$start_sequence;       //generating number to store in assign..
				for($i=1;$i<=$Number;$i++)
				{                                       //loop for generating assign as demand
					
					$this->load->library('ciqrcode'); //https://github.com/dwisetiyadi/CodeIgniter-PHP-QR-Code
					$name_qrcode=$Pattern.$num_repeat.date('Y-m-d');   //Generating the name of assign...
					$params['data'] = hash('sha1', $Pattern.$num_repeat.date('Y-m-d'));  ///encrypting the name//
					$params['level'] = 'H';         //level of assign...
					$params['size'] = 50;             //size of assign..
					$params['savename'] = 'qrcodes/'.$name_qrcode.'.png';  //name as save in folder...
					$this->ciqrcode->generate($params); // Calling function generate and passing perameters from 
					//library(ciassign)
					$this->db->query("insert into `m12_qrcode_info`(m12_qrcode_name,m12_qrcode_data)values('".$name_qrcode."','".$params['data']."')"); ///Inserting data in table of assign....
					$num_repeat++;    //increasing number use in assign by 1...
				}
				$this->message=$this->error_handler->_getStatusMessage('generation_successful');
				$this->isError = FALSE;
				$this->status = "Success";
			}
			else{
				$this->message=$this->error_handler->_getStatusMessage('generation_unsuccessful');
				$this->isError = TRUE;
				
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'message' => $this->message));
			$this->template('output',$data);
		}
		
		public function assign_qrcode()
		{
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{  
				
				$json = file_get_contents('php://input');//Getting data from angular service in json form...
				$postData = (array)json_decode($json);  //decoding data from json..
				$data['showHtml'] = @$postData['showHtml'];
				
				
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				if($this->isError === FALSE)
				{
					if($userNumber == '')
					{
						$this->message=$this->error_handler->_getStatusMessage('number_blank');
						$this->isError = TRUE;
					}
					else if(strlen($userNumber) > 10)
					{
						$this->message=$this->error_handler->_getStatusMessage('number_unvalid');
						$this->isError = TRUE;
					}
					else if($assignName == '')
					{
						$this->message=$this->error_handler->_getStatusMessage('user_name');
						$this->isError = TRUE;
					}
					else if($qrcodeName == '')
					{
						$this->message=$this->error_handler->_getStatusMessage('blank_qrcode');
						$this->isError = TRUE;
					}
					else{
						
						
						if($this->master_model->insert_assign_data($userNumber,$assignName,$qrcodeName))
						{
							$this->message=$this->error_handler->_getStatusMessage('assign_successful');
							$this->isError = FALSE;
							$this->status = "Success";
						}
						else{
							$this->message=$this->error_handler->_getStatusMessage('number_unavailable');
							$this->isError = TRUE;
						}
					}
				}
				$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'message' => $this->message));
				$this->template('output',$data);
			}
			
		}          
		public function get_user_data()
		{
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{  
				
				$json = file_get_contents('php://input');//Getting data from angular service in json form...
				$postData = json_decode($json);  //decoding data from json..
				$data['showHtml'] = @$postData['showHtml'];
				unset($postData['showHtml']);
				unset($postData['submit']);
				unset($postData['mode']);
				$userNumber=$postData;
				
				if($this->isError === FALSE)
				{
					if($userNumber == '')
					{
						$this->message=$this->error_handler->_getStatusMessage('number_blank');
						$this->isError = TRUE;
					}
					else if(strlen($userNumber) > 10)
					{
						$this->message=$this->error_handler->_getStatusMessage('number_unvalid');
						$this->isError = TRUE;
					}
					else{
						$get_data=$this->db->query("select m11_user_name as assignName from m11_user where m11_user_contactno = '".$userNumber."'")->row()->assignName;
						
						if($get_data)
						{
							
							$data['jsonData']=json_encode(array('data'=>$get_data,'error'=>$this->isError,'message'=>$this->message,'status'=>$this->status));
							$this->template('output',$data);
						}
						else{
							$this->message=$this->error_handler->_getStatusMessage('number_unavailable');
							$this->isError = TRUE;
						}
					}
				}
			}
		}
		
		public function get_site_config()
		{
			// get site configration..........
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			$get_query=$this->db->query('select * from`app_mbtc`.`m00_setconfig`')->result();
			/*$data= array(
				'configName' => $get_query->m00_name,
				'configValue' => $get_query->m00_value,
				'configDescription' => $get_query->m00_desc
			);*/
			$data['jsonData'] = json_encode($get_query);
			$this->template('output',$data);
			
		}
		
		public function add_site_config()
		{  
			$Site_Id='';
			$Site_Name='';
			$Site_Value='';
			$Site_Discription='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			
			/*if($Site_Id){
				$Site_Id=$postData['Site_Config_Id'];
				$Site_Name=$postData['Site_Config_Name'];
				$Site_Value=$postData['Site_Config_Value'];
				$Site_Discription=$postData['Site_Config_Discription'];
				}
				else{
				$Site_Name=$postData['Site_Config_Name'];
				$Site_Value=$postData['Site_Config_Value'];
				$Site_Discription=$postData['Site_Config_Discription'];
				}
				
				$Site_Id=$postData['Site_Config_Id'];
				$Site_Name=$postData['Site_Config_Name'];
				$Site_Value=$postData['Site_Config_Value'];
			$Site_Discription=$postData['Site_Config_Discription'];*/
			
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{
				
                foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				
				if($this->isError === FALSE)
				{
					if(trim($Site_Config_Name) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_site_name');
						$this->isError = TRUE;
					}
					
					else if(trim($Site_Config_Value) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_site_value');
						$this->isError = TRUE;
					}
					else if(trim($Site_Config_Discription) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_site_discription');
						$this->isError = TRUE;
					}
					else
					{
						$this->isError = FALSE;
					}
					
				}
				if($this->isError === FALSE)
				{
					if($Site_Id)
					{
						
						if($this->master_model->update_site_config($Site_Id,$Site_Config_Name,$Site_Config_Value,$Site_Config_Discription))
						{
							$this->message=$this->error_handler->_getStatusMessage('update_success');
							$this->isError = FALSE;
							$this->status = "Success";
						}
						
						else{
							$this->message=$this->error_handler->_getStatusMessage('update_unsuccessful');
							$this->isError = TRUE;
						}
					}
					else {
						
						if($this->master_model->insert_site_config($Site_Config_Name,$Site_Config_Value,$Site_Config_Discription))
						{
							$this->message=$this->error_handler->_getStatusMessage('insert_success');
							$this->isError = FALSE;
							$this->status = "Success";
						}
						else{
							$this->message=$this->error_handler->_getStatusMessage('insert_unsuccessful');
							$this->isError = TRUE;
						}
						
					}
					
					//$mail_send = $this->sendEmail(EMAIL, $var, $val,'admin_loginotp');
					//$this->send_sms('', $message);
					$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
					$this->template('output',$data);
					
				}
			}
			
		}
		public function get_sellerStatus_count()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$get_sellerStatus_count=$this->db->query("select(select count(*) from m13_seller_info where m13_seller_status = 'ACTIVE') as activeCount, (select count(*) from m13_seller_info where m13_seller_status= 'INACTIVE') as inactiveCount")->row();
			$data['jsonData']=json_encode($get_sellerStatus_count);
			$this->template('output',$data);
		}
		
		public function get_seller_data()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$get_seller_data=$this->db->query("SELECT m13_seller_id AS sellerId,
			`m11_user`.`m11_user_id` AS userId,
			`m11_user`.`m11_user_name` AS userName,
			m13_seller_name AS sellerName,
			m13_seller_company_display_name AS sellerCompanyName,
			m13_seller_mobile_no AS sellerContactno,
			m13_seller_primary_email AS sellerEmail, 
			m13_seller_city AS sellerCity, 
			m13_seller_pincode AS sellerPincode,
			m13_seller_state AS sellerState,
			m13_seller_gstin AS sellerGstin,
			m13_seller_vat_tin AS sellerVat,
			m13_seller_company_pan AS sellerCompanyPan,
			m13_seller_status AS sellerStatus,
			m13_seller_subscription AS sellerSubscription,
			m13_seller_onadd_date AS sellerJoinDate
			FROM m13_seller_info 
			LEFT JOIN m11_user ON m13_seller_info.m11_user_id = m11_user.m11_user_id ")->result();
			$data['jsonData']=json_encode($get_seller_data);
			$this->template('output',$data);
			
		}
		
		public function change_seller_status()
		{
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{  
				
				$json = file_get_contents('php://input');//Getting data from angular service in json form...
				$postData = json_decode($json);  //decoding data from json..
				$data['showHtml'] = @$postData['showHtml'];
				unset($postData['showHtml']);
				unset($postData['submit']);
				unset($postData['mode']);
				$sellerId=$postData;
				if($this->isError === FALSE)
				{
					if($sellerId =='')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_user_id');
						$this->isError = TRUE;
					}
					else{
						if($this->master_model->change_seller_status($sellerId))
						{
							$this->message=$this->error_handler->_getStatusMessage('status_changed');
							$this->isError = FALSE;
							$this->status="Success";
						}
						else{
							$this->message=$this->error_handler->_getStatusMessage('status_changed_unsuccessful');
							$this->isError = FALSE;
						}
					}
				}
				$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'message' => $this->message));
				$this->template('output',$data);
				
				
			}
		}
		public function get_userStatus_count()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$get_userStatus_count=$this->db->query("select(select count(*) from m11_user where m11_user_status = 'ACTIVE') as activeCount, (select count(*) from m11_user where m11_user_status= 'INACTIVE') as inactiveCount")->row();
			$data['jsonData']=json_encode($get_userStatus_count);
			$this->template('output',$data);
		}
		public function get_alluser_data()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$get_alluser_data=$this->db->query("select m11_user_id as userId,m11_user_name as userName,m11_first_name as userFirstName,m11_last_name as userLastName,m11_user_email as userEmail,m11_user_contactno as userContactNo,m11_user_gender as userGender,m11_user_role as userRole,m11_user_status as userStatus,m11_subcription as userSubscriptionStatus,m11_user_islogin as userActivity,m11_user_dob as userDob,m11_user_joined as userJoined from m11_user")->result();
			$data['jsonData']=json_encode($get_alluser_data);
			$this->template('output',$data);
		}
		public function change_user_status()
		{
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{  
				
				$json = file_get_contents('php://input');//Getting data from angular service in json form...
				$postData = json_decode($json);  //decoding data from json..
				$data['showHtml'] = @$postData['showHtml'];
				unset($postData['showHtml']);
				unset($postData['submit']);
				unset($postData['mode']);
				$userId=$postData;
				if($this->isError === FALSE)
				{
					if($userId =='')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_user_id');
						$this->isError = TRUE;
					}
					else{
						if($this->master_model->change_user_status($userId))
						{
							$this->message=$this->error_handler->_getStatusMessage('status_changed');
							$this->isError = FALSE;
							$this->status="Success";
						}
						else{
							$this->message=$this->error_handler->_getStatusMessage('status_changed_unsuccessful');
							$this->isError = FALSE;
						}
					}
				}
				$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'message' => $this->message));
				$this->template('output',$data);
				
			}
		}
		
		public function add_user()
		{
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{  
				
				$json = file_get_contents('php://input');//Getting data from angular service in json form...
				$postData = (array)json_decode($json);  //decoding data from json..
				$data['showHtml'] = @$postData['showHtml'];
				
				
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				if($this->isError === FALSE)
				{
					if($userUsername == '' || $userUsername === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('user_name');
						$this->isError= TRUE;
					}
					else if($userFirstname == '' || $userFirstname === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_firstname');
						$this->isError= TRUE;
					}
					else if($userLastname == '' || $userLastname === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_lastname');
						$this->isError= TRUE;
					}
					else if($userEmail == '' || $userEmail === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_email');
						$this->isError= TRUE;
					}
					else if($userPassword == '' || $userPassword === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_password');
						$this->isError= TRUE;
					}
					else if($userC_password == '' || $userC_password === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_password');
						$this->isError= TRUE;
					}
					else if($userRole == '' || $userRole === 'NULL')
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_role');
						$this->isError= TRUE;
					}
					else if($userPassword !== $userC_password)
					{
						$this->message=$this->error_handler->_getStatusMessage('confirm_password');
						$this->isError= TRUE;
					}
					else{
						
						if($this->master_model->add_user($insertArr))
						{
							$this->message=$this->error_handler->_getStatusMessage('user_added');
							$this->isError= FALSE;
							$this->status='Success';
						}
						else
						{
							$this->message=$this->error_handler->_getStatusMessage('tansection_unsuccess');
							$this->isError= TRUE;
						}
					}
				}
				else
				{
					$this->message=$this->error_handler->_getStatusMessage('register_failed');
					$this->isError= TRUE;
				}
				$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'message' => $this->message));
				$this->template('output',$data);
			}
			
		}
		public function get_message()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$get_message=$this->db->query('select m09_msg_id as messageId, m09_msg_title as messageTitle, m09_msg_subject as messageSubject, m09_msg_content as messageContent, m09_msg_status as messageStatus, m09_msg_onadd as messageAdd, m09_msg_sent_from as messageSentfrom from`app_mbtc`.`m09_message_config`')->result();
			$data['jsonData']=json_encode($get_message);
			$this->template('output',$data);
		}
		
		public function add_message()
		{
			$Site_Discription='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{    
				
                foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				
				
				if($this->isError === FALSE)
				{
					if(trim($messageTitle) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_message_title');
						$this->isError = TRUE;
					}
					
					else if(trim($messageSubject) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_message_subject');
						$this->isError = TRUE;
					}
					else if(trim($messageContent) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_message_content');
						$this->isError = TRUE;
					}
					else if(trim($messageStatus) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_message_status');
						$this->isError = TRUE;
					}
					else
					{
						$this->isError = FALSE;
					}
					
				}
				if($this->isError === FALSE)
				{
					if(isset($messageId))
					{
						
						if($this->master_model->update_message_data($messageId,$messageTitle,$messageSubject,$messageContent,$messageStatus))
						{
							$this->message=$this->error_handler->_getStatusMessage('update_success');
							$this->isError = FALSE;
							$this->status = "Success";
						}
						
						else{
							$this->message=$this->error_handler->_getStatusMessage('update_unsuccessful');
							$this->isError = TRUE;
						}
					}
					else {
						
						if($this->master_model->add_message_data($messageTitle,$messageSubject,$messageContent,$messageStatus))
						{
							$this->message=$this->error_handler->_getStatusMessage('insert_success');
							$this->isError = FALSE;
							$this->status = "Success";
						}
						else{
							$this->message=$this->error_handler->_getStatusMessage('insert_unsuccessful');
							$this->isError = TRUE;
						}
						
					}
					
					//$mail_send = $this->sendEmail(EMAIL, $var, $val,'admin_loginotp');
					//$this->send_sms('', $message);
					
				}
				$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
				$this->template('output',$data);
			}
			
		}
		
		
		public function fetch_category()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$fetch_category=$this->db->query("select m01_cat_id as categoryId,m01_cat_name as categoryName,m01_cat_hsncode as categoryHsnCode,m01_cat_variable as categoryIdentifire,m01_cat_content as categoryDescription,m01_cat_sgst as categorySgst,m01_cat_cgst as categoryCgst,m01_cat_igst as categoryIgst,m01_cat_commission as categoryCommission,m01_cat_parent as categoryParent,m01_cat_image as categoryImage,m01_cat_status as categoryStatus from m01_category where m01_cat_status != 3")->result();
			$data['jsonData']=json_encode($fetch_category);
			$this->template('output',$data);
		}
		
		public function deleteCategory()
		{
			$Site_Discription='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
            
			
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			
			
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{
				if($categoryId == '' || $categoryId =='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_category');
					$this->isError = TRUE;
				}
				else
				{ 
					if($this->master_model->deleteCategory($insertArr))
					{
						$this->message=$this->error_handler->_getStatusMessage('insert_success');
						$this->isError = FALSE;
						$this->status = "Success";
					}
					else
					{
						
					}
					
				}
			}
		}
		public function updateseller()
		{
			$Site_Discription='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
            
			
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			if($_SERVER['REQUEST_METHOD'] === 'POST')
			{   
				if($sellerCategory =='' || $sellerCategory == 'NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_category');
					$this->isError = TRUE;
				}
				else if($sellerCompanyName =='' || $sellerCompanyName =='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_companyname');
					$this->isError = TRUE;
				}
				else if($sellerDisplayName =='' || $sellerDisplayName=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_companydisplay');
					$this->isError = TRUE;
				}
				else if($sellerMobileNumber =='' || $sellerMobileNumber=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_mobileno');
					$this->isError = TRUE;
				}
				else if($sellerPrimaryEmail =='' || $sellerPrimaryEmail=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_email');
					$this->isError = TRUE;
				}
				else if($sellerShippingAddress =='' || $sellerShippingAddress=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_address');
					$this->isError = TRUE;
				}
				else if($sellerCity =='' || $sellerCity=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_city');
					$this->isError = TRUE;
				}
				else if($sellerState =='' || $sellerState=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_state');
					$this->isError = TRUE;
				}
				else
				{
					if($this->master_model->__updateseller($insertArr))
					{
                       	$this->message=$this->error_handler->_getStatusMessage('request_updated_success');
						$this->isError = FALSE;
						$this->status = "Success";
					}
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
			$this->template('output',$data);
			
		}
		public function fetch_seller_data()
		{
			$Site_Discription='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
            
			
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			$data['showHtml'] = @$postData['showHtml'];
			$get_seller_data=$this->db->query("SELECT m13_seller_id AS sellerId,
			`m11_user`.`m11_user_id` AS userId,
			`m11_user`.`m11_user_name` AS userName,
			m13_seller_name AS sellerName,
			m13_seller_company_name AS sellerCompanyName,
			m13_seller_company_display_name AS sellerDisplayName,
			m13_seller_mobile_no AS sellerMobileNumber,
			m13_seller_category AS sellerCategory,
			m13_seller_primary_email AS sellerPrimaryEmail, 
			m13_seller_shipping_address AS sellerShippingAddress, 
			m13_seller_city AS sellerCity, 
			m13_seller_pincode AS sellerPincode,
			m13_seller_state AS sellerState,
			m13_seller_gstin AS sellerGstin,
			m13_seller_vat_tin AS sellerVatTin,
			m13_seller_beneficiary_name AS sellerBenificiary,
			m13_seller_current_account_number AS sellerCurrentAcc,
			m13_seller_ifsc AS sellerIfsc,
			m13_seller_branch_name AS sellerBranchName,
			m13_seller_swift_code AS sellerSoftCode,
			m13_seller_company_pan AS sellerCompanyPan,
			m13_seller_status AS sellerStatus,
			m13_seller_subscription AS sellerSubscription,
			m13_seller_onadd_date AS sellerJoinDate
			FROM m13_seller_info 
			LEFT JOIN m11_user ON m13_seller_info.m11_user_id = m11_user.m11_user_id where m13_seller_id ='".$insertArr['sellerId']."'")->result();
			$data['jsonData']=json_encode($get_seller_data);
			$this->template('output',$data);
			
		}
		
		public function fetch_subcategory()
		{
			$Site_Discription='';
			$insertArr = array();
			$DataArr = array();
			$DataArr1 = array();
			$DataArr = $this->_debugMode();
			
			$json = file_get_contents('php://input');
			
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
            
			
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			$data['showHtml'] = @$postData['showHtml'];
			
			$fetch_subcategory=$this->db->query("SELECT 
			m01_cat_id AS categoryId, 
			m01_cat_name AS categoryName
			FROM m01_category
			WHERE m01_cat_parent = (SELECT m13_seller_category FROM m13_seller_info WHERE m13_seller_id = '".$insertArr['sellerId']."')")->result();
			
			$data['jsonData']=json_encode($fetch_subcategory);
			$this->template('output',$data);	
			
		}
		
		public function insert_brand()
		{
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
            
			
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			
			$data['showHtml'] = @$postData['showHtml'];
			
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if($subCategoryName == '' || $subCategoryName == 'NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_category');
					$this->isError = TRUE;
				}
				else if($brandName=='' || $brandName=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('empty_brandname');
					$this->isError = TRUE;
				}
				else if($brandProductCode=='' || $brandProductCode=='NULL')
				{
					$this->message=$this->error_handler->_getStatusMessage('productcode');
					$this->isError = TRUE;
				}
				else
				{
					if($this->master_model->insert_brand($insertArr))
					{
						$this->message=$this->error_handler->_getStatusMessage('empty_productcode');
						$this->isError = FALSE;
						$this->status ='Status';
					}
					
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message, 'userDisbled' => $this->userDisbled));
			$this->template('output',$data);
			
		}
		
		
		public function userOrder()
		{
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			
			$data['result'] = $this->db->get("view_user_token")->result();
			$this->isError = FALSE;
			$this->status = 'success';
			$this->message='Buy Token Receive';
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$data['result'] , 'message' =>$this->message));
			$this->template('output',$data); 
		}
		
		
		
		
		
		
	}/*Class ENDS*/
	
	
	
	
	/* End of file MasterWebService.php */
	/* Location: ./application/controllers/MasterWebService.php */
?>