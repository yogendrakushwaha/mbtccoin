<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class EnrollWebService extends CI_Controller
	{
		/**
			* Index Page for this controller.
			*
			* Maps to the following URL
			* 		http://example.com/index.php/AdminWebService
			*	- or -
			* 		http://example.com/index.php/AdminWebService/index
			*	- or -
			* Since this controller is set as the default controller in
			* config/routes.php, it's displayed at http://example.com/
			*
			* So any other public methods not prefixed with an underscore will
			* map to /index.php/AdminWebService/<method_name>
			*
			*
			*@category 	controller
			*@package 	application_controllers
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
		
		/**
			*Default class constructor
		*/
		
		var $status = "failed";
		var $message = "Invalid Operation";
		private $error_code = 0;
		private $isError = FALSE;
		private $record_per_page =10;
		private $userDisbled = 0;
		public function __construct()
		{
			//load parent class constructor
			parent::__construct();
			$this->load->model('EnrollWebService_model','Enroll');
			$this->load->model('MasterWebService_model','Master');
			$this->load->model('SignupWebService_model','Signup');
			$this->load->helper('functions');
			$this->load->library('error_handler');
			
		}
		
		/**
			*Default action for the admin controller or landing function
			*dated : 2016-12-08
		*/
		public function index()
		{
		}
		/**
			*This function is used to reset the password using forget password link
			*@param string $email
			*@return loaded view
		*/
		public function _debugMode()
		{
			$DataArr = array();
			if(isset($_POST['mode']) && $_POST['mode'] == "debug")
			{
				$this->status = "debug";
				$this->message = "Debug Mode";
				$DataArr = $_POST;
				return $DataArr;
			}
			else{
				return array();
			}
		}
		
		function _stripTags_trim($string)
		{
			return  strip_tags(trim($string));
		}
		
		public function template($template_name, $data = array())
		{
			$this->load->view('webservice/'.$template_name, $data);
		}
		
		/**
			* Method name: generateRandomString
			* @description :  used to generate the random string
			* @param:  int length
			* @return:  string
		*/
		public function generateRandomString($length = 10)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++)
			{
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		
		/**
			* Method name: user_signup
			* @description :  Used to signup user account
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		public function user_regsitration()
		{
			$insertArr = array();
			$val = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['Enroll']);
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			 
			if($this->input->server('REQUEST_METHOD') === "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				
				if($this->isError === FALSE)
				{
					if(trim($Role) == '' && trim($Role)!="-1")
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_role');
						$this->isError = TRUE;
					}
					elseif(trim($FirstName) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_name');
						$this->isError = TRUE;
					}
					elseif(!preg_match ("/^[a-zA-Z ]+$/",$FirstName))
					{
						$this->message = "Please enter only alphabets";
						$this->isError = TRUE;
					}
					else if(strlen($FirstName) > 110)
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_name_length');
						$this->isError = TRUE;
					}
					else if(trim($Email) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_email');
						$this->isError = TRUE;
					}
					else if(!valid_email($Email))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_email');
						$this->isError = TRUE;
					}
					else if($this->Enroll->sg_email_exist($Email,0,'Active'))
					{
						$this->message = $this->error_handler->_getStatusMessage('email_exists');
						$this->isError = TRUE;
					} 
					else if(trim($Contact) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_contactno');
						$this->isError = TRUE;
					}
					else if(!_valid_phone_number($Contact))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_contactno');
						$this->isError = TRUE;
					}
					else if($this->Enroll->sg_contact_exist($Contact,0,'Active'))
					{
						$this->message = $this->error_handler->_getStatusMessage('contactno_exists');
						$this->isError = TRUE;
					} 
					if(!isset($fb_id))
					{
						if(trim($Password) == '')
						{
							$this->message = $this->error_handler->_getStatusMessage('empty_password');
							$this->isError = TRUE;
						}
						else if(strlen($Password) < 6)
						{
							$this->message = $this->error_handler->_getStatusMessage('minlen_password');
							$this->isError = TRUE;
						}
						else if($Password != $RepeatPassword)
						{
							$this->message = $this->error_handler->_getStatusMessage('confirm_password');
							$this->isError = TRUE;
						}
					}
					
					else if(trim($IsProf) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_acctype');
						$this->isError = TRUE;
					}
					elseif(isset($_FILES["img"]) && ($_FILES["img"]['name'] =="") && $_FILES["img"]['error'] >0)
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_image');
						$this->isError = TRUE;
					}
					elseif(isset($_FILES["img"]) && ($_FILES["img"]['name'] !="") && ($this->_fileUploadValidation($_FILES["img"]['name']) == FALSE))
					{
						$this->message = $this->error_handler->_getStatusMessage('invalid_filetype');
						$this->isError = TRUE;
					}
				}
				if($this->isError === FALSE)
				{
					unset($insertArr['RepeatPassword']);
					$fileName="";
					if (!empty($_FILES)) 
					{
						$folder_name="user_images";
						$imagePath = "uploads/".$folder_name."/";
						if(!is_dir($imagePath))
						{
							if(!mkdir($imagePath,0777,true))
							{
								$response = Array(
								"error"=>'error',
								"status"=>'failed',
								"message"=>'Permission denied to create folder'
								);
								print json_encode($response);
								return;
							}
						}
						
						$maxSize = "2097152";
						$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
						$temp = explode(".", $_FILES["img"]["name"][0]);
						$extension = end($temp);
						$mimeType = array("image/png","image/jpeg","image/gif");
						$fileMime = trim($_FILES['img']['type'][0]);
						
						//Check write Access to Directory

						if(!is_writable($imagePath))
						{
							$response = Array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Can`t upload File; no write Access'
							);
							print json_encode($response);
							return;
						}
						if ( in_array($extension, $allowedExts))
						{
							$check = getimagesize($_FILES["img"]["tmp_name"][0]);
							$imagewidth = $check[0];
							$imageheight = $check[1];
							
							if ($_FILES["img"]["error"][0] > 0)
							{
								$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'ERROR Return Code: '. $_FILES["img"]["error"][0],
								);
							}
							elseif($_FILES["img"]["size"][0] <= 0 || $_FILES["img"]["size"][0] > $maxSize)
							{
								$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Maybe '.$_FILES["img"]["name"][0]. ' exceeds max 2 MB size',
								);
								}elseif(!in_array($fileMime, $mimeType)){
								$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Image type not supported',
								);
							}
							else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
							{
								$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
								);
							}
							else
							{
								$temp_filename = $_FILES["img"]["tmp_name"][0];
								list($width, $height) = getimagesize( $temp_filename );
								
								$fileName = $_FILES["img"]["name"][0];
								$fileName = str_replace(" ","-",$fileName);
								$fileName = str_replace("%","",$fileName);
								$fileName = str_replace(",","",$fileName);
								$fileName = str_replace("'","",$fileName);
								$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).'_'.$fileName;
								
								move_uploaded_file($temp_filename,  $imagePath .$fileName );
								$targetFile = str_replace('//','/',$imagePath).$fileName;
								$info = pathinfo( $imagePath .$fileName);
								$getimg = getimagesize( $imagePath .$fileName);
								//list($width, $height, $type, $attr) = getimagesize($targetPath.$imgName);
								$width 		= $getimg[0];
								$height 	= $getimg[1];
								$type 		= $getimg[2];
								$attr 		= $getimg[3];
								if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG')
								$img = imagecreatefromjpeg( "{$targetFile}" );
								if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
								$img = imagecreatefromgif( "{$targetFile}" );
								if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
								$img = imagecreatefrompng( "{$targetFile}" );
								if($thumbSize)
								$thumbWidth = 140;
								else
								$thumbWidth = 60;
								if( $thumbWidth )
								{
									############## code for thumb ################
									if($width < $thumbWidth)
									$thumbWidth = $width;
									
									$width = imagesx( $img );
									$height = imagesy( $img );
									$new_height = floor( $height * ( $thumbWidth / $width ) );
									$new_width = $thumbWidth;
									
									$tmp_img = imagecreatetruecolor( $new_width, $new_height );
									imagealphablending($tmp_img, false);
									imagesavealpha($tmp_img,true);
									$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
									imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
									$targetFile1 =  str_replace('//','/',$imagePath). 'thumb_' . $fileName;
									
									if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
									imagejpeg( $tmp_img, "{$targetFile1}" );
									if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
									imagegif( $tmp_img, "{$targetFile1}" );
									if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
									imagepng( $tmp_img, "{$targetFile1}" );
								}
								$response = array(
								"error" => 'success',
								"status" => 'success',
								"url" => base_url().$imagePath.$fileName,
								"image"=>$fileName,
								"width" => $width,
								"height" => $height
								);
								
							}
						}
						else
						{
							$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Only jpg,jpeg,png and gif image is allowed.',
							);
						}
					}
					$insertArr['Image_name'] = $fileName ;
					$insertArr['City'] = "N/A" ;
					if(!isset($fb_id))
					{
						$insertArr['Password'] 	= md5($insertArr['Password']);
					}
					$insertArr['Created_on'] 		= date('Y-m-d H:i:s');
					$insertArr['Membership_id'] 	= 1;
					$insertArr['New_login_user'] 	= 'Yes';
					//$insertArr['gender'] = $gender;
					if($IsProf == 'Yes')
					{
						$insertArr['User_type'] 			 = $insertArr['Role'];
						$insertArr['Subscription']			 = 'Active';
						$insertArr['Cancellation_policy']	 = '1';
					}
					else
					{
						$insertArr['User_type'] 			= $insertArr['Role'];
						$insertArr['Subscription'] 			= 'None';
						$insertArr['Cancellation_policy']	= '0' ;
					}
					
					if(isset($Is_verified) && $Is_verified == 'Yes')
					{
						$insertArr['Status'] = 'Active';
						// $link 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';	
					}
					else
					{
						if(trim($Status)!= '' && trim($Status)!= 'Pending' && trim($Status)!= 'InActive')
						{
							$insertArr['Status'] = trim($Status);
						}
						else
						{
							$insertArr['Status'] = 'Pending';
						}
					}
					unset($insertArr['Is_verified']);
					$insertID 		= $this->Enroll->create_user($insertArr);
					$DataArr   		= $this->Enroll->_getUser_by_email($Email);
					$generated_code = random_string('alnum', 16);
					$updateArray 	= array(
					'activate_code' => $generated_code,
					);
					$link = site_url('activateaccount/'.base64_encode($DataArr['m11_user_id'].'/'.$generated_code));
					$link 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';
					//$var = array('{name}', '{link}');
					$var = array('{name}', '{useremail}','{password}');
					//$val = array($postData['FirstName'].' '.$postData['LastName'], $link);
					$val = array($postData['FirstName'].' '.$postData['LastName'], $postData['Contact'],$postData['Password']);
					if($insertID)
					{
						if($DataArr['m11_user_status'] == 'ACTIVE')
						{
							 //$adminEmail		=  $this->Master->_getConfigurationByKey('EMAIL');
							 $mail_send 		= $this->sendEmail($postData['Email'], $var, $val,'account_creation');
							 $adminmailsend 	= $this->sendEmail(EMAIL,$var, $val,'account_creation');
                             $msg="Welcome to UNIQUE e BAZAAR,You account  has been create successfully. Please login to your account with the provided credentials.Login MobileNo :".$postData['Contact']." Login password : ".$postData['Password']." .";
							 $this->send_sms($postData['Contact'], $msg);
							$this->message 	= $this->error_handler->_getStatusMessage('success');
						}
						else
						{
							$this->message = 'Login successfully';
						}
						$this->status = "success";
					} 
				}
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $val, 'message' => $this->message));
			$this->template('output',$data);
		}
		
		
		/**
			* Method name: edit_user
			* @description :  Used to edit user data
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		public function edit_user()
		{
			$insertArr = array();
			$val = array();
			$DataArr = array();
			$data['showHtml'] = @$postData['showHtml']; 
		    $thumbSize=140;
			$postData = (array)json_decode($_POST['UserData']);
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
	
			//$postData = (array)json_decode($_POST['UserData']);
	     	//print_r($postData);
			 		
			if($this->input->server('REQUEST_METHOD')=== "POST")
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
				$updated = $this->Enroll->create_user($insertArr);
				if($updated)
				{
				$this->isError=FALSE;
				$this->status="success";
				$this->message="User Details has been updated succesfully";
				}
				
			}
				$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>$this->message, 'message' => $this->message));
				$this->template('output',$data);
		}
		
		/**
			* Method name: getUserByID
			* @description :  Get User by user id
			* @param:  Request data
			* @return:  user detail array with success message
		*/
		
		public function getUserByID($id=NULL)
		{
			$data['showHtml'] = @$postData['showHtml'];
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);		
			$UserId = (!empty($postData)) ? $postData['id'] : $id;
			if($UserId){
			$user = $this->Enroll->getUserByID($UserId);
			$data['jsonData'] = json_encode(array('error' => 'success','status' => 'success','userDATA'=>$user));
			$this->template('output',$data);
			} 
			
		}
		
		public function enrollreport()
		{
			/* if ($this->session->userdata('loggedin') == FALSE) {
				redirect('admin');
			} */
			$insertArr = array();
			$data['title'] ="User Management";
			//$data['users_data'] = $this->admin_model->getAllUsers('', 'All');
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);
			//if($this->input->server('REQUEST_METHOD') === "POST" )
			//{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			$whereClause="";
			$limit='All';
			if($insertArr['id']!="4")
				$UserId="'`m11_user`.`m11_user_role`=".(int)$insertArr['id']."'";
				else 
				$UserId="'`m11_user`.`m11_user_role`=5'";
			$user_data = $this->Enroll->_getUser_by_desig($UserId);
			$json_data=$user_data;
			
			//}
			$data['jsonData'] = json_encode(array('error' => 'success','status' => 'success','userDATA'=>$json_data));
			$this->template('output',$data);
		}
		public function get_team()
  {  
   $teamdata='';
   $insertArr = array();
   $json = file_get_contents('php://input');
   $postData = (array)json_decode($json);
   $data['showHtml'] = @$postData['showHtml'];
   unset($postData['showHtml']);
   unset($postData['submit']);
   unset($postData['mode']);
   if($this->input->server('REQUEST_METHOD') === "POST" )
   {
    foreach($postData as $key => $post)
    {
     $$key = self::_stripTags_trim($post);
     $insertArr[$key] = self::_stripTags_trim($post);
    } 
     if($postData['MemberId']){
     $query=$this->db->query("CALL enrolluser(1,'`m11_user`.`m11_user_referral_code`=".$postData['MemberId']."',@msg)");  
     mysqli_next_result( $this->db->conn_id ); 
     $teamdata = $query->result();
     $this->error = FALSE;
     }

   }
   $data['jsonData'] = json_encode(array('error' => $this->error,'status' => 'success','userDATA'=>$teamdata));
   $this->template('output',$data);
  }


 /**
		    * Method name: wallet_updateamount
			* @description :  Used to Update Capping Amount and Maintain Wallet 
			* @param       :  string $memberid,$amount
			* @return      :  array
		**/
		// Capping Amount and Maintain Wallet Update
		public function capping_updateamount()
		{
			$insertArr = array();
			$data['title'] ="Capping Amount and Maintain Wallet Update";
			$json = file_get_contents('php://input');
			$postData = (array)json_decode($json);
			$data['showHtml'] = @$postData['showHtml'];
			unset($postData['showHtml']);
			unset($postData['submit']);
			unset($postData['mode']);

			if($this->input->server('REQUEST_METHOD') === "POST" )
			{
				foreach($postData as $key => $post)
				{
					$$key = self::_stripTags_trim($post);
					$insertArr[$key] = self::_stripTags_trim($post);
				}
					if($insertArr['STATUS']=="CAPPING")
					{
						$status1="CAPPING";
						$status2=2;
					}
					if($insertArr['STATUS']=="MAINTAIN")
					{
						$status1="MAINTAIN WALLET";
						$status2=3;
					}
					if($insertArr['RegId']=="")
					{
						$this->message = 'Invalid Id';
						$this->isError = TRUE;
					}
					if($insertArr['MAINTAIN']=='' || trim(floatval($insertArr['MAINTAIN']))<0.00)
					{
						$this->message = 'Invalid Maintain Wallet';
						$this->isError = TRUE;
					}
					if($insertArr['CAPPING1']=='' || trim(floatval($insertArr['CAPPING1']))<0.00)
					{
						$this->message = 'Invalid Recharge Capping Amount';
						$this->isError = TRUE;
					}
					if($insertArr['CAPPING2']=='' || trim(floatval($insertArr['CAPPING2']))<0.00)
					{
						$this->message = 'Invalid Walllet Capping Amount';
						$this->isError = TRUE;
					}
			if($this->isError === FALSE)
		    {
					$capping=array
					(
						'proc'=>1,
						'id'=>$insertArr['RegId'],
						'maintainwallet'=>$insertArr['MAINTAIN'],
						'capping1'=>$insertArr['CAPPING1'],
						'capping2'=>$insertArr['CAPPING2'],
						'amt_for'=>$status1
			        );
				$query = " CALL update_capping(?" . str_repeat(",?", count($capping)-1) .",@a,@a1) ";
			    $data['rec']=$this->db->query($query, $capping);
				mysqli_more_results( $this->db->conn_id );
		        $data['response']=$this->db->query("SELECT @a as message")->row()->message;
				$data['status']=$this->db->query("SELECT @a1 as message1")->row()->message1;
				$this->message = $data['response'];
				$this->status = $data['status'];
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data); 
			}
		}



		/**
			* Method name: _send_email
			* @description :  used to send emails
			* @param:  string $email, $string $name, string $link,string $mail_obj
			* @return:  array
		*/
		public function _send_email($email, $name, $link,$mail_obj)
		{
			$adminEmail =  $this->Master->_getConfigurationByKey('signup_email');
			$msg = $this->Master->_getMessage($mail_obj);
			
			$message  	= html_entity_decode($msg->content);
			$subject 	= html_entity_decode($msg->subject);
			
			$patternFind1[0] 	= '/{name}/';
			$patternFind1[1] 	= '/{link}/';
			
			$replaceFind1[0] 	= $name;
			$replaceFind1[1] 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';
			
			$txtdesc_contact	= stripslashes($message);
			$contact_sub		= stripslashes($subject);
			$contact_sub		= preg_replace($patternFind1, $replaceFind1, $contact_sub);
			$ebody_contact 		= preg_replace($patternFind1, $replaceFind1, $txtdesc_contact);
			
			try
			{
				$email_config = array(
				'protocol'  => 'smtp',
				'smtp_host' => $this->Master->_getConfigurationByKey('smtp_server_host'),
				'smtp_port' => $this->Master->_getConfigurationByKey('smtp_port_number'),
				'smtp_user' => $this->Master->_getConfigurationByKey('smtp_uName'),
				'smtp_pass' => $this->Master->_getConfigurationByKey('smtp_uPass'),
				'mailtype'  => 'html',
				'starttls'  => true,
				'newline'   => "\r\n"
				);
				$contact_sub = '['.SITE_NAME.']  '. $contact_sub;
				$this->load->library('email', $email_config);
				$this->email->initialize($email_config);
				$this->email->from($adminEmail, SITE_NAME);
				$this->email->to($email);
				$this->email->subject($contact_sub);
				$this->email->message($ebody_contact);
				if($this->email->send()){
					return 1;
					}else{
					return 0;
				}
				}catch(Exception $ex){
				return 0;
			}
		}
		
		private function sendEmail($mailTo, $var, $val, $mailShortCode)
		{
			//=======================
			$this->load->library('phpmailer');
			$this->load->library('smtp');
			
			//$this->phpmailer->IsSMTP();
			$this->phpmailer->SMTPAuth = TRUE; // enable SMTP authentication
			$this->phpmailer->SMTPSecure = "ssl"; // sets the prefix to the servier
			$this->phpmailer->Host = $this->Signup->_getConfigurationByKey('smtp_server_host'); // sets GMAIL as the SMTP server
			$this->phpmailer->Port = $this->Signup->_getConfigurationByKey('smtp_port_number'); // set the SMTP port
			
			$this->phpmailer->Username = $this->Signup->_getConfigurationByKey('smtp_uName'); // GMAIL username
			$this->phpmailer->Password = $this->Signup->_getConfigurationByKey('smtp_uPass'); // GMAIL password
			
			$this->phpmailer->From = $this->Signup->_getConfigurationByKey('smtp_user');
			$this->phpmailer->FromName = $this->Signup->_getConfigurationByKey('smtp_user');
			//$this->phpmailer->setFrom($mail_from, SITE_NAME);
			$mail_from = $this->Signup->_getConfigurationByKey('signup_email');
			$this->phpmailer->setFrom($mail_from, 'UniqueeBazaar');
			//=========
			$mail_content = $this->Signup->_getMessage($mailShortCode);
			
			$message  	= html_entity_decode($mail_content->m09_msg_content);
			$subject 	= html_entity_decode($mail_content->m09_msg_subject);
			
			$mail_content = str_replace($var, $val, $message);
			$mail_body = html_entity_decode($mail_content);
			
			// $sign = $this->Signup->_getConfigurationByKey('EMAIL_SIGN');
			
			//$mail_body .= '<br />' . $sign;
			//==================
			
			
			$this->phpmailer->Subject = $subject;
			$this->phpmailer->Body = $mail_body; //HTML Body
			
			$this->phpmailer->clearAddresses();
			$this->phpmailer->AddAddress(strtolower($mailTo));
			$this->phpmailer->IsHTML(true); // send as HTML
			
			if(!$this->phpmailer->Send()) {
				//  echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
				//echo "\n\n";
				return false;
				exit;
				} else {
				echo " Message has been sent\n\n";
				return true;
			}
			//==========================================================
		}
		
		private function sendEmails($mailTo, $var, $val, $mailShortCode)
		{
			//=======================
			$this->load->library('phpmailer');
			$this->load->library('smtp');
			
			$this->phpmailer->IsSMTP();
			$this->phpmailer->SMTPAuth = TRUE; // enable SMTP authentication
			$this->phpmailer->SMTPSecure = "ssl"; // sets the prefix to the servier
			$this->phpmailer->Host = $this->Master->_getConfigurationByKey('smtp_server_host'); // sets GMAIL as the SMTP server
			$this->phpmailer->Port = $this->Master->_getConfigurationByKey('smtp_port_number'); // set the SMTP port
			
			$this->phpmailer->Username = $this->Master->_getConfigurationByKey('smtp_uName'); // GMAIL username
			$this->phpmailer->Password = $this->Master->_getConfigurationByKey('smtp_uPass'); // GMAIL password
			
			$this->phpmailer->From = $this->Master->_getConfigurationByKey('smtp_user');
			$this->phpmailer->FromName = $this->Master->_getConfigurationByKey('smtp_uName');
			
			//=========
			$mail_content = $this->Master->_getMessage($mailShortCode);
			
			$message  	= html_entity_decode($mail_content->content);
			$subject 	= html_entity_decode($mail_content->subject);
			
			$mail_content = str_replace($var, $val, $message);
			$mail_body = html_entity_decode($mail_content);
			
			// $sign = $this->admin_model->_getConfigurationByKey('EMAIL_SIGN');
			
			//$mail_body .= '<br />' . $sign;
			//==================
			
			
			$this->phpmailer->Subject = $subject;
			$this->phpmailer->Body = $mail_body; //HTML Body
			
			$this->phpmailer->clearAddresses();
			$this->phpmailer->AddAddress($mailTo);
			//$this->phpmailer->AddReplyTo($this->configModel->getConfig('SMTP_USER'),$this->configModel->getConfig('EMAIL_FROM'));
			$this->phpmailer->IsHTML(true); // send as HTML
			
			if(!$this->phpmailer->Send()) {
				//echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
				//echo "\n\n";
				//exit;
				} else {
				//echo "Gmail Message has been sent\n\n";
			}
			//==========================================================
		}
		
		public function send_Email($mailTo, $var, $val, $mailShortCode)
		{
			$this->load->library('phpmailer');
			$this->load->library('smtp');
			
			//$mail=new PHPMailer();
			
			$this->phpmailer->IsSMTP();
			$this->phpmailer->SMTPAuth = TRUE; // enable SMTP authentication
			$this->phpmailer->SMTPSecure = "ssl"; // sets the prefix to the servier
			//$mail->phpmailer->SMTPSecure = 'tls';
			$this->phpmailer->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
			$this->phpmailer->Port = 465; // set the SMTP port
			
			$this->phpmailer->Username = "appworks1302@gmail.com"; // GMAIL username
			$this->phpmailer->Password = "VAPlucknow1302@"; // GMAIL password
			
			$this->phpmailer->From = "appworks1302@gmail.com";
			$this->phpmailer->FromName = "APPWORKS";
			
			$mail_content = $this->Master->_getMessage($mailShortCode);
			
			$message  	= html_entity_decode($mail_content->content);
			$subject 	= html_entity_decode($mail_content->subject);
			
			$mail_content = str_replace($var, $val, $message);
			$mail_body = html_entity_decode($mail_content);
			
			// $sign = $this->admin_model->_getConfigurationByKey('EMAIL_SIGN');
			
			//$mail_body .= '<br />' . $sign;
			//==================
			
			
			$this->phpmailer->Subject = $subject;
			$this->phpmailer->Body = $mail_body; //HTML Body
			$this->phpmailer->AltBody = "Message"; //Text Body
			
			$this->phpmailer->WordWrap = 50; // set word wrap
			$this->phpmailer->clearAddresses();
			$this->phpmailer->AddAddress($mailTo);
			$this->phpmailer->AddAddress("contact@appworkstech.com");
			$this->phpmailer->AddReplyTo("contact@appworkstech.com","Webmaster");
			#$mail->AddAttachment("/path/to/file.zip"); // attachment
			#$mail->AddAttachment("/path/to/image.jpg", "new.jpg"); // attachment
			$this->phpmailer->IsHTML(true); // send as HTML
			
			if(!$this->phpmailer->Send()) {
				echo "Mailer Error: " . $this->phpmailer->ErrorInfo;
				echo "\n\n";
				exit;
				} else {
				echo "Gmail Message has been sent\n\n";
			}
		}

public function send_sms($mob,$msg)
		{
					$url="http://w2s.in/SendSMSAPI.aspx";
					$params = array (
					'mo'=>urlencode($mob),
					'ms'=>$msg,
					'r'=>urlencode(1),
					's'=>urlencode('PROMOT'),
					'u'=>urlencode('20039'),
					'p'=>urlencode('angel')
					);
					$options = array(
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0
					);
					
					$defaults = array(
					CURLOPT_URL => $url. (strpos($url, '?') 
					=== FALSE ? '?' : ''). http_build_query($params),
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_TIMEOUT =>56
					);
					
					$ch = curl_init();
					curl_setopt_array($ch, ($options + $defaults));
					$result = curl_exec($ch);
					if(!$result)
					{
						trigger_error(curl_error($ch));
						$flag=0;
					}
					else
					{	                
						$flag=1;
					}
					curl_close($ch);
		}
		
		public function check_sms()
		{
			
			$msg="Thank You for getting connected with our service. Your UID:9721789285,Pass:123,Tran.Pass:123";
			$mob='7080552897';
			$this->send_sms($mob,$msg);
		}



public function update_mobileno()
			{

					$insertArr = array();
					$val = array();
					$DataArr = array();
					 
				    //$thumbSize=140;
					$json = file_get_contents('php://input');
			        $postData1 = (array)json_decode($json);
					$postData = $postData1['Mobile'];

					$data['showHtml'] = @$postData1['showHtml'];
					unset($postData1['showHtml']);
					unset($postData1['submit']);
					unset($postData1['mode']);
					
					//print_r($postData);
					

					if($this->input->server('REQUEST_METHOD') === "POST")
					{
						foreach($postData as $key => $post)
						{
							$$key = self::_stripTags_trim($post);
							$insertArr[$key] = self::_stripTags_trim($post);
						}
						if($this->isError === FALSE)
						{
							if(trim($MemberId) == '' && trim($MemberId)!="-1")
							{
								$this->message = $this->error_handler->_getStatusMessage('empty_id');
								$this->isError = TRUE;
							}
							elseif(trim($OldMobile) == '')
							{
								$this->message = $this->error_handler->_getStatusMessage('empty_mobile');
								$this->isError = TRUE;
							}	
							else if(trim($NewMobile) == '')
							{
								$this->message = $this->error_handler->_getStatusMessage('empty_mobile');
								$this->isError = TRUE;
							}
							else if(!_valid_phone_number($NewMobile))
							{
								$this->message = $this->error_handler->_getStatusMessage('short_mobile');
								$this->isError = TRUE;
							}
							else if($this->Enroll->sg_contact_exist($NewMobile,0,'Active'))
							{
							$this->message = $this->error_handler->_getStatusMessage('contactno_exists');
							$this->isError = TRUE;
							} 
						}
						if($this->isError === FALSE)
						{
							if(!empty($insertArr))
							{
								$insertMobile = $this->Enroll->change_mobile($insertArr);
								$this->message="MOBILE NO UPDATED";
								$this->status = "success";
							} 

						}
					}
					$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $insertArr, 'message' => $this->message));
					$this->template('output',$data);
				}










		public function update_introducer()
			{

					$insertArr = array();
					$val = array();
					$DataArr = array();
					 
				    //$thumbSize=140;
					$json = file_get_contents('php://input');
			        $postData1 = (array)json_decode($json);
					$postData = $postData1['Introducer'];
					
					$data['showHtml'] = @$postData1['showHtml'];
					unset($postData1['showHtml']);
					unset($postData1['submit']);
					unset($postData1['mode']);
					
					//print_r($postData);
					

					if($this->input->server('REQUEST_METHOD') === "POST")
					{
						foreach($postData as $key => $post)
						{
							$$key = self::_stripTags_trim($post);
							$insertArr[$key] = self::_stripTags_trim($post);
						}
						if($this->isError === FALSE)
						{
							if(trim($MemberId) == '' && trim($MemberId)!="-1")
							{
								$this->message = $this->error_handler->_getStatusMessage('empty_id');
								$this->isError = TRUE;
							}
							elseif(trim($NewIntroducer) == '')
							{
								$this->message = $this->error_handler->_getStatusMessage('introducer_error');
								$this->isError = TRUE;
							}
						}
						if($this->isError === FALSE)
						{
							
							if(!empty($insertArr))
							{
								$insertIntroducer = $this->Enroll->change_introducer($insertArr);
								$this->message="INTRODUCER UPDATED";
								$this->status = "success";
							} 

						}
					}
					$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $insertArr, 'message' => $this->message));
					$this->template('output',$data);
				}
	}
	
	/* End of file Admin.php */
	/* Location: ./application/controllers/Admin.php */
?>