<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserprofileWebService extends CI_Controller
{
	/**
			* Index Page for this controller.
			*
			* Maps to the following URL
			*       http://example.com/index.php/SignupWebService
			*	- or -
			* 		http://example.com/index.php/SignupWebService/index
			**	- or -
			* Since this controller is set as the default controller in
			* config/routes.php, it's displayed at http://example.com/
			*
			* So any other public methods not prefixed with an underscore will
			* map to /index.php/LoginWebService/<method_name>
			*
			*
			*@category 	controller
			*@package 	application_controllers
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
	var $status = "failed";
	var $message = "Invalid Operation";
	private $error_code = 0;
	private $TOKEN="";
	private $isError = FALSE;
	private $record_per_page =10;
	private $userDisbled = 0;
	/**
			*Class default constructor
	**/
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('UserprofileWebService_model','Userprofile');
		$this->load->model('SignupWebService_model','Signup');
		$this->load->helper('functions');
		$this->load->library('error_handler');
		$this->load->library('JWT');
		$this->headers=apache_request_headers();
		if(isset($this->headers["Authorization"]))
		{
						$token=explode(" ", $this->headers["Authorization"]);
						$user= $this->jwt->decode(trim($token[1],'"'),KEY);
					    if($this->Userprofile->sg_email_exist($user->EMAIL,0,'Active'))
						{
									$sessiondata=array(
										'CONTACTNO'=>$user->CONTACTNO,
										'EMAIL'  => $user->EMAIL,
										'IWT'=>time(),
										'EXP'=>time()+20
										
									);
									$this->TOKEN = $this->jwt->encode($sessiondata,KEY);
						
						} 
						else
						{
						
						$this->isError=TRUE;
						$this->status='failed';
							$data['jsonData'] = json_encode(
								array(
									'error' => $this->isError,
									'status' => $this->status,
									'userDATA'=>"Unauthorized Access",
									'token'=>$this->TOKEN ,
								    'message' => $this->message));
						    $this->template('output',$data);
							die($data['jsonData']);	

						}
	}
	else
	{

		$this->isError=TRUE;
		$this->status='failed';
			$data['jsonData'] = json_encode(
				array(
					'error' => $this->isError,
					'status' => $this->status,
					'userDATA'=>"Unauthorized Access",
					'token'=>$this->TOKEN ,
				    'message' => $this->message));
		    $this->template('output',$data);
			die($data['jsonData']);	
	}
	
	}
	
	/**
			* Method name: _debugMode
			* @description :  used for debug the posted data
			* @param:  Request data
			* @return:  array
		*/
	public function _debugMode()
	{
		$DataArr = array();
		if(isset($_POST['mode']) && $_POST['mode'] == "debug")
		{
			$this->status = "debug";
			$this->message = "Debug Mode";
			$DataArr = $_POST;
			return $DataArr;
		}
		else{
			return array();
		}
	}

	function _stripTags_trim($string)
	{
		return  strip_tags(trim($string));
	}

	public function template($template_name, $data = array())
	{
		$this->load->view('webservice/'.$template_name, $data);
	}


	/**
			* Method name	: get_PersonalDetail
			* @description 	: Used to get Personal Detail data  
			* @param		: Request data  
			* @return		: Get Personal Detail data array with success message
		*/
	public function get_PersonalDetail()
	{
		$insertArr = array();
		$DataArr = array();
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);;
		if($this->input->server('REQUEST_METHOD') === "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}

			if(trim($insertArr['Mobileno']) == '')
			{
				$this->message = 'Please enter User id';
				$this->isError = TRUE;
			}
			$user_data = $this->Userprofile->_getUser_by_mobileno($Mobileno);
			if(empty($user_data->RegId))
			{
				$this->message = 'Please enter valid User id';
				$this->isError = TRUE;
			}		
			if($this->isError === FALSE)
			{				
				$DataArr=$user_data;
				$this->message = 'User details found';
				$this->status='success';
			}
			else
			{
				$DataArr="";
				$this->message = 'No Found';
				$this->status='failed';
			}
		}
		$data['jsonData'] = json_encode(array('error' => $this->isError,'status' => $this->status, 'userDATA'=> $DataArr, 'token'=>$this->TOKEN ,'message' => $this->message));
		$this->template('output',$data);
	} 


	/**
			* Method name 	: dashboard
			* @description 	: Used to signup user account
			* @param		: Request data for 
			* @return		: user detail array with success message.
		*/
	public function dashboard()
	{
		$insertArr = array();
		$DataArr = array();
		$image = $image_name = '';
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);

		//$postData = $this->input->post();
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);

		if($this->input->server('REQUEST_METHOD') === "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			if($this->isError === FALSE)
			{ 
			}
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message));
		$this->template('output',$data);
	}

	/**
			* Method name	: get_Allblog
			* @description 	: Used to get all blog data  
			* @param		: Request data  
			* @return		: Get All Blog data array with success message
		*/
	public function get_Allblog()
	{
		$insertArr = array();
		$DataArr = array();
		$image = $image_name = '';
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);

		//$postData = $this->input->post();
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);

		if($this->input->server('REQUEST_METHOD') === "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			if($this->isError === FALSE)
			{ 
			}
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message));
		$this->template('output',$data);
	} 

	/**
			* Method name	: get_StaticContent
			* @description 	: Used to get static content data  
			* @param		: Request data  
			* @return		: Get Static Content data array with success message
		*/
	public function get_StaticContent()
	{
		$insertArr = array();
		$DataArr = array();
		$DataArr = $this->_debugMode();
		$this->input->get('variable_name');
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);
		if($this->input->server('REQUEST_METHOD') === "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}

			if($this->isError === FALSE)
			{
				$DataArr = $this->Userprofile->getSiteStaticContent($insertArr['variable']);
				$this->status = 'success';
				$this->message = $this->error_handler->_getStatusMessage('contact_success');
				/*if($DataArr >0)
						{
						$this->status = 'success';
						$this->message = $this->error_handler->_getStatusMessage('contact_success');
						}
						else
						{
						$this->message = $this->error_handler->_getStatusMessage('contact_error');
					}*/
			}
		}
		$data['jsonData'] = json_encode(array('status' => $this->status, 'data'=> $DataArr, 'message' => $this->message));
		$this->template('output',$data);
	}

	/**
			* Method name	: get_AllFAQ
			* @description 	: Used to get FAQ data  
			* @param		: Request data  
			* @return		: Get FAQ data array with success message
		*/
	public function get_AllFAQ()
	{
		$insertArr = array();
		$DataArr = array();
		$image = $image_name = '';
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);

		//$postData = $this->input->post();
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);

		if($this->input->server('REQUEST_METHOD') === "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			if($this->isError === FALSE)
			{ 
			}
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message));
		$this->template('output',$data);
	} 

	/**
			* Method name	: get_TermOfUse
			* @description 	: Used to get Term Of Use data  
			* @param		: Request data  
			* @return		: Get Term Of Use data array with success message
		*/
	public function get_TermOfUse()
	{
		$content = $this->Userprofile->getSiteStaticContent('terms_conditions'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}
	} 


	/**
			* Method name	: getComingSoondata
			* @description 	: Used to get Coming Soon data  
			* @param		: Request data  
			* @return		: Get Term Of Use data array with success message
		*/
	public function getComingSoondata()
	{
		$content = $this->Userprofile->getSiteStaticContent('coming_soon'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}
	} 

	/**
			* Method name	: get_PrivacyPolicy
			* @description 	: Used to get Privacy Policy data  
			* @param		: Request data  
			* @return		: Get Privacy Policy data array with success message
		*/
	public function get_PrivacyPolicy()
	{
		$content = $this->Userprofile->getSiteStaticContent('privacy_policy'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}
	} 


	/**
			* Method name: user_signup
			* @description :  Used to signup user account
			* @param:   Request data
			* @return:  user detail array with success message
		*/
	public function user_signup()
	{
		$insertArr = array();
		$DataArr = array();
		$image = $image_name = '';
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);

		//$postData = $this->input->post();
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);

		if($this->input->server('REQUEST_METHOD') === "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}

			if($this->isError === FALSE)
			{
				if(trim($Fullname) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name');
					$this->isError = TRUE;
				}
				elseif(!preg_match ("/^[a-zA-Z ]+$/",$Fullname))
				{
					$this->message = "Please enter only alphabets";
					$this->isError = TRUE;
				}
				else if(strlen($Fullname) > 110)
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name_length');
					$this->isError = TRUE;
				}
				else if(trim($Email) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_email');
					$this->isError = TRUE;
				}
				else if(!valid_email($Email))
				{
					$this->message = $this->error_handler->_getStatusMessage('invalid_email');
					$this->isError = TRUE;
				}
				else if($this->Signup->sg_email_exist($Email,0,'Pending'))
				{
					$this->message = $this->error_handler->_getStatusMessage('email_exists');
					$this->isError = TRUE;
				} 

				if(!isset($fb_id))
				{
					if(trim($Password) == '')
					{
						$this->message = $this->error_handler->_getStatusMessage('empty_password');
						$this->isError = TRUE;
					}
					else if(strlen($Password) < 6)
					{
						$this->message = $this->error_handler->_getStatusMessage('minlen_password');
						$this->isError = TRUE;
					}
					else if($Password != $Passwordrepeat)
					{
						$this->message = $this->error_handler->_getStatusMessage('confirm_password');
						$this->isError = TRUE;
					}
				}

				else if(trim($IsProf) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_acctype');
					$this->isError = TRUE;
				}
				/* elseif(isset($_FILES['image_name']) && ($_FILES['image_name']['name'] =="") && $_FILES['image_name']['error'] >0)
						{
						$this->message = $this->error_handler->_getStatusMessage('empty_image');
						$this->isError = TRUE;
						}
						elseif(isset($_FILES['image_name']) && ($_FILES['image_name']['name'] !="") && ($this->_fileUploadValidation($_FILES['image_name']['name']) == FALSE))
						{
						$this->message = $this->error_handler->_getStatusMessage('invalid_filetype');
						$this->isError = TRUE;
					} */
			}
			if($this->isError === FALSE)
			{
				unset($insertArr['Passwordrepeat']);
				/* if(isset($_FILES['image_name']) && $_FILES['image_name']['name'] !="" && $_FILES['image_name']['error'] ==0)
						{
						$image = $_FILES['image_name']['name'];
						$tmp_path = $_FILES['image_name']['tmp_name'];
						$image_name = uploadImage($image, $tmp_path,'users_profile');
						//create thumbnail
						$this->load->library('upload');
						$this->load->library('image_lib');

						$source_imagepath='./uploads/users_profile/'.$image_name;

						$thumbimage_name="thumb_".$image_name;
						$dest_imagepath='./uploads/users_profile/'.$thumbimage_name;
						$config1['image_library'] = 'gd2';
						$config1['source_image']	= $source_imagepath;
						$user_image_size=$this->config->item('user_thumbimage_size');
						$user_image_size=$user_image_size[0];
						$config1['maintain_ratio'] = TRUE;
						$config1['width']	= $user_image_size[0];
						//$config1['height']	= $user_image_size[1];
						$config1['new_image']   = $dest_imagepath;

						$this->image_lib->initialize($config1);
						$this->image_lib->resize();
						$this->image_lib->clear();
					} */
				$insertArr['Image_name'] = $image_name ;
				if(!isset($fb_id))
				{
					$insertArr['Password'] 	= md5($insertArr['Password']);
				}
				$insertArr['Created_on'] 		= date('Y-m-d H:i:s');
				$insertArr['Membership_id'] 	= 1;
				$insertArr['New_login_user'] 	= 'Yes';
				//$insertArr['gender'] = $gender;
				if($IsProf == 'Yes')
				{
					$insertArr['User_type'] 			 = 'Service Center';
					$insertArr['Subscription']			 = 'Active';
					$insertArr['Cancellation_policy']	 = '1';
				}
				else
				{
					$insertArr['User_type'] 			= 'Consumer';
					$insertArr['Subscription'] 		= 'None';
					$insertArr['Cancellation_policy']	 = '0' ;
				}

				if(isset($Is_verified) && $Is_verified == 'Yes')
				{
					$insertArr['Status'] = 'Active';
					// $link 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';	
				}
				else
				{
					if(trim($Status)!= '' && trim($Status)!= 'Pending' && trim($Status)!= 'InActive')
					{
						$insertArr['Status'] = trim($Status);
					}
					else
					{
						$insertArr['Status'] = 'Pending';
					}
				}
				unset($insertArr['Is_verified']);
				$insertID 			= $this->Signup->create_user($insertArr);
				//$insertArr['dob']	= strtotime($dob);

				//$DataArr 			= $this->pages->_getUser_by_email($email);
				//$DataArr['dob'] 	= strtotime($dob);
				//$country			= $DataArr['country_id'];
				//$country_data     	= $this->webservice_model->getcountry($country);
				//$DataArr['country_name'] 	= $country_data['country_name'];
				$generated_code 				= random_string('alnum', 16);
				$updateArray 	= array(
					'activate_code' => $generated_code,
				);
				// $DataArr1 = $this->pages->update_user($DataArr['id'],$updateArray);
				/* if($IsProf == 'Yes')
						{
						//$insert_membership = $this->webservice->insertmembershipdetail($DataArr['id'], $insertArr['membership_id']);
						$insertportfolio = array(
						'added_on'=>date('Y-m-d H:i:s'),
						'user_id'=>$DataArr['id'],
						'title'=>'instagram portfolio',
						'type'=>'instagram',
						'status'=>'Active'
						);
						//$this->webservice->create_portfolio($insertportfolio);
					} */

				//$link = site_url('activateaccount/'.base64_encode($DataArr['id'].'/'.$generated_code));
				//$link 	= '<a href="'.$link.'" target="_blank">'.$link.'</a>';
				//$var = array('{name}', '{link}');
				//$val = array($postData['name'], $link);

				//if($DataArr['Status'] != 'Active')
				// {
				//$mail_send = $this->sendEmail($postData['Email'], $var, $val,'sign_up');
				//}
				//elseif($DataArr['Status'] == 'Active')
				//{
				// $adminEmail =  $this->admin_model->_getConfigurationByKey('admin_email');
				// $mail_send = $this->sendEmail($adminEmail,'new_sign_up');
				//}
				//$mail_send = $this->sendEmail($postData['Email'], $var, $val,'sign_up');
				$insertArr['Image_name'] = $image_name;
				$insertArr['Password'] = md5($insertArr['Password']);
				if($insertID)
				{
					if($insertArr['Status'] != 'Active')
					{
						//$adminEmail =  $this->admin_model->_getConfigurationByKey('admin_email');
						//$email_data['admin'] = 'Admin';
						//$mail_send = $this->sendEmail($adminEmail, $email_data, $val,'new_sign_up');
						//$var=array('{admin}');
						//$val='Admin';
						//$adminmailsend = $this->sendEmail($adminEmail,$var, $val,'new_sign_up');
						$this->message = $this->error_handler->_getStatusMessage('success');
					}
					else
					{
						$this->message = 'Login successfully';
					}
					$this->status = 'success';
				} 
			}
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=> $DataArr, 'message' => $this->message));
		$this->template('output',$data);
	}

	/**
			* Method name: edit_user
			* @description :  Used to edit user record
			* @param:  Request data
			* @return:  Bool
		*/
	public function edit_user()
	{

		$insertArr = array();
		$val = array();
		$DataArr = array();
		$data['showHtml'] = @$postData['showHtml']; 
		$thumbSize=140;
		$postData = (array)json_decode($_POST['UserData']);
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);

		if($this->input->server('REQUEST_METHOD')=== "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}

			$updateArray = array(
				'UserId'       => $UserId,
				'm11_first_name' => $FirstName,
				'm11_last_name' => $LastName,
				'm11_user_gender' => $Gender,
				'm11_user_location'=>$Location,
				'm11_user_city'=>$City,
				'm11_user_country'=>$Country,
				'm11_user_postalcode'=>$Postal
			);

			$fileName="";
			if (!empty($_FILES) && $_FILES["img"]["name"] !=' ') 
			{ 
				$folder_name="user_images";
				$imagePath = "uploads/".$folder_name."/";
				if(!is_dir($imagePath))
				{
					if(!mkdir($imagePath,0777,true))
					{
						$response = Array(
							"error"=>'error',
							"status"=>'failed',
							"message"=>'Permission denied to create folder'
						);
						print json_encode($response);
						return;
					}
				}

				$maxSize = "2097152";
				$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
				$temp = explode(".", $_FILES["img"]["name"]);
				$extension = end($temp);
				$mimeType = array("image/png","image/jpeg","image/gif");
				$fileMime = trim($_FILES['img']['type']);

				//Check write Access to Directory

				if(!is_writable($imagePath))
				{
					$response = Array(
						"error" => 'error',
						"status" => 'failed',
						"message" => 'Can`t upload File; no write Access'
					);
					print json_encode($response);
					return;
				}

				if ( in_array($extension, $allowedExts))
				{
					$check = getimagesize($_FILES["img"]["tmp_name"]);
					$imagewidth = $check[0];
					$imageheight = $check[1];

					if ($_FILES["img"]["error"] > 0)
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'ERROR Return Code: '. $_FILES["img"]["error"][0],
						);
					}
					elseif($_FILES["img"]["size"] <= 0 || $_FILES["img"]["size"] > $maxSize)
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Maybe '.$_FILES["img"]["name"]. ' exceeds max 2 MB size',
						);
					}elseif(!in_array($fileMime, $mimeType)){
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Image type not supported',
						);
					}
					else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
						);
					}
					else
					{
						$temp_filename = $_FILES["img"]["tmp_name"];
						list($width, $height) = getimagesize( $temp_filename );

						$fileName = $_FILES["img"]["name"];
						$fileName = str_replace(" ","-",$fileName);
						$fileName = str_replace("%","",$fileName);
						$fileName = str_replace(",","",$fileName);
						$fileName = str_replace("'","",$fileName);
						$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).'_'.$fileName;

						move_uploaded_file($temp_filename,  $imagePath .$fileName );
						$targetFile = str_replace('//','/',$imagePath).$fileName;
						$info = pathinfo( $imagePath .$fileName);
						$getimg = getimagesize( $imagePath .$fileName);
						//list($width, $height, $type, $attr) = getimagesize($targetPath.$imgName);
						$width 		= $getimg[0];
						$height 	= $getimg[1];
						$type 		= $getimg[2];
						$attr 		= $getimg[3];
						if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG')
							$img = imagecreatefromjpeg( "{$targetFile}" );
						if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
							$img = imagecreatefromgif( "{$targetFile}" );
						if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
							$img = imagecreatefrompng( "{$targetFile}" );
						if($thumbSize)
							$thumbWidth = 140;
						else
							$thumbWidth = 60;
						if( $thumbWidth )
						{
							############## code for thumb ################
							if($width < $thumbWidth)
								$thumbWidth = $width;

							$width = imagesx( $img );
							$height = imagesy( $img );
							$new_height = floor( $height * ( $thumbWidth / $width ) );
							$new_width = $thumbWidth;

							$tmp_img = imagecreatetruecolor( $new_width, $new_height );
							imagealphablending($tmp_img, false);
							imagesavealpha($tmp_img,true);
							$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
							imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
							$targetFile1 =  str_replace('//','/',$imagePath). 'thumb_' . $fileName;

							if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
								imagejpeg( $tmp_img, "{$targetFile1}" );
							if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
								imagegif( $tmp_img, "{$targetFile1}" );
							if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
								imagepng( $tmp_img, "{$targetFile1}" );
						}
						$response = array(
							"error" => 'success',
							"status" => 'success',
							"url" => base_url().$imagePath.$fileName,
							"image"=>$fileName,
							"width" => $width,
							"height" => $height
						);

					}
				}
				else
				{
					$response = array(
						"error" => 'error',
						"status" => 'failed',
						"message" => 'Only jpg,jpeg,png and gif image is allowed.',
					);
				}
				if($fileName!="")
				$updateArray['m11_user_image'] = $fileName;
			}

			$updated = $this->Userprofile->create_user($updateArray);
			if($updated){
				$this->message = $this->error_handler->_getStatusMessage('update_profile');
				$this->isError =FALSE;
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
			$this->template('output',$data);
		}
	}





	/**
			* Method name: update_userprofile
			* @description :  Used to Update user record from Mobile App
			* @param:  Request data
			* @return:  Bool
		*/
	public function update_userprofile()
	{
		$insertArr = array();
		$val = array();
		$DataArr = array();
		$data['showHtml'] = @$postData['showHtml']; 
		$thumbSize=140;
		$postData = $this->input->post();
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);
		if($this->input->server('REQUEST_METHOD')=== "POST")
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			}
			if($this->isError === FALSE)
			{
				$user_data = $this->Userprofile->_getUser_by_regid($UserId);
				if(isset($FirstName) && trim($FirstName) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name');
					$this->isError = TRUE;
				}
				elseif(isset($FirstName) && !preg_match ("/^[a-zA-Z ]+$/",$FirstName))
				{
					$this->message = "Please enter only alphabets";
					$this->isError = TRUE;
				}
				else if(isset($FirstName) && strlen($FirstName) > 110)
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name_length');
					$this->isError = TRUE;
				}
				else if(isset($LastName) && trim($LastName) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name');
					$this->isError = TRUE;
				}
				elseif(isset($LastName) && !preg_match ("/^[a-zA-Z ]+$/",$LastName))
				{
					$this->message = "Please enter only alphabets";
					$this->isError = TRUE;
				}
				else if(isset($LastName) && strlen($LastName) > 110)
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name_length');
					$this->isError = TRUE;
				}
				else if(isset($Email) &&trim($Email) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_email');
					$this->isError = TRUE;
				}
				else if(isset($Email) && !valid_email($Email))
				{
					$this->message = $this->error_handler->_getStatusMessage('invalid_email');
					$this->isError = TRUE;
				}
				else if(isset($Email) && trim($Email) !== $user_data->MemberEmail)
				{
					if($this->Signup->sg_email_exist($Email,0,'Active') === TRUE)
					{
						$this->message = $this->error_handler->_getStatusMessage('email_exists');
						$this->isError = TRUE;
					}
				}
				else if(isset($AboutMe) && trim($AboutMe) == '')
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_name');
					$this->isError = TRUE;
				}
				elseif(isset($AboutMe) && !preg_match ("/^[a-zA-Z 0-9]+$/",$AboutMe))
				{
					$this->message = "Please enter only alphnumeric";
					$this->isError = TRUE;
				}
				else if(isset($AboutMe) && strlen($AboutMe) > 110)
				{
					$this->message = $this->error_handler->_getStatusMessage('empty_aboutme_length');
					$this->isError = TRUE;
				}

			}

			if($this->isError === FALSE)
			{
				$updateArray['UserId'] = $UserId;

				if(isset($FirstName))
				{
					$updateArray['m11_first_name'] =$FirstName;
				}
				if( isset($LastName))
				{
					$updateArray['m11_last_name'] =$LastName;

				}
				if(isset($Email))
				{
					$updateArray['m11_user_email'] =$Email;
				}
				if(isset($AboutMe))
				{
					$updateArray['m11_user_about_me'] = $AboutMe;
				}

				$fileName="";
				if (!empty($_FILES) && $_FILES["image"]["name"] !=' '&& $_FILES['image']['error'] ==0) 
				{ 

					$folder_name="user_images";
					$imagePath = "uploads/".$folder_name."/";
					if(!is_dir($imagePath))
					{
						if(!mkdir($imagePath,0777,true))
						{
							$response = Array(
								"error"=>'error',
								"status"=>'failed',
								"message"=>'Permission denied to create folder'
							);
							print json_encode($response);
							return;
						}
					}

					$maxSize = "2097152";
					$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
					$temp = explode(".", $_FILES["image"]["name"]);
					$extension = end($temp);
					$mimeType = array("image/png","image/jpeg","image/gif");
					$fileMime = trim($_FILES['image']['type']);
					if($fileMime == "application/octet-stream"){
						$imageMime = getimagesize($_FILES["image"]["tmp_name"]); // get temporary file REAL info
						$fileMime = $imageMime['mime']; //set in our array the correct mime
					}
					//Check write Access to Directory

					if(!is_writable($imagePath))
					{
						$response = Array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Can`t upload File; no write Access'
						);
						print json_encode($response);
						return;
					}

					if ( in_array($extension, $allowedExts))
					{
						$check = getimagesize($_FILES["image"]["tmp_name"]);
						$imagewidth = $check[0];
						$imageheight = $check[1];

						if ($_FILES["image"]["error"] > 0)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'ERROR Return Code: '. $_FILES["image"]["error"][0],
							);
						}
						elseif($_FILES["image"]["size"] <= 0 || $_FILES["image"]["size"] > $maxSize)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Maybe '.$_FILES["image"]["name"]. ' exceeds max 2 MB size',
							);
						}elseif(!in_array($fileMime, $mimeType)){
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Image type not supported',
							);
						}
						else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
							);
						}
						else
						{
							$temp_filename = $_FILES["image"]["tmp_name"];
							list($width, $height) = getimagesize( $temp_filename );

							$fileName = $_FILES["image"]["name"];
							$fileName = str_replace(" ","-",$fileName);
							$fileName = str_replace("%","",$fileName);
							$fileName = str_replace(",","",$fileName);
							$fileName = str_replace("'","",$fileName);
							$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).'_'.$fileName;

							move_uploaded_file($temp_filename,  $imagePath .$fileName );
							$targetFile = str_replace('//','/',$imagePath).$fileName;
							$info = pathinfo( $imagePath .$fileName);
							$getimg = getimagesize( $imagePath .$fileName);
							//list($width, $height, $type, $attr) = getimagesize($targetPath.$imgName);
							$width 		= $getimg[0];
							$height 	= $getimg[1];
							$type 		= $getimg[2];
							$attr 		= $getimg[3];
							if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG')
								$img = imagecreatefromjpeg( "{$targetFile}" );
							if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
								$img = imagecreatefromgif( "{$targetFile}" );
							if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
								$img = imagecreatefrompng( "{$targetFile}" );
							if($thumbSize)
								$thumbWidth = 140;
							else
								$thumbWidth = 60;
							if( $thumbWidth )
							{
								############## code for thumb ################
								if($width < $thumbWidth)
									$thumbWidth = $width;

								$width = imagesx( $img );
								$height = imagesy( $img );
								$new_height = floor( $height * ( $thumbWidth / $width ) );
								$new_width = $thumbWidth;

								$tmp_img = imagecreatetruecolor( $new_width, $new_height );
								imagealphablending($tmp_img, false);
								imagesavealpha($tmp_img,true);
								$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255, 127);
								imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
								$targetFile1 =  str_replace('//','/',$imagePath). 'thumb_' . $fileName;

								if( $info['extension'] == 'jpg' ||  $info['extension'] == 'jpeg' ||  $info['extension'] == 'JPG' ||  $info['extension'] == 'JPEG' )
									imagejpeg( $tmp_img, "{$targetFile1}" );
								if( $info['extension'] == 'gif' || $info['extension'] == 'GIF' )
									imagegif( $tmp_img, "{$targetFile1}" );
								if( $info['extension'] == 'png' ||  $info['extension'] == 'PNG' )
									imagepng( $tmp_img, "{$targetFile1}" );
							}
							$response = array(
								"error" => 'success',
								"status" => 'success',
								"url" => base_url().$imagePath.$fileName,
								"image"=>$fileName,
								"width" => $width,
								"height" => $height
							);

						}
					}
					else
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Only jpg,jpeg,png and gif image is allowed.',
						);
					}
					if($fileName!="")
					$updateArray['m11_user_image'] = $fileName;
				}
				$updated = $this->Userprofile->create_user($updateArray);
				if($updated)
				{
					$this->message = $this->error_handler->_getStatusMessage('update_profile');
					$this->isError =FALSE;
					$this->status = 'success';
				}

			}
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
		$this->template('output',$data);
	}


	/**
			* Method name: change_password
			* @description :  Used to change user password
			* @param:  Request data
			* @return:  Bool
		*/
	public function change_password($id=NULL)
	{

		$insertArr = array();
		$DataArr = array();
		$DataArr = $this->_debugMode();
		$data['showHtml'] = @$postData['showHtml']; 
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$postData = $postData['UserPassData'];
		$UserId = (!empty($postData)) ? $postData->UserId : $id;
		if(!empty($postData )){

			if(trim($postData->newPass) == '')
			{
				$this->message = $this->error_handler->_getStatusMessage('empty_new_password');
				$this->isError = TRUE;
			}
			else if(strlen($postData->newPass) < 6)
			{
				$this->message = $this->error_handler->_getStatusMessage('minlen_new_password');
				$this->isError = TRUE;
			}
			else if($postData->newPass != $postData->confirmPass)
			{
				$this->message = $this->error_handler->_getStatusMessage('confirm_new_password');
				$this->isError = TRUE;
			}else{

 				$password = $this->Userprofile->getUserPasswordByID($UserId);

				if($password !='' && $password == md5( $postData->oldPass )){

					$updateArr = array('tr04_login_pwd'=>md5($postData->newPass));

					$updatedPass = $this->Userprofile->update_password($UserId,$updateArr);

					if($updatedPass){

						$this->message = $this->error_handler->_getStatusMessage('password_change');
						$this->isError = FALSE;
						$this->status  = 'Success';
					}
				}else{

					$this->message = $this->error_handler->_getStatusMessage('invalid_existing_password');
					$this->isError = TRUE;

				}
			}
		}

		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
		$this->template('output',$data); 
	}

	/**
			* Method name: getuserByPhone
			* @description :  Get user By phone
			* @param:  Request data
			* @return:  Bool
		*/
	public function getuserByPhone($phone=NULL)
	{

		$insertArr = array();
		$DataArr = array();
		$DataArr = $this->_debugMode();
		$data['showHtml'] = @$postData['showHtml']; 
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$phoneNumber = isset($postData['phoneNumber']) ? $postData['phoneNumber'] : $phone ;
		$user = $this->Userprofile->_getUser_by_mobileno($phoneNumber);
		if($user){
			$this->isError = FALSE;	
			$this->status = 'Success';
			$this->message = 'User account found';
		}else{
			$this->isError = TRUE;	
			$this->message = 'User not found';
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
		$this->template('output',$data); 

	}

	/**
			* Method name: getAboutContent
			* @description : Used to get about us content
			* @param: 
			* @return:  data
		*/
	public function getAboutContent(){
		$content = $this->Userprofile->getSiteStaticContent('about_us'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}

	} 
	/**
			* Method name: getTeamContent
			* @description : Used to get team content
			* @param: 
			* @return:  data
		*/
	public function getTeamContent(){
		$content = $this->Userprofile->getSiteStaticContent('team'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}

	} 
	/**
			* Method name: getCareerContent
			* @description : Used to get Career content
			* @param: 
			* @return:  data
		*/
	public function getCareerContent(){
		$content = $this->Userprofile->getSiteStaticContent('career'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}

	} 


	
	/**
			* Method name: getContactUsContent
			* @description : Used to get Contact us content
			* @param: 
			* @return:  data
		*/
	public function getSiteContactUsdata(){
		$content = $this->Userprofile->getSiteStaticContent('contact_us'); 
		if($content){
			$title = $content->m10_staticcnt_title;
			$content = $content->m10_staticcnt_content;	
			echo json_encode(array('error'=>FALSE,'data'=>array('title'=>$title,'content'=>$content)));
		}

	}

	/**
			* Method name: getRechargeDetails
			* @description : Used to getrecharge details
			* @param: 
			* @return:  data
		*/
	public function getRechargeDetails()
	{
		$y=0;
		$insertArr = array();
		$json_data= array();
		$data['title'] ="Recharge Report";
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);
		if($this->input->server('REQUEST_METHOD') === "POST" )
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			} 
			$user_data = $this->Userprofile->_getUser_by_mobileno($insertArr['userid']);
			$insertArr['Userid']=$user_data->RegId;
			if($insertArr['Userid']!=$insertArr['regid'])
				$y=$this->introducerid(trim($insertArr['userid']),trim($insertArr['regid']));	
			else
				$y=1;
			if($y)
			{
				$json_data=$this->Userprofile->show_recharge($insertArr);
			}
			else
			{
				$json_data=$y;
			}
		}
		$data['jsonData'] = json_encode(array('error' => 'success','status' => 'success','userDATA'=>$json_data));
		$this->template('output',$data);

	}
	/**
			* Method name: getTransactionDetails
			* @description : Used to getTransaction details
			* @param: 
			* @return:  data
		*/
	public function getTransactionDetails()
	{
		$y=0;
		$insertArr = array();
		$json_data= array();
		$data['title'] ="Recharge Report";
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$data['showHtml'] = @$postData['showHtml'];
		unset($postData['showHtml']);
		unset($postData['submit']);
		unset($postData['mode']);
		if($this->input->server('REQUEST_METHOD') === "POST" )
		{
			foreach($postData as $key => $post)
			{
				$$key = self::_stripTags_trim($post);
				$insertArr[$key] = self::_stripTags_trim($post);
			} 
			$user_data = $this->Userprofile->_getUser_by_mobileno($insertArr['userid']);
			$insertArr['Userid']=$user_data->RegId;
			if($insertArr['Userid']!=$insertArr['regid'])
			{
				$y=$this->introducerid(trim($insertArr['userid']),trim($insertArr['regid']));	
			}
			else
			{
				$y=1;	
			}
			if($y)
			{
				$json_data=$this->Userprofile->show_ladger($insertArr );
			}
			else
			{
				$json_data=$y;
			}
		}
		$data['jsonData'] = json_encode(array('error' => 'success','status' => 'success','userDATA'=>$json_data));
		$this->template('output',$data);

	}
	/**
			* Method name: getbalanceByID
			* @description : Used to Get User's Current Balance
			* @param: 
			* @return:  data
		*/
	public function getbalanceByID($MemberId = NULL){

		$data['showHtml'] = @$postData['showHtml']; 
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$MemberId = '';
		if(!empty($postData)){
			$MemberId = $postData['MemberId'];	
		}
		$balance = $this->Userprofile->getUserCurrentBalance($MemberId,1); 
		$mbalance = $this->Userprofile->getUserCurrentBalance($MemberId,2); 
		$maintainwallet = $this->Userprofile->getUserMaintainBalance($MemberId,3); 
		$this->isError = false;
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>'success', 'data'=>array('current_balance'=>$balance,'misc_balance'=>$mbalance,'maintain_balance'=>$maintainwallet,), 'message' =>''));
		$this->template('output',$data); 
	}

	/**
			* Method name: upgradeWallet
			* @description : Used to upgradewallet (KYC)
			* @param: 
			* @return:  data
		*/
	public function upgradeWallet(){

		$data['showHtml'] = @$postData['showHtml'];
		if(isset($_POST['WalletData'])){
			$postData = (array)json_decode($_POST['WalletData']);	
			$Userphoto = '';
			$idProofDoc = '';
			$addressProof = '';
			$kyc = $this->Userprofile->get_kyc($postData['UserId']); 
			if(!empty($kyc->KyIdproofdoc)) $idProofDoc = $kyc->KyIdproofdoc ;
			if(!empty($kyc->kyuserpic)) $Userphoto = $kyc->kyuserpic ;
			if(!empty($kyc->kYaddressproof)) $addressProof = $kyc->kYaddressproof ;

			if($postData['FirstName']==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_firstname');
				$this->isError = TRUE;
			}elseif($postData['LastName']==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_lastname');
				$this->isError = TRUE;
			}elseif($postData['Dob']==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_dob');
				$this->isError = TRUE;
			}elseif($postData['UIDNUMBER']==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_unumber');
				$this->isError = TRUE;
			}elseif($postData['address']==''){
				$this->message = $this->error_handler->_getStatusMessage('empty_address');
				$this->isError = TRUE;
			}else{


				$fileName="";

				$thumbSize=140;
				if (isset($_FILES['photo']) && $_FILES['photo']['name'] !='') 
				{
					$folder_name="user_kyc";
					$imagePath = "uploads/".$folder_name."/";
					if(!is_dir($imagePath))
					{
						if(!mkdir($imagePath,0777,true))
						{
							$response = Array(
								"error"=>'error',
								"status"=>'failed',
								"message"=>'Permission denied to create folder'
							);
							print json_encode($response);
							return;
						}
					}

					$maxSize = "2097152";
					$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
					$temp = explode(".", $_FILES["photo"]["name"]);
					$extension = end($temp);
					$mimeType = array("image/png","image/jpeg","image/gif");
					$fileMime = trim($_FILES['photo']['type']);

					//Check write Access to Directory

					if(!is_writable($imagePath))
					{
						$response = Array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Can`t upload File; no write Access'
						);
						print json_encode($response);
						return;
					}

					if ( in_array($extension, $allowedExts))
					{
						$check = getimagesize($_FILES["photo"]["tmp_name"]);
						$imagewidth = $check[0];
						$imageheight = $check[1];

						if ($_FILES["photo"]["error"] > 0)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'ERROR Return Code: '. $_FILES["photo"]["error"][0],
							);
						}
						elseif($_FILES["photo"]["size"] <= 0 || $_FILES["photo"]["size"] > $maxSize)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Maybe '.$_FILES["photo"]["name"]. ' exceeds max 2 MB size',
							);
						}elseif(!in_array($fileMime, $mimeType)){
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Image type not supported',
							);
						}
						else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
							);
						}
						else
						{
							$temp_filename = $_FILES["photo"]["tmp_name"];
							list($width, $height) = getimagesize( $temp_filename );

							$fileName = $_FILES["photo"]["name"];
							$fileName = str_replace(" ","-",$fileName);
							$fileName = str_replace("%","",$fileName);
							$fileName = str_replace(",","",$fileName);
							$fileName = str_replace("'","",$fileName);
							$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).'_'.$fileName;

							move_uploaded_file($temp_filename,  $imagePath .$fileName );
						}
					}
					else
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Only jpg,jpeg,png and gif image is allowed.',
						);
					}
					$Userphoto = $fileName;
				}

				if (isset($_FILES['idProofDoc']) && $_FILES['idProofDoc']['name'] !='') 
				{
					$folder_name="user_kyc";
					$imagePath = "uploads/".$folder_name."/";
					if(!is_dir($imagePath))
					{
						if(!mkdir($imagePath,0777,true))
						{
							$response = Array(
								"error"=>'error',
								"status"=>'failed',
								"message"=>'Permission denied to create folder'
							);
							print json_encode($response);
							return;
						}
					}

					$maxSize = "2097152";
					$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
					$temp = explode(".", $_FILES["idProofDoc"]["name"]);
					$extension = end($temp);
					$mimeType = array("image/png","image/jpeg","image/gif");
					$fileMime = trim($_FILES['idProofDoc']['type']);

					//Check write Access to Directory

					if(!is_writable($imagePath))
					{
						$response = Array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Can`t upload File; no write Access'
						);
						print json_encode($response);
						return;
					}

					if ( in_array($extension, $allowedExts))
					{
						$check = getimagesize($_FILES["idProofDoc"]["tmp_name"]);
						$imagewidth = $check[0];
						$imageheight = $check[1];

						if ($_FILES["idProofDoc"]["error"] > 0)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'ERROR Return Code: '. $_FILES["idProofDoc"]["error"][0],
							);
						}
						elseif($_FILES["idProofDoc"]["size"] <= 0 || $_FILES["idProofDoc"]["size"] > $maxSize)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Maybe '.$_FILES["idProofDoc"]["name"]. ' exceeds max 2 MB size',
							);
						}elseif(!in_array($fileMime, $mimeType)){
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Image type not supported',
							);
						}
						else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
							);
						}
						else
						{
							$temp_filename = $_FILES["idProofDoc"]["tmp_name"];
							list($width, $height) = getimagesize( $temp_filename );

							$fileName = $_FILES["idProofDoc"]["name"];
							$fileName = str_replace(" ","-",$fileName);
							$fileName = str_replace("%","",$fileName);
							$fileName = str_replace(",","",$fileName);
							$fileName = str_replace("'","",$fileName);
							$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).rand(1111,9999).'_'.$fileName;

							move_uploaded_file($temp_filename,  $imagePath .$fileName );
						}
					}
					else
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Only jpg,jpeg,png and gif image is allowed.',
						);
					}
					$idProofDoc = $fileName;
				}

				if (isset($_FILES['addressProof']) && $_FILES['addressProof']['name'] !='') 
				{
					$folder_name="user_kyc";
					$imagePath = "uploads/".$folder_name."/";
					if(!is_dir($imagePath))
					{
						if(!mkdir($imagePath,0777,true))
						{
							$response = Array(
								"error"=>'error',
								"status"=>'failed',
								"message"=>'Permission denied to create folder'
							);
							print json_encode($response);
							return;
						}
					}

					$maxSize = "2097152";
					$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
					$temp = explode(".", $_FILES["addressProof"]["name"]);
					$extension = end($temp);
					$mimeType = array("image/png","image/jpeg","image/gif");
					$fileMime = trim($_FILES['addressProof']['type']);

					//Check write Access to Directory

					if(!is_writable($imagePath))
					{
						$response = Array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Can`t upload File; no write Access'
						);
						print json_encode($response);
						return;
					}

					if ( in_array($extension, $allowedExts))
					{
						$check = getimagesize($_FILES["addressProof"]["tmp_name"]);
						$imagewidth = $check[0];
						$imageheight = $check[1];

						if ($_FILES["addressProof"]["error"] > 0)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'ERROR Return Code: '. $_FILES["addressProof"]["error"][0],
							);
						}
						elseif($_FILES["addressProof"]["size"] <= 0 || $_FILES["addressProof"]["size"] > $maxSize)
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Maybe '.$_FILES["addressProof"]["name"]. ' exceeds max 2 MB size',
							);
						}elseif(!in_array($fileMime, $mimeType)){
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Image type not supported',
							);
						}
						else if ( (1920 > $imagewidth || 370 > $imageheight) && $folder_name=="banner_images" ) 
						{
							$response = array(
								"error" => 'error',
								"status" => 'failed',
								"message" => 'Recommended Image size 1920x370 px Your image size is '.$imagewidth.'X'.$imageheight.' px.',
							);
						}
						else
						{
							$temp_filename = $_FILES["addressProof"]["tmp_name"];
							list($width, $height) = getimagesize( $temp_filename );

							$fileName = $_FILES["addressProof"]["name"];
							$fileName = str_replace(" ","-",$fileName);
							$fileName = str_replace("%","",$fileName);
							$fileName = str_replace(",","",$fileName);
							$fileName = str_replace("'","",$fileName);
							$fileName = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y")).rand(111,999).'_'.$fileName;

							move_uploaded_file($temp_filename,  $imagePath .$fileName );


						}
					}
					else
					{
						$response = array(
							"error" => 'error',
							"status" => 'failed',
							"message" => 'Only jpg,jpeg,png and gif image is allowed.',
						);
					}
					$addressProof = $fileName;
				}

				$insertArr = array(
					'm19_uk_user_id' =>$postData['UserId'],
					'm19_uk_address' =>$postData['address'],
					'm19_uk_uniqueid_num' =>$postData['UIDNUMBER'],
					'm19_uk_user_photo' =>$Userphoto ,
					'm19_uk_idproofdoc' =>$idProofDoc,
					'm19_uk_addressproof' =>$addressProof,
					'm19_uk_idproof_type' =>$postData['IdproofType'],
					'm19_uk_onadd' =>date('Y-m-d H:i:s'),
				);

				$PersonalDetails = array(
					'm11_first_name' =>$postData['FirstName'],
					'm11_last_name' =>$postData['LastName'],
					'm11_user_dob' =>'',
					'user_id' =>$postData['UserId'],
				);

				$updateUser = $this->Userprofile->create_user($PersonalDetails); 
				$kyc = $this->Userprofile->user_kyc($insertArr,$postData['UserId']); 
				if($updateUser && $kyc){
					$this->isError = TRUE;
					$this->status = 'success';	
					$this->message='WalletData Upgraded';
				}				
			}
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>'', 'message' => $this->message));
		$this->template('output',$data);
	}

	/**
			* Method name: upgradeWallet
			* @description : Used to upgradewallet (KYC)
			* @param: 
			* @return:  data
	**/
	public function get_kyc(){
		$kyc = '';
		$data['showHtml'] = @$postData['showHtml']; 
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$MemberId = '';
		if(!empty($postData)){

			$MemberId = $postData['MemberId'];	
			$kyc = $this->Userprofile->get_kyc($MemberId); 
			$user = $this->Userprofile->_getUser_by_regid($MemberId); 
			if(!empty($kyc)){
				$this->isError = FALSE;	
				$this->status = 'success';

			} 
		}

		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' => $this->status, 'data'=>array('kyc'=>$kyc,'user'=>$user), 'message' => $this->message));
		$this->template('output',$data);
	}


	/**
			* Method name: get_cms_content_by_id
			* @description : Used to Get User's CMS data
			* @param: 
			* @return:  data
		*/
	public function get_cms_content_by_id($MemberId = NULL){
		$content='';
		$data['showHtml'] = @$postData['showHtml']; 
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$MemberId = '';
		if(!empty($postData)){
			$MemberId = $postData['MemberId'];	
		}
		$cms = $this->Userprofile->getCmsByAffiliateID($MemberId); 
		if(!empty($cms)){
			$this->isError = FALSE;
			$this->status = 'failed';
			$content = $cms ;
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$content, 'message' =>''));
		$this->template('output',$data); 
	}

	/**
			* Method name: get_cms_content_by_id
			* @description : Used to Get User's CMS data
			* @param: 
			* @return:  data
		*/
	public function getContentData($MemberId = NULL){
		$content='';
		$data['showHtml'] = @$postData['showHtml']; 
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$MemberId = '';
		$SC_ID = '';
		if(!empty($postData)){
			$MemberId = $postData['MemberId'];	
			$SC_ID = $postData['SC_ID'];	
		}
		$cms = $this->Userprofile->getCmsByID($SC_ID,$MemberId); 
		if(!empty($cms)){
			$this->isError = FALSE;
			$this->status = 'failed';
			$content = $cms ;
		}
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$content, 'message' =>''));
		$this->template('output',$data); 
	}


	/**
			* Method name	: get_AllFAQ
			* @description 	: Used to get FAQ data  
			* @param		: Request data  
			* @return		: Get FAQ data array with success message
		*/
	public function saveStaticContent()
	{
		$insertArr = array();
		$DataArr = array();
		$data['showHtml'] = @$postData['showHtml']; 
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$postData = $postData['Content'];
		if(!empty($postData)){
			$SC_ID = $postData->SC_ID;	
			$insertArr = array(
				'm10_staticcnt_title' => $postData->Title,
				'm10_staticcnt_content' => $postData->Content,
				'm10_staticcnt_status' => $postData->status,
			);
			$msg='';
			$updated = $this->Userprofile->saveStaticContent($SC_ID,$insertArr); 
			if($updated){
				$this->isError =FALSE;	
				$this->status = 'success';	
				$msg = "Changes applied,Content has been updated !";
			}
			$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>'', 'message' =>$msg));
			$this->template('output',$data); 
		}
	}
	/**
			* Method name	: forgotPasswod
			* @description 	: Used to set new password
			* @param		: Request data or $email 
			* @return		:  
		*/
	public function forgotPasswod($Email=NULL)
	{

		$data['showHtml'] = @$postData['showHtml']; 
		$DataArr = $this->_debugMode();
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$postData = $postData['FormData'];
		if(!empty($postData->email)) $Email = $postData->email;

		if(trim($Email) == '')
		{
			$this->message = $this->error_handler->_getStatusMessage('empty_email');
			$this->isError = TRUE;
		}
		else if(!valid_email($Email))
		{
			$this->message = $this->error_handler->_getStatusMessage('invalid_email');
			$this->isError = TRUE;
		}
		else if(!$this->Userprofile->sg_email_exist($Email,0))
		{
			$this->message = $this->error_handler->_getStatusMessage('email_not_exists');
			$this->isError = TRUE;
		}else{

			//
		}

		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>'', 'message' =>$this->message));
		$this->template('output',$data); 
	}
	public function send_sms($mob,$msg)
	{
		$url="http://w2s.in/SendSMSAPI.aspx";
		$params = array (
			'mo'=>urlencode($mob),
			'ms'=>$msg,
			'r'=>urlencode(1),
			's'=>urlencode('PROMOT'),
			'u'=>urlencode(''),
			'p'=>urlencode('')
		);
		$options = array(
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0
		);

		$defaults = array(
			CURLOPT_URL => $url. (strpos($url, '?') 
								  === FALSE ? '?' : ''). http_build_query($params),
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_TIMEOUT =>56
		);

		$ch = curl_init();
		curl_setopt_array($ch, ($options + $defaults));
		$result = curl_exec($ch);
		if(!$result)
		{
			trigger_error(curl_error($ch));
			$flag=0;
		}
		else
		{	                
			$flag=1;
		}
		curl_close($ch);
		//echo $result; 


	}



	public function get_all_bank()
	{
		$data['result']=$this->db->where('m_bank_status',1)->get('m01_bank')->result();
		echo json_encode($data);
	}

	public function get_slottoken()
	{
		$data['showHtml'] = @$postData['showHtml']; 
		$data['result']=$this->db->get('m14_slot_token')->result();
		$this->isError = FALSE;
		$this->status = 'success';
		$this->message='ICO Token Recieved';
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$data['result'], 'message' =>$this->message));
		$this->template('output',$data); 
	}

	public function get_admin_btcaddress()
	{
		$data['showHtml'] = @$postData['showHtml']; 
		$data['result']=$this->db->query("SELECT * FROM `m15_btc_address` WHERE `m_btc_usertype` = 'Admin' ORDER BY RAND() LIMIT 1")->result();
		$this->isError = FALSE;
		$this->status = 'success';
		$this->message='Admin BTC Address Recieved';
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$data['result'], 'message' =>$this->message));
		$this->template('output',$data); 
	}

	public function get_usdtobtc()
	{
		$data['showHtml'] = @$postData['showHtml'];
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$price_in_usd = $postData['usdAmount'];
		
		$data['result'] = file_get_contents("https://blockchain.info/tobtc?currency=USD&value=" . $price_in_usd);
		$this->isError = FALSE;
		$this->status = 'success';
		$this->message='Admin BTC Address Recieved';
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$data['result'] , 'message' =>$this->message));
		$this->template('output',$data); 
	}

	public function user_buytoken()
	{
		$data['showHtml'] = @$postData['showHtml'];
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$btcaddress = $postData['btcAddress'];
		$usdamount = $postData['usdAmount'];
		$btcamount = $postData['BTCAmount'];
		$regid = $postData['regid'];
		
		$procedure_data = array(
			"regid" => $regid,
			"btcaddress" => $btcaddress,
			"usdamount" => $usdamount,
			"btcamount" => $btcamount,
			"buystatus" => '2',
			"proc" => 1
		);
		$call_procedure = " CALL sp_user_buytoken(?" . str_repeat(",?", count($procedure_data) - 1) . ") ";
		$this->db->query($call_procedure, $procedure_data);
		
		$this->isError = FALSE;
		$this->status = 'success';
		$this->message='Request Successfully done';
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>'' , 'message' =>$this->message));
		$this->template('output',$data); 
	}
	
	


	public function userOrder()
	{
		$data['showHtml'] = @$postData['showHtml'];
		$json = file_get_contents('php://input');
		$postData = (array)json_decode($json);
		$regid = $postData['regid'];

		$data['result'] = $this->db->where('t_reg_id',$regid)->get("tr05_users_topups")->result();
		$this->isError = FALSE;
		$this->status = 'success';
		$this->message='Admin BTC Address Recieved';
		$data['jsonData'] = json_encode(array('error'=>$this->isError,'status' =>$this->status, 'data'=>$data['result'] , 'message' =>$this->message));
		$this->template('output',$data); 
	}


} /* Class Closed */

?>																																						