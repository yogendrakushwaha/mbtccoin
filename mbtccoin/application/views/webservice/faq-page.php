<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TFCS</title> 
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>    
  </head>
  <!-- CSS Styling -->
  <style type="text/css">
    .wrapper.terms-page { font-family: 'Lato', sans-serif; background: url(<?php echo base_url(); ?>lib/images/faq-img01.jpg) no-repeat center top ; height: auto; width: 100%; background-size: cover;
     }
    .color-overlay { background-color: rgba(0, 0, 0, 0.81); height: 100%; }
    .container { padding: 0 20px; }
   
    .sub-head { padding: 150px 0 15px; }
   
    .terms-page .sub-head h2 { color: #008aad; font-size: 27px; font-weight: 700; margin: 0; text-transform: uppercase; }
    .terms-page .about-text p { color: #fff; font-size: 15px; font-weight: 400; margin: 10px  8px; line-height: 170%; }
    .footer-logo { padding: 15px 0; text-align: right; }
    body { margin :0 }
  </style>

  <body>
    <!-- AboutUs Section Start -->
    <div class="wrapper terms-page">
      <div class="color-overlay">
        <div class="container">
         <div class="sub-head">
          <h2><?php echo $content_title;?></h2>
        </div>
        <?php if($content) {?>
       <div class="about-text"><?php echo $content;?></div>
      <?php }?>
      </div>
          <div class="footer-logo">
            <a href="#"><img src="<?php echo base_url(); ?>lib/images/logo02.png" /></a>
          </div>
        </div>
      </div>
    </div>
  <!-- AboutUs Section End -->      
  </body>
</html>
