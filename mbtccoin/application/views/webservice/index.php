<!DOCTYPE html>
<html>
   <head>
	<script type="text/javascript" src="<?php echo base_url(); ?>lib/webservice/js/jquery-1.11.1.min.js" ></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>lib/webservice/css/pretty-json.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>lib/webservice/css/style.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>lib/webservice/js/custom.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo SITE_NAME; ?></title>

   </head>
   <body>
	<div id="top">&nbsp;</div>
	<div style="position:fixed;right: 5%;top:40%;">
	   <a href="#top"><input type="button" id="back-top" value="Top"/></a>
	</div>
	<div style="position:fixed;right: 5%;top:50%;">
            <div class="search-box">
            <div class="form_field">
                        <input type="text" name="search" id="search" placeholder="Search Service" value="" />
                         <input type="hidden" id="search_id" placeholder="Search Service" value="" />
                    </div>
        </div>
            <ul>
                <li><a href="#module1" id="module-1">Module 1 Web Services</a></li>
            </ul>

        </div>

<!--
*****************************************************************************************************************************************
******************************************** M O D U L E - 1   W E B    S E R V I C E S *************************************************
*****************************************************************************************************************************************
-->
      <h2 id="module1"><?php echo SITE_NAME; ?> webservices Module 1</h2>
	   <div class="module-div">
		<ol type="1" class="list_1">
			<li><a href="javascript:void(0);" data-href="#getCountryCode">Get Country Code</a><p>Service URL: <?php echo site_url('webservice/getCountryCode'); ?></p></li>
		   <li><a href="javascript:void(0);" data-href="#user_signup">Sign Up using Email</a><p>Service URL: <?php echo site_url('webservice/user_signup'); ?></p></li>
		   <li><a href="javascript:void(0);" data-href="#user_signup_facebook">Sign Up using Facebook</a><p>Service URL: <?php echo site_url('webservice/userSignupFacebook'); ?></p></li>
		   <li><a href="javascript:void(0);" data-href="#user_emaillogin">Login using Email</a><p>Service URL: <?php echo site_url('webservice/email_login'); ?></p></li>
		   <li><a href="javascript:void(0);" data-href="#user_facebooklogin">Login using Facebook</a><p>Service URL: <?php echo site_url('webservice/facebook_login'); ?></p></li>
		   <li><a href="javascript:void(0);" data-href="#forgetpassword">Forget Password</a><p>Service URL: <?php echo site_url('webservice/forgetpassword'); ?></p></li>
		   <li><a href="javascript:void(0);" data-href="#staticpage">Static Page</a><p>Service URL: <?php echo site_url('webservice/staticpage'); ?></p></li>
		   
		   
		</ol>

<!-- ############################################################## User Signup ##################################################### -->

         <div id="user_signup" class="div_service">
		<?php echo form_open_multipart('webservice/user_signup');?>
		<h3>User Signup</h3>
		<div class="form_row">
		   <div class="form_label">* name</div>
		   <div class="form_field">
			<input type="text" name="name" value="" />
			<div class="desc">key: name</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Email</div>
		   <div class="form_field">
			<input type="text" name="email" value="" />
			<div class="desc">key: email</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Password</div>
		   <div class="form_field">
			<input type="password" name="password" value="" />
			<div class="desc">key: password </div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Confirm Password</div>
		   <div class="form_field">
			<input type="password" name="confirm_password" value="" />
			<div class="desc">key: confirm_password </div>
		   </div>
		</div>
		<?php /* ?>
		<div class="form_row">
		   <div class="form_label">* Gender</div>
		   <div class="form_field">
			<select name="gender">
			   <option value="Male">Male</option>
			   <option value="Female">Female</option>
			</select>
			<div class="desc">key: gender ('Male', 'Female') </div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Date Of Birth</div>
		   <div class="form_field">
			<input type="text" name="dob" class= "mydate" value=""  />
			<div class="desc">key: dob (Format: YYYY-MM-DD)</div>
		   </div>
		</div>
		<?php */ ?>
		<div class="form_row">
		   <div class="form_label">* Country</div>
		   <div class="form_field">
			<input type="text" name="country_id" value="" />
			<div class="desc">key: country_id </div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Freelance Professional</div>
		   <div class="form_field">
			<select name="isFreelanceProfessional">
			   <option value="Yes">Yes</option>
			   <option value="No">No</option>
			</select>
			<div class="desc">key: isFreelanceProfessional </div>
		   </div>
		</div>
		<?php /* ?>
		<div class="form_row">
		   <div class="form_label">Image Name</div>
		   <input type="file" name="image_name" size="20" />
		   <?php echo form_error('image_name', '<span class="remove_error">', '</span>');?>
		</div>
		<?php */ ?>
		<div class="form_row">
		   <div class="form_label">Debug Mode</div>
		   <div class="form_field">
			<input type="text" name="mode" value="" />
			<div class="desc">key: mode <br />values: debug</div>
		   </div>
		</div>
		<div class="form_row" style="display: block;">
		   <div class="form_label">Show Html</div>
		   <div class="form_field">
			<input type="text" name="showHtml" value="yes" />
			<div class="desc">key: showHtml<br /> values: yes / no</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">&nbsp;</div>
		   <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
		</div>
	   <?php echo form_close(); ?>
	</div>
	   <!-- ############################################################## User Signup Facebook##################################################### -->

         <div id="user_signup_facebook" class="div_service">
		<?php echo form_open_multipart('webservice/userSignupFacebook');?>
		<h3>User Signup Facebook</h3>
		<div class="form_row">
		   <div class="form_label">* Facebook Id</div>
		   <div class="form_field">
			<input type="text" name="fb_id" value="" />
			<div class="desc">key: fb_id </div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Name</div>
		   <div class="form_field">
			<input type="text" name="name" value="" />
			<div class="desc">key: name</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Email</div>
		   <div class="form_field">
			<input type="text" name="email" value="" />
			<div class="desc">key: email</div>
		   </div>
		</div>
		<?php /* ?>
		<div class="form_row">
		   <div class="form_label"> Date Of Birth</div>
		   <div class="form_field">
			<input type="text" name="dob" class= "mydate" value=""  />
			<div class="desc">key: dob (Format: YYYY-MM-DD)</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label"> Gender</div>
		   <div class="form_field">
			<input type="text" name="gender" value="" />
			<div class="desc">key: gender ('Male', 'Female') </div>
		   </div>
		</div>
		<?php */ ?>
		<div class="form_row">
		   <div class="form_label">* Freelance Professional</div>
		   <div class="form_field">
			<select name="isFreelanceProfessional">
			   <option value="Yes">Yes</option>
			   <option value="No">No</option>
			</select>
			<div class="desc">key: isFreelanceProfessional </div>
		   </div>
		</div>
		<?php /* ?>
		<div class="form_row">
		   <div class="form_label">* Image Name</div>
		   <div class="form_field">
			<input type="file" name="image_name" size="20" />
			<div class="desc">key: image_name</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Image Name</div>
		   <div class="form_field">
			<input type="text" name="image_name" size="20" />
			<div class="desc">key: image_name</div>
		   </div>
		</div>
		<?php */ ?>
		<div class="form_row">
		   <div class="form_label">* Is verified E-mail from facebook</div>
		   <div class="form_field">
			<select name="is_verified">
			   <option value="No">No</option>
			   <option value="Yes">Yes</option>
			</select>
			<div class="desc">key: is_verified </div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">Debug Mode</div>
		   <div class="form_field">
			<input type="text" name="mode" value="" />
			<div class="desc">key: mode <br />values: debug</div>
		   </div>
		</div>
		<div class="form_row" style="display: block;">
		   <div class="form_label">Show Html</div>
		   <div class="form_field">
			<input type="text" name="showHtml" value="yes" />
			<div class="desc">key: showHtml<br /> values: yes / no</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">&nbsp;</div>
		   <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
		</div>
	   <?php echo form_close(); ?>
	</div>
<!-- ############################################################## User Login by email ############################################# -->

	<div id="user_emaillogin" class="div_service">
	   <?php echo form_open('webservice/email_login');?>
	   <h3>User Login</h3>
	   <div class="form_row">
		<div class="form_label">* Email</div>
		<div class="form_field">
		   <input type="text" name="email" value="" />
		   <div class="desc">key: email</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Password</div>
		<div class="form_field">
		   <input type="password" name="password" value="" />
		   <div class="desc">key: password </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Device Token</div>
		<div class="form_field">
		   <input type="text" name="device_token" value="" />
		   <div class="desc">key: device_token</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Device Type (android, ios)</div>
		<div class="form_field">
		   <input type="text" name="device_type" value="" />
		   <div class="desc">key: device_type</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">App Version</div>
		<div class="form_field">
		   <input type="text" name="app_version" value="" />
		   <div class="desc">key: app_version</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

<!-- ###################################### User Login by facebook ########################################## -->

	<div id="user_facebooklogin" class="div_service">
	   <?php echo form_open('webservice/facebook_login');?>
	   <h3>User Login</h3>
	   <div class="form_row">
		<div class="form_label">* fb_id</div>
		<div class="form_field">
		   <input type="text" name="fb_id" value="" />
		   <div class="desc">key: fb_id </div>
		</div>
	   </div>

	   <div class="form_row">
		   <div class="form_label">Email</div>
		   <div class="form_field">
			<input type="text" value="" name="email">
			<div class="desc">key: email</div>
		   </div>
		</div>

	   <div class="form_row">
		<div class="form_label">Image Name</div>
		<div class="form_field">
		   <input type="text" name="image_name" value="" />
		   <div class="desc">key: image_name</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Device Token</div>
		<div class="form_field">
		   <input type="text" name="device_token" value="" />
		   <div class="desc">key: device_token</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Device Type</div>
		<div class="form_field">
		   <input type="text" name="device_type" value="" />
		   <div class="desc">key: device_type</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">App Version</div>
		<div class="form_field">
		   <input type="text" name="app_version" value="" />
		   <div class="desc">key: app_version</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: none;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

<!-- ############################################### Forgot Password ######################################### -->

	<div id="forgetpassword" class="div_service">
	   <?php echo form_open('webservice/forgetpassword');?>
	   <h3>Forget Password</h3>
	   <div class="form_row">
		<div class="form_label">* Email</div>
		<div class="form_field">
		   <input type="text" name="email" value="" />
		   <div class="desc">key: email </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: none;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

<!-----------------####################get country code############################---------------->
	<div id="getCountryCode" class="div_service">
	   <?php echo form_open('webservice/getCountryCode');?>
	   <h3>Get Country Code</h3>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!--   ################ Save Chat #################### -->
        <div id="save_chat" class="div_service">
	   <?php echo form_open_multipart('webservice/save_chat',array('id'=>'savechatform'));?>
                <h3>Save Chat</h3>
                 <div class="form_row">
                    <div class="form_label">From ID</div>
                        <div class="form_field">
                           <input type="text" name="from_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">To ID</div>
                        <div class="form_field">
                           <input type="text" name="to_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Message</div>
                        <div class="form_field">
                           <input type="text" name="message" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">File</div>
                        <div class="form_field">
                          <input type="file" name="chat_files">
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Video Thumb</div>
                        <div class="form_field">
                          <input type="file" name="video_thumb">
                            <div class="desc"></div>
                        </div>
                </div>
		  <div class="form_row">
                    <div class="form_label">Product Id</div>
                        <div class="form_field">
                           <input type="text" name="product_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
		  <div class="form_row">
                    <div class="form_label">Chat Type</div>
                        <div class="form_field">

			   <select name="chat_type">
			      <option value="product">product</option>
			      <option value="request">request</option>
			      <option value="shopowner">shopowner</option>
			   </select>
                            <div class="desc"></div>
                        </div>
                </div>
		   <div class="form_row">
                    <div class="form_label">Deal Status</div>
                        <div class="form_field">
                           <input type="text" name="deal_status" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Debug Mode</div>
                        <div class="form_field">
                           <input type="text" name="mode" value="" />
                            <div class="desc">key: mode <br />values: debug</div>
                        </div>
                </div>
                <div class="form_row" style="display: block;">
                    <div class="form_label">Show Html</div>
                    <div class="form_field">
                       <input type="text" name="showHtml" value="yes" />
                        <div class="desc">key: showHtml<br /> values: yes / no</div>
                       </div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
                </div>
            <?php echo form_close(); ?>
        </div>
<!--   ####### END HERE ###### -->

<!--   ################ Get Chat History #################### -->
        <div id="get_chat_history_mobile" class="div_service">
	   <?php echo form_open('webservice/get_chat_history_mobile',array('id'=>'chathistoryform'));?>
                <h3>Get Chat History</h3>
                 <div class="form_row">
                    <div class="form_label">From ID</div>
                        <div class="form_field">
                           <input type="text" name="from_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">To ID</div>
                        <div class="form_field">
                           <input type="text" name="to_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
				<div class="form_row">
                    <div class="form_label">Product ID</div>
                        <div class="form_field">
                           <input type="text" name="product_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
		<div class="form_row">
                    <div class="form_label">Chat Type</div>
                        <div class="form_field">

			   <select name="chat_type">
			      <option value="product">product</option>
			      <option value="request">request</option>
			      <option value="shopowner">shopowner</option>
			      <option value="all">all</option>
			   </select>
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lat</div>
                        <div class="form_field">
                           <input type="text" name="lat" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Lang</div>
                        <div class="form_field">
                           <input type="text" name="lang" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Chat Id</div>
                        <div class="form_field">
                           <input type="text" name="id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Section</div>
                        <div class="form_field">
                           <select name="section">
                            <option value="recentChat">Recent Chat</option>
                            <option value="chathistory">Chat History</option>
                            <option value="friendList">Friend List</option>
                           </select>
                            <div class="desc"></div>
                        </div>
                </div>
	       <div class="form_row">
	       <div class="form_label">* Page Number</div>
	       <div class="form_field">
		  <input type="text" name="page_no" value="" />
		  <div class="desc">key: page_no </div>
	       </div>
	       </div>
                <div class="form_row">
                    <div class="form_label">Debug Mode</div>
                        <div class="form_field">
                           <input type="text" name="mode" value="" />
                            <div class="desc">key: mode <br />values: debug</div>
                        </div>
                </div>
                <div class="form_row" style="display: block;">
                    <div class="form_label">Show Html</div>
                    <div class="form_field">
                       <input type="text" name="showHtml" value="yes" />
                        <div class="desc">key: showHtml<br /> values: yes / no</div>
                       </div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
                </div>
            <?php echo form_close(); ?>
        </div>
<!--   ####### END HERE ###### -->

<!--   ################ Get Chat History #################### -->
        <div id="delete_chat_history" class="div_service">
	   <?php echo form_open('webservice/delete_chat_history',array('id'=>'deletechathistoryform'));?>
                <h3>Delete Chat History</h3>
                 <div class="form_row">
                    <div class="form_label">From ID*</div>
                        <div class="form_field">
                           <input type="text" name="from_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">To ID*</div>
                        <div class="form_field">
                           <input type="text" name="to_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Product Id*</div>
                        <div class="form_field">
                           <input type="text" name="product_id" value="" />
                            <div class="desc"></div>
                        </div>
                </div>
	       <div class="form_row">
                    <div class="form_label">Chat Type</div>
                        <div class="form_field">

			   <select name="chat_type">
			      <option value="product">product</option>
			      <option value="request">request</option>
			      <option value="shopowner">shopowner</option>
			   </select>
                            <div class="desc"></div>
                        </div>
                </div>
                <div class="form_row">
                    <div class="form_label">Debug Mode</div>
                        <div class="form_field">
                           <input type="text" name="mode" value="" />
                            <div class="desc">key: mode <br />values: debug</div>
                        </div>
                </div>
                <div class="form_row" style="display: block;">
                    <div class="form_label">Show Html</div>
                    <div class="form_field">
                       <input type="text" name="showHtml" value="yes" />
                        <div class="desc">key: showHtml<br /> values: yes / no</div>
                       </div>
                </div>
                <div class="form_row">
                    <div class="form_label">&nbsp;</div>
                    <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
                </div>
            <?php echo form_close(); ?>
        </div>
<!--
<!-- ############################################### Category listing ######################################### -->

	<div id="category_list" class="div_service">
	   <?php echo form_open('webservice/getAllCategories');?>
	   <h3>Get All Catgeory Listing</h3>
	   <div class="form_row">
		<div class="form_label"> Category Status</div>
		<div class="form_field">
		   <select name="category_status">
			<option value="">Select Status</option>
			<option value="Active">Active</option>
			<option value="Inactive">Inactive</option>
		   </select>
		   <div class="desc">key: category_status </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Page Number</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-- ############################################### product/shop listing by category ######################################### -->

	<div id="category_list2" class="div_service">
	   <?php echo form_open('webservice/shopProductListByCategory');?>
	   <h3>Get All Shop/Product Listing By Category</h3>
	   <div class="form_row">
		<div class="form_label">* Category Id</div>
		<div class="form_field">
		   <input type="text" name="id_category" value="" />
		   <div class="desc">key: id_category </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* List For</div>
		<div class="form_field">
		   <select name="list_type">
			<option value="">Select Status</option>
			<option value="shop">shop</option>
			<option value="product">product</option>
		   </select>
		   <div class="desc">key: list_type </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Page Number</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-- ############################################### post request ######################################### -->

	<div id="post_request" class="div_service">
	   <?php echo form_open_multipart('webservice/postConsumerRequest');?>
	   <h3>Post Request</h3>
	   <div class="form_row">
		<div class="form_label">* Request Id</div>
		<div class="form_field">
		   <input type="text" name="request_id" value="" />
		   <div class="desc">key: request_id (For Updating Provide request Id) </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* User Id{logged in}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Title</div>
		<div class="form_field">
		   <input type="text" name="title" value="" />
		   <div class="desc">key: title </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Description</div>
		<div class="form_field">
		   <input type="text" name="description" value="" />
		   <div class="desc">key: description </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Price</div>
		<div class="form_field">
		   <input type="text" name="price" value="" />
		   <div class="desc">key: price </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Product Id (not mandatory for open request)</div>
		<div class="form_field">
		   <input type="text" name="product_id" value="" />
		   <div class="desc">key: product_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Category Id</div>
		<div class="form_field">
		   <input type="text" name="id_category" value="" />
		   <div class="desc">key: id_category </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Shop Id(not mandatory for open request)</div>
		<div class="form_field">
		   <input type="text" name="shop_id" value="" />
		   <div class="desc">key: shop_id </div>
		</div>
	   </div>
	   <div class="form_row">
		   <div class="form_label">* Image Name</div>
		   <div class="form_field">
			<input type="file" name="request_image" size="20" />
			<div class="desc">key: request_image</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">* Image Name</div>
		   <div class="form_field">
			<input type="text" name="request_image" size="20" />
			<div class="desc">key: request_image</div>
		   </div>
		</div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------####################Request List############################---------------->
	<div id="request_list" class="div_service">
	   <?php echo form_open('webservice/getRequestList');?>
	   <h3>Request List</h3>
	   <div class="form_row">
		<div class="form_label">* User Id{logged in}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Only Close Request</div>
		<div class="form_field">
		   <input type="text" name="close" value="" />
		   <div class="desc">key: close <br />values: close for Shopowner</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label"> Category Id</div>
		<div class="form_field">
		   <input type="text" name="id_category" value="" />
		   <div class="desc">key: id_category <br/> for Shopowner</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Page No</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------####################Request List############################---------------->
	<div id="category_search" class="div_service">
	   <?php echo form_open('webservice/getCategorySearch');?>
	   <h3>Catgeory Search</h3>
	   <div class="form_row">
		<div class="form_label">* User Id{logged in}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Category Id{mandatory for search within a category}</div>
		<div class="form_field">
		   <input type="text" name="id_category" value="" />
		   <div class="desc">key: id_category</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Search Type</div>
		<div class="form_field">
		   <select name="search_type">
			<option value="">Select Type</option>
			<option value="cunsumers">cunsumers</option>
			<option value="category">category</option>
			<option value="category_shop">category_shop</option>
			<option value="category_product">category_product</option>
			<option value="category_request">category_request</option>
		   </select>
		   <div class="desc">key: search_type </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label"> Search Text</div>
		<div class="form_field">
		   <input type="text" name="search_text" value="" />
		   <div class="desc">key: search_text</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label"> Filter Type .</div>
		<div class="form_field">
		        <select name="filter_type">
			<option value="">Filter Type</option>
			<option value="latest">latest</option>
			<option value="most_popular">most_popular</option>
			<option value="highest_ratings">highest_ratings</option>
			<option value="most_products">most_products</option>
			<option value="most_followers">most_followers</option>
		        </select>
		   <div class="desc">key: filter_type </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Page No</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Static page ############################---------------->
	<div id="staticpage" class="div_service">
	   <?php echo form_open('webservice/staticpage');?>
	   <h3>Static Page Detail </h3>
	   <!--<div class="form_row">
		<div class="form_label">* User Id{logged in}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">* Static Page Variable Name</div>
		<div class="form_field">
		   <select name="variable_name">
			<option value="about_us">about_us</option>
			<option value="privacy_policy">privacy_policy</option>
			<option value="terms_of_services">terms_of_services</option>
		   </select>
		   <div class="desc">key: variable_name</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Static page ############################---------------->
	<div id="chat_read" class="div_service">
	   <?php echo form_open('webservice/readUnreadMessages');?>
	   <h3>Read Chat messages </h3>
	   <!--<div class="form_row">
		<div class="form_label">* User Id{logged in}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">*From id</div>
		<div class="form_field">
		   <input type="text" name="from_id" value="" />
		   <div class="desc">key: from_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*To id{logged in}</div>
		<div class="form_field">
		   <input type="text" name="to_id" value="" />
		   <div class="desc">key: to_id </div>
		</div>
	   </div>
	    <div class="form_row">
		<div class="form_label">*Product id</div>
		<div class="form_field">
		   <input type="text" name="product_id" value="" />
		   <div class="desc">key: product_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Chat id</div>
		<div class="form_field">
		   <input type="text" name="chat_id" value="" />
		   <div class="desc">key: chat_id </div>
		</div>
	   </div>
	    <div class="form_row">
                    <div class="form_label">Chat Type</div>
                        <div class="form_field">

			   <select name="chat_type">
			      <option value="product">product</option>
			      <option value="request">request</option>
			      <option value="shopowner">shopowner</option>
			   </select>
                            <div class="desc"></div>
                        </div>
            </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### unread_badge ############################---------------->
	<div id="unread_badge" class="div_service">
	   <?php echo form_open('webservice/getUnreadBadgeCount');?>
	   <h3>Unread Badge Count Of User</h3>
	   <div class="form_row">
		<div class="form_label">*To id {logged user id}</div>
		<div class="form_field">
		   <input type="text" name="to_id" value="" />
		   <div class="desc">key: to_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### unread_badge ############################---------------->
	<div id="read_notifications" class="div_service">
	   <?php echo form_open('webservice/readNotificationList');?>
	   <h3>Read All notifications for notification list</h3>
	   <div class="form_row">
		<div class="form_label">*To id {logged user id}</div>
		<div class="form_field">
		   <input type="text" name="to_id" value="" />
		   <div class="desc">key: to_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Reply To request############################---------------->
	<div id="request_reply" class="div_service">
	   <?php echo form_open('webservice/replyToRequest');?>
	   <h3>Add Comment on request/product</h3>
	   <!--<div class="form_row">
		<div class="form_label">*Reply To</div>
		<div class="form_field">
		   <input type="text" name="reply_to" value="" />
		   <div class="desc">key: reply_to </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">*Reply From {logged user} </div>
		<div class="form_field">
		   <input type="text" name="reply_from" value="" />
		   <div class="desc">key: reply_from </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Request ID</div>
		<div class="form_field">
		   <input type="text" name="id_request" value="" />
		   <div class="desc">key: id_request </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Reply Content</div>
		<div class="form_field">
		   <input type="text" name="reply_content" value="" />
		   <div class="desc">key: reply_content </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Type {request,product}</div>
		<div class="form_field">
		   <input type="text" name="type" value="request" />
		   <div class="desc">key: type </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Get Request Details############################---------------->
	<div id="request_details" class="div_service">
	   <?php echo form_open('webservice/getRequestDetails');?>
	   <h3>Get user details</h3>
	   <div class="form_row">
		<div class="form_label">*User id {logged user id}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Request id </div>
		<div class="form_field">
		   <input type="text" name="request_id" value="" />
		   <div class="desc">key: request_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Get Request Conversation############################---------------->
	<div id="request_conversation" class="div_service">
	   <?php echo form_open('webservice/getRequestConversation');?>
	   <h3>Get Request Conversation</h3>
	   <!--<div class="form_row">
		<div class="form_label">*Reply From {logged user id}</div>
		<div class="form_field">
		   <input type="text" name="reply_from" value="" />
		   <div class="desc">key: reply_from </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">*Request id </div>
		<div class="form_field">
		   <input type="text" name="request_id" value="" />
		   <div class="desc">key: request_id </div>
		</div>
	   </div>
	   <!--<div class="form_row">
		<div class="form_label">*Reply To </div>
		<div class="form_field">
		   <input type="text" name="reply_to" value="" />
		   <div class="desc">key: reply_to </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">Reply Id </div>
		<div class="form_field">
		   <input type="text" name="id_reply" value="" />
		   <div class="desc">key: id_reply </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">List Type </div>
		<div class="form_field">
		   <input type="text" name="list_type" value="" />
		   <div class="desc">key: list_type (old/new=> in case of loading messages) </div>
		</div>
	   </div>
	   <!--<div class="form_row">
		<div class="form_label">*page_no </div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Get Request Delete############################---------------->
	<div id="request_delete" class="div_service">
	   <?php echo form_open('webservice/deleteConsumerRequest');?>
	   <h3>Get Request Delete</h3>
	   <div class="form_row">
		<div class="form_label">*User id {logged user id}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Request id </div>
		<div class="form_field">
		   <input type="text" name="request_id" value="" />
		   <div class="desc">key: request_id </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------####################Logout############################---------------->
	<div id="logout" class="div_service">
	   <?php echo form_open('webservice/logout');?>
	   <h3>Logout</h3>
	   <div class="form_row">
		<div class="form_label">* User Id</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

	<!-----------------####################Logout############################---------------->
	<div id="request_follow_unfollow" class="div_service">
	   <?php echo form_open('webservice/requestfollow');?>
	   <h3>Request Follow/Unfollow</h3>
	   <div class="form_row">
		<div class="form_label">* Request Id</div>
		<div class="form_field">
		   <input type="text" name="request_id" value="" />
		   <div class="desc">key: request_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* User Id</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		   <div class="form_label">* Type</div>
		   <div class="form_field">
			<select name="type">
			   <option value="follow">follow</option>
			   <option value="unfollow">unfollow</option>
			</select>
			<div class="desc">key: type </div>
		   </div>
		</div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

   <!-----------------####################Logout############################---------------->
	<div id="request_followers" class="div_service">
	   <?php echo form_open('webservice/requestfollowers');?>
	   <h3>Request Followers</h3>
	   <div class="form_row">
		<div class="form_label">* User Id</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Page Number</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

	<!-----------------####################Logout############################---------------->
	<div id="wishlist_add_delete" class="div_service">
	   <?php echo form_open('webservice/wishlist_add_delete');?>
	   <h3>Wishlist Add/Delete</h3>
	   <div class="form_row">
		<div class="form_label">* Product Id</div>
		<div class="form_field">
		   <input type="text" name="product_id" value="" />
		   <div class="desc">key: product_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* User Id</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		   <div class="form_label">* Type</div>
		   <div class="form_field">
			<select name="type">
			   <option value="add">add</option>
			   <option value="delete">delete</option>
			</select>
			<div class="desc">key: type </div>
		   </div>
		</div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

	<!-----------------####################Logout############################---------------->
	<div id="wishlist_products" class="div_service">
	   <?php echo form_open('webservice/wishlist_products');?>
	   <h3>Get Wishlist Products</h3>
	   <div class="form_row">
		<div class="form_label">* User Id</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Page Number</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>

	<!-----------------####################Logout############################---------------->
	<div id="wishlist_status" class="div_service">
	   <?php echo form_open('webservice/wishlist_status');?>
	   <h3>Wishlist Add/Delete</h3>
	   <div class="form_row">
		<div class="form_label">* Product Id</div>
		<div class="form_field">
		   <input type="text" name="product_id" value="" />
		   <div class="desc">key: product_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* User Id</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>


<!-----------------#################### Product Featured and UnFeatured ############################---------------->
	<div id="featured_unfeatured" class="div_service">
	   <?php echo form_open('webservice/product_featured');?>
	   <h3>Product Featured and Unfeatured</h3>
	   <div class="form_row">
		<div class="form_label">*User id {logged user id shopowner}</div>
		<div class="form_field">
		   <input type="text" name="shop_id" value="" />
		   <div class="desc">key: shop_id </div>
		</div>
	   </div>

	   <div class="form_row">
		<div class="form_label">* Product Id</div>
		<div class="form_field">
		   <input type="text" name="product_id" value="" />
		   <div class="desc">key: product_id</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">* Featured</div>
		<div class="form_field">
		   <select name="featured">
			   <option value="Yes">Yes</option>
			   <option value="No">No</option>
			</select>
		   <div class="desc">key: featured (Yes/No)</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>


<!-- ############################################### Shop Product listing ######################################### -->

<!-- ############################################### Bought Listing ######################################### -->

	<div id="boughtListing" class="div_service">
	   <?php echo form_open('webservice/bought_listing');?>
	   <h3>Shop Product View</h3>
	  <!-- <div class="form_row">
		<div class="form_label">* Shop Product Id</div>
		<div class="form_field">
		   <input type="text" name="id" value="" />
		   <div class="desc">key: id </div>
		</div>
	   </div>
	  -->
	   <div class="form_row">
		<div class="form_label">* User Id {loggedin user}</div>
		<div class="form_field">
		   <input type="text" name="user_id" value="" />
		   <div class="desc">key: user_id </div>
		</div>
	   </div>
	   <!-- <div class="form_row">
		<div class="form_label">* To Id</div>
		<div class="form_field">
		   <input type="text" name="to_id" value="" />
		   <div class="desc">key: to_id </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: none;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
<!-- ############################################### Bought End ######################################### -->

<!-----------------#################### Update reply comment ############################---------------->
	<div id="update_reply_comment" class="div_service">
	   <?php echo form_open('webservice/updateReplyComment');?>
	   <h3>Update Comment on request/product</h3>

	   <div class="form_row">
		<div class="form_label">*Reply From {logged user} </div>
		<div class="form_field">
		   <input type="text" name="reply_from" value="" />
		   <div class="desc">key: reply_from </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Comment Id</div>
		<div class="form_field">
		   <input type="text" name="id_reply" value="" />
		   <div class="desc">key: id_reply </div>
		</div>
	   </div>
	   <!--<div class="form_row">
		<div class="form_label">*Request ID</div>
		<div class="form_field">
		   <input type="text" name="id_request" value="" />
		   <div class="desc">key: id_request </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">*Reply Content</div>
		<div class="form_field">
		   <input type="text" name="reply_content" value="" />
		   <div class="desc">key: reply_content </div>
		</div>
	   </div>
	  <!-- <div class="form_row">
		<div class="form_label">*Type {request,product}</div>
		<div class="form_field">
		   <input type="text" name="type" value="request" />
		   <div class="desc">key: type </div>
		</div>
	   </div>-->
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!--end-->
	<!-----------------#################### Delete reply comment ############################---------------->
	<div id="delete_reply_comment" class="div_service">
	   <?php echo form_open('webservice/deleteReplyComment');?>
	   <h3>Delete Comment on request/product</h3>

	   <div class="form_row">
		<div class="form_label">*Reply From {logged user} </div>
		<div class="form_field">
		   <input type="text" name="reply_from" value="" />
		   <div class="desc">key: reply_from </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Comment Id</div>
		<div class="form_field">
		   <input type="text" name="id_reply" value="" />
		   <div class="desc">key: id_reply </div>
		</div>
	   </div>

	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!--end-->
<!-----------------#################### get reply comment ############################---------------->
	<?php /* ?>
	<div id="getreplyComments class="div_service">
	   <?php echo form_open('webservice/getreplyComments');?>
	   <h3>Get Reply Comments</h3>

	   <div class="form_row">
		<div class="form_label">* Reply From {loggedin userid} </div>
		<div class="form_field">
		   <input type="text" name="reply_from" value="" />
		   <div class="desc">key: reply_from </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Request ID (Request Id/ Product Id)</div>
		<div class="form_field">
		   <input type="text" name="id_request" value="" />
		   <div class="desc">key: id_request </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">*Type {request,product}</div>
		<div class="form_field">
		   <input type="text" name="type" value="request" />
		   <div class="desc">key: type </div>
		</div>
	   </div>
	    <div class="form_row">
		<div class="form_label">* Page Number</div>
		<div class="form_field">
		   <input type="text" name="page_no" value="" />
		   <div class="desc">key: page_no </div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<?php */ ?>
<!-----------------#################### get reply comment end ############################---------------->

<!-----------------#################### Adv Plan ############################---------------->
	<div id="advplan" class="div_service">
	   <?php echo form_open('webservice/advplan');?>
	   <h3>Get Adv Plan </h3>

	   <div class="form_row">
		<div class="form_label">* Adv Plan Type</div>
		<div class="form_field">
		   <select name="variable_name">
		        <option value="banner">banner</option>
			<option value="showproduct">showproduct</option>
			<option value="showprofile">showprofile</option>
		   </select>
		   <div class="desc">key: variable_name</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">Debug Mode</div>
		<div class="form_field">
		   <input type="text" name="mode" value="" />
		   <div class="desc">key: mode <br />values: debug</div>
		</div>
	   </div>
	   <div class="form_row" style="display: block;">
		<div class="form_label">Show Html</div>
		<div class="form_field">
		   <input type="text" name="showHtml" value="yes" />
		   <div class="desc">key: showHtml<br /> values: yes / no</div>
		</div>
	   </div>
	   <div class="form_row">
		<div class="form_label">&nbsp;</div>
		<div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
	   </div>
	   <?php echo form_close(); ?>
	</div>
	<!-----------------#################### Adv Plan ############################---------------->

	<!--############################################# Update Time zone #######################################################-->
	<div id="updateUserTimeZone" class="div_service">
		<?php echo form_open_multipart('webservice/updateUserTimeZone');?>
		<h3>Update Time zone</h3>
		<div class="form_row">
		   <div class="form_label">* User Id</div>
		   <div class="form_field">
			<input type="text" name="user_id" value="" />
			<div class="desc">key: user_id</div>
		   </div>
		</div>

		<div class="form_row">
		   <div class="form_label">Debug Mode</div>
		   <div class="form_field">
			<input type="text" name="mode" value="" />
			<div class="desc">key: mode <br />values: debug</div>
		   </div>
		</div>
		<div class="form_row" style="display: block;">
		   <div class="form_label">Show Html</div>
		   <div class="form_field">
			<input type="text" name="showHtml" value="yes" />
			<div class="desc">key: showHtml<br /> values: yes / no</div>
		   </div>
		</div>
		<div class="form_row">
		   <div class="form_label">&nbsp;</div>
		   <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
		</div>
	   <?php echo form_close(); ?>
	</div>
        <!-- ------------- end update time zone -->
</div>

<!--
*****************************************************************************************************************************************
************************************************* M O D U L E - 2   W E B    S E R V I C E S ********************************************
*****************************************************************************************************************************************
-->

<!--                <h2 id="module2">Warehouse services Module 2</h2>
                <ol type="1" class="list_1">
                        <li><a href="javascript:void(0);" data-href="#get_friend_list">Get Friend List</a><p>Service URL: <?php //echo 'http://'.$_SERVER['HTTP_HOST'].'/warehouse/webservice/' ?>get_friend_list</p></li>
                </ol>-->

<!-- ############################################### Get My Friend List List ####################################################### -->

<!--                <div id="get_friend_list" class="div_service">
                        <form action="webservice/get_friend_list" method="post" enctype="multipart/form-data">
                                <h3>Get Friend List</h3>
                                <div class="form_row">
                                        <div class="form_label">User ID</div>
                                        <div class="form_field">
                                                <input type="text" name="u_id" value="" />
                                                <div class="desc">key: u_id</div>
                                        </div>
                                </div>
                                <div class="form_row">
                                        <div class="form_label">Debug Mode</div>
                                        <div class="form_field">
                                                <input type="text" name="mode" value="" />
                                                <div class="desc">key: mode <br />values: debug</div>
                                        </div>
                                </div>
                                <div class="form_row" style="display: none;">
                                        <div class="form_label">Show Html</div>
                                        <div class="form_field">
                                                <input type="text" name="showHtml" value="yes" />
                                                <div class="desc">key: showHtml<br /> values: yes / no</div>
                                        </div>
                                </div>
                                <div class="form_row">
                                        <div class="form_label">&nbsp;</div>
                                        <div class="form_field"><input type="submit" name="submit" value="Submit" /></div>
                                </div>
                        </form>
                </div>-->
        </body>
</html>
