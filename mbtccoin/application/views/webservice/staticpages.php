<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TFCS</title> 
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>    
  </head>
  <!-- CSS Styling -->
  <style type="text/css">
    .wrapper { font-family: 'Lato', sans-serif; }
    .container { padding: 0 20px; }
    .logo h1 { margin-bottom: 0; text-align: center; }
    .sub-head { padding: 10px 0 15px; }
    .sub-head h2 { color: #008aad; font-size: 27px; font-weight: 700; margin: 0; }
    .about-text p { color: #1c1c1c; font-size: 15px; font-weight: 400; margin: 0 0 8px; line-height: 170%;}
    .about-text ul { margin-bottom: 35px; }
    .about-text li { font-size: 15px; padding-bottom: 7px; }
    .about-images ul { margin: 0; padding: 0; white-space: nowrap; }
    .about-images li { display: inline-block; width: 19.5%; }
    .about-images li.last-img { width: 19%; }
    .about-images img { max-width: 100%; }


  </style>

  <body>
    <!-- AboutUs Section Start -->
    <div class="wrapper">
      <div class="container">
        <div class="logo">
          <h1><a href="#"><img src="<?php echo base_url(); ?>lib/images/logo01.png" style="height:150px"/></a></h1>
        </div>
        <div class="sub-head">
          <h2><?php echo $content_title;?></h2>
        </div>
        <div class="about-text">
		   <?php if($content)
		   {
         echo $content;
		   }?>
        </div>
      </div>
      <div class="about-images">
        
      </div>
    </div>
  <!-- AboutUs Section End -->      
  </body>
</html>
