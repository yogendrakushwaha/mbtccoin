<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TFCS</title> 
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>    
  </head>
  <!-- CSS Styling -->
  <style type="text/css">
    .wrapper { font-family: 'Lato', sans-serif; }
    .container { padding: 0 20px; }
    .logo h1 { margin-bottom: 0; text-align: center; }
    .sub-head { padding: 10px 0 15px; }
    .sub-head h2 { color: #008aad; font-size: 27px; font-weight: 700; margin: 0; }
    .about-text p { color: #1c1c1c; font-size: 15px; font-weight: 400; margin: 0 0 8px;font-family: Arial, Helvetica, sans-serif; }
    .about-text ul { margin-bottom: 35px;font-family: Arial, Helvetica, sans-serif; }
    .about-text li { font-size: 15px; padding-bottom: 7px;font-family: Arial, Helvetica, sans-serif; }
    .about-images ul { margin: 0; padding: 0; white-space: nowrap; }
    .about-images li { display: inline-block; width: 20%; }
    .about-images li.last-img { width: 17%; }
    .about-images img { max-width: 100%; }


  </style>
  <style>
 
   .y
   {
	margin:  0px;
	text-align: center;
   }
</style>
<?php if($content) {?>
   <div><?php echo $content;?></div>
<?php }?>
  <body>
    <!-- AboutUs Section Start -->
    <div class="wrapper">
      <div class="container">
        <div class="logo">
          <h1><a href="#"><img src="<?php echo base_url(); ?>lib/images/logo01.png" /></a></h1>
        </div>
        <div class="sub-head">
          <h2>Lorem ipsum dolor sit amet.</h2>
        </div>
        <div class="about-text">
          <p>Lorem ipsum dolor sit amet, consectetur krhab adipiscing elit. Mauris sed orci finibus, varius leo non, hendrerit sem. Mauris sed orci finibus, varius leo non, hendrerit sem. Mauris veldaxsdw maximus quam, eget tempor nisi. Etiam sit amet nibh egestas ligula varius faucibus. Ut sed dcsd sagittis nibh, et bibendum ipsu condimentum semper sollicitudin. In hac habitasse asdictumst. Etiam vel augue dolor. Quisque dxkmxysxnk ere pulvinarvulputate risus quis efficitur.</p>
          <ul>
            <li>Cras vel vestibulum </li>
            <li>Mante sapienlectus.</li>
            <li>Ras Svestibulum lectus.</li>
          </ul>
        </div>
      </div>
      <div class="about-images">
        <ul>
          <li><img src="images/about-img01.png" /></li>
          <li><img src="images/about-img02.png" /></li>
          <li><img src="images/about-img03.png" /></li>
          <li><img src="images/about-img04.png" /></li>
          <li class="last-img"><img src="images/about-img05.png" /></li>
        </ul>
      </div>
    </div>
  <!-- AboutUs Section End -->      
  </body>
</html>
