<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />

		<title><?php echo SITE_NAME; ?></title>
		<meta name="description" content="<?php echo COPYRIGHT; ?>" />
		<meta name="keywords" content="<?php echo COPYRIGHT; ?>"/>
		<meta name="robots" content="index, follow">
		<meta name="copyright" content="<?php echo COPYRIGHT; ?>" />
		<meta name="author" content="<?php echo "TECHAARJAVAM"; ?>" />
		<meta name="email" content="<?php echo EMAIL; ?>" />

		<link rel="stylesheet" href="<?php echo base_url();?>libs/assets/css/angular-material.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>libs/assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url();?>libs/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>libs/assets/css/lf-ng-md-file-input.min.css">
		<link href="https://fonts.googleapis.com/css?family=Merienda+One" rel="stylesheet">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="<?php echo base_url();?>libs/assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>libs/assets/css/ionicons.min.css">
		<link rel='stylesheet' href="<?php echo base_url();?>libs/assets/css/loading-bar.min.css" type="text/css" media='all' />
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">


		<script>
			var facebook_link 	= '<?php echo FACEBOOKPAGE; ?>';
			var twitter_link 	  = '<?php echo TWITTER; ?>';
			var gplus_link 		  = '<?php echo GPLUS; ?>';
			var linkedin_link	  = '<?php echo LINKEDIN; ?>';
			var copyright_link	= '<?php echo COPYRIGHT; ?>';
			var SITE_NAME 		  = '<?php echo SITE_NAME; ?>';
			var BASE_URL       	='<?php echo  BASE_URL; ?>';
			var EMAIL 			    ='<?php echo  EMAIL; ?>';
			var CONTACT_NO	   	='<?php echo CONTACTNO; ?>';
			var DEFAULT_PAGE    ='<?php echo DEFAULT_PAGE; ?>';
		</script>
	</head>

	<body ng-app="app" ng-cloak  ng-controller="AppCtrl">
		<div layout="column">
			<md-toolbar class="md-theme-indigo header fixed">
				<!--- navbar start -->
				<div ui-view="navbar"></div>
				<!--- navbar end -->
			</md-toolbar>
			<section layout="row" flex>
				<!-- Overlay for fixed sidebar -->
				<div class="sidebar-overlay"></div>
				<!-- Material sidebar Start -->
				<div ui-view="sidebar"></div>
				<md-content flex layout-padding class="page-content">
					<!--- Body Area Start -->
					<div ui-view="main" class="main-view"></div>
					<!--- Body Area Endt -->
					<!--- Footer Start -->
					<div class="devider"></div>
					<div class="devider"></div>
					<div class="devider"></div> 
					<div class="footer">
						<strong><?php echo COPYRIGHT; ?></strong>.
					</div>
					<!--- Footer End -->
				</md-content>
			</section>
		</div>
		<!-- jQuery -->

		<script type='text/javascript'  src="<?php echo base_url();?>libs/assets/js/jquery.min.js"></script>
		<!-- Angular Material requires Angular.js Libraries -->
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-messages.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-animate.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-aria.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-messages.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/ocLazyLoad.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-ui-router.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/loading-bar.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-storage.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-jwt.min.js"></script>

		<!-- Ui bootstrap -->
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/ui-bootstrap.js"></script>
		<script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-114/svg-assets-cache.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/angular-material.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/lf-ng-md-file-input.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/bootstrap.min.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>libs/assets/js/main.js"></script>
		<script type='text/javascript' src="<?php echo base_url();?>uib/template/alert/alert.html.js"></script>

		<script type='text/javascript' src="app/script/app.js"></script>
		<script type='text/javascript' src="app/script/lazyload.js"></script>
		<script type='text/javascript' src="app/script/ui-router.js"></script>

		<!-- Controllers -->
		<script type='text/javascript' src="app/script/controller/appCtrl.js"></script>
		<script type='text/javascript' src="app/script/controller/configCtrl.js"></script>
		<script type='text/javascript' src="app/script/controller/loginCtrl.js"></script>
		<script type='text/javascript' src="app/script/controller/catalogCtrl.js"></script>
		<script type='text/javascript' src="app/script/controller/user.js"></script>
		<script type='text/javascript' src="app/script/controller/actionCtrl.js"></script>

		<!-- services -->
		<script type='text/javascript' src="app/script/service/masterService.js"></script>
		<script type='text/javascript' src="app/script/service/loginService.js"></script>
		<script type='text/javascript' src="app/script/service/signupService.js"></script>
		<script type='text/javascript' src="app/script/service/userService.js"></script>
		<script type='text/javascript' src="app/script/service/actionService.js"></script>
		<!-- directive -->
		<script type='text/javascript' src="app/script/directives/compare-to.js"></script>
	</body>
</html>