<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SignupWebService_model extends CI_Model
{
   /**
    *@category 	model
    *@package 	application_models
    *@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
    *@version 	0.0.1
    *dated  	2016-12-08
    */

   public function __construct() {
	  	$this->load->model('UserprofileWebService_model','Userprofile');
   }
   /**
    *get all the configurations by key
    *@param string $key
    *$return string
    */
   public function _getConfigurationByKey($key ="")
   {
	if($key !=""){
	   $this->db->select('m00_value');
	   $query = $this->db->get_where('m00_setconfig',array('m00_name'=>$key));
	   $data_array = $query->row_array();
	   return $data_array['m00_value'];
	}else{
	   $query = $this->db->get('m00_setconfig');
	   $data_array = $query->result_array();
	   return $data_array;
	}

   }
   
    public function _getMessage($opt)
   {
	   $this->db->select('m09_msg_subject , m09_msg_content ' );
	   $this->db->where('m09_msg_title', $opt);
	   $q = $this->db->get('m09_message_config');

	   if($q->num_rows() > 0)
	   {
		 return $q->row();
	   }
	   return array();
   }
   
  /**
    *This function is used to add/update the user details
    *@param array $posted data
    *@return int val
    */
   public function create_user($insertArray)
   {
	if(isset($insertArray['user_id']))
	{
	   $user_id = $insertArray['user_id'];
	   unset($insertArray['user_id']);
	   $query = $this->db->update('m01_user', $insertArray, array('m11_user_id'=>$user_id));
	   return  $query;
	}
	else
	{
	    $delete_user = $this->db->delete("m11_user",array('m11_user_contactno'=> $insertArray['Mobile'], 'm11_user_status ='=>'Pending'));
	    $delete_login = $this->db->delete("tr04_login",array('m11_user_contactno'=> $insertArray['Mobile']));
		$user_data = $this->Userprofile->_getUser_by_mobileno($insertArray['Referral']);
		$insertArray['introid']=$user_data->RegId;
               
	   $user=array(
			//'proc'=>2,
			'm11_first_name'=>$insertArray['FirstName'],
			'm11_last_name'=>$insertArray['LastName'],
			'm11_user_name'=>$insertArray['FirstName']." ".$insertArray['LastName'],
			'm11_user_email'=>$insertArray['Email'],
			'm11_user_contactno'=>$insertArray['Mobile'],
			'm11_user_location'=>$insertArray['City'],
			'm11_user_image'=>$insertArray['Image_name'],
			'm11_user_joined'=>$insertArray['Created_on'],
			'm11_user_status'=>$insertArray['Status'],
			'm11_user_role'=>'B2cUSER',
			'm11_cancellation_policy'=>$insertArray['Cancellation_policy'],
			'm11_subcription'=>$insertArray['Subscription'],
			'm11_user_referral_code'=>$insertArray['introid'],
			'm11_user_onadd'=>$insertArray['Created_on']
			);
			//$query = " CALL sp_user(?" . str_repeat(",?", count($user)-1) .",@a) ";
			//$data['rec']=$this->db->query($query, $smenu);
			//$this->db->free_db_resource();
			//$data['response']=$this->db->query("SELECT @a as resp");
			$this->db->insert('m11_user', $user);
			$id=$this->db->insert_id();
			$login=array(
			//'proc'=>2,
			'm11_user_id'=>$id,
			'm11_user_contactno'=>$insertArray['Mobile'],
			'm11_user_email'=>$insertArray['Email'],
			'tr04_login_pwd'=>$insertArray['Password'],
			'tr04_pin_pwd'=>$insertArray['Password'],
			'tr04_account_id'=>1,
			'tr04_login_type'=>1
			);
			$this->db->insert('tr04_login', $login);
		 if($insertArray['Role']=="MERCHANT")
		 {
			$sellerinfo=array(
						  'm11_user_id'=>$id,
						  'm13_seller_name'=>$insertArray['FirstName']." ".$insertArray['LastName'],
						  'm13_seller_mobile_no'=>$insertArray['Mobile'],
						  'm13_seller_primary_email'=>$insertArray['Email'],
						  'm13_seller_image'=>$insertArray['Image_name']
						) ;
		
			$this->db->insert('m13_seller_info', $sellerinfo);
		}
			return $id;
			
	}
   }
	/**
    *This function is used to check whether the user email exists or not
    *@param string $email, int $user_id, string $status
    *return boolean
    */
   public function sg_email_exist($u_email,$user_id = 0,$status="")
   {
	if(empty($u_email)){
	   return FALSE;
	}
	else
	{
	   $this->db->select('m11_user_email');
	   $this->db->from('m11_user');
	   $this->db->where('m11_user_email',$u_email);
	   if($status !=""){
		$this->db->where('m11_user_status !=',$status);
	   }
	   if($user_id >0){
		$this->db->where('m11_user_id !=',$user_id);
	   }
	   $q = $this->db->get();
	   if($q->num_rows() > 0)
	   {
		return TRUE;
	   }
	}
	return FALSE;
   }
   
   /**
	   *This function is used to check whether the user email exists or not
    *@param string $email, int $user_id, string $status
    *return boolean
    */
   public function sg_contact_exist($u_email,$user_id = 0,$status="")
   {
	if(empty($u_email))
	{
	   return FALSE;
	}
	else
	{
	   $this->db->select('m11_user_email');
	   $this->db->from('m11_user');
	   $this->db->where('m11_user_contactno',$u_email);
	   if($status !=""){
		$this->db->where('m11_user_status !=',$status);
	   }
	   if($user_id >0){
		$this->db->where('m11_user_id !=',$user_id);
	   }
	   $q = $this->db->get();
	   if($q->num_rows() > 0)
	   {
		return TRUE;
	   }
	}
	return FALSE;
   }
   
/**
	*This function is used to update activation Code
    *@param  int $user_id
    *return status
    */
   public function update_user($id,$updatArray){
	   $this->db->where('m11_user_id', $id);
		$this->message=$this->db->update('m11_user', $updatArray);
		
	   $fundreq=array(
				'proc'=>2,
				'to_user_id'=>$id,
				'from_user_id'=>0,
				'amount'=>floatval(OFFER_TOKEN),
				'requesttype'=>1,
				'balfor'=>1,
				'transtype'=>1,
				'timelimit'=>900,
				'defaultstatus'=>'Success'
				);
				$query = " CALL wallettransfer(?" . str_repeat(",?", count($fundreq)-1) .",@a,@a1)";
				$query_result=$this->db->query($query, $fundreq);
				//mysqli_next_result( $this->db->conn_id );
				$data['response']=$this->db->query("SELECT @a as message")->row()->message;
				$data['status']=$this->db->query("SELECT @a1 as message1")->row()->message1;
				$this->message = $data['response'];
				$this->status = $data['status'];
	 
	  return $this->message;

	}



}
?>