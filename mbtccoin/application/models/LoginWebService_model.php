<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class LoginWebService_model extends CI_Model
	{
		/**
			*@category 	model
			*@package 	application_models
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
		
		public function __construct() {
			
		}
		
		
		public function _isValid_user_adminser($username,$pass)
		{
			$query 		= $this->db->get_where('m00_admin_login',array('`m00_admin_login`.`m00_username`'=>$username,'`m00_admin_login`.`m00_password`'=>$pass));
			$details 	= $query->row();
			if(count($details)==1)
			return $details;
			return FALSE;
		}
/// For Api
		public function _isValid_user_webapiser($username,$pass,$ip)
		{
			$query = $this->db->get_where('tr04_login',array('m11_user_contactno'=>$username,'tr04_login_pwd'=>md5($pass),'tr04_ip'=>$ip));
			$details = $query->row();
			if(count($details)==1)
			return $details;
			return FALSE;
		}

/// For Web 
		public function _isValid_user_webser($username,$pass)
		{
			$query = $this->db->get_where('tr04_login',array('m11_user_email'=>$username,'tr04_login_pwd'=>md5($pass)));
			$details = $query->row();
			if(count($details)==1)
			return $details;
			return FALSE;
		}
		/**
			***************************************************************************
			* Code to get user detail by Mobile/Email.                                 	  **
			* @param $email string email address /Mobile No 				  	  **
			* @return boolean OR array						  **
			* *************************************************************************
		*/
		public function _getUser_by_mobileno($mobile = FALSE)
		{
			if($mobile === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(2,".$mobile.",@msg)")->result();
			mysqli_next_result( $this->db->conn_id );
			$imagepath= base_url().'uploads/user_images/';
			if(!empty($user[0]->MemberImage) && empty($user[0]->MemberFbId))
			{
				$user[0]->thumb_image=$imagepath.'thumb_'.$user[0]->MemberImage;
				$user[0]->MemberImage=$imagepath.$user[0]->MemberImage;
			}
			elseif(!empty($user[0]->MemberFbId) && !empty($user[0]->MemberImage))
			{
				$user[0]->MemberImage=$user[0]->MemberImage;
			}
			elseif(empty($user[0]->MemberFbId) && empty($user[0]->MemberImage))
			{
				$user[0]->MemberImage="no.jpg";
			}
			elseif(empty($user->MemberImage))
			{
				$user[0]->MemberImage="no.jpg";
			}
			else{
				$user[0]->MemberImage="no.jpg";
			}
			return $user[0];
		}

		/**
			***************************************************************************
			* Code to get user detail by Mobile/Email.                                 	  **
			* @param $email string email address /Mobile No 				  	  **
			* @return boolean OR array						  **
			* *************************************************************************
		*/
		public function _getUser_by_email($email = FALSE)
		{
			if($email === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(4,'".$email."',@msg,@msg2)")->result();
			mysqli_next_result( $this->db->conn_id );
			$imagepath= base_url().'uploads/user_images/';
			if(!empty($user[0]->MemberImage) && empty($user[0]->MemberFbId))
			{
				$user[0]->thumb_image=$imagepath.'thumb_'.$user[0]->MemberImage;
				$user[0]->MemberImage=$imagepath.$user[0]->MemberImage;
			}
			elseif(!empty($user[0]->MemberFbId) && !empty($user[0]->MemberImage))
			{
				$user[0]->MemberImage=$user[0]->MemberImage;
			}
			elseif(empty($user[0]->MemberFbId) && empty($user[0]->MemberImage))
			{
				$user[0]->MemberImage="no.jpg";
			}
			elseif(empty($user->MemberImage))
			{
				$user[0]->MemberImage="no.jpg";
			}
			else{
				$user[0]->MemberImage="no.jpg";
			}
			return $user[0];
		}
	
	}
?>