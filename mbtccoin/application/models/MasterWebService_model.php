<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class MasterWebService_model extends CI_Model
	{
		/**
			*@category 	model
			*@package 	application_models
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
		
		public function __construct() 
		{
			
		}
		
		/**
			*This function is used to check Category Name details
			*@param array $posted data
			*@return int val
		*/
		
		public function checkcategory_byname($cat_name)
		{
			if($cat_name!="")
			{
				$this->db->select('m01_cat_id, m01_cat_variable');
				$this->db->where('m01_cat_name', $cat_name);
				$this->db->where('m01_cat_status', 'Active');
				$q = $this->db->get('m01_category');
				if($q->num_rows() > 0)
				{
					return $q->num_rows();
				}
			}
			return 0;
		}
		
		/**
			*This function is used to check Category Variable details
			*@param array $posted data
			*@return int val
		*/
		
		public function checkcategory_byvariable($cat_variable)
		{
			if($cat_variable!="")
			{
				$this->db->select('m01_cat_id, m01_cat_name');
				$this->db->where('m01_cat_variable', $cat_variable);
				$this->db->where('m01_cat_status', 'Active');
				$q = $this->db->get('m01_category');
				if($q->num_rows() > 0)
				{
					return $q->num_rows();
				}
			}
			return 0;
		}
		/**
			*This function is used to add category data
			*@param array $productdata
			*@return int
		*/
		public function save_categoryData($categorydata)
		{
			if(isset($categorydata['m01_cat_id']) && $categorydata['m01_cat_id']>0){
				$categoryId = $categorydata['m01_cat_id'];
				$categorydata['m01_cat_onupdate']= date('Y-m-d H:i:s');
				$query = $this->db->update('m01_category', $categorydata ,array('m01_cat_id'=>$categoryId));
				return $query;
			}
			else
			{
				$categorydata['m01_cat_onadd']= date('Y-m-d H:i:s');
				$categorydata['m01_cat_onupdate']= date('Y-m-d H:i:s');
				$this->db->insert('m01_category', $categorydata);
				$category_id = $this->db->insert_id();
				return $category_id;
			}
		}
		
		/**
			*This function is used to check the configuration key exists
		*/
		
		public function _checkConfigurationKeyExists($key)
		{
			$query = $this->db->get_where('m00_setconfig',array('m00_name'=>$key));
			$data_array = $query->num_rows();
			return $data_array > 0 ? TRUE : FALSE;
		}
		
		/**
			*get all the configurations by key
			*@param string $key
			*$return string
		*/
		public function _getConfigurationByKey($key ="")
		{
			if($key !="")
			{
				$this->db->select('m00_value');
				$query = $this->db->get_where('m00_setconfig',array('m00_name'=>$key));
				$data_array = $query->row_array();
				return $data_array['m00_value'];
			} 
			else
			{
				$query = $this->db->get('m00_setconfig');
				$data_array = $query->result_array();
				return $data_array;
			}
			
		}
		
		
		/**
			*get all the configurations by key
			*@param string $key
			*$return string
		*/
		public function _getConfigurationAllArray()
		{
			$config_array = array();
			$query = $this->db->get('m00_setconfig');
			$data_array = $query->result_array();
			foreach($data_array as $key=>$val)
			{
				$config_array[$val['m00_name']] = $val['m00_value'];
			}
			return $config_array;
			
		}
		
		/**
			*get MAIL Content by Title
			*@param string $title
			*$return string
		*/
		
		public function _getMessage($opt)
   {
	   $this->db->select('m09_msg_subject as subject , m09_msg_content as content');
	   $this->db->where('m09_msg_title', $opt);
	   $q = $this->db->get('m09_message_config');

	   if($q->num_rows() > 0)
	   {
		 return $q->row();
	   }
	   return array();
   }
		
	   
 
	
			   
   /*
	Method : cancellation_policy
    Description : Used to add & update cancellation_policy
    Params : $Cdata & $Cid
    Return : BOOL
   */
   
   public function cancellation_policy($Cdata,$Cid=null){
	if($Cid){
	//update existing 
	$query = $this->db->update('m08_cancelation_policy',$Cdata, array('m08_cpolicy_id'=>$Cid));
	return  $query;		
	}else{
	//insert new 
	$this->db->insert('m08_cancelation_policy', $Cdata);
	return  $this->db->insert_id();
	}
	}	   
   /*
	Method : updateMessage
    Description : Used to add & update messages
    Params : $Mdata & $Mid
    Return : BOOL
   */
   
   public function updateMessage($Mdata,$Mid=null){
	if($Mid){
	//update existing 
	$query = $this->db->update('m09_message_config',$Mdata, array('m09_msg_id'=>$Mid));
	return  $query;		
	}
	else
	{
	//insert new 
	$this->db->insert('m09_message_config', $Mdata);
	return  $this->db->insert_id();
	}
	}	   
   /*
	Method : add_operator
    Description : Used to add & update operator
    Params :$data,$id
    Return : BOOL
   */
   
   public function add_operator($data,$id=null){
	if($id){
	//update existing 
	$query = $this->db->update('m04_telecom_brand',$data, array('m04_tc_brand_id'=>$id));
	return  $query;		
	}else{
	//insert new 
	$this->db->insert('m04_telecom_brand', $data);
	return  $this->db->insert_id();
	}
	}

	/**
			*This function is used to fetch all categories
			*@param array $posted data
			*@return int val
		*/
		
		


		public function select_category(){
			$this->db->select('m01_cat_id as CatID,m01_cat_name as CatName,m01_cat_variable as CatVariable,m01_cat_content as CatContent,m01_cat_parent as CatParent,m01_cat_image as CatImage,m01_cat_status as CatStatus');
			
			//get all category
			$this->db->where('m01_cat_parent', '0');
			
			$query = $this->db->get('m01_category');
			$method = 'result_array';
			return json_encode($query ? $query->$method() : '');
			}

			public function select_sub_category(){
			$this->db->select('m01_cat_id as CatID,m01_cat_name as CatName,m01_cat_variable as CatVariable,m01_cat_content as CatContent,m01_cat_parent as CatParent,m01_cat_image as CatImage,m01_cat_status as CatStatus');
			
			//get all category
			$this->db->where('m01_cat_parent >=', '1');
			
			$query = $this->db->get('m01_category');
			$method = 'result_array';
			return json_encode($query ? $query->$method() : '');
			}

			public function insert_assign_data($userNumber,$assignName,$qrcodeNumber)
			{
			$get_id=$this->db->query("select m11_user_id,m11_user_role from m11_user where m11_user_contactno ='".$userNumber."'")->row();
			$assign_qrcode=$this->db->query("update m12_qrcode_info set m12_assign_to ='".$get_id->m11_user_id."',m12_assign_type='".$get_id->m11_user_role."' where m12_qrcode_name= '".$qrcodeNumber."'");
			if($assign_qrcode)
			  return TRUE;
			  return FALSE;
			}
			public function update_site_config($siteId,$siteName,$siteValue,$siteDiscription)
			   {
			    #Update code below.....

			    if($siteId && $siteName && $siteValue && $siteDiscription != '' && $siteId && $siteName && $siteValue && $siteDiscription != 'NULL')
			    {
			      $data= array(
			        'm00_name' => $siteName,
			        'm00_value' => $siteValue,
			        'm00_desc' => $siteDiscription
			      );

			      $this->db->where('`m00_setconfig`.`m00_id`',$siteId);
			      $update_site_config=$this->db->update('`app_mbtc`.`m00_setconfig`',$data);

			      if($update_site_config)
			        return TRUE;
			        return FALSE;

			    }
			   }
			    public function insert_site_config($siteName,$siteValue,$siteDiscription)
				   {
				   	# code...
				     if($siteName && $siteValue && $siteDiscription != '')
				     { 

				     	$data=array(
				     		'm00_name' => $siteName,
				     		'm00_value' => $siteValue,
				     		'm00_desc' => $siteDiscription
				     	);

				       $insert_site_config=$this->db->insert('m00_setconfig',$data);
				       if($insert_site_config)
				       	  return TRUE;
				       	  return FALSE;
				     }

				   }
	   public function change_seller_status($id)
		   {
		     $change_status=$this->db->query("update m13_seller_info set m13_seller_status = (case when(m13_seller_status = 'ACTIVE') then 'INACTIVE' else 'ACTIVE' end) where m11_user_id ='".$id."'");
		     if($change_status)
		      return TRUE;
		      return FALSE;
		   }
	    public function change_user_status($id)
		   {
		     $change_status=$this->db->query("update m11_user set m11_user_status = (case when(m11_user_status = 'ACTIVE') then 'INACTIVE' else 'ACTIVE' end) where m11_user_id ='".$id."'");
		     if($change_status)
		      return TRUE;
		      return FALSE;
		   }

		  public function add_category($insertArr)
		   {
		   	if($insertArr['categoryId'] == '')
		   	{
		    $query="insert into `m01_category`(m01_cat_parent,m01_cat_name,m01_cat_variable,m01_cat_hsncode,m01_cat_content,m01_cat_cgst,m01_cat_sgst,m01_cat_image,m01_cat_status,m01_cat_commission) values('".$insertArr['categoryParent']."','".$insertArr['categoryName']."','".$insertArr['categoryIdentifire']."','".$insertArr['categoryHsnCode']."','".$insertArr['categoryDescription']."','".$insertArr['categoryCgst']."','".$insertArr['categorySgst']."','".$insertArr['categoryImage']."','".$insertArr['categoryStatus']."','".$insertArr['categoryCommission']."')";
		    $execute=$this->db->query($query);
		    if($execute)
		      return TRUE;
		      return FALSE;
		     }
		     else
		     {
		     	$query="update `m01_category` set m01_cat_name='".$insertArr['categoryName']."',m01_cat_parent='".$insertArr['categoryParent']."',m01_cat_variable='".$insertArr['categoryIdentifire']."',m01_cat_hsncode='".$insertArr['categoryHsnCode']."',m01_cat_content='".$insertArr['categoryDescription']."',m01_cat_cgst='".$insertArr['categoryCgst']."',m01_cat_sgst='".$insertArr['categorySgst']."',m01_cat_image='".$insertArr['categoryImage']."',m01_cat_status='".$insertArr['categoryStatus']."',m01_cat_commission='".$insertArr['categoryCommission']."' where m01_cat_id='".$insertArr['categoryId']."'";
		     	 $execute=$this->db->query($query);
			    if($execute)
			      return TRUE;
			      return FALSE;
		     }
		   }
		     public function add_user($insertArr)
		   { 
		     unset($insertArr['userC_password']);
		     $data=array(
		                'proc'=> 4,
		                'userDob'=>'',
		                'userGender' =>'',
		                'userAboutMe' => '',
		                'userIntrestedIn' => '',
		                'userLocation' => '',
		                'userImage' => '',
		                'userCancellationPolicy'=> '',
		                'userSubscription' => '',
		                'userFbId' => '',
		                'userGplusId' => '',
		                'userStatus' => '',
		                'userIsLogin' => '',
		                'userIsTravelling' =>'',
		                'userNotification' => '',
		                'userEmailAlert' => '',
		                'userNewsLetter' => '',
		                'userLanguage' => '',
		                'userActivateCode' => '',
		                'userDeviceToken' => '',
		                'userDivceType' => '',
		                'userAppVersion' => '',
		                'userLatitude' => '',
		                'userLongitude' => '',
		                'userReferralCode' => '',
		                'userLastLogin' => '',
		                'userid' => '',
		                'userUsername' =>$insertArr['userUsername'],
		                'userEmail'=>$insertArr['userEmail'],
		                'userFirstname'=>$insertArr['userFirstname'],
		                'userLastname'=>$insertArr['userLastname'],
		                'userWebsite'=>$insertArr['userWebsite'],
		                'userPassword'=>$insertArr['userPassword'],
		                'userRole'=>$insertArr['userRole']
		        
		              );
		     $add_user="CALL user_detail(?". str_repeat(",?",count($data)-1). ",@msg,@msg2)";
		     $this->db->query($add_user,$data);
		   }
		   public function update_message_data($messageId,$messageTitle,$messageSubject,$messageContent,$messageStatus)
		   {
		    $data= array('m09_msg_title' => $messageTitle,
		                    'm09_msg_subject' => $messageSubject,
		                    'm09_msg_content' => $messageContent,
		                    'm09_msg_status' =>$messageStatus );

		    $this->db->where('`m09_message_config`.`m09_msg_id`',$messageId);
		      $update_site_config=$this->db->update('`app_mbtc`.`m09_message_config`',$data);
		      if($update_site_config)
		        return TRUE;
		        return FALSE;

		   }

		   public function add_message_data($messageTitle,$messageSubject,$messageContent,$messageStatus)
		   {

		      $data= array('m09_msg_title' => $messageTitle,
		                    'm09_msg_subject' => $messageSubject,
		                    'm09_msg_content' => $messageContent,
		                    'm09_msg_status' =>$messageStatus );

		      $insert_message=$this->db->insert('`app_mbtc`.`m09_message_config`',$data);
		      if($insert_message)
		        return TRUE;
		        return FALSE;
		   }

		   public function deleteCategory($insertArr)
		   {
		   	$query=$this->db->query("update m01_category set m01_cat_status = 3 where m01_cat_id ='".$insertArr['categoryId']."'");
		   	if($query)
		   		return TRUE;
		   	    return FALSE;
		   }


 public function __updateseller($insertArr)
		   {
		   	$query=$this->db->query("update m13_seller_info set 
		   		m13_seller_company_name = '".$insertArr['sellerCompanyName']."',
		   		m13_seller_company_display_name = '".$insertArr['sellerDisplayName']."',
		   		m13_seller_category = '".$insertArr['sellerCategory']."',
		   		m13_seller_mobile_no = '".$insertArr['sellerMobileNumber']."',
		   		m13_seller_primary_email = '".$insertArr['sellerPrimaryEmail']."',
		   		m13_seller_shipping_address = '".$insertArr['sellerShippingAddress']."',
		   		m13_seller_city = '".$insertArr['sellerCity']."',
		   		m13_seller_pincode = '".$insertArr['sellerPincode']."',
		   		m13_seller_state = '".$insertArr['sellerState']."',
		   		m13_seller_gstin = '".$insertArr['sellerGstin']."',
		   		m13_seller_vat_tin = '".$insertArr['sellerVatTin']."',
		   		m13_seller_company_pan = '".$insertArr['sellerCompanyPan']."',
		   		m13_seller_beneficiary_name = '".$insertArr['sellerBenificiary']."',
		   		m13_seller_current_account_number = '".$insertArr['sellerCurrentAcc']."',
		   		m13_seller_ifsc = '".$insertArr['sellerIfsc']."',
		   		m13_seller_branch_name = '".$insertArr['sellerBranchName']."',
		   		m13_seller_swift_code = '".$insertArr['sellerSoftCode']."',
		   		m13_seller_subscription = '".$insertArr['sellerSubscription']."',
		   		m13_seller_status = '".$insertArr['sellerStatus']."' 
		   		where m13_seller_id =".$insertArr['sellerId']." ");
		   	if($query)
		   		return TRUE;
		   	    return FALSE;
		   }







	
}
?>
