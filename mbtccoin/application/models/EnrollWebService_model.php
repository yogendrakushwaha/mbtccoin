<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class EnrollWebService_model extends CI_Model
	{
		/**
			*@category 	model
			*@package 	application_models
			*@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
			*@version 	0.0.1
			*dated  	2016-12-08
		*/
		
		public function __construct() 
		{
				$this->load->model('UserprofileWebService_model','Userprofile');
		}
		
		
		
		/**
			*This function is used to add/update the user details
			*@param array $posted data
			*@return int val
		*/
		public function create_user($insertArray)
		{
			if(isset($insertArray['UserId']) && $insertArray['UserId'] >0)
			{
				$user_id = $insertArray['UserId'];
				unset($insertArray['UserId']);
				$user=array(
				'm11_first_name'=>$insertArray['FirstName'],
				'm11_last_name'=>$insertArray['LastName'],
				'm11_user_dob'=>$insertArray['Dob'],
				'm11_user_location'=>$insertArray['Address'],
				'm11_user_email'=>$insertArray['Email'],
				'm11_user_contactno'=>$insertArray['Contact'],
				'm11_user_status'=>$insertArray['Status']
				);
				$query = $this->db->update('m11_user',$user, array('m11_user_id'=>$user_id));
				return  $query;
			}
			else
			{
				$delete_user = $this->db->delete("m11_user",array('m11_user_contactno'=> $insertArray['Contact'], 'm11_user_status ='=>'Pending'));
				$delete_login = $this->db->delete("tr04_login",array('m11_user_contactno'=> $insertArray['Contact']));
                $user_data = $this->Userprofile->_getUser_by_mobileno($insertArray['Referral']);
		        $insertArr['introid']=$user_data->RegId;
				$user=array(
				'm11_first_name'=>$insertArray['FirstName'],
				'm11_last_name'=>$insertArray['LastName'],
				'm11_user_dob'=>$insertArray['Dob'],
				'm11_user_gender'=>'',
				'm11_user_email'=>$insertArray['Email'],
				'm11_user_contactno'=>$insertArray['Contact'],
				'm11_user_location'=>$insertArray['Address'],
				'm11_user_image'=>$insertArray['Image_name'],
				'm11_user_joined'=>$insertArray['Created_on'],
				'm11_user_status'=>$insertArray['Status'],
				'm11_user_role'=>$insertArray['User_type'],
				'm11_cancellation_policy'=>$insertArray['Cancellation_policy'],
				'm11_subcription'=>$insertArray['Subscription'],
				'm11_user_referral_code'=>$insertArr['introid'],
				'm11_user_onadd'=>$insertArray['Created_on']
				);
				//$query = " CALL sp_user(?" . str_repeat(",?", count($user)-1) .",@a) ";
				//$data['rec']=$this->db->query($query, $smenu);
				//$this->db->free_db_resource();
				//$data['response']=$this->db->query("SELECT @a as resp");
				$this->db->insert('m11_user', $user);
				$id=$this->db->insert_id();
				$login=array(
			'm11_user_id'=>$id,
			'm11_user_contactno'=>$insertArray['Contact'],
			'm11_user_email'=>$insertArray['Email'],
			'tr04_login_pwd'=>$insertArray['Password'],
			'tr04_pin_pwd'=>$insertArray['Password'],
			'tr04_account_id'=>1,
			'tr04_login_type'=>1
			);
			$this->db->insert('tr04_login', $login);
				$capping=array(
				'm_curru_id'=>$id,
				'm_curru_bal'=>0.00,
				'm_cap_amt'=>50.00,
				'm_cap_amt2'=>50.00
				);
				$this->db->insert('tr12_curr_bal', $capping);

if($insertArray['User_type']==1 || $insertArray['User_type']==2)
				{
				
				$contentColumns = array(
						array('title'=>'About Us','variable'=>'about_us'),
						array('title'=>'Privacy Policy','variable'=>'privacy_policy'),
						array('title'=>'Terms Conditions','variable'=>'terms_conditions'),
						array('title'=>'Contact Us','variable'=>'contact_us'),
						);
				foreach($contentColumns as $col){
				 $InsertStatic = array(
				 'm10_staticcnt_title'=>$col['title'],
				 'm10_staticcnt_variable'=>$col['variable'],
				 'm10_affilate_id'=>$id,
				 'm10_staticcnt_status'=>'Active',
				 );
				$this->db->insert('m10_static_content', $InsertStatic);	
				}
				}

				return $id;
				
			}
		}
		/**
			*This function is used to check whether the user email exists or not
			*@param string $email, int $user_id, string $status
			*return boolean
		*/
		public function sg_email_exist($u_email,$user_id = 0,$status="")
		{
			if(empty($u_email)){
				return FALSE;
			}
			else
			{
				$this->db->select('m11_user_email');
				$this->db->from('m11_user');
				$this->db->where('m11_user_email',$u_email);
				if($status !=""){
					$this->db->where('m11_user_status =',$status);
				}
				if($user_id >0){
					$this->db->where('m11_user_id !=',$user_id);
				}
				$q = $this->db->get();
				if($q->num_rows() > 0)
				{
					return TRUE;
				}
			}
			return FALSE;
		}
		

/**
			***************************************************************************
			* Code to get user detail by By Desig Nation.                                 	  **
			* @param $regid 				  	  **
			* @return boolean OR array						  **
			* *************************************************************************
		*/
		public function _getUser_by_desig($regid  = FALSE)
		{
			if($regid === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(1,".$regid.",@msg)")->result();
			mysqli_next_result( $this->db->conn_id );
			return $user;
		}

        /**
			***************************************************************************
			* Code to get user detail by By Desig Nation.                                 	  **
			* @param $regid 				  	  **
			* @return boolean OR array						  **
			* *************************************************************************
		*/
		public function _getUser_by_mobileno($mobileno = FALSE)
		{
			if($mobileno === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(2,".$mobileno.",@msg)")->result();
			mysqli_next_result( $this->db->conn_id );
			return $user[0]->RegId;
		}



		/**
			*This function is used retrive data of specific user
			*@param int $user_id
			*return @ARRAY
		*/
		public function getUserByID($id){
			if(empty($id)) return FALSE;

			$query = $this->db->query("SELECT * FROM `enroll` WHERE `RegId`='".$id."'");
			return $query->row();
		}
		/**
			*This function is used to check whether the user email exists or not
			*@param string $email, int $user_id, string $status
			*return boolean
		*/
		public function sg_contact_exist($u_email,$user_id = 0,$status="")
		{
			if(empty($u_email)){
				return FALSE;
			}
			else
			{
				$this->db->select('m11_user_email');
				$this->db->from('m11_user');
				$this->db->where('m11_user_contactno',$u_email);
				if($status !=""){
					$this->db->where('m11_user_status =',$status);
				}
				if($user_id >0){
					$this->db->where('m11_user_id !=',$user_id);
				}
				$q = $this->db->get();
				if($q->num_rows() > 0)
				{
					return TRUE;
				}
			}
			return FALSE;
		}
		
		/**
	 ***************************************************************************
	 * Code to get user detail by email.                                 	  **
	 * @param $email string email address  				  	  **
	 * @return boolean OR array						  **
	 * *************************************************************************
	 */

	public function _getUser_by_email($email = FALSE)
	{
	   if($email === FALSE)
		return FALSE;

	   $query = $this->db->get_where('m11_user', array('m11_user_email' => $email));
	   $user= $query->row_array();
	   $imagepath= base_url().'uploads/users_profile/';
	   if(!empty($user['m11_user_image']) && empty($user['m11_fb_id']))
	   {
		
		    $user['thumb_image']=$imagepath.'thumb_'.$user['m11_user_image'];
			$user['image_name']=$imagepath.$user['m11_user_image'];
	   }
	   elseif(!empty($user['m11_fb_id']) && !empty($user['m11_user_image']))
	  {
		  $user['m11_user_image']=$user['m11_user_image'];
	  }
	   elseif(empty($user['m11_fb_id']) && empty($user['m11_user_image']))
	   {
		 $user['m11_user_image']="";
	   }
	    elseif(empty($user['m11_user_image']))
		 {
		  $user['m11_user_image']="";
		 }
		 else{
			$user['m11_user_image']="";
		 }
	   return $user;
	}
	
	public function _getUser_by_id($uid = FALSE)
	{
	   if($uid === FALSE)
		return FALSE;

	   $query = $this->db->get_where('m11_user', array('m11_user_id' => $uid,'m11_user_status'=>'Active'));
	   return $query->row_array();
	}
	
	public function _get_User_by_email($email = FALSE)
	{
	   if($email === FALSE)
		return FALSE;

	   $query = $this->db->get_where('m11_user', array('m11_user_email' => $email));
	   $user= $query->row_array();
	   
	   return $user;
	}

	

	public function _getuserdetail_by_email($email = FALSE)
	{
	   if($email === FALSE)
		return FALSE;

	   $query = $this->db->get_where('m11_user', array('m11_user_email' => $email));
	   return $query->row_array();
	}


		public function change_mobile($insertMobile)
		{
			//print_r($insertMobile);
			//			die();
			$OldMobile = $insertMobile['OldMobile'];
			$NewMobile = $insertMobile['NewMobile'];
		    $MemberId = $insertMobile['MemberId'];
		    $id = $this->_getUser_by_mobileno($MemberId);
		    
		 	$query = $this->db->query("UPDATE m11_user SET m11_user_contactno=".$NewMobile." WHERE m11_user_contactno =".$OldMobile." and m11_user_id=".$id." and m11_user_status='Active'");


		 	$query = $this->db->query("UPDATE tr04_login SET m11_user_contactno=".$NewMobile." WHERE m11_user_contactno =".$OldMobile." and m11_user_id=".$id."");
		 	

		 	$query = $this->db->query("INSERT INTO tr11_actionrecord (tr11_ar_tblnm,tr11_ar_old_value,tr11_ar_new_value,tr11_ar_uid,tr11_ar_description) VALUES ('m11_user','$OldMobile','$NewMobile','$id','Mobile No. Updated')");
		}
	}
?>