<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserprofileWebService_model extends CI_Model
{
   /**
    *@category 	model
    *@package 	application_models
    *@author 	TechAarjavam Pvt. Ltd. (00001) <info@techaarjavam.com>
    *@version 	0.0.1
    *dated  	2016-12-08
    */

   public function __construct() 
   {
	  
   }
  
   
		/**
			*This function is used to add/update the user details
			*@param array $posted data
			*@return int val
		*/
		public function create_user($insertArray)
		{
		 
			if(isset($insertArray['UserId']))
			{
				$user_id = $insertArray['UserId'];
				unset($insertArray['UserId']);

				$query = $this->db->update('m11_user', $insertArray, array('m11_user_id'=>$user_id));
				echo $this->db->last_query();
				DIE();
				return  $query;
			}
			else
			{
				//$delete_user = $this->db->delete("m11_user",array('m11_user_contactno'=> $insertArray['Contact'], 'm11_user_status ='=>'Pending'));
				//$delete_login = $this->db->delete("tr04_login",array('m11_user_contactno'=> $insertArray['Contact']));
				$user=array(
				//'proc'=>2,
				'm11_first_name'=>$insertArray['FirstName'],
				'm11_last_name'=>$insertArray['LastName'],
				'm11_user_dob'=>$insertArray['Dob'],
				'm11_user_gender'=>'',
				'm11_user_email'=>$insertArray['Email'],
				'm11_user_contactno'=>$insertArray['Contact'],
				'm11_user_location'=>$insertArray['Address'],
				'm11_user_image'=>$insertArray['Image_name'],
				'm11_user_joined'=>$insertArray['Created_on'],
				'm11_user_status'=>$insertArray['Status'],
				'm11_user_role'=>$insertArray['User_type'],
				'm11_cancellation_policy'=>$insertArray['Cancellation_policy'],
				'm11_subcription'=>$insertArray['Subscription'],
				'm11_fb_id'=>'',
				'm11_user_onadd'=>$insertArray['Created_on']
				);
				//$query = " CALL sp_user(?" . str_repeat(",?", count($user)-1) .",@a) ";
				//$data['rec']=$this->db->query($query, $smenu);
				//$this->db->free_db_resource();
				//$data['response']=$this->db->query("SELECT @a as resp");
				$this->db->insert('m11_user', $user);
				$id=$this->db->insert_id();
				$login=array(
				//'proc'=>2,
				'or_reg_id'=>$id,
				'or_login_id'=>$insertArray['Contact'],
				'or_login_pwd'=>$insertArray['Password'],
				'or_pin_pwd'=>$insertArray['Password'],
				'or_account_id'=>1,
				'or_login_type'=>1
				);
				$this->db->insert('tr04_login', $login);
                $maintain_wallet=array(
						'm_curru_id'=>$id,
						'm_curru_bal'=>0.00,
						'm_cap_amt'=>50.00,
						'm_cap_amt2'=>50.00
					);
				$this->db->insert('tr12_curr_bal', $maintain_wallet);
				return $id;
				
			}
		}
		/**
			*This function is used to check whether the user email exists or not
			*@param string $email, int $user_id, string $status
			*return boolean
		*/
		public function sg_email_exist($u_email,$user_id = 0,$status="")
		{
			if(empty($u_email)){
				return FALSE;
			}
			else
			{
				$this->db->select('m11_user_email');
				$this->db->from('m11_user');
				$this->db->where('m11_user_email',$u_email);
				if($status !=""){
					$this->db->where('m11_user_status =',$status);
				}
				if($user_id >0){
					$this->db->where('m11_user_id !=',$user_id);
				}
				$q = $this->db->get();
				if($q->num_rows() > 0)
				{
					return TRUE;
				}
			}
			return FALSE;
		}
		
		/**
			*This function is used retrive data of specific user
			*@param int $user_id
			*return @ARRAY
		*/
		public function getUserByID($id){
			if(empty($id)) return FALSE;

			$query = $this->db->query("SELECT * FROM `enroll` WHERE `RegId`='".$id."' AND MemberStatus='Active'");
			return $query->row();
		}
		
		/**
			*This function is used retrive password of specific user
			*@param int $user_id
			*return @Password
		*/
		public function getUserPasswordByID($id){
			if(!$id) return FALSE;
			
				$query = $this->db->query("SELECT * FROM `tr04_login` WHERE m11_user_contactno='".$id."'");
			$loginuser = $query->row();
			return (!empty($loginuser) ) ? $loginuser->tr04_login_pwd : '';
			
		}
		/**
			*This function is used retrive data of specific user
			*@param int $MobileNo
			*return @ARRAY
		*/
		public function getUserByMobile($mobileno){
			if(empty($mobileno)) return FALSE;
			$query = $this->db->query("SELECT * FROM `enroll` WHERE `MemberContactNo`='".$mobileno."'");
			return $query->row()->RegId;
		}
		/**
			*This function is used to update password of specific user
			*@param int $user_id
			*return @BOOL
		*/
		function update_password($id,$data=array()){
		if(!$id) 
		return FALSE;
	
			$query = $this->db->update('tr04_login',$data, array('m11_user_contactno'=>$id));
		return  $query;
		}		
		/**
			*This function is used to retrive user data by phone number
			*@param int $PhoneNumber
			*return @BOOL
		*/
		function getUserDataByPhone($PhoneNumber)
		{
			if(!$PhoneNumber) return FALSE;
			$query = $this->db->query("SELECT * FROM `enroll` WHERE `MemberContactNo`='".$PhoneNumber."'");
			$user = $query->row();
			return (!empty($user) ) ? 1:0;
			
		}
		/**
			*This function is used to retrive user's CurrentBalance
			*@param int $userID
			*return @CurrentBalance
		*/
		function getUserCurrentBalance($userID,$type)
		{
		if(!$userID) return FALSE;
			
			$query = $this->db->query("SELECT get_available_bal(".$userID.",".$type.") as CurrentBalance");
			$CurrentBalance = $query->row()->CurrentBalance;
			return (!empty($CurrentBalance) ) ? $CurrentBalance : '';
			
		}
	/**
			*This function is used to retrive user's MaintainBalance
			*@param int $userID
			*return @MaintainBalance
		*/
		function getUserMaintainBalance($userID,$type)
		{
		if(!$userID) return FALSE;
			
			$query1 = $this->db->query("SELECT get_capping_amt(".$userID.",".$type.") as MaintainBalance");
			$MaintainBalance = $query1->row()->MaintainBalance;
			return (!empty($MaintainBalance) ) ? $MaintainBalance : '';
			
		}
		/**
			*This function is used to retrive user's CurrentCapping
			*@param int $userID
			*return @CurrentCapping
		*/
		function getUserCurrentCapping($userID){
		if(!$userID) return FALSE;
			
			$query = $this->db->query("SELECT get_capping_amt(".$userID.",1) as CurrentCapping");
			$CurrentCapping = $query->row()->CurrentCapping;
			return (!empty($CurrentCapping) ) ? $CurrentCapping : '';
			
		}
		/**
			*This function is used to retrive user's CurrentBalance
			*@param int $userID
			*return @CurrentBalance
		*/
		function getSiteStaticContent($variable){
			$query = $this->db->query("SELECT * FROM `m10_static_content` WHERE `m10_staticcnt_variable`= '".$variable."' AND m10_affilate_id =1");
			$data = $query->row();
			return (!empty($data) ) ? $data : '';
			
		}
		
		/**
			*This function is used to get Recharge details
			*@param array $posted data
			*@return Array of Recharge Data
		**/
		
		public function show_recharge($insertArr)
	    {
			if($insertArr['userid']!="")
				{
				//$user_data = $this->Userprofile->_getUser_by_mobileno($insertArr['userid']);
				//$insertArr['userid']=$user_data->RegId;
               
			$recereport=array(
				'proc'=>2,
				'account'=>$insertArr['account'],
				'transid'=>$insertArr['transid'],
				'userid'=>$insertArr['userid'],
				'fromdate'=>date('Y-m-d',strtotime($insertArr['fromdate'])),
				'todate'=>date('Y-m-d',strtotime($insertArr['todate'])),
				'noofrecords'=>$insertArr['noofrecords'],
				'recstatus'=>$insertArr['recstatus'],
				'descrip'=>1,
			);
			$query = " CALL recharge_report(?" . str_repeat(",?", count($recereport)-1) .",@a) ";
			$data['rec']=$this->db->query($query, $recereport)->result();
            //echo $this->db->last_query();
            //die();
			return $data;
			}
		}
		/**
			*This function is used to get Ladger details
			*@param array $posted data
			*@return Array of Ladger Data
		*/
		
		public function show_ladger($insertArr)
	    {
			if($insertArr['userid']!="")
				{
				$user_data = $this->Userprofile->_getUser_by_mobileno($insertArr['userid']);
				$insertArr['userid']=$user_data->RegId;
				//$insertArr['account']=$user_data->RegId;
			$ladgerreport=array(
				'proc'=>2,
				'account'=>$insertArr['account'],
				'transid'=>$insertArr['transid'],
				'userid'=>$insertArr['userid'],
				'fromdate'=>date('Y-m-d',strtotime($insertArr['fromdate'])),
				'todate'=>date('Y-m-d',strtotime($insertArr['todate'])),
				'noofrecords'=>$insertArr['noofrecords'],
				'ladegerstatus'=>$insertArr['status'],
				'descrip'=>'1'
			);
			$query = " CALL ladger(?" . str_repeat(",?", count($ladgerreport)-1) .",@a) ";
			$data['rec']=$this->db->query($query, $ladgerreport)->result();
			//echo $this->db->last_query();
			//die();
			return $data;
			}
		}
	 /*
	Method : get_kyc
    Description : Used to get kyc data
    Params : $ID
    Return : Array
   */
   
   public function get_kyc($id=null){
	$this->db->select('m19_uk_id as kYID,m19_uk_user_id as kYUserId,m19_uk_user_photo as kyuserpic, m19_uk_idproof_type as Idtype, m19_uk_uniqueid_num as kYUnumber, m19_uk_idproofdoc as KyIdproofdoc,m19_uk_address as kyAddress,m19_uk_addressproof as kYaddressproof');
	if($id){
	//get row by id
	$this->db->where('m19_uk_user_id', $id);
	$method = 'row';
	}else{
	//get all kyc
	$method = 'result_array';
	}
	$query = $this->db->get('m19_user_kyc');
	return $query->num_rows() ? $query->$method() : '';
	}
	/**
	*This function is used add user_kyc
	*@param array $posted data
	*@return bool
	**/
		
	public function user_kyc($data,$kyc_id=null){
	if($kyc_id){
	//update existing API
	$query = $this->db->update('m19_user_kyc',$data, array('m19_uk_id'=>$kyc_id));
	return  $query;		
	}else{
	//insert new API
	$this->db->insert('m19_user_kyc', $data);
	return  $this->db->insert_id();
	}
	}
   
   
   /**
			*This function is used retrive data of specific user
			*@param int $Service_Typeid and $status 
			*return @ARRAY
		*/
		public function getOperatorByServiceId($id,$status){
			if(empty($id)) return FALSE;

			$query = $this->db->query("SELECT * FROM `telecom_brand` WHERE `ServiceTypeId`=".$id." and `BrandStatus`=".$status."");
			return $query->result();
		}
		
				
   /**
			*This function is used retrive data of CMS By AffiliateID
			*@param int $AffiliateID  
			*return @ARRAY
		*/
		public function getCmsByAffiliateID($id){
			if(empty($id)) return FALSE;
			$this->db->select('*');
			$this->db->where('m10_affilate_id', $id);
			$query = $this->db->get('m10_static_content');
			return $query->num_rows() ? $query->result() : '';
		}
			
   /**
			*This function retrives CMS data
			*@param int $id and $userID 
			*return @ARRAY
		*/
		public function getCmsByID($id,$userID=NULL){
			if(empty($id)) return FALSE;
			$this->db->select('*');
			$this->db->where('m10_staticcnt_id', $id);
			if($userID) $this->db->where('m10_affilate_id', $userID);
			$query = $this->db->get('m10_static_content');
			return $query->num_rows() ? $query->row() : '';
		}
				
   /**
			*This function is used to save CMS content
			*@param int $id and $userID 
			*return @BOOL
		*/
		public function saveStaticContent($id,$data){
		$query = $this->db->update('m10_static_content',$data, array('m10_staticcnt_id'=>$id));
		return  $query;
		}
/**
			***************************************************************************
			* Code to get user detail by Mobile/Email.                                 	  **
			* @param $email string email address /Mobile No 				  	  **
			* @return boolean OR array						  **
			* *************************************************************************
		*/
		public function _getUser_by_mobileno($mobile = FALSE)
		{
			if($mobile === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(2,".$mobile.",@msg,@msg2)")->result();
			mysqli_next_result( $this->db->conn_id );
			$imagepath= base_url().'uploads/user_images/';
			if(!empty($user[0]->MemberImage) && empty($user[0]->MemberFbId))
			{
				$user[0]->thumb_image=$imagepath.'thumb_'.$user[0]->MemberImage;
				$user[0]->MemberImage=$imagepath.$user[0]->MemberImage;
			}
			else if(!empty($user[0]->MemberFbId) && !empty($user[0]->MemberImage))
			{
				$user[0]->MemberImage=$imagepath.$user[0]->MemberImage;
			}
			else if(empty($user[0]->MemberFbId) && empty($user[0]->MemberImage))
			{
				$user[0]->MemberImage=$imagepath."nouser.png";
			}
			else{
				$user[0]->MemberImage="nouser.png";
			}
			return $user[0];
		}
	
		/**
			***************************************************************************
			* Code to get user detail by RegId.                                 	  **
			* @param $regid 				  	                                          **
			* @return boolean OR array						                      **
			* *************************************************************************
		*/
		public function _getUser_by_regid($regid  = FALSE)
		{
			if($regid === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(2,".$regid.",@msg,@msg2)")->result();
			mysqli_next_result( $this->db->conn_id );
			return $user[0];
		}
		/**
			***************************************************************************
			* Code to get user detail by By Desig Nation.                                 	  **
			* @param $regid 				  	  **
			* @return boolean OR array						  **
			* *************************************************************************
		*/
		public function _getUser_by_desig($regid  = FALSE)
		{
			if($regid === FALSE)
			return FALSE;
			$user=$this->db->query("CALL `enrolluser`(3,".$regid.",@msg,@msg2)")->result();
			mysqli_next_result( $this->db->conn_id );
			return $user;
		}
}
?>