<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_closing extends CI_Controller {

	/////////////////////////////////////////////////////////////////////////
	//////////     Constructor In Master Controller    ///////
	//////////////////////////////////////////////////////////////////////

	public function __construct()
	{
		parent::__construct();
		$this->_is_logged_in();
		$this->data['page'] = "Admin Closing Pannel"; 
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     Check Login    ///////
	//////////////////////////////////////////////////////////////////////

	public function _is_logged_in() 
	{
		if ($this->session->userdata('user_id')=="" || $this->session->userdata('profile_id')!="0")
		{
			redirect('auth/logout');
			die();
		}
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     Generate Closing Date     ///////
	//////////////////////////////////////////////////////////////////////

	public function view_closing_date()
	{
		$data['form_name'] = "Generate Closing Date";

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_closing_date',$data);
		$this->load->view('common/footer');
	}

	public function insert_direct_date()
	{
		$this->db->query("CALL `sp_insert_closing_date`(3)");
		header("Location:".base_url()."admin_closing/view_closing_date");
	}

	public function insert_closing_date()
	{
		$this->db->query("CALL `sp_insert_closing_date`(1)");
		header("Location:".base_url()."admin_closing/view_closing_date");
	}

	public function insert_payout_date()
	{
		$this->db->query("CALL `sp_insert_closing_date`(2)");
		header("Location:".base_url()."admin_closing/view_closing_date");
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     		Generate Direct Income	      ///////
	//////////////////////////////////////////////////////////////////////

	public function view_pay_direct_income()
	{
		$data['form_name'] = "Generate Direct Income";
		$data['clsd'] = $this->db->where("tr_direct_status",1)->get("tr12_direct_date");
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_pay_direct_income',$data);
		$this->load->view('common/footer');
	}

	public function insert_direct_income()
	{
		$dddate = $this->input->post('dddate');
		if($dddate != '' && $dddate != '-1' && $dddate != '0')
		{
			$clsdt=array(
				'closingdate'=>$dddate
			);
			$call_procedure1 = "CALL sp_direct_income(?)";
			$this->db->query($call_procedure1,$clsdt);
			mysqli_next_result( $this->db->conn_id );
		}
		header("Location:".base_url()."admin_closing/view_pay_direct_income");
	}

	/////////////////////////////////////////////////////////////////////////
	//////////         View Payout Report         ///////
	//////////////////////////////////////////////////////////////////////

	public function view_direct_report()
	{
		$data['form_name'] = "Select Direct Date";
		$data['table_name'] = "View Direct Report";

		$data['clsd'] = $this->db->where("tr_closing_status !=",1)->get("tr05_closing_date");

		$currdate = $this->input->post('dddate');

		if($currdate != '' && $currdate != '-1' && $currdate != '-1')
		{
			$queery = "DATE_FORMAT(tr04_payout_detail.`tr_to_payout_date`,'%Y-%m-%d') <= DATE_FORMAT('".$currdate."','%Y-%m-%d')";
			$queery = $queery." AND `tr04_payout_detail`.`tr_payout_type` = 2";
			$queery = $queery." ORDER BY `tr04_payout_detail`.`tr_from_payout_date` DESC";
			$dt = array(
				'queery' => $queery
			);
			$call_procedure = "CALL sp_admin_payout_report(?)";
			$data['payout_details'] = $this->db->query($call_procedure,$dt);
			mysqli_next_result( $this->db->conn_id );
		}

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_direct_report',$data);
		$this->load->view('common/footer');
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     		Generate Closing Data	      ///////
	//////////////////////////////////////////////////////////////////////

	public function view_generate_payout()
	{
		$data['form_name'] = "Generate Payout";
		$data['clsd'] = $this->db->where("tr_closing_status",1)->get("tr05_closing_date");
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_generate_payout',$data);
		$this->load->view('common/footer');
	}

	public function insert_generate_payout()
	{
		$dddate = $this->input->post('dddate');
		if($dddate != '' && $dddate != '-1' && $dddate != '0')
		{
			// $from_closing = $this->db->query("SELECT `tr05_closing_date`.`tr_closing_date` AS from_close_date FROM `tr05_closing_date` ORDER BY `tr05_closing_date`.`tr_closing_date` DESC LIMIT 1,1")->row()->from_close_date;

			$dt = array(
				'closing_from_date'=>$dddate,
				'closing_to_date'=>$dddate
			);
			$call_procedure = "CALL sp_match_payout_detail2(?,?)";
			$this->db->query($call_procedure,$dt);

			mysqli_next_result( $this->db->conn_id );

			$call_procedure2 = "CALL sp_fill_spot_income('".$dddate."')";
			$this->db->query($call_procedure2);

			$clsdt=array(
				'proc'=>1,
				'closingdate'=>$dddate
			);
			$call_procedure1 = "CALL sp_get_payout_date(?,?)";
			$this->db->query($call_procedure1,$clsdt);
			mysqli_next_result( $this->db->conn_id );
		}
		header("Location:".base_url()."admin_closing/view_generate_payout");
	}

	/////////////////////////////////////////////////////////////////////////
	//////////         View Payout Report         ///////
	//////////////////////////////////////////////////////////////////////

	public function view_payout_report()
	{
		$data['form_name'] = "Select Payout Date";
		$data['table_name'] = "View Payout Report";

		$data['clsd'] = $this->db->where("tr_closing_status !=",1)->get("tr05_closing_date");

		$currdate = $this->input->post('dddate');

		if($currdate != '' && $currdate != '-1' && $currdate != '-1')
		{
			$queery = "DATE_FORMAT(tr04_payout_detail.`tr_to_payout_date`,'%Y-%m-%d') = DATE_FORMAT('".$currdate."','%Y-%m-%d')";
			$queery = $queery." AND `tr04_payout_detail`.`tr_payout_type` = 1";
			$queery = $queery." ORDER BY `tr04_payout_detail`.`tr_from_payout_date` DESC";
			$dt = array(
				'queery' => $queery
			);
			$call_procedure = "CALL sp_admin_payout_report(?)";
			$data['payout_details'] = $this->db->query($call_procedure,$dt);
			mysqli_next_result( $this->db->conn_id );
		}

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_payout_report',$data);
		$this->load->view('common/footer');
	}

	/////////////////////////////////////////////////////////////////////////
	//////////         View Payout Publish         ///////
	//////////////////////////////////////////////////////////////////////

	public function view_payout_publish()
	{
		$data['form_name'] = "Select Publish Date";

		$pdate = $this->input->post('dddate');

		if($pdate != '' && $pdate != '-1' && $pdate != '-1')
		{
			$procedure_data = array(
				'closing_to_date'=>$pdate
			);
			$call_procedure = "CALL sp_publish_payout(?)";
			$this->db->query($call_procedure,$procedure_data);

			$procedure_data2 = array(
				'closing_to_date'=>$pdate
			);
			$call_procedure2 = "CALL sp_matching_release(?)";
			$this->db->query($call_procedure2,$procedure_data2);
		}

		$data['clsd'] = $this->db->where("tr_closing_status",2)->get("tr05_closing_date");

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_payout_publish',$data);
		$this->load->view('common/footer');
	}

	/////////////////////////////////////////////////////////////////////////
	//////////         View Payment Release         ///////
	//////////////////////////////////////////////////////////////////////

	public function view_payment_release()
	{
		$data['form_name'] = "Payment";
		$data['table_name'] = "View Payment Release";

		$call_procedure = "CALL sp_payment_report()";
		$data['record'] = $this->db->query($call_procedure);

		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('closing/view_payment_release',$data);
		$this->load->view('common/footer');
	}

	public function insert_payment_release()
	{
		if($this->session->userdata('profile_id')==0)
		{
			$txtquid = $this->input->post('txtquid');
			$str = $this->input->post('txtquid');
			$c = explode(',', $str);
			$no=count($c);
			for($i=0; $i<$no-1; $i++)
			{
				$co = explode(',', $str);
				$co1=explode('_', $co[$i]);
				
				$curr_bal = $this->db->query("SELECT get_available_bal(".trim($co1[0]).",1) AS BAL")->row()->BAL;
				$transid = $this->db->query("SELECT get_transaction_id() AS transid")->row()->transid;
				
				$insert_data = array(
					'm_u_id'=>$co1[0],
					'm_trans_id'=>$transid,
					'm_cramount'=>0,
					'm_dramount'=>$co1[1],
					'm_description'=>"Payment Release.",
					'm_transdate'=>YmdHis,
					'm_refrence_id'=>$this->session->userdata('profile_id'),
					'm_ledger_type'=>2,
					'm_bal_type'=>1,
					'm_current_balance'=>($curr_bal-$co1[1]),
					'm_datetime'=>YmdHis
				); 
				$this->db->insert('tr07_manage_ledger', $insert_data);
			}
		}
	}

}
?>