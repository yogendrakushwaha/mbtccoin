<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_action extends CI_Controller {

	//////////////////////////////////////////////////////////////////////////
	//////////     Constructor In Member Controller    ///////
	/////////////////////////////////////////////////////////////////////////

	public function __construct()
	{
		parent::__construct();
		$this->_is_logged_in();
		$this->data['page'] = "User Pannel";
		$this->data['bal'] = $this->db->query("SELECT `get_available_bal`(".$this->session->userdata('profile_id').",1) AS bal")->row()->bal;
		$this->data['check_topup'] = $this->db->query("SELECT COUNT(*) as check_topup FROM `tr10_mem_topup` WHERE `tr10_mem_topup`.`tr10_mem_uid` = ".$this->session->userdata('profile_id')." LIMIT 1")->row()->check_topup;		
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     Check Login    ///////
	//////////////////////////////////////////////////////////////////////

	public function _is_logged_in() 
	{
		if ($this->session->userdata('user_id')=="")
		{
			redirect('auth/index');
			die();
		}
	}

	public function index()
	{
		header("Location:".base_url()."userprofile/dashboard");
	}

	/*|---------------------------------------------------------|*/
	/*|------  		     BEGIN INSERT FUND REQUSET	     -------|*/
	/*|---------------------------------------------------------|*/

	public function view_fund_request()
	{
		$data['form_name'] = "Fund Request";
		$data['table_name'] = "View Fund Request";
		$data['rid'] = $this->db->where("tr09_req_uid",$this->session->userdata('profile_id'))->get("view_fund_request");
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user_Action/view_fund_request',$data);
		$this->load->view('common/footer');
	}

	public function insert_fund_requset()
	{
		$res = $this->user_model->insert_fund_requset();
		$this->session->set_flashdata('info',$res);
		header("Location:".base_url()."User_action/view_fund_request");
	}

	/*|---------------------------------------------------------|*/
	/*|------  		     BEGIN INSERT FUND TRANSFER	     -------|*/
	/*|---------------------------------------------------------|*/

	public function view_fund_transfer()
	{
		$data['form_name'] = "Fund Transfer";
		$data['table_name'] = "View Fund Transfer";
		$data['rid'] = $this->db->where("tr09_req_uid",$this->session->userdata('profile_id'))->get("view_fund_request");
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user_Action/view_fund_transfer',$data);
		$this->load->view('common/footer');
	}

	public function insert_fund_transfer()
	{
		$res = $this->user_model->insert_fund_transfer();
		$this->session->set_flashdata('info',$res);
		header("Location:".base_url()."User_action/view_fund_transfer");
	}

	/*|---------------------------------------------------------|*/
	/*|------  		     BEGIN MEMBER TOPUP			     -------|*/
	/*|---------------------------------------------------------|*/

	public function view_member_topup()
	{
		$data['form_name']= "View Member Topup";
		$data['pack'] = $this->db->where('m_pack_id',2)->get("m06_package");
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('member/view_member_topup',$data);
		$this->load->view('common/footer');
	}

	public function insert_member_topup()
	{
		$res = $this->user_model->insert_member_topup();
		$this->session->set_flashdata('info',$res);
		echo $res;
	}

	/*|---------------------------------------------------------|*/
	/*|------  		     BEGIN TOOLS DISPLAY		     -------|*/
	/*|---------------------------------------------------------|*/

	public function view_member_tools()
	{
		$data['form_name']= "View Details";
		$data['tools'] = $this->db->where('m_tool_status',1)->get("m07_tools");
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user_action/view_tools',$data);
		$this->load->view('common/footer');
	}

	public function user_reply_tool()
	{
		$data = array(
			"m_tool_user_response" => $this->input->post('txtresponse')
		);
		$this->db->where("m_tool_id",$this->uri->segment(3))->update("m07_tools",$data);
		redirect("user_action/view_member_tools");
	}

	/////////////////////////////////////////////////////////////////////////
	//////////         View Direct Report         ///////
	//////////////////////////////////////////////////////////////////////

	public function view_direct_report()
	{
		$data['form_name'] = "Select Direct Date";
		$data['table_name'] = "View Direct Report";

		$data['clsd'] = $this->db->where("tr_closing_status !=",1)->get("tr05_closing_date");

		$currdate = $this->input->post('dddate');

		if($currdate != '' && $currdate != '-1' && $currdate != '-1')
		{
			$queery = "DATE_FORMAT(tr04_payout_detail.`tr_to_payout_date`,'%Y-%m-%d') <= DATE_FORMAT('".$currdate."','%Y-%m-%d')";
			$queery = $queery." AND `tr04_payout_detail`.`tr_payout_type` = 2";
			$queery = $queery." AND `tr04_payout_detail`.`or_m_reg_id` = ".$this->session->userdata('profile_id');
			$queery = $queery." ORDER BY `tr04_payout_detail`.`tr_from_payout_date` DESC";
			$dt = array(
				'queery' => $queery
			);
			$call_procedure = "CALL sp_admin_payout_report(?)";
			$data['payout_details'] = $this->db->query($call_procedure,$dt);
			mysqli_next_result( $this->db->conn_id );
		}

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('closing/view_direct_report',$data);
		$this->load->view('common/footer');
	}

	/////////////////////////////////////////////////////////////////////////
	//////////         View Payout Report         ///////
	//////////////////////////////////////////////////////////////////////

	public function view_payout_report()
	{
		$data['form_name'] = "Select Payout Date";
		$data['table_name'] = "View Payout Report";

		$data['clsd'] = $this->db->where("tr_closing_status !=",1)->get("tr05_closing_date");

		$currdate = $this->input->post('dddate');

		if($currdate != '' && $currdate != '-1' && $currdate != '-1')
		{
			$queery = "DATE_FORMAT(tr04_payout_detail.`tr_to_payout_date`,'%Y-%m-%d') = DATE_FORMAT('".$currdate."','%Y-%m-%d')";
			$queery = $queery." AND `tr04_payout_detail`.`tr_payout_type` = 1";
			$queery = $queery." AND `tr04_payout_detail`.`or_m_reg_id` = ".$this->session->userdata('profile_id');
			$queery = $queery." ORDER BY `tr04_payout_detail`.`tr_from_payout_date` DESC";
			$dt = array(
				'queery' => $queery
			);
			$call_procedure = "CALL sp_admin_payout_report(?)";
			$data['payout_details'] = $this->db->query($call_procedure,$dt);
			//echo $this->db->last_query(); die;
			mysqli_next_result( $this->db->conn_id );
		}

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('closing/view_payout_report',$data);
		$this->load->view('common/footer');
	}

}

?>