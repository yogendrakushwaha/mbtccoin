<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Get_details extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
		}
		
		public function index()
		{
			die;
		}
		
		//-----------------Get Setting Details------------------
		
		public function get_setting_details()
		{
			$this->db->where('m00_id',$this->input->post('id'));
			$query['rec'] = $this->db->get('m00_setconfig')->result();
			$csrf =  $this->security->get_csrf_hash();
			$query['csrf'] = $csrf;
			$json=json_encode($query);
			echo $json;
		}
		
		//-------------------Get member name------------------
		
		public function get_member_name()
		{
			$id="";
			$id=$this->input->post('txtintuserid');
			$query['name'] = get_intro_name($id);						// Get Details From Admin_Helper
			$csrf =  $this->security->get_csrf_hash();
			$query['csrf'] = $csrf;
			$json=json_encode($query);
			echo $json;;
		}
		
		//Get Extreme parent on behalf of introducer
		public function get_parent_detail()
		{
			$i="";
			$lrt=0;
			$rrt=0;
			$t=0;
			$id1=$this->input->post('txtintuserid');
			$this->db->where('or_m_user_id',$id1);
			$data['lid']=$this->db->get('m03_user_detail');
			foreach($data['lid']->result() as $row)
			{
				$i=$row->or_m_reg_id;
				break;
			}
			if($i!="")
			{
				
				$id=$i;
				$leg = $this->input->post('leg');
				$query = "select fetch_extreme_left($id,'$leg') as res"; 
				$res = $this->db->query($query)->row();
				if($res)
					$mid = $res->res;
				else
					$mid = "Error";
			}
			else
			{
				$mid = "This id is not registered";
			}
			
			$csrf =  $this->security->get_csrf_hash();
			$data1['csrf'] = $csrf;
			$data1['rec'] = $mid;
			$json=json_encode($data1);
			echo $json;
		}
		
		
		//-----------------Get City With Respective State--------------
		
		public function get_city()
		{
			$this->db->where('m_parent_id',$this->input->post('ddstate'));
			$this->db->where('m_status',1);
			$query['rec'] = $this->db->get('m02_location')->result();
			$csrf =  $this->security->get_csrf_hash();
			$query['csrf'] = $csrf;
			$json=json_encode($query);
			echo $json;
		}
		
		//Validate Mobile No
		public function validate_mobile()
		{
			$query_D = $this->db->query("SELECT COUNT(*) as count_mobile FROM `m03_user_detail` where `or_m_mobile_no`=".$this->input->post('phone'));
			$rows = $query_D->row();
			$query['csrf'] = $this->security->get_csrf_hash();
			$query['mob'] = $rows->count_mobile;
			$json=json_encode($query);
			echo $json;
		}
		
		//Validate Pancard
		public function validate_pancard()
		{
			$query_D=$this->db->query("SELECT COUNT(*) as pan FROM `m04_user_bank` WHERE `m04_user_bank`.`or_m_b_pancard`='".$this->input->post('txtpancard')."'");
			$rows=$query_D->row();
			$query['csrf'] = $this->security->get_csrf_hash();
			$query['pan'] = $rows->pan;
			$json=json_encode($query);
			echo $json;
		}
		
		//Validate Email ID
		public function validate_email()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$query=$this->db->query("SELECT COUNT(*) as email FROM `m03_user_detail` WHERE `m03_user_detail`.`or_m_email`='".$this->input->post('txtemail')."'");
			$rows=$query->row();
			echo $rows->email;
		}
		
		//Get Ticket Details From Ticket Id
		public function get_ticket_details()
		{
			$query['rec'] = $this->db->query("SELECT * FROM `view_ticket` where TICKET_ID=".$this->input->post('id'))->result();
			$csrf =  $this->security->get_csrf_hash();
			$query['csrf'] = $csrf;
			$json=json_encode($query);
			echo $json;
		}
		
		//Validate USer Exist Or Not
		public function validte_user()
		{
			$query = $this->db->get_where('tr01_login',array('or_login_id'=>$this->input->post('txtlogin')));
			$row2 = $query->row();
			if($query->num_rows()>0)
			{
				$data['vali'] = 1;
			}
			else
			{
				$data['vali'] = 0;
			}
			$csrf =  $this->security->get_csrf_hash();
			$query['csrf'] = $csrf;
			$json=json_encode($query);
			echo $json;
		}

		//Check User Topup
		public function view_check_topup()
		{
			$query = $this->db->get_where('m03_user_detail',array('or_m_user_id'=>$this->input->post('txtintuserid')))->row();
			$m_topup_pin_id = $query->m_topup_pin_id;
			
			if($m_topup_pin_id != '0')
			{
				$data['vali'] = 1;
			}
			else
			{
				$data['vali'] = 0;
			}
			$csrf =  $this->security->get_csrf_hash();
			$data['csrf'] = $csrf;
			$json=json_encode($data);
			echo $json;
		}
		
		//-----------------Get Tree Tooltip----------------------
		public function get_tree_tooltips()
		{
			$query['json_result'] = $this->db->query("call sp_tree_tooltip(".$this->input->post('regid').")")->result();
			mysqli_next_result( $this->db->conn_id );
			$query['csrf'] = $this->security->get_csrf_hash();
			$json = json_encode($query);
			echo $json;
		}
		
	}
	
?>