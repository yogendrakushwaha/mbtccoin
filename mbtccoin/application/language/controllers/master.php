<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Master extends CI_Controller {
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Constructor In Master Controller    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function __construct()
		{
			parent::__construct();
			$this->_is_logged_in();
			$this->data['page'] = "Master Pannel"; 
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Check Login    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function _is_logged_in() 
		{
			if ($this->session->userdata('user_id')=="" || $this->session->userdata('profile_id')!="0")
			{
				redirect('auth/logout');
				die();
			}
		}
		
		public function dashboard_data()
		{
			$data['form_name'] = "Dashboard";
			
			$data['ticket'] = $this->db->where("TICKET_STATUS_ID",1)->get("view_ticket");
			
			$call_procedure = ' CALL sp_admin_dashboard()';
			$data['rid'] = $this->db->query($call_procedure)->row();
			mysqli_next_result( $this->db->conn_id );
			
			return $data;
		}
		
		public function index()
		{
			$data = $this->dashboard_data();
			// for topup users
			$data['rec1'] = $this->db->query("SELECT or_m_name,`get_package_amount`(3,m_topup_pin_id) AS package FROM m03_user_detail WHERE DATE_FORMAT(`m03_user_detail`.`or_m_topup_date`,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') and `m03_user_detail`.`m_topup_pin_id` <> 0")->result();
			// for Non topup users
			$data['rec2'] = $this->db->query("SELECT or_m_name,`get_package_amount`(3,m_topup_pin_id) AS package FROM m03_user_detail WHERE DATE_FORMAT(`m03_user_detail`.`or_m_regdate`,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') and `m03_user_detail`.`m_topup_pin_id` = 0 ")->result();
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/dashboard',$data);
			$this->load->view('common/footer');
		}
		
		public function dashboard()
		{
			$data = $this->dashboard_data();
			
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/dashboard',$data);
			$this->load->view('common/footer');
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Main Admin Login    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function view_soft_login()
		{
			$data['form_name'] = "Admin Login Details";
			$data['config'] = $this->mastermodel->select_config();
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_soft_login',$data);
			$this->load->view('common/footer');
		}
		
		public function update_mainconfig()
		{
			$this->mastermodel->update_config();
			$this->session->set_flashdata('info','Setting Updated Successfully!!');
			header("Location:".base_url()."master/view_soft_login");
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Main Admin Login    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function view_soft_setting()
		{
			$data['form_name'] = "Admin Setting Details";
			$data['table_name'] = "Admin Setting Details";
			$data['config'] = $this->db->get("m00_setconfig");
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_soft_setting',$data);
			$this->load->view('common/footer');
		}
		
		public function update_setting()
		{
			$data = [
					"m00_name"=>$this->input->post("txtname"),
					"m00_value"=>$this->input->post("txtval"),
					"m00_desc"=>$this->input->post("txtdesc"),
					];
			$this->db->where("m00_id",$this->uri->segment(3))->update("m00_setconfig",$data);
			$this->session->set_flashdata('info','Setting Updated Successfully!!');
			header("Location:".base_url()."master/view_soft_setting");
		}
		
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Start News Master    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function view_news()
		{
			$data['form_name'] = "View News";
			$data['table_name'] = "View News";
			$data['content']=$this->mastermodel->view_news();
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('master/view_news',$data);
			$this->load->view('common/footer');
		}
		
		public function add_news()
		{
			$data['form_name'] = "Add News";
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('master/add_news',$data);
			$this->load->view('common/footer');
		}
		
		public function insert_news()
		{
			$this->mastermodel->insert_news();
			$this->session->set_flashdata('info','News Inserted Successfully!!');
			header("Location:".base_url()."Master/view_news");
		}
		
		public function edit_news()
		{
			$data['form_name'] = "Edit News";
			$this->db->where('m_news_id',$this->uri->segment(3));
			$data['content']=$this->db->get('m24_news');
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('master/view_edit_news',$data);
			$this->load->view('common/footer');
		}
		
		public function update_news()
		{
			$this->mastermodel->update_news();
			$this->session->set_flashdata('info','News Updated Successfully!!');
			header("Location:".base_url()."Master/view_news");
		}
		
		public function delete_news()
		{
			$this->mastermodel->delete_news();
			$this->session->set_flashdata('info','News Status Updated Successfully!!');
			header("Location:".base_url()."Master/view_news");
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Start Bank Master    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function view_bank()
		{
			$data['form_name'] = "Manage Bank";
			$data['table_name'] = "View Bank";
			$data['info']=$this->mastermodel->view_bank();
			mysqli_next_result( $this->db->conn_id );
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_bank_master',$data);
			$this->load->view('common/footer');
		}
		
		public function inser_bank_master()
		{			
			$this->mastermodel->insert_bank();
			$this->session->set_flashdata('info','Bank Inserted Successfully!!');
			header("Location:".base_url()."Master/view_bank");
		}
		
		public function view_edit_bank()
		{	
			$data['form_name'] = "Edit Bank";
			$this->db->where('m_bank_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m01_bank');
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_edit_bank',$data);
			$this->load->view('common/footer');
		}
		
		public function view_update_bank()
		{			
			$this->mastermodel->view_update_bank();
			$this->session->set_flashdata('info','Bank Details Updated Successfully!!');
			header("Location:".base_url()."Master/view_bank");
		}
		
		public function bank_status()
		{			
			$this->mastermodel->bank_status();
			$this->session->set_flashdata('info','Bank Status Updated Successfully!!');
			header("Location:".base_url()."Master/view_bank");
		}
		
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Start Tool Master    ///////
		//////////////////////////////////////////////////////////////////////

		public function view_tools()
		{
			$data['form_name'] = "Manage Tools";
			$data['table_name'] = "View Tools";
			$data['info']=$this->db->get('m07_tools');
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_tool_master',$data);
			$this->load->view('common/footer');
		}

		public function insert_tools_master()
		{			
			$this->mastermodel->insert_tools_master();
			$this->session->set_flashdata('info','Tool Inserted Successfully!!');
			header("Location:".base_url()."Master/view_tools");
		}

		public function view_edit_tools()
		{	
			$data['form_name'] = "Edit Tool";
			$data['info']=$this->db->where('m_tool_id',$this->uri->segment(3))->get('m07_tools')->row();
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_edit_tools',$data);
			$this->load->view('common/footer');
		}

		public function view_update_tools()
		{			
			$this->mastermodel->view_update_tools();
			$this->session->set_flashdata('info','Tool Details Updated Successfully!!');
			header("Location:".base_url()."Master/view_tools");
		}

		public function tools_status()
		{			
			$this->mastermodel->tools_status();
			$this->session->set_flashdata('info','Tool Status Updated Successfully!!');
			header("Location:".base_url()."Master/view_tools");
		}

				public function view_send_sms()
		{
			$data['form_name'] = "Send Message";
			$data['table_name'] = "All Users";
			$data['rid']=$this->db->query(" SELECT or_m_mobile_no,or_m_name FROM m03_user_detail GROUP BY or_m_mobile_no ");
			$this->load->view('common/header');
			$this->load->view('common/menu',$this->data);
			$this->load->view('Master/view_sms',$data);
			$this->load->view('common/footer');
		}
		
	public function send_sms_hid()
	{
		$mid=$this->input->post('txtquid');
		$msg=$this->input->post('txtdesc');
		$pr=explode(',',$mid);
		for($t=0;count($pr)>$t;$t++)
		{
			$uid=$pr[$t];
			$this->crud_model->send_sms($uid,$msg);
		}
		
	}

	public function view_atransfer_pin_report()
	{
		$data['form_name'] = "Search Transfer Pin Report";
		$data['table_name'] = "View Transfer Pin Report";

		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}
		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		$condition='';

		if($todate!='' && $fromdate!='')
		{
			$condition=$condition."`m09_pin_detail`.`m_pin_gen_date` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d %H:%i:%s') and DATE_FORMAT('$todate','%Y-%m-%d %H:%i:%s') and ";
		}

		if($this->input->post('txtpin_no')!="")
		{
			$condition=$condition." `m09_pin_detail`.`m_pin`= '".$this->input->post('txtpin_no')."' and ";
		}

		$condition=$condition."`m09_pin_detail`.`m_pin`!='0' ";
		$condition=$condition." ORDER BY `m09_pin_detail`.`m_pin_gen_date` DESC";

		$record=array(
			'query'=>$condition
		);
		$query = " CALL sp_pin_transfer_full_report(?" . str_repeat(",?", count($record)-1) . ") ";
		$data['rec']=$this->db->query($query, $record);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_transfer_pin_report',$data);
		$this->load->view('common/footer');
	}
		
	}
?>				