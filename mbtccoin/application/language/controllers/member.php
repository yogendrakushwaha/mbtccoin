<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	/////////////////////////////////////////////////////////////////////////
	//////////     Constructor In Member Controller    ///////
	//////////////////////////////////////////////////////////////////////

	public function __construct()
	{
		parent::__construct();
		$this->_is_logged_in();
		$this->data['page'] = "Member Panel";
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     Check Login    ///////
	//////////////////////////////////////////////////////////////////////

	public function _is_logged_in() 
	{
		if ($this->session->userdata('user_id')=="" || $this->session->userdata('profile_id')!="0")
		{
			redirect('auth/logout');
			die();
		}
	}

	public function index()
	{
		header("Location:".base_url()."master/view_mainconfig");
	}

	/*|-----------------------------------------|*/
	/*|------     USER REGISTRATION      -------|*/
	/*|-----------------------------------------|*/

	public function join_member()
	{
		$data['form_name'] = "Member Joining";

		$this->db->where('m_parent_id',1);
		$this->db->where('m_status',1);
		$data['state']=$this->db->get('m02_location');

		$data['bank']=$this->db->where("m_bank_status",1)->get('m01_bank');

		$this->db->where('m_pack_status',1);
		$data['pack']=$this->db->get('m06_package');

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('Member/registration',$data);
		$this->load->view('common/footer');
	}


	// -------------USER REGISTRATION INSERTION---------------

	public function register_candidate()
	{
		$output = $this->member_model->signup();

		echo $output;
	}


	// -------------USER REGISTRATION INSERTION---------------

	public function view_member_topup()
	{
		$data['form_name'] = "Member Topup";
		$data['pack'] = $this->db->where('m_pack_id',2)->get("m06_package");
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('Member/view_member_topup',$data);
		$this->load->view('common/footer');
	}



	// -------------USER DETAILS UPDATION VIEW---------------

	public function view_member_edit()
	{	
		$data['form_name'] = "Edit Member";

		$data['uid'] = $uid = $this->input->post('txtlogin');

		$data['rec'] = '';

		if($uid != '')
		{
			$this->db->where('m_parent_id',1);
			$this->db->where('m_status',1);
			$data['state']=$this->db->get('m02_location');

			$id = get_uid($uid);

			$condition = "`m03_user_detail`.`or_m_reg_id` = '".$id."'";
			$call_procedure = ' CALL sp_member_detail("'. $condition .'")';
			$data['rec'] = $this->db->query($call_procedure)->row();
			mysqli_next_result( $this->db->conn_id );
		}

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('Member/view_member_edit',$data);
		$this->load->view('common/footer');
	}

	// -------------USER DETAILS UPDATION---------------

	public function update_member()
	{
		$this->member_model->update_member();
		$this->session->set_flashdata('info','Updated Successfully!!');
		header("Location:".base_url()."member/view_member_edit");
	}

	/*|--------------------------------------|*/
	/*|------     VIEW USER PROFILE   -------|*/
	/*|--------------------------------------|*/

	public function view_member_details()
	{	
		$data['form_name'] = "View Member Profile";
		$data['uid'] = $uid = $this->input->post('txtlogin');

		$data['rec'] = '';
		if($uid)
		{
			$id = get_uid($uid);
			$condition = "`m03_user_detail`.`or_m_reg_id` = '".$id."'";
			$call_procedure = ' CALL sp_member_detail("'. $condition .'")';
			$data['rec'] = $this->db->query($call_procedure)->row();
			mysqli_next_result( $this->db->conn_id );
		}	
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('Member/view_member_details',$data);
		$this->load->view('common/footer');
	}

	/*|------------------------------------------|*/
	/*|------     USER SEARCH MEMEBR      -------|*/
	/*|------------------------------------------|*/

	public function view_all_member()
	{
		$data['form_name'] = "View Search Member";
		$data['table_name'] = "View All Member";
		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition."`m03_user_detail`.`or_m_regdate` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('$todate','%Y-%m-%d') and ";
		}

		if($this->input->post('txtlogin')!="" && $this->input->post('txtlogin')!="0")
		{
			$id=get_uid($this->input->post('txtlogin'));
			$condition=$condition." `m03_user_detail`.`or_m_reg_id`= ".$id."  and";
		}

		if($this->input->post('txtmob')!="" && $this->input->post('txtmob')!="0")
		{
			$condition=$condition." `m03_user_detail`.`or_m_mobile_no`= ".$this->input->post('txtmob')."  and";
		} 

		if($this->input->post('txtemail')!="" && $this->input->post('txtemail')!="0")
		{
			$condition=$condition." `m03_user_detail`.`or_m_email`= '".$this->input->post('txtemail')."'  and";
		}

		$condition=$condition." `m03_user_detail`.`or_m_reg_id` !=0 ";
		$condition=$condition." ORDER BY `m03_user_detail`.`or_m_reg_id` DESC";

		$call_procedure = ' CALL sp_member_detail("'.$condition.'")';
		$data['rid']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_all_member',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------------|*/
	/*|------     USER ACTIVATION VIEW      -------|*/
	/*|--------------------------------------------|*/

	public function view_activate_members()
	{
		$data['form_name'] = "Activate Member";
		$data['table_name'] = "View Deactive Member";

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_status` = 0")';
		$data['rec']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_activate_member',$data);
		$this->load->view('common/footer');
	}

	/*-------------ACTIVATE USER-------------------*/

	public function update_activate_member()
	{
		$regid=$this->input->post('txtquid');
		$this->db->query("CALL sp_change_status_of_user('".$regid."',1)");
		$this->session->set_flashdata('info','Member Activated Successfully!!');
		header("Location:".base_url()."member/view_activate_members");
	}

	/*|----------------------------------------------|*/
	/*|------     USER DEACTIVATION VIEW      -------|*/
	/*|----------------------------------------------|*/

	public function view_deactivate_members()
	{
		$data['form_name'] = "Deactivate Member";
		$data['table_name'] = "View Active Member";

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_status` = 1")';
		$data['rec']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_deactivate_member',$data);
		$this->load->view('common/footer');
	}

	/*-------------DEACTIVATE USER-------------------*/

	public function update_deactivate_member()
	{
		$regid=$this->input->post('txtquid');
		$this->db->query("CALL sp_change_status_of_user('".$regid."',2)");
		$this->session->set_flashdata('info','Member Deactivated Successfully!!');
		header("Location:".base_url()."member/view_deactivate_members");
	}

	/*|-----------------------------------------------------|*/
	/*|------   ADD BANK DETAILS      -------|*/
	/*|-----------------------------------------------------|*/

	public function view_bank_details()
	{
		$data['form_name'] = "View Bank Details";
		$data['table_name'] = "Update Bank Details";

		$data['bank'] = $this->db->where("m_bank_status",1)->get("m01_bank");

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_status` = 1")';
		$data['bank_details']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_bank_details',$data);
		$this->load->view('common/footer');
	}

	public function update_bank_details()
	{
		$this->member_model->update_bank_details();
		$this->session->set_flashdata('info','Bank Details Updated Successfully!!');
		header("Location:".base_url()."member/view_bank_details");
	}

	/*|--------------------------------------------------|*/
	/*|------     	USER UPDATE PASSWORD	      -------|*/
	/*|--------------------------------------------------|*/


	public function change_password()
	{
		$data['form_name'] = "Change Password";
		$data['table_name'] = "View All Member";

		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition."`m03_user_detail`.`or_m_regdate` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('$todate','%Y-%m-%d') and ";
		}

		if($this->input->post('txtlogin')!="" && $this->input->post('txtlogin')!="0")
		{
			$id=get_uid($this->input->post('txtlogin'));
			$condition=$condition." `m03_user_detail`.`or_m_reg_id`= ".$id."  and";
		}

		if($this->input->post('txtmob')!="" && $this->input->post('txtmob')!="0")
		{
			$condition=$condition." `m03_user_detail`.`or_m_mobile_no`= ".$this->input->post('txtmob')."  and";
		} 

		if($this->input->post('txtemail')!="" && $this->input->post('txtemail')!="0")
		{
			$condition=$condition." `m03_user_detail`.`or_m_email`= '".$this->input->post('txtemail')."'  and";
		}

		$condition=$condition." `m03_user_detail`.`or_m_reg_id` !=0 ";
		$condition=$condition." ORDER BY `m03_user_detail`.`or_m_reg_id` DESC";

		$call_procedure = ' CALL sp_member_detail("'.$condition.'")';
		$data['rid']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('Member/view_change_password',$data);
		$this->load->view('common/footer');
	}

	/*-------------UPDATE PASSWORD---------------*/

	public function update_password()
	{
		$ulgpd=array(
			'or_login_pwd'=>$this->input->post('txtnew')
		);
		$this->db->where('or_user_id',$this->uri->segment(3));
		$this->db->update('tr01_login',$ulgpd);
		$this->session->set_flashdata('info','Password Updated Successfully!!');
		header("Location:".base_url()."member/change_password");
	}

	/*-------------UPDATE PIN PASSWORD---------------*/

	public function update_pin_password()
	{
		$ulgpd=array(
			'or_pin_pwd'=>$this->input->post('txtnew')
		);
		$this->db->where('or_user_id',$this->uri->segment(3));
		$this->db->update('tr01_login',$ulgpd);
		$this->session->set_flashdata('info','Transaction Password Updated Successfully!!');
		header("Location:".base_url()."member/change_password");
	}

	/*|---------------------------------------------------------|*/
	/*|------  		     USER DIRECT REFFERAL            -------|*/
	/*|---------------------------------------------------------|*/

	public function view_direct_referal()
	{
		$data['form_name'] = "Search Direct Referal";
		$data['table_name'] = "View Direct Referal";
		$data['rec'] = '';
		if($this->input->post('txtuserid'))
		{
			$id = get_uid($this->input->post('txtuserid'));

			$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_intr_id` = '.$id.'")';
			$data['rec'] = $this->db->query($call_procedure);
			mysqli_next_result( $this->db->conn_id );
		}

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_direct_referal',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------------------------|*/
	/*|------        			USER DOWNLINE   	    -------|*/
	/*|--------------------------------------------------------|*/

	public function view_user_downline()
	{
		$leg='';
		$data['rec'] = '';
		$data['form_name'] = "Search Downline Member";
		$data['table_name'] = "View Downline Member";
		$position=$this->input->post('ddposition');
		if($this->input->post('txtuserid'))
		{
			$regid=get_uid($this->input->post('txtuserid'));

			if($position=='')
			{	
				$leg='1';
			}
			else
			{
				$leg=$position;
			}

			$this->db->query("CALL get_downline(".$regid.",'".$leg."')");

			$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_reg_id`  IN (SELECT member_id FROM tmp_downline)")';
			$data['rec'] = $this->db->query($call_procedure);

			mysqli_next_result( $this->db->conn_id );
		}
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_user_downline',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------------------|*/
	/*|------     		USER TREE STRUCTURE	      -------|*/
	/*|--------------------------------------------------|*/

	public function tree()
	{
		$data['form_name'] = "Tree Stucture";

		$id=$this->uri->segment(3);
		$this->db->query("CALL `sp_user_at_level`($id,3)");
		$data['tr']=$this->db->query("SELECT * FROM `view_tree` WHERE `REGID` IN (SELECT * FROM total_user)");

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_tree',$data);
		$this->load->view('common/footer');
	}

	/*----------------SEARCH USER ID IN TREE-------------------*/

	public function get_tree()
	{
		if($this->input->post('search_id')!='' || $this->input->post('search_id')!='0')
		{
			$this->db->where('or_m_user_id',trim($this->input->post('search_id')));
			$data['rec']=$this->db->get('m03_user_detail');
			foreach($data['rec']->result() as $row)
			{
				$reg_id=$row->or_m_reg_id;
				break;
			}
			header("Location:".base_url()."index.php/member/tree/".$reg_id);
		}
	}

	/*|---------------------------------------------------------------------|*/
	/*|------    			  LEDGER REPORT OF USER            		 -------|*/
	/*|---------------------------------------------------------------------|*/

	public function view_ledger_report()
	{
		$condition="";
		$todate=0;
		$fromdate=0;

		$data['form_name'] = "Search Member Ledger";
		$data['table_name'] = "View Member Ledger";

		if($this->input->post('end')!="")
		{
			$todate=date('Y-m-d',strtotime($this->input->post('end')));
		}

		if($this->input->post('start')!="")
		{
			$fromdate=date('Y-m-d',strtotime($this->input->post('start')));
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition." `LEDGER_DATETIME`>=DATE_FORMAT('$fromdate','%d-%b-%Y') AND `LEDGER_DATETIME`<=DATE_FORMAT('$todate','%d-%b-%Y') AND ";
		}

		$condition=$condition." `LEDGER_UID`!=0 ";

		$data['rid']=$this->db->query("SELECT * FROM view_ledger WHERE ".$condition);

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_ledger_report',$data);
		$this->load->view('common/footer');
	}



	/*|------------------------------------------------|*/
	/*|------  			ADMIN REPLY      		-------|*/
	/*|------------------------------------------------|*/

	public function view_admin_reply()
	{
		$data['form_name'] = "Admin Reply";
		$data['table_name'] = "View Member Ticket";

		$this->db->where('TICKET_STATUS_ID',1);
		$data['rec']=$this->db->get('view_ticket');
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_admin_reply',$data);
		$this->load->view('common/footer');
	}

	/*-------------DEACTIVATE TICKET---------------*/

	public function deactive_ticket()
	{
		$data=array(
			'tr_ticket_id'=>$this->uri->segment(3),
			'tr_ticket_userid'=> '',
			'tr_ticket_title'=>'',
			'tr_ticket_desc'=>'',
			'tr_ticket_reply'=>'',
			'tr_ticket_status'=>$this->uri->segment(4),
			'tr_ticket_date'=>'',
			'proc'=>3
		);
		$query = " CALL sp_ticket(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
		$this->db->query($query, $data);
		$query1=$this->db->query("SELECT @msg as message");
		$row = $query1->row()->message;
		$this->session->set_flashdata('info',$row);
		header("Location:".base_url()."member/view_admin_reply");
	}

	/*-------------UPDATE REPLY---------------*/

	public function update_reply()
	{
		$data=array(
			'tr_ticket_id'=>trim($this->input->post('txtid')),
			'tr_ticket_userid'=> '',
			'tr_ticket_title'=>'',
			'tr_ticket_desc'=>'',
			'tr_ticket_reply'=>trim($this->input->post('txtreply')),
			'tr_ticket_status'=>'',
			'tr_ticket_date'=>'',
			'proc'=>2
		);
		$query = " CALL sp_ticket(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
		$this->db->query($query, $data);
		$query1=$this->db->query("SELECT @msg as message");
		$row = $query1->row()->message;
		$this->session->set_flashdata('info',$row);
		header("Location:".base_url()."member/view_admin_reply");
	}

	/*-------------FUND REQUEST---------------*/

	public function view_fund_request()
	{
		$data['form_name'] = "Fund Request";
		$data['table_name']= "Manage Fund Request";
		$data['rid'] = $this->db->query("SELECT * FROM `view_fund_request` ORDER BY `view_fund_request`.`m_top_status` DESC");
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('member/view_fund_request',$data);
		$this->load->view('common/footer');
	}

	public function fund_request_action()
	{
		$res = $this->member_model->fund_request_action();
		$this->session->set_flashdata('info',$res);
		header("Location:".base_url()."member/view_fund_request");
	}

}
?>