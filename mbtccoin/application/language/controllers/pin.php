<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pin extends CI_Controller {

	/////////////////////////////////////////////////////////////////////////
	//////////     Constructor In Member Controller    ///////
	//////////////////////////////////////////////////////////////////////

	public function __construct()
	{
		parent::__construct();
		$this->_is_logged_in();
		$this->data['page'] = "Pin Panel";
				
		$lid = $this->session->userdata('profile_id');
		$this->data['rec1'] = $this->db->query("CALL `sp_member_detail`('`m03_user_detail`.`or_m_reg_id`= $lid ')")->row();
		mysqli_next_result( $this->db->conn_id );
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     Check Login    ///////
	//////////////////////////////////////////////////////////////////////

	public function _is_logged_in() 
	{
		if ($this->session->userdata('user_id')=="")
		{
			redirect('auth/logout');
			die();
		}
	}

	public function index()
	{
		header("Location:".base_url()."master/view_mainconfig");
	}

	/*|-----------------------------------------|*/
	/*|------      	  Genrate Pin        -------|*/
	/*|-----------------------------------------|*/


	public function generate_pin()
	{
		$data['form_name'] = "Genrate Pin";

		$this->db->where('m_pack_status',1);
		$data['package']=$this->db->get('m06_package');

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/generate_pin',$data);	
		$this->load->view('common/footer');
	}

	public function select_points()
	{
		$plan=$this->input->post('plan');
		$this->db->where('m_pack_id',$plan);
		$this->db->where('m_pack_status',1);
		$points=$this->db->get('m06_package')->row();

		$csrf =  $this->security->get_csrf_hash();
		$query['csrf'] = $csrf;
		$query['m_pack_fee'] = $points->m_pack_fee;
		$json=json_encode($query);
		echo $json;
	}

	public function insert_plan()
	{
		$data=array(
			'm_pin_id'=>'',
			'm_pack_id'=>$this->input->post('pln_nm'),
			'no_pin'=>$this->input->post('no_pin'),
			'm_pin_assigned_to'=>'',
			'login_id'=>$this->input->post('login_id'),
			'm_pin_status'=>'1',
			'm_pin_gen_date'=>YmdHis,
			'm_pin_gen_by'=>$this->session->userdata('profile_id'),
			'm_pin_is_reg'=>$this->input->post('pln_nm'),
			'proc'=>1
		);
		$query = " CALL sp_pin_genrate(?" . str_repeat(",?", count($data)-1) . ") ";
		$this->db->query($query, $data);
		header("Location:".base_url()."pin/generate_pin");
	}

	public function PinTransfer()
	{
		$data['form_name'] = "Pin Transfer";
		$data['table_name'] = "View Pin";

		$this->db->where('m_pack_status',1);
		$data['package']=$this->db->get('m06_package');
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/PinTransfer',$data);
		$this->load->view('common/footer');
	}

	public function select_pin_transfer()
	{
		$id=get_uid($this->uri->segment(4));

		$data=array(
			'user_id'=> $id,
			'quantity'=>$this->uri->segment(3),
			'm_pin_is_reg'=>$this->uri->segment(5),
			'm_pack_id'=>$this->uri->segment(6)
		);
		$query = " CALL sp_select_pin_trans(?" . str_repeat(",?", count($data)-1) . ") ";
		$data['rec'] = $this->db->query($query, $data);
		
		mysqli_next_result( $this->db->conn_id );
		$this->load->view('pin/select_pin_table',$data);
	}

	public function transfer_to_update()
	{
		$transfer = get_uid($this->input->post('login_id'));
		$login = get_uid($this->input->post('txtowner'));
		$pin=explode(',',$this->input->post('txtquid'));
		$no=(count($pin)-1);
		for($i=0; $i<$no; $i++)
		{
			$this->db->query("INSERT INTO `m10_pin_history` (`or_m_user_id`,`m_pin_id`,`m_pin_transfer_byid`,`m_pin_date`) VALUES ($transfer,$pin[$i],$login,NOW())");
			$this->db->query("UPDATE `m09_pin_detail` SET `m09_pin_detail`.`m_pin_assigned_to`=$transfer WHERE `m09_pin_detail`.`m_pin_id`=$pin[$i] AND `m09_pin_detail`.`m_pin_status`=1");
		}
		if($this->uri->segment(3) == 1)
		{
				redirect('pin/view_pin_transfer/1');
		}else{
				header("Location:".base_url()."pin/PinTransfer");
		}
		
	}

	/*------------------Cancel Pin Report------------------------*/

	public function view_cancel_pin()
	{
		$data['form_name'] = "Cancel Pin";
		$data['table_name'] = "View Pin Report";

		$this->db->where('m_pack_status',1);
		$data['package']=$this->db->get('m06_package');
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_cancel_pin',$data);
		$this->load->view('common/footer');
	}


	public function getcancel_pin_report()
	{
		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('txtfrom')!="")
		{
			$fromdate=$this->input->post('txtfrom');
		}
		if($this->input->post('txtto')!="")
		{
			$todate=$this->input->post('txtto');
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition."m09_pin_detail.m_pin_gen_date BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('$todate','%Y-%m-%d') and ";
		}

		if($this->input->post('txtuser')!="" && $this->input->post('txtuser')!="0")
		{
			$id = get_uid($this->input->post('txtuser'));
			$condition=$condition." m09_pin_detail.m_pin_assigned_to='".$id."'  and";
		}

		if($this->input->post('buisnespln')!="" && $this->input->post('buisnespln')!="-1")
		{
			$condition=$condition." m09_pin_detail.m_pack_id= ".$this->input->post('buisnespln')."  and";
		}

		$condition=$condition." m09_pin_detail.m_pin_status=1 ";
		$condition=$condition." ORDER BY m09_pin_detail.m_pin_id ASC ";
		$record=array(
			'query'=>$condition
		);

		$query = " CALL sp_cancel_pin(?" . str_repeat(",?", count($record)-1) . ") ";
		$query_data=$this->db->query($query, $record);
		mysqli_next_result( $this->db->conn_id );
		$json=json_encode($query_data->result());
		echo $json;
	}


	public function update_cancel_pin()
	{
		$data = array(
			'm_pin_status' => '0'
		);
		$id=$this->uri->segment(3);
		$this->db->where('m_pin_id',$id);
		$this->db->update('m09_pin_detail', $data);
		header("Location:".base_url()."pin/view_cancel_pin");
	}


	public function view_pin_reoprt()
	{
		$data['form_name'] = "Search Pin Report";
		$data['table_name'] = "View Pin";

		$todate=0;
		$fromdate=0;
		$id=0;
		$condition='';
		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}
		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition." DATE_FORMAT('`m09_pin_detail.m_pin_gen_date`','%Y-%m-%d' )>=DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('`m09_pin_detail.m_pin_gen_date`','%Y-%m-%d' )<=DATE_FORMAT('$todate','%Y-%m-%d') and ";
		}

		if($this->input->post('ddstatus')!="" && $this->input->post('ddstatus')!="-1")
		{
			$condition=$condition." m09_pin_detail.m_pin_status= ".$this->input->post('ddstatus')."  and";
		}

		if($this->input->post('txtpin_issue')!="" && $this->input->post('txtpin_issue')!="0")
		{
			$id = get_uid($this->input->post('txtpin_issue'));
			$condition=$condition." m09_pin_detail.m_pin_assigned_to= ".$id."  and";
		}
		if($this->input->post('txtpin_no')!="")
		{
			$condition=$condition." m09_pin_detail.m_pin= '".$this->input->post('txtpin_no')."' and";
		}
		$condition=$condition." m09_pin_detail.or_m_reg_id !=0 ";
		$condition=$condition." ORDER BY m09_pin_detail.m_pin_id DESC ";

		$record=array(
			'query'=>$condition
		);

		$query = " CALL sp_pin_report(?" . str_repeat(",?", count($record)-1) . ") ";
		$data['info'] = $this->db->query($query, $record);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_pin_report',$data);
		$this->load->view('common/footer');
	}

	public function view_member_pin_details()
	{
		$data['form_name'] = "Search Member Pin Report";
		$data['table_name'] = "View Member Pin";

		$todate=0;
		$fromdate=0;

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}
		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}
		$condition='';

		if($todate!='' && $fromdate!='')
		{
			$condition=$condition."DATE_FORMAT(m09_pin_detail.m_pin_gen_date,'%Y-%m-%d') BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('$todate','%Y-%m-%d') and ";
		}

		if($this->input->post('txtpin_issue')!="")
		{
			$txtpin_issue=get_uid($this->input->post('txtpin_issue'));
			$condition=$condition." m09_pin_detail.m_pin_assigned_to =".$txtpin_issue."  and ";
		}
		$condition=$condition." m09_pin_detail.or_m_reg_id !=0 ";
		if($condition=='')
		{
			$condition=$condition." GROUP BY m09_pin_detail.m_pin_assigned_to,m09_pin_detail.m_pack_id,DATE_FORMAT(m_pin_gen_date,'%Y-%m-%d') HAVING COUNT(m_pin_gen_date) >= 1 order by m_pin_gen_date desc";
		}
		else
		{
			$condition=$condition." GROUP BY m09_pin_detail.m_pin_assigned_to, m09_pin_detail.m_pack_id, DATE_FORMAT(m_pin_gen_date,'%Y-%m-%d') HAVING COUNT(m_pin_gen_date) >= 1 order by m_pin_gen_date desc";
		}

		$record=array(
			'query'=>$condition
		);
		$query = " CALL sp_member_pin_detail(?" . str_repeat(",?", count($record)-1) . ") ";
		$data['rec']=$this->db->query($query, $record);

		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_member_pin_details',$data);
		$this->load->view('common/footer');
	}

	public function view_current_pin_position()
	{	
		$data['form_name'] = "Search Current Pin Position";
		$data['table_name'] = "View Current Pin Position";

		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}
		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition."m09_pin_detail.m_pin_gen_date BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('$todate','%Y-%m-%d') and ";
		}

		if($this->input->post('txtlogin_id')!="" && $this->input->post('txtlogin_id')!="0")
		{
			$id=get_uid($this->input->post('txtlogin_id'));
			$condition=$condition." m09_pin_detail.m_pin_assigned_to= ".$id."  and";
		}


		if($this->input->post('ddstatus')!="" && $this->input->post('ddstatus')!="-1")
		{
			$condition=$condition." m09_pin_detail.m_pin_status= ".$this->input->post('ddstatus')."  and";
		}

		if($this->input->post('txtpin_no')!="" && $this->input->post('txtpin_no')!="0")
		{
			$condition=$condition." m09_pin_detail.m_pin= '".$this->input->post('txtpin_no')."' and";
		}

		if($this->input->post('txtpin_issue')!="")
		{
			$id1=get_uid($this->input->post('txtpin_issue'));
			$condition=$condition." m09_pin_detail.or_m_reg_id= ".$id1." and";
		}

		$condition=$condition." m09_pin_detail.or_m_reg_id !=0 ";
		$condition=$condition." ORDER BY m09_pin_detail.m_pin_id DESC ";
		$record=array(
			'query'=>$condition
		);

		$query = " CALL sp_current_pin_position(?" . str_repeat(",?", count($record)-1) . ") ";
		$data['rec']=$this->db->query($query, $record);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_curr_pin_pos',$data);
		$this->load->view('common/footer');
	}

	/*--------------------------Start Transfer Pin Report--------------------------*/

	public function view_transfer_pin_report()
	{
		$data['form_name'] = "Search Transfer Pin Report";
		$data['table_name'] = "View Transfer Pin Report";

		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}
		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		$condition='';

		if($todate!='' && $fromdate!='')
		{
			$condition=$condition."`m09_pin_detail`.`m_pin_gen_date` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d %H:%i:%s') and DATE_FORMAT('$todate','%Y-%m-%d %H:%i:%s') and ";
		}

		if($this->input->post('txtpin_no')!="")
		{
			$condition=$condition." `m09_pin_detail`.`m_pin`= '".$this->input->post('txtpin_no')."' and ";
		}

		$condition=$condition."`m09_pin_detail`.`m_pin`!='0' ";
		$condition=$condition." ORDER BY `m09_pin_detail`.`m_pin_gen_date` DESC";

		$record=array(
			'query'=>$condition
		);
		$query = " CALL sp_pin_transfer_full_report(?" . str_repeat(",?", count($record)-1) . ") ";
		$data['rec']=$this->db->query($query, $record);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('pin/view_transfer_pin_report',$data);
		$this->load->view('common/footer');
	}

	public function view_pin_request()
	{
		$data['form_name'] = "View Pin Request Report";
		$data['table_name'] = "View Pin Request Report";

		$this->db->where('status',2);
		$data['req']=$this->db->get('view_pin_req');
		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_pin_request',$data);
		$this->load->view('common/footer');
	}

	public function view_pin_request_status()
	{
		$id=$this->uri->segment(3);
		$id2=$this->uri->segment(4);

		if($id2 == 0)
		{
			$record=array(
				"tr_req_status" => $id2
			);
			$this->db
				->where('tr_req_id',$id)
				->update('tr26_pin_request',$record);
		}
		if($id2 == 1)
		{
			$query=$this->db
				->where('tr_req_id',$id)
				->get('view_pin_req')->row();
			$this->db->free_db_resource();
			$data=array(
				'm_pin_id'=>'',
				'm_pack_id'=>$query->tr_req_packid,
				'no_pin'=>$query->tr_noofpin,
				'm_pin_assigned_to'=>'',
				'login_id'=>$query->or_m_user_id,
				'm_pin_status'=>'1',
				'm_pin_gen_date'=>YmdHis,
				'm_pin_gen_by'=>$this->session->userdata('profile_id'),
				'm_pin_is_reg'=>2,
				'proc'=>1
			);
			$query = " CALL sp_pin(?" . str_repeat(",?", count($data)-1) . ") ";
			$this->db->query($query, $data);

			$this->db->free_db_resource();
			$record=array(
				"tr_req_status" => $id2
			);
			$this->db
				->where('tr_req_id',$id)
				->update('tr26_pin_request',$record);
		}
		header("Location:".base_url()."pin/view_pin_request");
	}

	//-------------------Select Pin In Joining--------------------------

	public function select_pin()
	{
		$id = get_uid($this->input->post('txtintuserid'));
		$planid = $this->input->post('product_id');
		$this->db->where('m_pack_id',$planid);
		$this->db->where('m_pin_assigned_to',$id);
		$this->db->where('m_pin_status',1);
		$query['pin'] = $this->db->get('m09_pin_detail')->result();

		$csrf =  $this->security->get_csrf_hash();
		$query['csrf'] = $csrf;
		$json=json_encode($query);
		echo $json;
	}

	//-------------------Select Pin In Topup--------------------------

	public function select_topup_pin()
	{
		$id = get_uid($this->input->post('txtuserid'));
		$planid = $this->input->post('product_id');
		$query['pin'] = $this->db->query("SELECT * FROM `m09_pin_detail` WHERE `m09_pin_detail`.`m_pack_id`=$planid and `m09_pin_detail`.`m_pin_status`=1 and (`m09_pin_detail`.`m_pin_assigned_to`=$id OR `m09_pin_detail`.`m_pin_assigned_to`=".$this->session->userdata('profile_id')." OR `m09_pin_detail`.`m_pin_assigned_to`=1) ")->result();
		
		$csrf =  $this->security->get_csrf_hash();
		$query['csrf'] = $csrf;
		$json=json_encode($query);
		echo $json;
	}
	
	public function view_pin_bank()
	{
		$data['form_name'] = "view pin bank";
		$data['table_name'] = "view pin bank";
		$id=$this->session->userdata('user_id');
		$data['info']=$this->db->query("CALL sp_member_pin_bank(".$id.")");	
		mysqli_next_result( $this->db->conn_id );
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_pin_bank',$data);
		$this->load->view('common/footer');
	}
	
		
	public function view_pin_transfer()
	{
			
			$data['form_name'] = "view pin transfer";
			$data['table_name'] = "view pin transfer";
			$this->db->where('m_pack_status',1);
		$data['package']=$this->db->get('m06_package');
			$this->load->view('common/header');
			$this->load->view('common/user_menu',$this->data);
			$this->load->view('user/view_pin_transfer',$data);
			$this->load->view('common/footer');
			
	}

	public function view_atransfer_pin_report()
	{
		$data['form_name'] = "Search Transfer Pin Report";
		$data['table_name'] = "View Transfer Pin Report";

		$todate=0;
		$fromdate=0;
		$condition='';

		if($this->input->post('end')!="")
		{
			$todate=$this->input->post('end');
		}
		if($this->input->post('start')!="")
		{
			$fromdate=$this->input->post('start');
		}

		$condition='';

		if($todate!='' && $fromdate!='')
		{
			$condition=$condition."`m09_pin_detail`.`m_pin_gen_date` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d %H:%i:%s') and DATE_FORMAT('$todate','%Y-%m-%d %H:%i:%s') and ";
		}

		if($this->input->post('txtpin_no')!="")
		{
			$condition=$condition." `m09_pin_detail`.`m_pin`= '".$this->input->post('txtpin_no')."' and ";
		}

		$condition=$condition."`m09_pin_detail`.`m_pin`!='0' ";
		$condition=$condition." ORDER BY `m09_pin_detail`.`m_pin_gen_date` DESC";

		$record=array(
			'query'=>$condition
		);
		$query = " CALL sp_pin_transfer_full_report(?" . str_repeat(",?", count($record)-1) . ") ";
		$data['rec']=$this->db->query($query, $record);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/menu',$this->data);
		$this->load->view('pin/view_atransfer_pin_report',$data);
		$this->load->view('common/footer');
	}




}
?>