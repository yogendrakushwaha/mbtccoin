<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userprofile extends CI_Controller {

	//////////////////////////////////////////////////////////////////////////
	//////////     Constructor In Member Controller    ///////
	/////////////////////////////////////////////////////////////////////////

	public function __construct()
	{
		parent::__construct();
		$this->_is_logged_in();
		$this->data['page'] = "User Pannel";
		$this->data['bal'] = $this->db->query("SELECT `get_available_bal`(".$this->session->userdata('profile_id').",1) AS bal")->row()->bal;
		$this->data['check_topup'] = $this->db->query("SELECT COUNT(*) as check_topup FROM `tr10_mem_topup` WHERE `tr10_mem_topup`.`tr10_mem_uid` = ".$this->session->userdata('profile_id')." LIMIT 1")->row()->check_topup;
		
		$lid = $this->session->userdata('profile_id');
		$this->data['rec1'] = $this->db->query("CALL `sp_member_detail`('`m03_user_detail`.`or_m_reg_id`= $lid ')")->row();
		mysqli_next_result( $this->db->conn_id );
		
	}

	/////////////////////////////////////////////////////////////////////////
	//////////     Check Login    ///////
	//////////////////////////////////////////////////////////////////////

	public function _is_logged_in() 
	{
		if ($this->session->userdata('user_id')=="")
		{
			redirect('auth/index');
			die();
		}
	}

	public function index()
	{
		header("Location:".base_url()."userprofile/dashboard");
	}

	/*----------------------User Dashboard----------------------------*/

	public function dashboard()
	{
		$data['form_name'] = "Dashboard";

		$this->db->where('m_news_status',1);
		$this->db->order_by("m_entrydate","desc");
		$data['news']=$this->db->get('m24_news');

		$data['tickets']=$this->db
			->where('TICKET_REGID',$this->session->userdata('profile_id'))
			->where('TICKET_STATUS_ID',1)
			->order_by('TICKET_DATE',"DESC")
			->get('view_ticket');

		$query = " CALL sp_user_dashboard(".$this->session->userdata('profile_id').")";
		$data['user']=$this->db->query($query)->row();
		mysqli_next_result( $this->db->conn_id );
		
		// this used in session start
		
		
		 // this used in session end

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/dashboard',$data);
		$this->load->view('common/footer');
	}

	/*|-----------------------------------------|*/
	/*|------     USER REGISTRATION      -------|*/
	/*|-----------------------------------------|*/

	public function join_member()
	{
		$data['form_name'] = "Member Joining";

		$this->db->where('m_parent_id',1);
		$this->db->where('m_status',1);
		$data['state']=$this->db->get('m02_location');

		$data['bank']=$this->db->where("m_bank_status",1)->get('m01_bank');

		$this->db->where('m_pack_status',1);
		$data['pack']=$this->db->get('m06_package');

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/registration',$data);
		$this->load->view('common/footer');
	}

	// -------------USER REGISTRATION INSERTION---------------

	public function register_candidate()
	{
		$output=$this->member_model->signup();
		$this->session->set_flashdata('succ','Registerd Successfully!!');
		echo $output;
	}

	/*|--------------------------------------|*/
	/*|------   UPDATE USER PROFILE   -------|*/
	/*|--------------------------------------|*/

	public function view_member_edit()
	{	
		$data['form_name'] = "Edit Member";

		$this->db->where('m_parent_id',1);
		$this->db->where('m_status',1);
		$data['state']=$this->db->get('m02_location');

		$condition = "`m03_user_detail`.`or_m_reg_id` = '".$this->session->userdata('profile_id')."'";
		$call_procedure = ' CALL sp_member_detail("'. $condition .'")';
		$data['rec'] = $this->db->query($call_procedure)->row();
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_member_edit',$data);
		$this->load->view('common/footer');
	}

	/*-------------UPDATE USER DETAILS-----------------*/

	public function update_member()
	{
		$this->member_model->update_member();
		$this->session->set_flashdata('info','Updated Successfully!!');
		header("Location:".base_url()."userprofile/view_member_edit");
	}

	/*|--------------------------------------|*/
	/*|------     VIEW USER PROFILE   -------|*/
	/*|--------------------------------------|*/

	public function view_member_details()
	{	
		$data['form_name'] = "View Member Profile";

		$condition = "`m03_user_detail`.`or_m_reg_id` = '".$this->session->userdata('profile_id')."'";
		$call_procedure = ' CALL sp_member_detail("'. $condition .'")';
		$data['rec'] = $this->db->query($call_procedure)->row();
		mysqli_next_result( $this->db->conn_id );
		
		/* $data['da'] = $this->session->set_userdata(array(
										'user_image'       => $data['rec']->User_image,
										'username'      => $data['rec']->Associate_Name,
										'mob'       => $data['rec']->Mobile_No,
										'email'          => $data['rec']->Email,
										'state'        => $data['rec']->State,
										'status'        => TRUE
										));  */

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_member_details',$data);
		$this->load->view('common/footer');
	}

	/*|-----------------------------------------------------|*/
	/*|------   ADD BANK DETAILS      -------|*/
	/*|-----------------------------------------------------|*/

	public function view_bank_details()
	{
		$data['form_name'] = "View Bank Details";
		$data['table_name'] = "Update Bank Details";

		$data['bank'] = $this->db->where("m_bank_status",1)->get("m01_bank");

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_reg_id` = '.$this->session->userdata('profile_id').'")';
		$data['bank_details']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_bank_details',$data);
		$this->load->view('common/footer');
	}

	public function update_bank_details()
	{
		$this->member_model->update_bank_details();
		$this->session->set_flashdata('info','Bank Details Updated Successfully!!');
		header("Location:".base_url()."userprofile/view_bank_details");
	}

	/*|--------------------------------------|*/
	/*|------     Welcome Letter     -------|*/
	/*|--------------------------------------|*/

	public function member_welcome_letter()
	{
		$data['form_name'] = "Welcome Letter";

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_reg_id` = '.$this->session->userdata('profile_id').'")';
		$data['info']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/member_welcome_letter',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------|*/
	/*|------     Welcome Invoice     -------|*/
	/*|--------------------------------------|*/

	public function member_welcome_invoice()
	{
		$data['form_name'] = "User Invoice";

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_reg_id` = '.$this->session->userdata('profile_id').'")';
		$data['info']=$this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/member_welcome_invoice',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------|*/
	/*|------     UPDATE Password     -------|*/
	/*|--------------------------------------|*/

	public function change_password()
	{
		$data['form_name'] = "Change Password";
		$data['form_name1'] = "Change Pin Password";
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_change_password',$data);
		$this->load->view('common/footer');
	}

	public function update_password()
	{
		$ulgpd=array(
			'or_login_pwd'=>$this->input->post('txtpassword')
		);
		$this->db->where('or_user_id',$this->session->userdata('profile_id'));
		if($this->db->update('tr01_login',$ulgpd))
			$this->session->set_flashdata('info','Updated Successfully!!');
		else
			$this->session->set_flashdata('info','Oh Snap!! Some thing went wrong.');
		header("Location:".base_url()."userprofile/change_password");
	}

	/*|---------------------------------------------------------|*/
	/*|------  		    USER DIRECT REFFERAL             -------|*/
	/*|---------------------------------------------------------|*/

	public function view_direct_referal()
	{
		$data['form_name'] = "Search Direct Referal";
		$data['table_name'] = "View Direct Referal";
		$data['rec'] = '';

		$id = $this->session->userdata('profile_id');

		$call_procedure = ' CALL sp_member_detail("`m03_user_detail`.`or_m_intr_id` = '.$id.'")';
		$data['rec'] = $this->db->query($call_procedure);
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_direct_referal',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------------------------|*/
	/*|------        		USER DOWNLINE  		 	    -------|*/
	/*|--------------------------------------------------------|*/

	public function view_user_downline()
	{
		$leg='';
		$data['rec'] = '';
		$condition = '';
		$data['form_name'] = "Search Downline Member";
		$data['table_name'] = "View Downline Member";
		$position=$this->input->post('ddposition');

		$id = $this->session->userdata('profile_id');

		if($position=='')
		{	
			$leg='1';
		}
		else
		{
			$leg=$position;
		}

		$this->db->query("CALL get_downline(".$id.",'".$leg."')");
		
		$condition = $condition."`m03_user_detail`.`or_m_reg_id` IN (SELECT member_id FROM tmp_downline) ";
		
		if($_SERVER['REQUEST_METHOD'] != "POST") 
		$condition = $condition." and DATE_FORMAT(`m03_user_detail`.`or_m_regdate`,'%m') = DATE_FORMAT(NOW(),'%m')";
		
		$sp_data = array(
				"condition"=>$condition
			);
		$call_procedure = ' CALL sp_member_detail(?)';
		$data['rec'] = $this->db->query($call_procedure, $sp_data);
		
		mysqli_next_result( $this->db->conn_id );

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_user_downline',$data);
		$this->load->view('common/footer');
	}

	/*|--------------------------------------------------|*/
	/*|------     		USER TREE STRUCTURE	      -------|*/
	/*|--------------------------------------------------|*/

	public function tree()
	{
		$data['form_name'] = "Tree Stucture";

		$id=$this->uri->segment(3);
		$res=$this->member_model->scan_team($id);
		if($res=='true')
		{
			$data['s']=1;
			$this->db->query("CALL `sp_user_at_level`($id,3)");
			$data['tr']=$this->db->query("SELECT * FROM `view_tree` WHERE `REGID` IN (SELECT * FROM total_user)");
		}
		else
		{
			$data['s']=0;
			$this->db->query("CALL `sp_user_at_level`(".$this->session->userdata('profile_id').",3)");
			$data['tr']=$this->db->query("SELECT * FROM `view_tree` WHERE `REGID` IN (SELECT * FROM total_user)");
			header("location:".base_url()."userprofile/tree/".$this->session->userdata('profile_id'));
		}

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_tree',$data);
		$this->load->view('common/footer');
	}

	/*----------------SEARCH USER ID IN TREE-------------------*/

	public function get_tree()
	{
		if($this->input->post('search_id')!='' || $this->input->post('search_id')!='0')
		{
			$this->db->where('or_m_user_id',trim($this->input->post('search_id')));
			$data['rec']=$this->db->get('m03_user_detail');
			foreach($data['rec']->result() as $row)
			{
				$reg_id=$row->or_m_reg_id;
				break;
			}
			header("Location:".base_url()."userprofile/tree/".$reg_id);
		}
	}

	/*|--------------------------------------------------------------------|*/
	/*|------    LEDGER REPORT OF USER        -------|*/
	/*|--------------------------------------------------------------------|*/


	public function view_ledger_report()
	{
		$condition="";
		$todate=0;
		$fromdate=0;

		$data['form_name'] = "Search Member Ledger";
		$data['table_name'] = "View Member Ledger";

		if($this->input->post('end')!="")
		{
			$todate=date('Y-m-d',strtotime($this->input->post('end')));
		}

		if($this->input->post('start')!="")
		{
			$fromdate=date('Y-m-d',strtotime($this->input->post('start')));
		}

		if($todate!='0' && $fromdate!='0')
		{
			$condition=$condition." `LEDGER_DATETIME`>=DATE_FORMAT('$fromdate','%d-%b-%Y') AND `LEDGER_DATETIME`<=DATE_FORMAT('$todate','%d-%b-%Y') AND ";
		}

		$condition=$condition." `LEDGER_UID`=".$this->session->userdata('profile_id');

		$data['rid']=$this->db->query("SELECT * FROM view_ledger WHERE ".$condition);

		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_ledger_report',$data);
		$this->load->view('common/footer');
	}

	/*|-----------------------------------------------------|*/
	/*|------  		    BEGIN TRANSFER IN TEAM 	     -------|*/
	/*|-----------------------------------------------------|*/

	public function find_in_team()
	{
		$sessionid=$this->input->post('sessionid');
		$userid=$this->input->post('userid');
		$info=$this->db->query("SELECT get_member_in_team('".$sessionid."','".$userid."') as find_result");
		$row=$info->row();
		echo $row->find_result;
	}

	/*|---------------------------------------------------------|*/
	/*|------  		      BEGIN VIEW ALL NEWS      	     -------|*/
	/*|---------------------------------------------------------|*/

	public function view_all_news()
	{
		$data['form_name'] = "View All News";
		$this->db->where('m_news_status',1);
		$data['news']=$this->db->get('m24_news');
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_all_news',$data);
		$this->load->view('common/footer');
	}

	/*|---------------------------------------------------------|*/
	/*|------  		      BEGIN VIEW ALL NEWS      	     -------|*/
	/*|---------------------------------------------------------|*/

	public function view_query_form()
	{
		$data['form_name'] = "Submit Query Here";
		$data['table_name'] = "View All Query";
		$this->db->where('TICKET_REGID',$this->session->userdata('profile_id'));
		$data['rec']=$this->db->get('view_ticket');
		$this->load->view('common/header');
		$this->load->view('common/user_menu',$this->data);
		$this->load->view('user/view_query_form',$data);
		$this->load->view('common/footer');
	}

	public function insert_ticket()
	{
		$data=array(
			'tr_ticket_id'=>0,
			'tr_ticket_userid'=> trim($this->input->post('txtuserid')),
			'tr_ticket_title'=>trim($this->input->post('txttitle')),
			'tr_ticket_desc'=>$this->input->post('txtmsg'),
			'tr_ticket_reply'=>0,
			'tr_ticket_status'=>1,
			'tr_ticket_date'=>YmdHis,
			'proc'=>1
		);
		$query = " CALL sp_ticket(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
		$this->db->query($query, $data);
		$query1=$this->db->query("SELECT @msg as message");
		$row = $query1->row();
		$this->session->set_flashdata('info',$row->message);
		header("Location:".base_url()."userprofile/view_query_form");
	}

}

?>