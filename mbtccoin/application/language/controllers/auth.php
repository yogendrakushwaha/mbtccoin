<?php 
	
	class Auth extends CI_Controller 
	{
		
		public function __construct() 
		{
			parent::__construct();
		}
		
		public function index()
		{
			$this->load->view('common/header');
			$this->load->view('auth/login');
		}

		public function check_sms()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$mob='9044592446';
			$msg="TestingFinal";
			$this->crud_model->send_sms($mob,$msg);
		}
		
		public function login()
		{
			if($this->session->userdata('user_id')=="")
			{
				/*-----------------------Create Array For Login-----------------------------*/
				
				$login_data = array(
				'login_id'=>trim($this->input->post('txtlogin')),
				'user_pwd'=>trim($this->input->post('txtpwd')),
				'user_type'=>trim($this->input->post('ddtype'))
				);
				$query="CALL sp_login(?" . str_repeat(",?", count($login_data)-1) . ",@msg,@msg1)";
				$query_result = $this->db->query($query,$login_data);
				
				/*-----------------------Check User Logged In or Not-----------------------------*/
				$count = $query_result->num_rows();
				
				/*-----------------------Free Result For Next Query-----------------------------*/
				mysqli_next_result( $this->db->conn_id );
				$msg = $this->db->query("SELECT @msg as message")->row()->message;
				$msg1 = $this->db->query("SELECT @msg1 as message1")->row()->message1;
				
				/*-----------------------Set Message Of Status Or Reason-----------------------------*/
				$this->session->set_flashdata('info',$msg);
				
				if($count == 1)
				{
					/*-----------------------Fetch Data And Create Session For Login -----------------------------*/
					foreach($query_result->result() as $row)
					{
						break;
					}
					$sessiondata=array(
					'user_id'		=>	$row->user_id,
					'profile_id'	=>	$row->profile_id,
					'e_email'  		=>	$row->e_email,
					'name'     		=> 	$row->name,
					'designation' 	=>	$row->designation,
					'mobile_no' 	=>	$row->mobile_no,
					'doj' 			=>	$row->doj,
					'prifile_pic' 	=>	$row->or_m_userimage,
					'location' 		=>	$row->location,
					'logged_in' 	=> 	$row->logged_in
					);
					$this->session->set_userdata($sessiondata);
					
					/*-----------------------Redirect Url For Different User-----------------------------*/
					if($msg1 == 1)
					header("Location:".base_url()."master/index");
					if($msg1 == 2)
					header("Location:".base_url()."Userprofile/index");
				}
				else
				{
					header("Location:".base_url());
				}
			}
			else
			{
				$this->session->set_flashdata('info','Another Person is Already logged in! First Logout Then try Again');
				$this->session->sess_destroy();
				header("Location:".base_url());
			}
		}
		
		public function logout()
	    {
			if($this->session->userdata('profile_id')!=0)
			$data['msg'] = $this->db->query("SELECT fn_logout(".$this->session->userdata('profile_id').") as message")->row()->message;
			else
			$data['msg'] = "Logout Successfully";
			$this->session->sess_destroy();
			header("Location:".base_url());
		}
		
		////////-------Forgot Password---------////////
		
		public function forgot_password()
		{
			$this->load->view('common/header');
			$this->load->view('auth/view_forgot_password');
		}
		
		public function resetpassword()
		{			
			$userid=$this->input->post('txtuserid');
			$this->db->where('or_login_id',$userid);
			$loginpwd = $this->db->get('tr01_login')->row();
			
			$this->db->where('or_m_user_id',$userid);
			$email = $this->db->get('m03_user_detail')->row();
			
			$msg = "Welcome To ".SITE_NAME." &nbsp; ".$loginpwd->or_m_name."  Your UID is ".$userid."  and PASS ".$loginpwd->or_login_pwd.".  Thanks for Choosing ".WEBSITE_NAME;
			
			$this->crud_model->send_sms(trim($email->or_m_mobile_no),$msg);
			
			$this->crud_model->send_email(trim($email->or_m_email),'Your Password Details in assvigroup.in',$msg,'Forget Password');
			
			header("Location:".base_url());
		}
		
		
	}
?>