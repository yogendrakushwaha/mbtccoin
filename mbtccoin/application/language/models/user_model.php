<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class user_model extends CI_Model
	{
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Insert User Fund Requset     ///////
		//////////////////////////////////////////////////////////////////////
		
		public function insert_fund_requset()
		{
			$file = '';
			$file_name = $_FILES['userfile']['name'];
			
			if($file_name!='')
			{
				$config['upload_path']   =   "application/recipt/";
				$config['allowed_types'] =   "gif|jpg|jpeg|png|bmp"; 
				$config['max_size']      =   "5000";
				$config['max_width']     =   "5000";
				$config['max_height']    =   "5000";
				$this->load->library('upload',$config);
				$this->upload->do_upload();
				$finfo=$this->upload->data();
				$file = ($finfo['raw_name'].$finfo['file_ext']);
			}
			
			$data=array(
				'tr09_req_id'=>0,
				'tr09_req_uid'=> trim($this->session->userdata('profile_id')),
				'tr09_req_amount'=>trim($this->input->post('txtamount')),
				'tr09_req_type'=>1,
				'tr09_req_curr_type'=>0,
				'tr09_req_recipt'=>$file,
				'm_top_status'=>2,
				'proc'=>1
				);
			$query = " CALL sp_fund_request(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$query1=$this->db->query("SELECT @msg as message");
			$row = $query1->row();
			return $row->message;
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Insert User Fund Transfer     ///////
		//////////////////////////////////////////////////////////////////////
		
		public function insert_fund_transfer()
		{
			$data=array(
				'tr_trans_userid'=>$this->session->userdata('profile_id'),
				'tr_trans_transid'=>$this->input->post('txtuserid'),
				'tr_trans_amt'=>$this->input->post('txtamount'),
				'tr_trans_pass'=>$this->input->post('txtpinpwd')
				);
			
			$query = "CALL sp_transfer_balance(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			return $this->db->query("SELECT @msg as message")->row()->message;
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Insert Member Topup Request     ///////
		//////////////////////////////////////////////////////////////////////
		
		public function insert_member_topup()
		{
			$txtuserid = $this->input->post('txtuserid');
			$ddpackid = $this->input->post('ddpackid');
			$ddpin = $this->input->post('ddpin');
			
			if($txtuserid != '' && $ddpin != '')
			{
				$data = [
						"tr10_mem_uid" => get_uid(trim($txtuserid)),
						"tr10_mem_pin" => $ddpin
						];
				$query = "CALL sp_pin_member_topup(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
				$this->db->query($query, $data);
				return $this->db->query("SELECT @msg as message")->row()->message;
			}
			else
			{
				return "Topup Failed";
			}
		}
		
		
	}
?>