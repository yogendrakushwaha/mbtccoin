<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class member_model extends CI_Model
	{
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Signup In model     ///////
		//////////////////////////////////////////////////////////////////////
		
		/*--------------MAKE REGISTRATION DATA-----------------*/
		
		public function reg_data($pass=null,$pinpass=null,$file=null)
		{
			$data=array(
			'or_m_user_id'=> trim($this->input->post('txtregid')),
			'or_m_designation'=> trim($this->input->post('txtage')),
			'or_m_name'=>trim($this->input->post('txtassociate_name')),
			'or_m_dob'=>date('Y-m-d', strtotime($this->input->post('txtdob'))),
			'or_m_gurdian_name'=>trim($this->input->post('txtparent')),
			'or_member_joining_date'=>YmdHis,
			'or_m_gender'=>trim($this->input->post('rbgender')),
			'or_m_email'=>trim($this->input->post('txtemail')),
			'or_m_landline_no'=>'',
			'or_m_mobile_no'=>trim($this->input->post('txtmobile')),
			'or_m_address'=>trim($this->input->post('txtaddress')),
			'or_m_pincode'=>trim($this->input->post('txtpincode')),
			'or_m_country'=>1,
			'or_m_state'=>trim($this->input->post('ddstate')),
			'or_m_userimage'=>$file,
			'or_m_city'=>trim($this->input->post('ddcity')),
			'or_m_status'=>1,
			'or_m_intr_id'=>get_uid($this->input->post('txtintroducer_id')),
			'or_m_intr_name'=>trim($this->input->post('txtintroducer_name')),
			'or_m_aff_id'=>1,
			'or_m_upliner_id'=>get_uid($this->input->post('txtparent_id')),
			'or_m_position'=>trim($this->input->post('txtjoin_leg')),
			'm_pin_id'=>trim($this->input->post('ddpin')),
			'or_m_regdate'=>YmdHis,			
			'or_m_n_name'=>trim($this->input->post('txtnoname')),
			'or_m_n_age'=>trim($this->input->post('txtage')),
			'or_m_n_relation'=>trim($this->input->post('txtrelation')),			
			'or_m_b_ifscode'=>trim($this->input->post('txtifscode')),
			'or_m_b_cbsacno'=>trim($this->input->post('txtaccount')),
			'or_m_b_name'=>trim($this->input->post('ddbank_name')),
			'or_m_b_branch'=>trim($this->input->post('txtbranch_name')),
			'or_m_b_pancard'=>trim($this->input->post('txtpancard')),
			'or_m_b_adhar'=>trim($this->input->post('txtadhar')),
			'or_login_pwd'=>trim($pass),
			'or_pin_pwd'=>trim($pinpass),
			'or_reg_from'=>trim($this->input->post('txtreg_from')),
			'proc' => $this->input->post('txtproc'),
			);
			return $data;
		}
		
		/*--------------MAKE USER REGISTRATION-----------------*/
		
		public function signup()
		{
			if($this->input->post('txtpassword')=='')
			{
				$pass=rand(100000,999999);
				$pinpass=rand(100000,999999);
			}
			else
			{
				$pass=$this->input->post('txtpassword');
				$pinpass=$this->input->post('txtpassword');
			}
			$data=$this->reg_data($pass,$pinpass,'');
			$query = " CALL sp_member(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			
			$id=$this->db->query("SELECT @msg as message")->row()->message;
			//$msg1 = "Welcome To ".SITE_NAME." Your UID is ".$id."  and PASS ".$pass.". Thanks for Choosing ".WEBSITE_NAME;
			
		$msg1="Hello ".$this->input->post('txtassociate_name')."<br>Your login details are - <br>
		<br>URL : ".SITE_NAME."<br>Userid : ".$id."<br>Password : ".$pass."<br><br>Thankyou";

			if(SMS_SEND_STATUS==1)
			$this->crud_model->send_sms(trim($this->input->post('txtmobile')),$msg1);
			
			if(EMAIL_SEND_STATUS==1)
			$this->crud_model->send_email($this->input->post('txtemail'),'Login Details',$msg1,'Login Details');
			
			return $msg1;
		}
		
		/*--------------MAKE USER PROFILE UPDATE-----------------*/
		
		public function update_member()
		{
			$file_name = $_FILES['userfile']['name'];
			if($file_name!='')
			{
				$config['upload_path']   =   "application/user_image/";
				$config['allowed_types'] =   "gif|jpg|jpeg|png|bmp"; 
				$config['max_size']      =   "5000";
				$config['max_width']     =   "5000";
				$config['max_height']    =   "5000";
				$this->load->library('upload',$config);
				$this->upload->do_upload();
				$finfo=$this->upload->data();
				$file=($finfo['raw_name'].$finfo['file_ext']);
			}
			else
			{
				$file = trim($this->input->post('txtpic'));
			}
			$data = $this->reg_data('','',$file);
			$query = " CALL sp_member(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);

			$this->db->query("SELECT @msg as message")->row()->message;
		}
		
		/*--------------MAKE USER PROFILE UPDATE-----------------*/
		
		public function update_bank_details()
		{
			$data = array(
			'or_m_b_ifscode' => $this->input->post('txtifsc'),
			'or_m_b_cbsacno' => $this->input->post('txtacc'),
			'or_m_b_name' => $this->input->post('txtbank'),
			'or_m_b_branch' => $this->input->post('txtbranch'),
			'or_m_b_pancard' => $this->input->post('txtpancard'),
			'or_m_b_adhar' => $this->input->post('txtadhar')
			);
			$this->db->where("or_m_id",$this->uri->segment(3))->update('m04_user_bank',$data);
		}
		
		/*--------------Scan Team-----------------*/
		
		public function scan_team($id)
		{
			if($id==$this->session->userdata('profile_id'))
			{
				return 'true';
			}
			else
			{
				$this->db->where('or_m_reg_id',$id);
				$data['mem1']=$this->db->get('m03_user_detail');
				foreach($data['mem1']->result() as $row1)
				{
					$intdid1=$row1->or_m_upliner_id;
					if($intdid1==$this->session->userdata('profile_id'))
					{
						return 'true';
					}
					else
					{
						$y=$this->scan_team($intdid1);
						if($y=="true")
						{
							return "true";
						}
						else
						{
							return "false";
						}
					}
				}
			}
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Insert User Fund Requset     ///////
		//////////////////////////////////////////////////////////////////////
		
		public function fund_request_action()
		{
			$data=array(
				'tr09_req_id'=>$this->uri->segment(3),
				'tr09_req_uid'=> '',
				'tr09_req_amount'=>'',
				'tr09_req_type'=>'',
				'tr09_req_curr_type'=>0,
				'tr09_req_recipt'=>'',
				'm_top_status'=>$this->uri->segment(4),
				'proc'=>2
				);
			$query = " CALL sp_fund_request(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$query1=$this->db->query("SELECT @msg as message");
			$row = $query1->row();
			return $row->message;
		}
		
	}
?>