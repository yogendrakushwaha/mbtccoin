<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class mastermodel extends CI_Model
	{
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Configuration Data In Model     ///////
		//////////////////////////////////////////////////////////////////////
		
		public function select_config()
		{
			$this->db->where('m00_login_id',1);
			return $this->db->get('m00_admin_login');
		}
		
		public function update_config()
		{
			$mcf=array(
			'm00_login_id'=>$this->uri->segment(3),
			'm00_username'=>$this->input->post('txtusername'),
			'm00_password'=>$this->input->post('txtuserpass'),
			'm00_pinpassword'=>$this->input->post('txtuserpinpass'),
			'm00_admin_type'=>$this->input->post('txtusertype'),
			'm00_admin_status'=>$this->input->post('txtuserstatus')
			);
			$query="CALL sp_setconfig(?" . str_repeat(",?", count($mcf)-1) . ")";
			$this->db->query($query,$mcf);
		}
		
	
		/////////////////////////////////////////////////////////////////////////
		//////////     News Master In Model    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function view_news()
		{
			return $this->db->get('m24_news');
		}
		
		public function insert_news()
		{
			$data=array
			(
			'm_news_id'=>'',
			'm_news_title'=>$this->input->post('txttitle'),
			'm_news_des'=>$this->input->post('txtdescription'),
			'm_news_status'=>$this->input->post('ddstatus'),
			'm_affid'=>0,
			'proc_id'=>'1'
			);
			$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		public function update_news()
		{
			$data=array
			(
			'm_news_id'=>$this->uri->segment(3),
			'm_news_title'=>$this->input->post('txttitle'),
			'm_news_des'=>$this->input->post('txtdescription'),
			'm_news_status'=>$this->input->post('ddstatus'),
			'm_affid'=>$this->input->post('ddtype'),
			'proc_id'=>'2'
			);
			$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		public function delete_news()
		{
			$data=array
			(
			'm_news_id'=>$this->uri->segment(3),
			'm_news_title'=>'',
			'm_news_des'=>'',
			'm_news_status'=>$this->uri->segment(4),
			'm_affid'=>'0',
			'proc_id'=>'3'
			);
			$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Manage Bank in Model    ///////
		//////////////////////////////////////////////////////////////////////
		
		public function view_bank()
		{
			$data=array(
			'm_bank_id'=>'',
			'm_bank_name'=>'',
			'm_bank_status'=>'1',
			'proc'=>'2'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			return $this->db->query($query, $data);
		}
		
		public function insert_bank()
		{
			$data=array(
			'm_bank_id'=>'',
			'm_bank_name'=>$this->input->post('txtbank'),
			'm_bank_status'=>'1',
			'proc'=>'1'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		public function view_update_bank()
		{
			$data=array(
			'm_bank_id'=>$this->uri->segment(3),
			'm_bank_name'=>$this->input->post('txtbank'),
			'm_bank_status'=>'',
			'proc'=>'3'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		public function bank_status()
		{
			$data=array(
			'm_bank_id'=>$this->uri->segment(3),
			'm_bank_name'=>'',
			'm_bank_status'=>$this->uri->segment(4),
			'proc'=>'4'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		/////////////////////////////////////////////////////////////////////////
		//////////     Manage Tool in Model    ///////
		//////////////////////////////////////////////////////////////////////

		
		public function insert_tools_master()
		{
			$data=array(
				'm_tool_title'=>$this->input->post('txttitle'),
				'm_tool_description'=>$this->input->post('txtdescription'),
				'm_tool_video'=>$this->input->post('txturl'),
				'm_tool_type'=>'1',
				'm_tool_status'=>'1'
			);
			$this->db->insert('m07_tools', $data);
		}

		public function view_update_tools()
		{
			$data=array(
				'm_tool_title'=>$this->input->post('txttitle'),
				'm_tool_description'=>$this->input->post('txtdescription'),
				'm_tool_video'=>$this->input->post('txturl')
			);
			$this->db->where('m_tool_id',$this->uri->segment(3))->update('m07_tools', $data);
		}

		public function tools_status()
		{
			$data=array(
				'm_tool_status'=>$this->uri->segment(4)
			);
			$this->db->where('m_tool_id',$this->uri->segment(3))->update('m07_tools', $data);
		}
		
		
	}
?>