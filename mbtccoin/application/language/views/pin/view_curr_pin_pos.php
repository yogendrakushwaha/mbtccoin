<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('pin/view_current_pin_position',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						
						<div class="form-group">
							<label class="col-md-2 control-label">From Joining Date</label>
							<div class="col-md-3">
								<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
									<input type="text" class="form-control" name="start" id="txtfrom" />
									<span class="input-group-addon bg-custom b-0 text-white">to</span>
									<input type="text" class="form-control" name="end" id="txtto" />
								</div>
							</div>
							
						</div>
						
						
						<div class="form-group">
							<label class="col-md-2 control-label">Transfer / Receive Login Id</label>
							<div class="col-md-3">
								<input type="text" id="txtlogin_id" name="txtlogin_id" class="form-control input-inline input-medium" placeholder="Enter Transfer / Receive Login Id." >
							</div>
							
							<label class="col-md-2 control-label">Pin Issued To</label>
							<div class="col-md-3">
								<input type="text" id="txtpin_issue" name="txtpin_issue" class="form-control input-inline input-medium" placeholder="Enter Pin Issued To.">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label">Status</label>
							<div class="col-md-3">
								<select id="ddstatus" name="ddstatus" class="form-control input-inline input-medium">
									<option selected="selected" value="">All</option>
									<option value="1">In-Active</option>
									<option value="2">Active</option>
									<option value="0">Deactive</option>
								</select>
							</div>
							
							<label class="col-md-2 control-label">Pin Number</label>
							<div class="col-md-3">
								<input type="text" id="txtpin_no" name="txtpin_no" class="form-control input-inline input-medium" placeholder="Enter Pin Number." >
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" id="btnsubmit" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
								<tr>
									<th>S No.</th>
									<th>Pin Number</th>
									<th>Generated Date</th>
									<th>Status</th>
									<th>Amount</th>
									<th>Issued(Login Id)</th>
									<th>Login Id</th>
									<th>Associate Name</th>
									<th>Type</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									foreach($rec->result() as $row1)
									{
									?>
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $row1->PIN; ?></td>
										<td><?php echo date('d-m-y h:i:s',strtotime($row1->GENERATION_DATE)); ?></td>
										<td><?php echo $row1->PIN_Status; ?></td>
										<td><?php echo $row1->Amount; ?></td>
										<td><?php echo $row1->Issued_LOGIN_ID; ?></td>
										<td><?php echo $row1->Associate_id; ?></td>
										<td><?php echo $row1->Associate_name; ?></td>
										<td><?php echo $row1->PIN_IS_REG;?></td>
									</tr>
									<?php
										$sn++;
									}
								?>
							</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>													