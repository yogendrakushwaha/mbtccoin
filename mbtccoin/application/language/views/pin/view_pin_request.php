<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
								<tr>
									<th>S No.</th>
									<th>Login Id</th>
									<th>Login Name</th>
									<th>Package</th>
									<th>Pin Requested</th>
									<th>Pin Amount</th>
									<th>User Pay Amount</th>
									<th>Payment Mode</th>
									<th>NEFT/IMPS</th>
									<th>File</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
								<?php
									$sn=1;
									foreach($req->result() as $row1)
									{
									?>
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $row1->or_m_user_id; ?></td>
										<td><?php echo $row1->or_m_name; ?></td>
										<td><?php echo $row1->m_pack_name; ?></td>
										<td><?php echo $row1->tr_noofpin; ?></td>
										<td><?php echo $row1->tr_pinamt; ?></td>
										<td><?php echo $row1->tr_req_amount; ?></td>
										<td><?php echo $row1->tr_req_mode; ?></td>
										<td><?php echo $row1->tr_req_neftid; ?></td>
										<td><a href="<?php echo base_url(); ?>application/pin_req/<?php echo $row1->tr_req_file; ?>"><?php echo $row1->tr_req_file; ?></a></td>
										<td><?php echo $row1->tr_req_status; ?></td>
										<td>
											<?php
												if($row1->status == 2)
												{
												?>
												<a href="<?php echo base_url(); ?>pin/view_pin_request_status/<?php echo $row1->tr_req_id; ?>/1">Approve</a>
												/ 
												<a href="<?php echo base_url(); ?>pin/view_pin_request_status/<?php echo $row1->tr_req_id; ?>/0">Reject</a>
												<?php
												}
											?>
										</td>
									</tr>
									<?php
										$sn++;
									}
								?>
								
							</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>															