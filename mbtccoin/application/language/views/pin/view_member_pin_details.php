<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('pin/view_member_pin_details',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						
						<div class="form-group">
							<label class="col-md-2 control-label">From Joining Date</label>
							<div class="col-md-3">
								<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
									<input type="text" class="form-control" name="start" id="txtfrom" />
									<span class="input-group-addon bg-custom b-0 text-white">to</span>
									<input type="text" class="form-control" name="end" id="txtto" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-2 control-label">Pin Issue To</label>
								<div class="col-md-3">
									<input type="text" id="txtpin_issue" name="txtpin_issue" class="form-control input-medium" placeholder="Enter Pin Issue To" >
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" id="btnsubmit" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Login Id</th>
								<th>Associate Name</th>
								<th>Plan</th>
								<th>Generation Date</th>
								<th>Total Count</th>
								<th>Total Amount</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=1;
								foreach($rec->result() as $row1)
								{
								?>
								<tr>
									<td><?php echo $sn; ?></td>
									<td><?php echo $row1->LoginId; ?></td>
									<td><?php echo $row1->Associate_Name; ?></td>
									<td><?php echo $row1->Plan_Name; ?></td>
									<td><?php echo $row1->Generation_date; ?></td>
									<td><?php echo $row1->Total_Count; ?></td>
									<td><?php echo $row1->Total_Amount; ?></td>
								</tr>
								<?php 
									$sn++;
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>												