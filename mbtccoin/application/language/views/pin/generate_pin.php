<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('pin/insert_plan',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Login ID <span class="required"> * </span></label>
							<div class="col-md-9">
								<input type="text" id="login_id" name="login_id" class="form-control empty" placeholder="Enter Login ID." onblur="validate_user()">
								<div id="validate_user"></div>
								<span id="divlogin_id" style="color:red;"></span>
							</div>
						</div>
						
						<input type="hidden" id="pintype" name="pintype" value="1"/>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Name <span class="required"> * </span></label>
							<div class="col-md-9">
								<div id="pack">
									<select id="pln_nm" name="pln_nm" class="form-control opt" onChange="fill_points()">
										<option selected="selected" value="-1">Select</option>
										<?php 
											foreach($package->result() as $row)
											{
											?>
											<option value="<?php echo $row->m_pack_id;?>"><?php echo $row->m_pack_name;?></option>
											<?php
											}
										?>
									</select>
									<span id="divpln_nm" style="color:red;"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Point</label>
							<div class="col-md-9">
								<div id="points">
									<input type="text" id="point" name="point" class="form-control empty" readonly placeholder="Enter Point." >
									<span id="divpoint" style="color:red;"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">No Of Pin <span class="required"> * </span></label>
							<div class="col-md-9">
								<input type="text" id="no_pin" name="no_pin" class="form-control empty" placeholder="Enter No Of Pin." >
								<span id="divno_pin" style="color:red;"></span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
						
		</div>
	</div>