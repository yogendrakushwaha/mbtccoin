<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('pin/view_atransfer_pin_report',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						
						<div class="form-group">
							<label class="col-md-2 control-label">From Joining Date</label>
							<div class="col-md-3">
								<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
									<input type="text" class="form-control" name="start" id="txtfrom" />
									<span class="input-group-addon bg-custom b-0 text-white">to</span>
									<input type="text" class="form-control" name="end" id="txtto" />
								</div>
							</div>
							
							<label class="col-md-2 control-label">Pin No</label>
							<div class="col-md-3">
								<input type="text" id="txtpin_no" name="txtpin_no" class="form-control input-inline input-medium" placeholder="Enter Pin No.">
							</div>
						</div>
						
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" id="btnsubmit" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Pin No.</th>
								<th>Pin Amount</th>
								<th>From User Id</th>
								<th>From Name</th>
								<th>To User Id</th>
								<th>To Name</th>
								<th>Pin Status</th>
								<th>Transfer Date</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=0;
								foreach($rec->result() as $row1)
								{
									$sn++;
								?>
								<tr>
									<td><?php echo $sn; ?></td>
									<td><?php echo $row1->PIN_NO; ?></td>
									<td><?php echo $row1->PACKAGE_FEE; ?></td>
									<td><?php echo $row1->FROM_USER_ID; ?></td>
									<td><?php echo $row1->FROM_USER_NAME; ?></td>
									<td><?php echo $row1->TO_ID; ?></td>
									<td><?php echo $row1->TO_NAME; ?></td>
									<td><?php echo $row1->PIN_STATUS; ?></td>
									<td><?php echo date('d-m-y',strtotime($row1->GENERATION_DATE)); ?></td>
								</tr>
								<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>															