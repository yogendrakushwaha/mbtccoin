<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('pin/transfer_to_update',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						
						<div class="form-group">
							<label class="col-md-2 control-label">From Joining Date</label>
							<div class="col-md-4">
								<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
									<input type="text" class="form-control" name="start" id="txtfrom" />
									<span class="input-group-addon bg-custom b-0 text-white">to</span>
									<input type="text" class="form-control" name="end" id="txtto" />
								</div>
							</div>
						</div>
						
						
						
						
						<div class="form-group">
							<label class="col-md-2 control-label">User Id</label>
							<div class="col-md-4">
								<input type="text" id="txtuser" name="txtuser" class="form-control" placeholder="Enter User Id.">
							</div>
							
							
							<label class="col-md-2 control-label">Business Plan</label>
							<div class="col-md-4">
								<select id="buisnespln" name="buisnespln" class="form-control">
									<option selected="selected" value="">Select</option>
									<?php foreach($package->result() as $row)
										{
										?>
										<option value="<?php echo $row->m_pack_id;?>"><?php echo $row->m_pack_name;?></option>
										<?php
										}
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" id="btnsubmit" onclick="get_active_pin()">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table class="table table-striped table-bordered" id="datatable">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Associate Id</th>
								<th>Associate Name</th>
								<th>Pin</th>
								<th>Busness Plan</th>
								<th>Amount</th>
								<th>Type</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
							<tr class="odd gradeX">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>							