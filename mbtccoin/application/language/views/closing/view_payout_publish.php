<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('admin_closing/view_payout_publish',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>

						<div class="row">

							<div class="form-group">
								<label class="col-md-3 control-label">Select Date</label>
								<div class="col-md-4">
									<select name="dddate" id="dddate" class="form-control">
										<option value="-1">Select</option>
										<?php
										foreach($clsd->result() as $row)
										{
										?>
											<option value="<?php echo $row->tr_closing_date; ?>"><?php echo date("d-m-Y", strtotime($row->tr_closing_date)); ?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="submit">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
	</div>