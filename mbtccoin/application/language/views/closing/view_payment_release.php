<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">

			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>

					<div class="form">
						<?= form_open($this->router->fetch_class().'/view_payout_report',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>

						<input type="hidden" name="txtquid" id="txtquid"/>

						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-5 col-md-7">
									<button class="btn btn-info" onclick="approve_payment()" type="button">Approve Payment!</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>

				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>

					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th><input type="checkbox" id="checkAll" name="checkAll" value="" onClick="chbcheckall()"></th>
								<th>S No.</th>
								<th>User Id</th>
								<th>User Name</th>
								<th>Bank Name</th>
								<th>Branch Name</th>
								<th>Account No</th>
								<th>IFSC</th>
								<th>Balance</th>
							</tr>
						</thead>
						<tbody id="userid">
							<?php
							$sn=0;
							$sum=0;
							if(!empty($record))
							{
								foreach($record->result() as $row)
								{ 
									$sn++;
									$sum = $sum + $row->curr_bal;
							?>
							<tr>
								<td><input type="checkbox" id="<?php echo $row->m_u_id.'_'.$row->curr_bal;?>" name="<?php echo $row->m_u_id.'_'.$row->curr_bal;?>" value="<?php echo $row->m_u_id.'_'.$row->curr_bal;?>" onClick="chbchecksin()"></td>
								<td><?php echo $sn; ?></td>
								<td><?php echo $row->or_m_user_id; ?></td>
								<td><?php echo $row->Username; ?></td>
								<td><?php echo $row->or_m_b_name; ?></td>
								<td><?php echo $row->or_m_b_branch; ?></td>
								<td><?php echo $row->or_m_b_cbsacno; ?></td>
								<td><?php echo $row->or_m_b_ifscode; ?></td>
								<td><?php echo $row->curr_bal; ?></td>
							</tr>
							<?php
								}
							}
							?>
							<tr>
								<td></td>
								<td><?php echo ++$sn; ?></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><b>Total</b></td>
								<td><b><?php echo $sum; ?></b></td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>

		</div>
	</div>