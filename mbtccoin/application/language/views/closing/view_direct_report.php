<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">

			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open($this->router->fetch_class().'/view_direct_report',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>

						<div class="row">

							<div class="form-group">
								<label class="col-md-3 control-label">Select Date</label>
								<div class="col-md-4">
									<select name="dddate" id="dddate" class="form-control">
										<option value="-1">Select</option>
										<?php
										foreach($clsd->result() as $row)
										{
										?>
											<option value="<?php echo $row->tr_closing_date; ?>"><?php echo date("d-m-Y", strtotime($row->tr_closing_date)); ?></option>
										<?php
										}
										?>
									</select>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="submit">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
				
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>

					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th style="display:none;">S No.</th>
								<th>Associate</th>
								<th>Closing Date</th>
								<th>Reffer From</th>
								<th>Total Amount</th>
								<th>Admin Charges(5%)</th>
								<th>TDS Charges(5%)</th>
								<th>Processing Charges(5%)</th>
								<th>Net Income</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$total=0;
								$t_admin=0;
								$tds=0;
								$pds=0;
								$final_amt=0;
								$sn=0;
								if(!empty($payout_details))
								{
									foreach($payout_details->result() as $row)
									{ 
										$sn++;
										$total = $total+$row->Payout_Amount;
										$t_admin = $t_admin+$row->Admin_Charges;
										$tds = $tds+$row->TDS_Charges;
										$pds = $pds+$row->Processing_charge;
										$final_amt = $final_amt+$row->Final_Amount;
									?>
									<tr>
										<td style="display:none;"><?php echo $sn; ?></td>
										<td><?php echo $row->Associate_Id.'/'.$row->Associate_Name; ?></td>
										<td><?php echo date('d-m-Y h:i:s', strtotime($row->From_Date)); ?></td>
										<td><?php echo $row->Refer_from; ?></td>
										<td><?php echo $row->Payout_Amount; ?></td>
										<td><?php echo $row->Admin_Charges; ?></td>
										<td><?php echo $row->TDS_Charges; ?></td>
										<td><?php echo $row->Processing_charge; ?></td>
										<td><?php echo $row->Final_Amount; ?></td>
									</tr>
									<?php
									}
								}
							?>
							<tr>
								<td style="display:none;"><?php echo $sn+1; ?></td>
								<td></td>
								<td></td>
								<td><b>TOTAL</td>
								<td><b><?php echo round($total,2); ?></b></td>
								<td><b><?php echo round($t_admin,2); ?></b></td>
								<td><b><?php echo round($tds,2); ?></b></td>
								<td><b><b><?php echo round($pds,2); ?></b></td>
								<td><b><?php echo round($final_amt,2); ?></b></td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
			
		

		</div>
	</div>