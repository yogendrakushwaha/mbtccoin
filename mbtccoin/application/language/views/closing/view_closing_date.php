<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->

		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					<div class="row">
						<div class="col-md-offset-5 col-md-8">
							<!--button class="btn btn-primary" type="button" onclick="get_direct_date()">Direct Date</button-->
							<button class="btn btn-info" type="button" onclick="get_closing_date()">Closing Date</button>
							<!--&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger" type="button" onclick="get_payout_date()">Payout Date</button-->
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>