<body>
	<div class="account-pages"></div>
	<div class="clearfix"></div>
	<div class="wrapper-page">
		<div class="card-box" style="background-color: rgba(168, 207, 69, 0.74) !important;">
            <div class="panel-heading"> 
                <h3 class="text-center"><strong class="text-custom"><?php echo SITE_NAME; ?></strong> </h3>
                <h5 class="text-center">Forgot Password?</h5>
			</div> 
			
            <div class="panel-body">
				<?php 
					echo form_open('auth/resetpassword', 'class="form-horizontal m-t-20" id="myform"');
					if($this->uri->segment(1) == 'admin_login')
					{
					?>
					<input type="hidden" name="ddtype" value="1">
					<?php
					}
					else
					{
					?>
					<input type="hidden" name="ddtype" value="2">
					<?php
					}
				?>
				<div class="form-group ">
					<div class="col-xs-12">
						<input class="form-control" type="text" required="" placeholder="UserID" name="txtuserid">
					</div>
				</div>
				
				<?php
					if($this->session->flashdata('info'))
					{
					?>
					<div class="alert alert-danger">
						<button class="close" data-close="alert"></button>
						<span><?php echo $this->session->flashdata('info'); ?> </span>
					</div>
					<?php
					}
				?>
				<div class="form-group text-center m-t-40">
					<div class="col-xs-12">
						<a href=""><button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">Redeem Your Password</button></a>
					</div>
				</div>
				
				<?php echo form_close(); ?> 
				
			</div>   
		</div>                              
		
	</div>
	
	<script>
		var resizefunc = [];
	</script>
	
	<!-- jQuery  -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/detect.js"></script>
	<script src="assets/js/fastclick.js"></script>
	<script src="assets/js/jquery.slimscroll.js"></script>
	<script src="assets/js/jquery.blockUI.js"></script>
	<script src="assets/js/waves.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/jquery.nicescroll.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>
	
	
	<script src="assets/js/jquery.core.js"></script>
	<script src="assets/js/jquery.app.js"></script>
	
</body>
</html>