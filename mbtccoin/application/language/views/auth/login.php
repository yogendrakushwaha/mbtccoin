<body>
	<div class="account-pages"></div>
	<div class="clearfix"></div>
	<div class="wrapper-page">
		<div class="card-box" style=" background-color: rgba(90, 185, 72, 0.92) !important;">
            <div class="panel-heading"> 
                <center> <img src="logo/logologin1.png" class="img-responsive" onclick="window.location.href='http://assvigroup.in'" /> </center>
			</div> 
			
            <div class="panel-body">
				<?php 
					echo form_open('auth/login', 'class="form-horizontal m-t-20" id="myform"');
					if($this->uri->segment(1) == 'admin_login')
					{
					?>
					<input type="hidden" name="ddtype" value="1">
					<?php
					}
					else
					{
					?>
					<input type="hidden" name="ddtype" value="2">
					<?php
					}
				?>
				<div class="form-group ">
					<div class="col-xs-12">
						<input class="form-control" type="text" required="" placeholder="Username" name="txtlogin">
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-xs-12">
						<input class="form-control" type="password" required="" placeholder="Password" name="txtpwd">
					</div>
				</div>
				
				<div class="form-group ">
					<div class="col-xs-12">
						<div class="checkbox checkbox-primary">
							<input id="checkbox-signup" type="checkbox">
							<label for="checkbox-signup">
								Remember me
							</label>
						</div>
						
					</div>
				</div>
				<?php
					if($this->session->flashdata('info'))
					{
					?>
					<div class="alert alert-danger">
						<button class="close" data-close="alert"></button>
						<span><?php echo $this->session->flashdata('info'); ?> </span>
					</div>
					<?php
					}
				?>
				<div class="form-group text-center m-t-40">
					<div class="col-xs-12">
						<button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
					</div>
				</div>
				
				<div class="form-group m-t-30 m-b-0">
					<div class="col-sm-12">
						<a href="<?php echo base_url();?>auth/forgot_password" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
					</div>
				</div>
				<?php echo form_close(); ?> 
				
			</div>   
		</div>                              
		
	</div>
	
	<script>
		var resizefunc = [];
	</script>
	
	<!-- jQuery  -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/detect.js"></script>
	<script src="assets/js/fastclick.js"></script>
	<script src="assets/js/jquery.slimscroll.js"></script>
	<script src="assets/js/jquery.blockUI.js"></script>
	<script src="assets/js/waves.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/jquery.nicescroll.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>
	
	
	<script src="assets/js/jquery.core.js"></script>
	<script src="assets/js/jquery.app.js"></script>
	
</body>
</html>