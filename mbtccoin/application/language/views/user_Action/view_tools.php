<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo $page; ?></h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
		
			<div class="col-md-3">
				<div class="col-md-12">
					<div class="profile-detail card-box">
						<div>
							<?php
								if($this->session->userdata('prifile_pic')!='')
								{
								?>
								<img src="<?php echo base_url(); ?>application/user_image/<?php echo $this->session->userdata('prifile_pic') ?>" class="img-circle" alt="profile-image">
								<?php
								}
								else
								{
								?>
								<img src="<?php echo base_url(); ?>application/libraries/profile.png" class="img-circle" alt="profile-image">
								<?php
								}
							?>
							<hr>
							
							<div class="text-left">
								<p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15"><?php echo $this->session->userdata('name'); ?></span></p>
								
								<p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15"><?php echo $this->session->userdata('mobile_no'); ?></span></p>
								
								<p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $this->session->userdata('e_email'); ?></span></p>
								
								<p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15"><?php echo $this->session->userdata('location'); ?></span></p>
								
							</div>
							
						</div>
						
					</div>
					
				</div>
			</div>
				
		
			<?php 
			foreach($tools->result() as $row)
			{
			?>
			<div class="col-md-9">
				<div class="panel panel-purple panel-border">
					<?= form_open('user_action/user_reply_tool/'.$row->m_tool_id,array("id" => "signupForm")); ?>
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $row->m_tool_title; ?></h3>
						</div>
						<div class="panel-body">
							<p>
								<?php if($row->m_tool_video!=''){ ?>
								<input type="button" onclick="open_model('<?php echo $row->m_tool_video; ?>')" href="#custom-modal" value="Show Video" data-animation="blur" data-plugin="custommodal"
									   data-overlaySpeed="100" data-overlayColor="#36404a" class="btn btn-info waves-effect waves-ligh" />
								<?php } ?>
							</p>
							<p>
								<?php echo $row->m_tool_description; ?>
							</p>
							<?php if($row->m_tool_user_response==''){ ?>
							<p>
								<textarea rows="3" class="form-control" id="txtresponse" name="txtresponse" placeholder="Enter Reply"></textarea><br>
								<input type="submit" name="btnsbt" id="btnsbt" class="btn btn-primary pull-right" />
							</p>
							<?php } ?>
						</div>
					</form>
				</div>
			</div>
			<?php
			}
			?>
		</div>

	</div>
	
	<!-- Modal -->
	<div id="custom-modal" class="modal-demo">
		<button type="button" class="close" onclick="Custombox.close();">
			<span>&times;</span><span class="sr-only">Close</span>
		</button>
		<h4 class="custom-modal-title">Video</h4>
		<div class="custom-modal-text" id="display_video">
			
		</div>
	</div>
