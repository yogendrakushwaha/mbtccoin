<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo $page; ?></h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('User_action/insert_fund_transfer',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-4 control-label">User Id</label>
							<div class="col-md-6">
								<input type="text" id="txtsenuserid" name="txtsenuserid" readonly value="<?php echo $this->session->userdata('user_id'); ?>" class="form-control empty" placeholder="Enter Transfer Id.">
								<span id="divtxtsenuserid" style="color:red"></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Transfer Id</label>
							<div class="col-md-6">
								<input type="text" id="txtuserid" name="txtuserid" class="form-control empty" placeholder="Enter Transfer Id." onblur="validate_user()">
								<span id="divtxtuserid" style="color:red"></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Amount</label>
							<div class="col-md-6">
								<input type="text" id="txtamount" name="txtamount" class="form-control numeric" placeholder="Enter Amount.">
								<span id="divtxtamount" style="color:red"></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Transaction Password</label>
							<div class="col-md-6">
								<input type="password" id="txtpinpwd" name="txtpinpwd" class="form-control empty" placeholder="Enter Transaction Password.">
								<span id="divtxtpinpwd" style="color:red"></span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>