<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo $page; ?></h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('User_action/insert_fund_requset/',array("class" => "cmxform form-horizontal", "enctype" => "multipart/form-data", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Amount</label>
							<div class="col-md-6">
								<input type="text" id="txtamount" name="txtamount" class="form-control empty" placeholder="Enter Amount.">
								<span id="divtxtamount" style="color:red"></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Recipt</label>
							<div class="col-md-6">
								<input type="file" class="filestyle" data-size="sm" name="userfile" id="userfile" accept="image/*">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Date</th>
								<th>Amount</th>
								<th>Recipt</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=1;
								if(!empty($rid))
								{
									foreach($rid->result() as $rows)
									{ 
									?>
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $rows->m_top_reqdate; ?></td>
										<td><?php echo $rows->tr09_req_amount; ?></td>
										<td><?php echo $rows->tr09_req_recipt; ?></td>
										<td><?php echo $rows->m_top_status_desc; ?></td>
									</tr>   
									<?php
										$sn++;
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
		
		
	</div>