<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
		
	<?php include('user_detail.php'); ?>
				
		
			<div class="col-lg-9">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('userprofile/view_all_member',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="row">
							
							<div class="form-group">
								<label class="col-md-2 control-label">Select Date</label>
								<div class="col-md-5">
									<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control" name="start" />
										<span class="input-group-addon bg-custom b-0 text-white">to</span>
										<input type="text" class="form-control" name="end"  />
									</div>
								</div>
							</div>
							
						</div>
						
						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="submit">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Date</th>
								<th>User Id</th>
								<th>Description</th>
								<th>Credit</th>
								<th>Debit</th>
								<th>Current Balance</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=1;
								$cr=0;
								$dr=0;
								foreach($rid->result() as $rows)
								{ 
									$cr=$cr+$rows->LEDGER_CR;
									$dr=$dr+$rows->LEDGER_DR;
								?>
								<tr>
									<td><?php echo $sn; ?></td>
									<td><?php echo $rows->LEDGER_DATETIME; ?></td>
									<td><?php echo $rows->LEDGER_USERID; ?></td>
									<td><?php echo $rows->LEDGER_DESC; ?></td>
									<td><?php echo $rows->LEDGER_CR; ?></td>
									<td><?php echo $rows->LEDGER_DR; ?></td>
									<td><?php echo $rows->LEDGER_CURRBAL; ?></td>
								</tr>   
								<?php
									$sn++;
								}
							?>
								<tr>
									<td><?php echo $sn; ?></td>
									<td></td>
									<td></td>
									<td><b>Total</b></td>
									<td><b><?php echo $cr; ?></b></td>
									<td><b><?php echo $dr; ?></b></td>
									<td></td>
								</tr>   
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>