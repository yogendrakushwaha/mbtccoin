<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			
			<?php include('user_detail.php');  ?>
				
			
			<div class="col-sm-9">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Login Id</th>
								<th>Associate</th>
								<th>Bank</th>
								<th>Branch</th>
								<th>IFSCCode</th>
								<th>AccNo</th>
								<th>Pancard</th>
								<th>Adhar</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=1;
								foreach($bank_details->result() as $rows)
								{
								?>
								<tr>
									<td><?php echo $sn;?></td>
									<td><?php echo $rows->Login_Id;?></td>
									<td><?php echo $rows->Associate_Name;?></td>
									<td><?php echo $rows->Bankname;?></td>
									<td><?php echo $rows->Branch;?></td>
									<td><?php echo $rows->Ifsc;?></td>
									<td><?php echo $rows->AcNo;?></td>
									<td><?php echo $rows->Pancard;?></td>
									<td><?php echo $rows->Adhar;?></td>
									<td><a href="#custom-modal" class="waves-effect waves-light" onclick="get_bank_details('<?php echo $rows->BANKID; ?>','<?php echo $rows->Branch; ?>','<?php echo $rows->Ifsc; ?>','<?php echo $rows->AcNo; ?>','<?php echo $rows->Pancard; ?>',<?php echo $rows->regid; ?>,'<?php echo $rows->Adhar; ?>')" data-animation="sign" data-plugin="custommodal"  data-overlaySpeed="100" data-overlayColor="#36404a" >Update Details</a></td>
								</tr>   
							<?php $sn++; }?>                   
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="custom-modal" class="modal-demo">
		<button type="button" class="close" onclick="Custombox.close();">
			<span>&times;</span><span class="sr-only">Close</span>
		</button>
		<h4 class="custom-modal-title" id="headtxt">Update Bank Details</h4>
		<div class="custom-modal-text">
			<div class="form">
				<?= form_open('',array("class" => "form-horizontal", "id" => "signupForm")); ?>
				
				<div class="form-group">
					<label class="col-md-3 control-label" id="chg_name">Pancard</label>
					<div class="col-md-8">
						<input type="text" id="txtpancard" name="txtpancard" class="form-control empty" placeholder="Enter Pancard" onblur="validate_pancard()">
						<span id="divtxtpancard" style="color:red"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" id="chg_name">Adhar</label>
					<div class="col-md-8">
						<input type="text" id="txtadhar" name="txtadhar" class="form-control empty" placeholder="Enter Adhar">
						<span id="divtxtadhar" style="color:red"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" id="chg_name">Bank Name</label>
					<div class="col-md-8">
						<select id="txtbank" name="txtbank" class="form-control opt">
							<option value="-1">Select</option>
							<?php
							foreach($bank->result() as $b)
							{
							?>
							<option value="<?php echo $b->m_bank_id; ?>"><?php echo $b->m_bank_name; ?></option>
							<?php
							}
							?>
						</select>
						<span id="divtxtbank" style="color:red"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" id="chg_name">Branch Name</label>
					<div class="col-md-8">
						<input type="text" id="txtbranch" name="txtbranch" class="form-control empty" placeholder="Enter Branch Name">
						<span id="divtxtbranch" style="color:red"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" id="chg_name">IFSC Code</label>
					<div class="col-md-8">
						<input type="text" id="txtifsc" name="txtifsc" class="form-control empty" placeholder="Enter IFSC Code">
						<span id="divtxtifsc" style="color:red"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" id="chg_name">Account No.</label>
					<div class="col-md-8">
						<input type="text" id="txtacc" name="txtacc" class="form-control empty" placeholder="Enter Account No.">
						<span id="divtxtacc" style="color:red"></span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-offset-1 col-md-8">
						<button class="btn btn-primary" type="submit" onclick="return check('signupForm')">Submit</button>
						<button type="button" class="btn btn-default">Cancel</button>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>