<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 style="float:left;" class="page-title"><?php echo $page; ?>&nbsp;&nbsp;&nbsp;|</h4><p class="text-muted page-title-alt">&nbsp;&nbsp; <?php echo $form_name; ?></p>
			</div>
		</div>
		
		<div class="row">
			
			<?php include('user_detail.php') ?>
			
			
			<div class="col-md-9">
					<div class="col-md-6 col-lg-3">
						<div class="widget-bg-color-icon card-box fadeInDown animated">
							<div class="bg-icon bg-icon-info pull-left">
								<i class="fa fa-inr text-info"></i>
							</div>
							<div class="text-right">
								<h3 class="text-dark"><b style="font-size:18px;"><?php echo $user->direct; ?></b></h3>
								<p class="text-muted">Direct Income</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					
					<div class="col-md-6 col-lg-3">
						<div class="widget-bg-color-icon card-box fadeInDown animated">
							<div class="bg-icon bg-icon-info pull-left">
								<i class="fa fa-inr text-info"></i>
							</div>
							<div class="text-right">
								<h3 class="text-dark" style="font-size:18px;"><b><?php echo $user->matching; ?></b></h3>
								<p class="text-muted">Matching Income</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					
					<div class="col-md-6 col-lg-3">
						<div class="widget-bg-color-icon card-box fadeInDown animated">
							<div class="bg-icon bg-icon-info pull-left">
								<i class="fa fa-inr text-info"></i>
							</div>
							<div class="text-right">
								<h3 class="text-dark" style="font-size:18px;"><b><?php echo $user->tds; ?></b></h3>
								<p class="text-muted">Total TDS</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					
					<div class="col-md-6 col-lg-3">
						<div class="widget-bg-color-icon card-box fadeInDown animated">
							<div class="bg-icon bg-icon-info pull-left">
								<i class="fa fa-inr text-info"></i>
							</div>
							<div class="text-right">
								<h3 class="text-dark"><b style="font-size:18px;"><?php echo $user->direct+$user->matching; ?></b></h3>
								<p class="text-muted">Total Income</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				<div class="clearfix"></div>
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box fadeInDown animated">
						<div class="bg-icon bg-icon-info pull-left">
							<i class="glyphicon glyphicon-circle-arrow-down text-info"></i>
						</div>
						<div class="text-right">
							<h3 class="text-dark"><b><?php echo $user->direct_id; ?></b></h3>
							<p class="text-muted">Direct</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-pink pull-left">
							<i class="md-fast-rewind text-pink"></i>
						</div>
						<div class="text-right">
							<h3 class="text-dark"><b><?php echo $user->left_id; ?></b></h3>
							<p class="text-muted">Total Left</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-purple pull-left">
							<i class="md-fast-forward text-purple"></i>
						</div>
						<div class="text-right">
							<h3 class="text-dark"><b><?php echo $user->right_id; ?></b></h3>
							<p class="text-muted">Total Right</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-success pull-left">
							<i class="md-call-split text-success"></i>
						</div>
						<div class="text-right">
							<h3 class="text-dark"><b><?php echo $user->tot_id; ?></b></h3>
							<p class="text-muted">Total</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box fadeInDown animated">
						<div class="bg-icon bg-icon-info pull-left">
							<i class="md-assignment-ind text-info"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0" style="font-size:12px;"><a href="<?php echo base_url(); ?>userprofile/view_member_details">View Profile</a><br><a href="<?php echo base_url(); ?>userprofile/view_member_edit">Update Profile</a></h4>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-purple pull-left">
							<i class="fa fa-sitemap text-purple"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a href="<?php echo base_url(); ?>userprofile/tree/1">My Team</a></h4>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-success pull-left">
							<i class="md-perm-identity text-success"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a href="<?php echo base_url(); ?>userprofile/join_member">Add New Member</a></h4>
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-pink pull-left">
							<i class="md-receipt text-pink"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a href="<?php echo base_url(); ?>userprofile/member_welcome_letter">My Joining Letter</a></h4>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>	
				
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-custom pull-left">
							<i class="fa fa-inr text-custom"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a href="<?php echo base_url(); ?>user_action/view_payout_report">My Payout</a></h4>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-danger pull-left">
							<i class="fa fa-cog text-danger"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a style="font-size:15px;" href="<?php echo base_url(); ?>userprofile/change_password">Change Password</a></h4>
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-danger pull-left">
							<i class="fa fa-database text-danger"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a style="font-size:15px;" href="<?php echo base_url(); ?>pin/view_pin_bank/1">Manage Pin</a></h4>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-6 col-lg-3">
					<div class="widget-bg-color-icon card-box">
						<div class="bg-icon bg-icon-danger pull-left">
							<i class="fa fa-university text-danger"></i>
						</div>
						<div class="text-right">
							<h4 class="text-dark header-title m-t-0"><a style="font-size:15px;" href="<?php echo base_url(); ?>userprofile/view_bank_details">Bank Details</a></h4>
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
			</div>
		</div>
		
		<!--<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			<h4 class="modal-title" id="mySmallModalLabel">Reward</h4>
			</div>
			<div class="modal-body">
			<img src="<?php echo base_url(); ?>application/libraries/pop.jpg" height="500" width="100%" class="img-responsive" />
			</div>
			</div>
			</div>
		</div>--><!-- /.modal-content --><!-- /.modal-dialog --><!-- /.modal -->
		
		
		
		
		
		
		
		<script type="text/javascript">
			$(window).load(function(){
				$('.bs-example-modal').modal('show');
			});
		</script>				