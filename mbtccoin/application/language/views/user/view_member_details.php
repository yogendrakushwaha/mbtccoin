<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			
			<?php
				if(!empty($rec))
				{
				?>
				<?php include('user_detail.php');  ?>
				
				<div class="col-sm-9">
					<div class="card-box table-responsive">
						<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
						<table id="datatable-buttons" class="table table-striped table-bordered">
							<tbody>
								<tr>
									<td>Sponser Id</td>
									<td><?php echo $rec->Intro_Userid;?></td>
								</tr>
								<tr>
									<td>Sponser Name</td>
									<td><?php echo $rec->Intro_Name;?></td>
								</tr>
								<tr>
									<td>Postion</td>
									<td><?php echo $rec->USER_POSITION_DESC;?></td>
								</tr>
								<tr>
									<td>Upliner Id</td>
									<td><?php echo $rec->Upliner_Userid;?></td>
								</tr>
								<tr>
									<td>Upliner Name</td>
									<td><?php echo $rec->Upliner_Name;?></td>
								</tr>
								<tr>
									<td>DOB</td>
									<td><?php echo $rec->DOB;?></td>
								</tr>
								<tr>
									<td>Joining Date</td>
									<td><?php echo $rec->Joining_Date_1;?></td>
								</tr>
								<tr>
									<td>Password</td>
									<td><?php echo $rec->Password;?></td>
								</tr>
								<tr>
									<td>Pin Password</td>
									<td><?php echo $rec->Pin_Password;?></td>
								</tr>
								<tr>
									<td>Bank Name</td>
									<td><?php echo $rec->Bankname;?></td>
								</tr>
								<tr>
									<td>Branch Name</td>
									<td><?php echo $rec->Branch;?></td>
								</tr>
								<tr>
									<td>Account No.</td>
									<td><?php echo $rec->AcNo;?></td>
								</tr>
								<tr>
									<td>IFSC Code</td>
									<td><?php echo $rec->Ifsc;?></td>
								</tr>
								<tr>
									<td>Pancard</td>
									<td><?php echo $rec->Pancard;?></td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
				<?php
				}
			?>
		</div>
	</div>