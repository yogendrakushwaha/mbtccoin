<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>userprofile/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		
		<div class="row">
		
		<?php include('user_detail.php'); ?>
				
		
			<div class="col-md-9">
				<section id="cd-timeline" class="cd-container">
					<?php
						foreach($news->result() as $row)
						{
						?>
						<div class="cd-timeline-block">
							<div class="cd-timeline-img cd-info">
								<i class="md md-cast"></i>
							</div> <!-- cd-timeline-img -->
							
							<div class="cd-timeline-content">
								<h3><?php echo $row->m_news_title; ?></h3>
								<p><?php echo $row->m_news_des; ?></p>
								<span class="cd-date">
									<?php
										$originalDate = $row->m_entrydate;
										$newDate = date("M d", strtotime($originalDate));
										echo $newDate;
									?>
								</span>
							</div> <!-- cd-timeline-content -->
						</div> <!-- cd-timeline-block -->
						<?php
						}
					?>
				</section> <!-- cd-timeline -->
			</div>
			
		</div>				