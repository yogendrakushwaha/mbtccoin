<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						
						<?= form_open('userprofile/insert_member_topup',array("class" => "cmxform horizontal-form", "id" => "signupForm")); ?>
						
						<div class="row">
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">User ID <span class="required"> * </span></label>
									<input type="text" class="form-control empty" name="txtuser_id" id="txtuser_id" readonly value="<?php echo $this->session->userdata('user_id'); ?>">
									<span id="divtxtuser_id" style="color:red"></span>
								</div>
							</div>
							<!--/span-->
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">User Name <span class="required"> * </span></label>
									<input type="text" class="form-control empty" name="txtuser_name" id="txtuser_name" readonly value="<?php echo $this->session->userdata('name'); ?>">
									<span id="divtxtusername_name" style="color:red"></span>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Plan <span class="required"> * </span></label>
									<select id="ddpack" name="ddpack" class="form-control opt" onchange="change_plan()">
										<option selected="selected" value="-1">Select Plan</option>
										<?php
										foreach($pack->result() as $row)
										{
										?>
										<option value="<?php echo $row->m_pack_id; ?>"><?php echo $row->m_pack_name; ?> (<?php echo $row->m_pack_fee; ?>)</option>
										<?php
										}
										?>
									</select>
									<span id="divddpack" style="color:red"></span>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Amount <span class="required"> * </span></label>
									<input type="text" class="form-control empty" name="txtamt" id="txtamt" readonly>
									<span id="divpinno" style="color:red"></span>
								</div>
							</div>
							
						</div>
						<!--/span-->
					</div>
					
					<div class="row">
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		
	</div>
</div>