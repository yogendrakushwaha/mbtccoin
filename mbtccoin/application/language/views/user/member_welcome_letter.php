<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>userprofile/dashboard">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
		
			
		
			<div class="col-lg-12">
				
				<div style="max-width:600px; border:1px solid #b0b0b0; margin:0 auto; background:url(<?php echo base_url(); ?>application/libraries/20.png) repeat;">
					
					<header style="text-align:center; padding:15px;">
						<img src="<?php echo base_url(); ?>letter/logo.png" />
						<div class="clearfix"></div>
						<h4><i class="fa fa-envelope-o"></i> <?php echo EMAIL; ?> | <i class="fa fa-globe"></i> <?php echo WEBSITE_NAME; ?></h4>
						
						
					</header>
					<img src="<?php echo base_url(); ?>letter/welcome.png" />
					
					<p align="justify" style="line-height:25px; padding:15px;">
						We <?php echo SITE_NAME; ?>, wish you a very Happy SEASON. We welcome you to our <?php  echo SITE_NAME; ?> for your joining with us. We appreciate your decision taken on the right time in the right perspective. We assure you as being a member of <?php  echo SITE_NAME; ?> that we will try to fulfill your wishes to have financial prosperity and whatever good things in your mind come true. <?php  echo SITE_NAME; ?> be able to perform more positive works for the society with your dedicated co-operation. We welcome your suggestions to improve OUR OBJECTIVES MISSIONS. Please don’t hesitate to write to us. 
					</p>
					
					<div class="clearfix"></div>
					
					<?php
						foreach($info->result() as $rinfo)
						{
							break;
						}
					?>
					
					
					<div class="row" style="padding:15px;">
						<h2>Personal Details ::</h2>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Associated Id ::</th>
									<td><?php echo $rinfo->Login_Id;?></td>
									<th>Associated Name ::</th>
									<td><?php echo $rinfo->Associate_Name;?></td>
								</tr>
								
								<tr>
									<th>Joining date ::</th>
									<td><?php echo $rinfo->Joining_Date_1;?></td>
								</tr>
								
								<tr>
									<th>Address ::</th>
									<td><?php echo $rinfo->Address;?></td>
									<th>city ::</th>
									<td><?php echo $rinfo->City;?></td>
								</tr>
								
								<tr>
									<th>State ::</th>
									<td><?php echo $rinfo->State;?></td>
									<th>Pincode ::</th>
									<td><?php echo $rinfo->Pincode;?></td>
								</tr>
								
								<tr>
									<th>Sponser Id ::</th>
									<td><?php echo $rinfo->Intro_Userid;?></td>
									<th>Sponser ::</th>
									<td><?php echo $rinfo->Intro_Name;?></td>
								</tr>
								
							</thead>
						</table>
					</div>
					
				</div>
				
			</div>
		</div>
	</div>