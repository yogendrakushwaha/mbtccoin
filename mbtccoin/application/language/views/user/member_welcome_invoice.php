<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>userprofile/dashboard">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
		
			
				
		
			<div class="col-lg-12">
				
				<div style="max-width:600px; min-height:700px; border:1px solid #b0b0b0; margin:0 auto; background:url(<?php echo base_url(); ?>application/libraries/20.png) repeat;">
					
					<header style="text-align:center; padding:15px;">
						<img src="<?php echo base_url(); ?>letter/logo.png" />
						<div class="clearfix"></div>
						<h4><i class="fa fa-envelope-o"></i> <?php echo EMAIL; ?> | <i class="fa fa-globe"></i> <?php echo WEBSITE_NAME; ?></h4>
						
						
					</header>
					<img src="<?php echo base_url(); ?>letter/invoice.png" />
					
					<div class="clearfix"></div>
					
					<?php
						foreach($info->result() as $rinfo)
						{
							break;
						}
					?>
					
					<div class="row" style="padding:15px;">
						<table class="table table-hover">
							<tr>
								<th>Date ::</th>
								<td><?php echo $rinfo->Joining_Date_1;?></td>
							</tr>
							<tr>
								<th>Associated Id ::</th>
								<td><?php echo $rinfo->Login_Id;?></td>
							</tr>
							<tr>
								<th>Recieved With Thanks Form ::</th>
								<td><?php echo $rinfo->Associate_Name;?></td>
							</tr>
							<tr>
								<th>Mobile No ::</th>
								<td><?php echo $rinfo->Mobile_No;?></td>
							</tr>
							
							<tr>
								<th>Thanks Form ::</th>
								<td><?php echo WEBSITE_NAME; ?></td>
							</tr>
						</table>
						
						<p>(this is auto generated online reciept this is not required to sign)</p>
						
					</div>
					
				</div>
				
			</div>
		</div>
	</div>