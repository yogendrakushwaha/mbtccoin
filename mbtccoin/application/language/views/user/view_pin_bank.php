<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
		
	<?php include('user_detail.php'); ?>
				
		
			<div class="col-lg-9">
				
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Pin Id</th>
								<th>Package</th>
								<th>Amount</th>
								<th>Associate_id</th>
								<th>Associate_name</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=1;
								
								foreach($info->result_array() as $rows)
								{ 
								?>
								<tr>
									<td><?php echo $sn; ?></td>
									<td><?php echo $rows['PIN']; ?></td>
									<td><?php echo $rows['Package_name']; ?></td>
									<td><?php echo $rows['Amount']; ?></td>
									<td><?php echo $rows['Associate_id']; ?></td>
									<td><?php echo $rows['Associate_name']; ?></td>
									<td><?php echo $rows['PIN_Status']; ?></td>
								</tr>   
								<?php
									$sn++;
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>