<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>userprofile/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
		<?php include('user_detail.php'); ?>
			<div class="col-lg-9">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('pin/transfer_to_update/1',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Enter Login ID</label>
							<div class="col-md-5">
								<input type="text" value="<?php echo $this->session->userdata('user_id') ?>" id="txtowner" name="txtowner" class="form-control empty" placeholder="Enter Login Id" readonly>
								<span id="divtxtowner" style="color:red;"></span>
							</div>
						</div>
						
						<input type="hidden" id="pintype" name="pintype" value="1"/>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Plan Name <span class="required"> * </span></label>
							<div class="col-md-5">
								<div id="pack">
									<select id="pln_nm" name="pln_nm" class="form-control opt">
										<option selected="selected" value="-1">Select</option>
										<?php 
											foreach($package->result() as $row)
											{
											?>
											<option value="<?php echo $row->m_pack_id;?>"><?php echo $row->m_pack_name;?></option>
											<?php
											}
										?>
									</select>
									<span id="divpln_nm" style="color:red;"></span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Quantity</label>
							<div class="col-md-5">
								<input type="text" id="quantity" name="quantity" class="form-control empty" placeholder="Enter Quantity" >
								<span id="divquantity" style="color:red;"></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-5">
								<input type="button" class="btn btn-danger" value="Show" onclick="show_pin()"/>
								<input type="hidden" id="txtquid" name="txtquid" class="form-control empty" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Transfer To UserID</label>
							<div class="col-md-5">
								<input type="text" id="login_id" name="login_id" class="form-control empty" placeholder="Enter Transfer To UserID" onblur="validate_user()">
								<span id="divlogin_id" style="color:red;"></span>
								<div id="validate_user"></div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" id="btnsubmit" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-9">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table class="table table-striped table-bordered" id="pin">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Pin</th>
								<th>Busness Plan</th>
								<th>Amount</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>		