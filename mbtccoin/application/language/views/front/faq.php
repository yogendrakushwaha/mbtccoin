<div class="clearfix"></div>
	
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h4>FAQ</h4>
					<span>Frequently asked questions</span>
				</div>
				<div class="col-md-6">
					<div class="crumbs">You are here: <a href="index.html">Home</a><span class="crumbs-span">&raquo;</span><span>FAQ</span></div>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</div><!-- End breadcrumbs -->
	
	<div class="sections sections-padding-b-50">
		<div class="container">
			<div class="sections-title">
				<div class="sections-title-h3"><h3>Frequently asked questions</h3></div>
				<iframe src="<?php echo base_url(); ?>application/libraries/front/images/arun b.pdf" width="1100PX" height="1000PX"></iframe>			</div><!-- End sections-title -->
			<div class="row">
				<!-- End member-slide -->
			</div><!-- End row -->
		</div><!-- End container -->
	</div><!-- End sections -->
	
	<!-- End sections -->
	
	<!-- End sections -->
	
	<!-- End sections -->
	
	<!-- End footer -->
<footer id="footer-bottom">
		<div class="container">
			<div class="copyrights">Copyright 2017 Assvi Group Sales & Marketing Pvt. Ltd </div>
			<nav class="navigation-footer">
				<ul>
					<li><a href="index.html">Home</a></li>
					<li><a href="javascript:void(0);">About us</a></li>
					<li><a href="javascript:void(0);">Business Plan</a></li>
					<li><a href="javascript:void(0);">Gallery</a></li>
					<li><a href="javascript:void(0);">Career</a></li>
					<li><a href="javascript:void(0);">Legal</a></li>
					<li><a href="javascript:void(0);">Contact</a></li>
				</ul>
			</nav>
		</div><!-- End container -->
	</footer>
</div><!-- End wrap -->

<div class="go-up"><i class="fa fa-chevron-up"></i></div>

<!-- js -->
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/html5.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.appear.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/count-to.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/twitter/jquery.tweet.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.inview.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.bxslider.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/custom.js"></script>
<!-- End js -->

</body>


</html>