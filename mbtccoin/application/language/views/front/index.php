<!--pop up-->
<link rel="stylesheet" href="facebox/facebox.css" type="text/css" media="all" />
<script src="facebox/jquery.js"></script>	
<script src="facebox/facebox.js"></script>	

<script type="text/javascript">
jQuery(document).ready(function($) {
$.facebox.settings.opacity = 0.5;
$('a[rel*=facebox]').facebox();
$.facebox('<img src="b112.jpg" alt="" height="550px" width="800px" title="" style="max-width: none !important;" />');

})
</script>
<!--pop up-->

<div class="clearfix"></div>

<section class="main-ctn">
	<div class="container">
		<div class="row">

			<div class="col-md-8">

				<div class="slideshow">
					<div class="tp-banner-container">
						<div class="tp-banner">
							<ul>
								<li data-transition="random" data-slotamount="7" data-masterspeed="1500">
									<!-- MAIN IMAGE -->
									<img src="<?php echo base_url(); ?>application/libraries/front/images/slideshow.png" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

									<div class="slideshow-bg"
										 data-y="310"
										 data-x="center"
										 data-start="0"></div>
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="slide-h2 tp-caption randomrotate skewtoleft tp-resizeme start white"
										 data-y="160"
										 data-x="center"
										 data-hoffset="0"
										 data-start="300"
										 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										 data-speed="500"
										 data-easing="Power3.easeInOut"
										 data-endspeed="300"
										 style="z-index: 2"><h2>Believe in yourself</h2>
									</div>

									<!-- LAYER NR. 2 -->
									<div class="slide-h3 tp-caption customin white"
										 data-y="230"
										 data-x="center"
										 data-hoffset="0"
										 data-start="600"
										 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										 data-speed="500"
										 data-easing="Power3.easeInOut"
										 data-endspeed="300"
										 style="z-index: 2"><h2>"Opportunities don't happen. You create them."</h2>
									</div>

									<!-- LAYER NR. 3 -->




								</li>
								<li data-transition="random" data-slotamount="7" data-masterspeed="1000">
									<!-- MAIN IMAGE -->
									<img src="<?php echo base_url(); ?>application/libraries/front/images/slideshow-2.png" alt="" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

									<div class="slideshow-bg"
										 data-y="310"
										 data-x="center"
										 data-start="0"></div>
									<!-- LAYERS -->
									<div class="slide-h2 tp-caption randomrotate skewtoleft tp-resizeme start white"
										 data-y="160"
										 data-x="center"
										 data-hoffset="0"
										 data-start="300"
										 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										 data-speed="500"
										 data-easing="Power3.easeInOut"
										 data-endspeed="300"
										 style="z-index: 2"><h2>Hope, Action, Change</h2>
									</div>

									<!-- LAYER NR. 1 -->
									<div class="slide-h3 tp-caption customin white"
										 data-y="230"
										 data-x="center"
										 data-hoffset="0"
										 data-start="600"
										 data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										 data-speed="500"
										 data-easing="Power3.easeInOut"
										 data-endspeed="300"
										 style="z-index: 2"><h2>“Mistakes are the growing pains of wisdom.”</h2>
									</div>


								</li>


								<!-- SLIDE  -->

							</ul>
						</div>
					</div><!-- End tp-banner-container -->
				</div><!-- End slideshow -->

			</div>
			<div class="col-md-4">
				<div class="logininfo">


					<div class="comment-form">
						<?= form_open('auth/login',array("class" => "form-js", "id" => "signupForm")); ?>
						<div class="sections-title-h3"><h3 style="color:#fff;"><center>Login Here!</center></h3></div>
						<input type="hidden" name="ddtype" value="2">
						<div class="row">
							<div class="col-md-12">
								<div class="form-input">
									<i class="fa fa-user"></i>
									<input name="txtlogin" id="txtlogin" type="text" placeholder="User Name">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-input">
									<i class="fa fa-key"></i>
									<input name="txtpwd" id="txtpwd" type="password" placeholder="Password">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-input">
									<a href="javascript:;" onclick="get_form(1)"><p style="color:#fff;">Forget Password?</p></a>
								</div>
							</div>
							<div class="col-md-12">
								<input type="submit" class="button-3" value="Submit">
							</div>
						</div>
						</form>

					<?= form_open('auth/resetpassword',array("class" => "form-js", "id" => "forgetForm" ,"style" => "display:none;")); ?>
					<div class="sections-title-h3"><h3 style="color:#fff;"><center>Forget Password!</center></h3></div>
					<input type="hidden" name="ddtype" value="2">
					<div class="row">
						<div class="col-md-12">
							<div class="form-input">
								<i class="fa fa-user"></i>
								<input name="txtuserid" id="txtuserid" type="text" placeholder="Enter Userid">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-input">
								<a href="javascript:;" onclick="get_form(2)"><p style="color:#fff;">Login Here</p></a>
							</div>
						</div>
						<div class="col-md-12">
							<input type="submit" class="button-3" value="Submit">
						</div>
					</div>
					</form>
				<script>
					function get_form(id)
					{
						if(id == 1)
						{
							$("#forgetForm").show();
							$("#signupForm").hide();
						}
						else
						{
							$("#forgetForm").hide();
							$("#signupForm").show();
						}
					}
				</script>
			</div>

		</div>

	</div>

	</div>
</div>
</section>


<div class="sections">
	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<div class="team">
					<div class="sections-title-h3"><center><h3>Our Team</h3></center></div>

					<marquee behavior="scroll" direction="up" scrollamount="3" onmouseover="this.stop();" height="280" onmouseout="this.start();" >

						<ul>

							<li><img src="<?php echo base_url(); ?>application/libraries/front/images/marketing-head.jpg" class="img-responsive center-block">

								<h4>Marketing Head</h4>
							</li>
							<li><img src="<?php echo base_url(); ?>application/libraries/front/images/team-head.jpg" class="img-responsive center-block">

								<h4>Team Manager</h4>

							</li>

						</ul>
					</marquee>
				</div>
			</div>

			<div class="col-md-9">

				<div class="sections-title">
					<div class="sections-title-h3"><h3>Assvi Group Sales & Marketing Pvt. Ltd.</h3></div>
					<p>Assvi Group is the India's largest and fastest-growing business community & we've been home to a new breed of happy and successful people of all ages, races, gender and socioeconomic status. It is a business process management service, uses process to help its client's power intelligence across their enterprise to run smarter operations, make smarter decisions and use smarter technology.We offer a fully supported entrepreneurial opportunity to promote the business and product portfolio to others. With or without availing of the business opportunity, Assvi Group Sales & Marketing Pvt. Ltdretails a variety of products that enhanse the everyday lives of our customers around the India. More than seven decades of a strong, customer-focused approach and the continuous quest for world-class quality have enabled it to attain and sustain leadership in all its major lines of business. The company's business are supported by a wide marketing and distribution network, and have established a reputation for strong customer support. A commitment to community welfare and environmental protection are an integral part of the corporate vision.</p>

				</div><!-- End sections-title -->

			</div>
		</div>
		<!-- End row -->
	</div><!-- End container -->
</div><!-- End sections -->

<!-- End sections -->

<div class="sections sections-padding-b-50">
	<div class="container">
		<div class="sections-title">
			<div class="sections-title-h3"><h3>Take your business to next level</h3></div>
			<p>
				To build a global enterprise for all our Partners and great future for our country. We are inspired to be a respected global entrepreneur, through the power of Positive Action and to give millions of Indians the power to shape their destiny, the means to realize their full potential.</p>
		</div><!-- End sections-title -->
		<div class="row">
			<div class="col-md-6">
				<div class="accordion">
					<div class="section-content">
						<h4 class="accordion-title active"><a href="#">Our vision <i class="fa fa-plus"></i></a></h4>
						<div class="accordion-inner active">
							Assvi Group Sales & Marketing Pvt. Ltd. was founded on a simple but incredibly powerful vision for the future of the people. To be the Top Performing and Most Admired Refinery in Asia. A carefully crafted vision statement can help you communicate our company's goals to employees and management in a single sentence or a few concise paragraphs.
						</div>
					</div>
					<div class="section-content">
						<h4 class="accordion-title"><a href="#">Our Mission <i class="fa fa-minus"></i></a></h4>
						<div class="accordion-inner">
							Our mission is to drive growth profitably through innovative business development processes.Responsible towards our customers by providing them high quality products & services.

							Providing the best marketing plan & training programs for our business partners.

							Creating a conducive environment for our staff to unleash their leadership potential to the fullest.
						</div>
					</div>
					<div class="section-content">
						<h4 class="accordion-title"><a href="#">Our Quality<i class="fa fa-minus"></i></a></h4>
						<div class="accordion-inner">
							Assvi Group Sales & Marketing Pvt. Ltd.  India Private Limited consistently offers wide range of quality products and outstanding services and gives you an opportunity to become the owner of own business and leader of your team.
						</div>
					</div>
				</div><!-- End accordion -->
			</div>
			<div class="col-md-6">
				<img src="<?php echo base_url(); ?>application/libraries/front/images/grow-your-business.png" class="img-responsive img-thumbnail"><!-- End progressbar-warp -->
			</div>
		</div><!-- End row -->
	</div><!-- End container -->
</div><!-- End sections -->


<!-- End footer -->
<footer id="footer-bottom">
	<div class="container">
		<div class="copyrights">Copyright 2017 Assvi Group Sales & Marketing Pvt. Ltd </div>
		<nav class="navigation-footer">
			<ul>
				<li><a href="index.html">Home</a></li>
				<li><a href="javascript:void(0);">About us</a></li>
				<li><a href="javascript:void(0);">Business Plan</a></li>
				<li><a href="javascript:void(0);">Gallery</a></li>
				<li><a href="javascript:void(0);">Career</a></li>
				<li><a href="javascript:void(0);">Legal</a></li>
				<li><a href="javascript:void(0);">Contact</a></li>
			</ul>
		</nav>
	</div><!-- End container -->
</footer><!-- End footer-bottom -->

</div><!-- End wrap -->

<div class="go-up"><i class="fa fa-chevron-up"></i></div>

<!-- js -->
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/html5.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.isotope.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.appear.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/count-to.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/twitter/jquery.tweet.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.inview.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.bxslider.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.themepunch.plugins.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/front/js/custom.js"></script>
<!-- End js -->

</body>


</html>