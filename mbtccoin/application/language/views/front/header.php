<!DOCTYPE html>
<html lang="en"> 
<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>Assvi Group Sales & Marketing Pvt. Ltd</title>
	<meta name="description" content="Assvi Group Sales & Marketing Pvt. Ltd">
	<meta name="author" content="2code.info">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/libraries/front/style.css">
	
	<!-- Skins -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/libraries/front/css/skins/skins.css">
	
	<!-- Responsive Style -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/libraries/front/css/responsive.css">
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>application/libraries/front/images/fevi.png">
    
    <script type="text/javascript">
tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

function GetClock(){
var d=new Date();
var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getYear();
if(nyear<1000) nyear+=1900;
var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

if(nhour==0){ap=" AM";nhour=12;}
else if(nhour<12){ap=" AM";}
else if(nhour==12){ap=" PM";}
else if(nhour>12){ap=" PM";nhour-=12;}

if(nmin<=9) nmin="0"+nmin;
if(nsec<=9) nsec="0"+nsec;

document.getElementById('clockbox').innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
}

window.onload=function(){
GetClock();
setInterval(GetClock,1000);
}
</script>
  
</head>
<body class="body-boxed">

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200 boxed">
	
	<header id="header-top">
		<div class="container clearfix">
			<div class="row">
				<div class="col-md-9">
					<div class="phone-email"><i class="fa fa-phone"></i>0522 40 74 642</div>
					<div class="phone-email phone-email-2"><i class="fa fa-envelope"></i>info@assvigroup.com</div>
                    	<div class="phone-email phone-email-2" id="clockbox"> <i class="fa fa-envelope"></i>7oroof@7oroof.com</div>
				</div>
				<div class="col-md-3">
					<div class="social-ul">
						<ul>
                       
							<li class="social-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
							<li class="social-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
							<li class="social-google"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                            	
							
						</ul>
					</div>
				</div>
			</div>
		</div><!-- End container -->
	</header><!-- End header -->
	<header id="header">
		<div class="container clearfix">
			<div class="logo"><a href=""><img alt="logo" src="<?php echo base_url(); ?>application/libraries/front/images/logo.png"></a></div>
			
			<nav class="navigation">
				<ul>
					<li class="current_page_item"><a href="<?= base_url(); ?>">Home</a></li>
					
					<li><a href="javascript:void(0);">About us</a>
						<ul>
                        	<li><a href="javascript:void(0);">About Company</a></li>
							<li><a href="javascript:void(0);">Vision &amp; Mission</a></li>
							<li><a href="<?= base_url(); ?>welcome/cmd">Message</a></li>
						</ul>
					</li>
                    <li><a href="javascript:void(0);">Products</a></li>
                     <li><a href="javascript:void(0);">Incentive Plan</a></li>
                     <li><a href="javascript:void(0);">Gallery</a>
                     	<ul>
                        	<li><a href="javascript:void(0);">Photo Gallery</a></li>
							<li><a href="javascript:void(0);">Press Release</a></li>
							<li><a href="javascript:void(0);">Videos</a></li>
                            <li><a href="javascript:void(0);">Achievement</a></li>
						</ul>
					</li>
					  <li><a href="javascript:void(0);">Download</a>
						<ul>
							<li><a href="javascript:void(0);">Brochure</a></li>
							<li><a href="javascript:void(0);">Form</a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0);">Legal</a><ul>
                    <li><a href="javascript:void(0);">Our Certificate</a></li>
							<li><a href="javascript:void(0);">Legal Advisor</a></li>
							<li><a href="<?php echo base_url(); ?>welcome/faq">FAQ</a></li>
                            <li><a href="<?php echo base_url(); ?>welcome/term">Term & Condition</a></li>
						</ul>
						
					</li>
                          <li><a href="javascript:void(0);">Contact</a>
                          
                          	<ul>
                        	<li><a href="javascript:void(0);">Feedback</a></li>
							
							
							
						</ul>
						
					</li>
					
					
				</ul>
			</nav><!-- End navigation -->
		</div><!-- End container -->
	</header><!-- End header -->