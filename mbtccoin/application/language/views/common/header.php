<!DOCTYPE html>
<html>
      <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="<?php echo DESCRIPTION; ?>">
            <meta name="author" content="<?php echo SITE_NAME; ?>">

            <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon/fevi.png">

            <title><?php echo SITE_NAME; ?></title>

            <link rel="stylesheet" href="<?php echo base_url(); ?>application/libraries/assets/plugins/morris/morris.css">

            <link href="<?php echo base_url(); ?>application/libraries/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url(); ?>application/libraries/assets/css/core.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url(); ?>application/libraries/assets/css/components.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url(); ?>application/libraries/assets/css/icons.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url(); ?>application/libraries/assets/css/pages.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url(); ?>application/libraries/assets/css/menu.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url(); ?>application/libraries/assets/css/responsive.css" rel="stylesheet" type="text/css" />
            <?php
            if($this->router->fetch_method()!='dashboard')
            {
            ?>
            <!-- Custom Data Table Js -->
            <link href="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>


            <!-- Custom Date Picker Js -->
            <link href="<?php echo base_url(); ?>application/libraries/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

            <!-- Custom box css -->
            <link href="<?php echo base_url(); ?>application/libraries/assets/plugins/custombox/css/custombox.css" rel="stylesheet">
            <!--form wysiwig-->
            <script src="<?php echo base_url(); ?>application/libraries/assets/plugins/tinymce/tinymce.min.js"></script>

            <?php
            }
            ?>
            <script src="<?php echo base_url(); ?>application/libraries/assets/js/modernizr.min.js"></script>
            <script src="<?php echo base_url(); ?>application/libraries/assets/js/jquery.min.js"></script>

      </head>	