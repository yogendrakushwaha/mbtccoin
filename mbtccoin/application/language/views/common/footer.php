<!-- Footer -->
<footer class="footer text-center">
	<div class="container">
		<div class="row">
			<center>
				<?php echo SITE_NAME; ?> © <?php echo date('Y'); ?>. All rights reserved.
			</center>
		</div>
	</div>
</footer>
<!-- End Footer -->

</div>
</div>

<!--User Defined jQuery  -->
<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" />
<input type="hidden" id="txtmethod" value="<?php echo $this->router->fetch_method(); ?>" />
<input type="hidden" id="txtclass" value="<?php echo $this->router->fetch_class(); ?>" />

<!--Common jQuery  -->

<script src="<?php echo base_url(); ?>application/libraries/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/js/detect.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/js/fastclick.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/peity/jquery.peity.min.js"></script>

<?php
if($this->router->fetch_method()=='dashboard')
{
?>
<!-- jQuery  -->
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/counterup/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/raphael/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/jquery-knob/jquery.knob.js"></script>

<script src="<?php echo base_url(); ?>application/libraries/assets/pages/jquery.dashboard.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		
		$(".knob").knob();
		
	});
</script>
<?php
}
else
{
?>
<!-- Data Table plugins js -->
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/datatables/dataTables.colVis.js"></script>

<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>

<!-- Date picker plugins js -->
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- Modal-Effect -->
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/plugins/custombox/js/legacy.min.js"></script>


<script src="<?php echo base_url(); ?>application/libraries/assets/pages/datatables.init.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/pages/jquery.form-pickers.init.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#datatable').dataTable();
	});
	TableManageButtons.init();
</script>

<?php
}
?>
<script src="<?php echo base_url(); ?>application/third_party/js/check.js"></script>
<script src="<?php echo base_url(); ?>application/third_party/js/bootbox.js"></script>
<script src="<?php echo base_url() ?>application/third_party/js/baseUrl.js"></script>
<script src="<?php echo base_url(); ?>application/third_party/js/registration.js"></script>
<script src="<?php echo base_url(); ?>application/third_party/js/custom.js"></script>
<script src="<?php echo base_url(); ?>application/third_party/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/js/jquery.core.js"></script>
<script src="<?php echo base_url(); ?>application/libraries/assets/js/jquery.app.js"></script>
<?php
	if($this->router->fetch_class()=="pin")
	{
	?>
	<script src="<?php echo base_url(); ?>application/third_party/js/pin_js.js"></script>
	<?php
	}
?>

</body>
</html>