<body>
	<!-- Navigation Bar-->
	<header id="topnav">
		<div class="topbar-main">
			<div class="container">

				<!-- Logo container-->
				<div class="logo">
					<a href="javascript:;" class="logo"><span><img style="height:40px; margin: 8px;" src="<?php echo base_url(); ?>logo/logologin.png" class="img-responsive" /></span></a>
				</div>
				<!-- End Logo container-->
				<div class="menu-item">
					<!-- Mobile menu toggle-->
					<a class="navbar-toggle">
						<div class="lines">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
					<!-- End mobile menu toggle-->
				</div>

			</div>
		</div>

		<div class="navbar-custom">
			<div class="container">
				<div id="navigation">
					<!-- Navigation Menu-->
					<ul class="navigation-menu">
						<li>
							<a href="<?php echo base_url(); ?>master/index"><i class="md md-dashboard"></i>Dashboard</a>
						</li>

						<li class="has-submenu">
							<a href="#"><i class="md-assignment-returned"></i>Master</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>master/view_soft_login"><i class="ti-star"></i> Manage Login</a></li>
								<li><a href="<?php echo base_url() ?>master/view_soft_setting"><i class="ti-settings"></i> Manage Configuration</a></li>
								<li><a href="<?php echo base_url() ?>master/view_news"><i class="ti-bell"></i> Manage News</a></li>
								<li><a href="<?php echo base_url() ?>master/view_bank"><i class="md-account-balance"></i> Manage Bank</a></li>
								<li><a href="<?php echo base_url() ?>master/view_send_sms"><i class="md-account-balance"></i> Sms Campaigning</a></li>
								<!--li><a href="<?php echo base_url() ?>master/view_tools"><i class="fa fa-cog"></i> Manage Tools</a></li-->
							</ul>
						</li>

						<?php if(FUND==1) { ?>
						<li class="has-submenu">
							<a href="#"><i class="fa fa-th-list"></i>Manage</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>member/view_fund_request"><i class="fa fa-money"></i> Fund Request</a></li>
							</ul>
						</li>
						<?php } ?>

						<?php if(PIN_SYSTEM==1) { ?>
						<li class="has-submenu">
							<a href="#"><i class="ti-save-alt"></i>Pin Management</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>pin/generate_pin"><i class="ti-server"></i> Genrate Pin</a></li>
								<li><a href="<?php echo base_url() ?>pin/PinTransfer"><i class="ti-shift-right"></i> Pin Transfer</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_cancel_pin"><i class="fa fa-pause-circle-o"></i> Cancel Pin</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_pin_reoprt"><i class="fa fa-bars"></i> Pin Report</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_member_pin_details"><i class="fa fa-sliders"></i> Pin Report Member</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_current_pin_position"><i class="fa fa-tasks"></i> Current Pin Position</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_atransfer_pin_report"><i class="fa fa-align-justify"></i> Pin Transfer Report</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_pin_request"><i class="fa fa-align-center"></i> Pin Request Report</a></li>
							</ul>
						</li>
						<?php } ?>

						<li><a href="<?php echo base_url() ?>member/join_member"><i class="fa fa-plus-square"></i> Registration</a></li>

						<li class="has-submenu">
							<a href="#"><i class="ion-android-social"></i>Member</a>
							<ul class="submenu">
								<?php if(TOPUP==1) { ?>
								<li><a href="<?php echo base_url() ?>member/view_member_topup"><i class="ion-flag"></i> Member Topup</a></li>
								<?php } ?>
								<li><a href="<?php echo base_url() ?>member/view_member_edit"><i class="fa fa-edit"></i> Edit Member Detail</a></li>
								<li><a href="<?php echo base_url() ?>member/view_member_details"><i class="fa fa-th-list"></i> View Member Detail</a></li>
								<li><a href="<?php echo base_url() ?>member/view_all_member"><i class="fa fa-users"></i> View Member</a></li>
								<li><a href="<?php echo base_url() ?>member/view_activate_members"><i class="fa fa-thumbs-up"></i> Activate Member</a></li>
								<li><a href="<?php echo base_url() ?>member/view_deactivate_members"><i class="fa fa-thumbs-o-down"></i> DeActivate Member</a></li>
								<li><a href="<?php echo base_url() ?>member/view_bank_details"><i class="md-account-balance"></i> View Bank Details</a></li>
							</ul>
						</li>

						<li>
							<a href="<?php echo base_url() ?>member/change_password"><i class="ion-locked"></i>Password</a>
						</li>

						<li class="has-submenu">
							<a href="#"><i class="fa fa-slideshare"></i>Recent Partners</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>member/view_direct_referal"><i class="ti-direction"></i> Direct Referal</a></li>
								<li><a href="<?php echo base_url() ?>member/view_user_downline"><i class="fa fa-angle-double-down"></i> Downline</a></li>
								<li><a href="<?php echo base_url() ?>member/tree/1"><i class="fa fa-sitemap"></i> Tree View</a></li>
							</ul>
						</li>


						<?php if(CLOSING==1) { ?>
						<li class="has-submenu">
							<a href="#"><i class="ti-wallet"></i>Payout Details</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>admin_closing/view_closing_date"><i class="ion-ios7-calendar-outline"></i> Closing Date</a></li>
								<li><a href="<?php echo base_url() ?>admin_closing/view_generate_payout"><i class="ion-ios7-bolt"></i> Generate Payout</a></li>
								<li><a href="<?php echo base_url() ?>admin_closing/view_payout_report"><i class="ion-ios7-paper"></i> Payout Report</a></li>
								<li><a href="<?php echo base_url() ?>admin_closing/view_direct_report"><i class="ion-grid"></i> Direct Income Report</a></li>
								<li><a href="<?php echo base_url() ?>admin_closing/view_payout_publish"><i class="ion-ios7-keypad-outline"></i> Payout Publish</a></li>
								<li><a href="<?php echo base_url() ?>admin_closing/view_payment_release"><i class="ion-navicon"></i> Payment Release</a></li>
								<li><a href="<?php echo base_url() ?>member/view_ledger_report"><i class="ti-bar-chart-alt"></i> Payout Statement</a></li>
							</ul>
						</li>
						<?php } ?>

						<li>
							<a href="<?php echo base_url() ?>member/view_admin_reply"><i class="md md-markunread"></i>User Query</a>
						</li>

						<li>
							<a href="<?php echo base_url('auth/logout') ?>"><i class="ion-log-out"></i> Logout</a>
						</li>

					</ul>
					<!-- End navigation menu -->
				</div>
			</div> <!-- end container -->
		</div> <!-- end navbar-custom -->
	</header>
	<!-- End Navigation Bar-->		