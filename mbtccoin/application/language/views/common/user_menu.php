<body>
	<!-- Navigation Bar-->
	<header id="topnav">
		<div class="topbar-main">
			<div class="container">

				<!-- Logo container-->
				<div class="logo">
					<a href="javascript:;" class="logo"><span><img style="height:40px; margin: 8px;" src="<?php echo base_url(); ?>logo/logologin.png" class="img-responsive" /></span></a>
				</div>
				<!-- End Logo container-->

				<div class="menu-item">
					<!-- Mobile menu toggle-->
					<a class="navbar-toggle">
						<div class="lines">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
					<!-- End mobile menu toggle-->
				</div>

				<!--div class="menu-extras">

<ul class="nav navbar-nav navbar-right pull-right">

<li class="dropdown navbar-c-items">
<a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo base_url(); ?>application/libraries/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
<ul class="dropdown-menu">
<li><a href="javascript:void(0)"><i class="ion-android-contact"></i> <?php echo $this->session->userdata('name'); ?></a></li>
<li><a href="javascript:void(0)"><i class="ion-android-social-user"></i> <?php echo $this->session->userdata('user_id'); ?></a></li>
<li class="divider"></li>
<li><a href="<?php echo base_url(); ?>logout"><i class="ion-log-out"></i> Logout</a></li>
</ul>
</li>
</ul>

</div-->

			</div>
		</div>

		<div class="navbar-custom">
			<div class="container">
				<div id="navigation">
					<!-- Navigation Menu-->
					<ul class="navigation-menu">
						<li>
							<a href="<?php echo base_url(); ?>userprofile/index"><i class="md md-dashboard"></i> Dashboard</a>
						</li>

						<li><a href="<?php echo base_url() ?>userprofile/join_member"><i class="fa fa-plus-square"></i> Registration</a></li>

						<li class="has-submenu">
							<a href="#"><i class="ion-android-social"></i> Member</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>userprofile/view_member_details"><i class="fa fa-users"></i> View Profile</a></li>
								<li><a href="<?php echo base_url() ?>userprofile/view_member_edit"><i class="fa fa-edit"></i> Edit Profile</a></li>
								<li><a href="<?php echo base_url() ?>userprofile/view_bank_details"><i class="md-account-balance"></i> Bank details</a></li>
							</ul>
						</li>

						<?php if(FUND==1) { ?>
						<li class="has-submenu">
							<a href="#"><i class="fa fa-th-list"></i> Manage</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url(); ?>User_action/view_fund_request"><i class="fa fa-money"></i> Fund Request</a></li>
								<li><a href="<?php echo base_url(); ?>User_action/view_fund_transfer"><i class="ion-share"></i> Fund Transfer</a></li>
							</ul>
						</li>
						<?php } ?>

						<?php if(TOPUP == 1) { ?>
						<li class="has-submenu">
							<a href="#"><i class="menu-icon md-assignment-return"></i> Topup</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>User_action/view_member_topup"><i class="menu-icon md-assignment-ind"></i> Member Topup</a></li>
							</ul>
						</li>
						<?php } ?>

						<li class="has-submenu">
							<a href="#"><i class="typcn typcn-device-laptop"></i> Member Doccument</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url(); ?>userprofile/member_welcome_letter"><i class="fa fa-file-text-o"></i> Welcome Letter</a></li>
								<li><a href="<?php echo base_url(); ?>userprofile/member_welcome_invoice"><i class="fa fa-book"></i> Invoice</a></li>
							</ul>
						</li>

						<li>
							<a href="<?php echo base_url() ?>userprofile/change_password"><i class="ion-locked"></i> Password</a>
						</li>

						<li class="has-submenu">
							<a href="#"><i class="fa fa-slideshare"></i> My Team</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>userprofile/view_direct_referal"><i class="ti-direction"></i> Direct Referal</a></li>
								<li><a href="<?php echo base_url() ?>userprofile/view_user_downline"><i class="fa fa-angle-double-down"></i> Downline</a></li>
								<li><a href="<?php echo base_url() ?>userprofile/tree/1"><i class="fa fa-sitemap"></i> Tree View</a></li>
							</ul>
						</li>



						<li class="has-submenu">
							<a href="#"><i class="ion-xbox"></i>Pin Managment</a>
							<ul class="submenu">

								<li><a href="<?php echo base_url() ?>pin/view_pin_bank/1"><i class="ion-grid"></i> Pin Bank</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_pin_transfer/1"><i class="ion-arrow-graph-up-right"></i> Pin Transfer</a></li>
								<li><a href="<?php echo base_url() ?>pin/view_transfer_pin_report/1"><i class="ion-arrow-graph-up-right"></i> Transfer Pin Report</a></li>
							</ul>
						</li>
						<?php if(CLOSING==1) { ?>
						<li class="has-submenu">
							<a href="#"><i class="ti-wallet"></i> Payout Details</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>user_action/view_direct_report"><i class="ion-grid"></i> Direct Income Report</a></li>
								<li><a href="<?php echo base_url() ?>user_action/view_payout_report"><i class="ion-arrow-graph-up-right"></i> Matching Payout Report</a></li>
								<li><a href="<?php echo base_url() ?>userprofile/view_ledger_report"><i class="ti-bar-chart-alt"></i> Payout Statement</a></li>
							</ul>
						</li>
						<?php } ?>

						<li class="has-submenu">
							<a href="#"><i class="md md-gamepad"></i> Extra</a>
							<ul class="submenu">
								<li><a href="<?php echo base_url() ?>userprofile/view_query_form"><i class="md md-question-answer"></i> Submit Query</a></li>
								<li><a href="<?php echo base_url() ?>userprofile/view_all_news"><i class="ion-ios7-bell-outline"></i> View News</a></li>
							</ul>
						</li>
						<li>
							<a href="<?php echo base_url('auth/logout') ?>"><i class="ion-log-out"></i> Logout</a>
						</li>
					</ul>
					<!-- End navigation menu -->
				</div>
			</div> <!-- end container -->
		</div> <!-- end navbar-custom -->



	</header>
	<!-- End Navigation Bar-->	