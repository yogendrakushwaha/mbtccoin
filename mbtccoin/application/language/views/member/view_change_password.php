<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('member/change_password',array("class" => "cmxform horizontal-form", "id" => "signupForm")); ?>
						
						<!--/row-->
						<div class="row">
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Login Id<span class="required"> * </span></label>
									<input type="text" id="txtlogin" name="txtlogin"  class="form-control input-inline input-medium" placeholder="Enter login id.">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Mobile No</label>
									<input type="text" id="txtmob" name="txtmob"  class="form-control input-inline input-medium" placeholder="Enter Mobile No.">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Email</label>
									<input type="text" id="txtemail" name="txtemail"  class="form-control input-inline input-medium" placeholder="Enter Email."> 
								</div>
							</div>
							<!--/span-->
						</div>
						
						<div class="row">
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">From Joining Date<span class="required"> * </span></label>
									<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control" name="start" />
										<span class="input-group-addon bg-custom b-0 text-white">to</span>
										<input type="text" class="form-control" name="end"  />
									</div>
								</div>
							</div>
							<!--/span-->
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="submit">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Login Id</th>
								<th>Associate Name</th>
								<th>Password</th>
								<!--th>Transaction Password</th-->
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="userid">
							<?php
								$sn=1;
								foreach($rid->result() as $rows)
								{?>
								
								<tr>
									
									<td><?php echo $sn;?></td>
									<td><?php echo $rows->Login_Id;?></td>
									<td><?php echo $rows->Associate_Name;?></td>
									<td><?php echo $rows->Password;?></td>
									<!--td><?php echo $rows->Pin_Password;?></td-->
									<td>
										<a href="#custom-modal" onclick="change_pwd(<?php echo $rows->regid; ?>,1)" class="waves-effect waves-light" data-animation="sign" data-plugin="custommodal"  data-overlaySpeed="100" data-overlayColor="#36404a">Password</a>
										<!--a href="#custom-modal" onclick="change_pwd(<?php echo $rows->regid; ?>,2)" class="waves-effect waves-light" data-animation="sign" data-plugin="custommodal" data-overlaySpeed="100" data-overlayColor="#36404a">Pin Password</a-->
									</td>
								</tr>   
							<?php $sn++; }?>                   
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="custom-modal" class="modal-demo">
		<button type="button" class="close" onclick="Custombox.close();">
			<span>&times;</span><span class="sr-only">Close</span>
		</button>
		<h4 class="custom-modal-title" id="headtxt">Change Password</h4>
		<div class="custom-modal-text">
			<div class="form">
				<?= form_open('',array("class" => "form-horizontal", "id" => "signupForm1")); ?>
				
				<div class="form-group">
					<label class="col-md-4 control-label" id="chg_name">Password</label>
					<div class="col-md-6">
						<input type="password" id="txtnew" name="txtnew" class="form-control empty" placeholder="Enter New Password">
					</div>
				</div>
				
				
				<div class="form-group">
					<div class="col-md-offset-2 col-md-8">
						<button class="btn btn-primary" type="submit" onclick="return check('signupForm')">Submit</button>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>