<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('member/view_all_member',array("class" => "cmxform horizontal-form", "id" => "signupForm")); ?>
						
						<!--/row-->
						<div class="row">
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Login Id<span class="required"> * </span></label>
									<input type="text" id="txtlogin" name="txtlogin"  class="form-control input-inline input-medium" placeholder="Enter login id.">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Mobile No</label>
									<input type="text" id="txtmob" name="txtmob"  class="form-control input-inline input-medium" placeholder="Enter Mobile No.">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Email</label>
									<input type="text" id="txtemail" name="txtemail"  class="form-control input-inline input-medium" placeholder="Enter Email."> 
								</div>
							</div>
							<!--/span-->
						</div>
						
						<div class="row">
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">From Joining Date<span class="required"> * </span></label>
									<div class="input-daterange input-group" id="date-range" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control" name="start" />
										<span class="input-group-addon bg-custom b-0 text-white">to</span>
										<input type="text" class="form-control" name="end"  />
									</div>
								</div>
							</div>
							<!--/span-->
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="submit">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Login Id</th>
								<th>Associate</th>
								<th>Joining Date</th>
								<th>City</th>
								<th>Mobile No</th>
								<th>Email</th>
								<th>Password</th>
								<th>Transaction Password</th>
								<th>Bank</th>
								<th>Branch</th>
								<th>IFSCCode</th>
								<th>AccNo</th>
								<th>Pancard</th>
							</tr>
						</thead>
						<tbody id="userid">
							<?php
								$sn=1;
								foreach($rid->result() as $rows)
								{?>								
								<tr>									
									<td><?php echo $sn;?></td>
									<td><?php echo $rows->Login_Id;?></td>
									<td><?php echo $rows->Associate_Name;?></td>
									<td><?php echo date('d-m-y h:i:s',strtotime($rows->Joining_Date));?></td>
									<td><?php echo $rows->City;?></td>
									<td><?php echo $rows->Mobile_No;?></td>
									<td><?php echo $rows->Email;?></td>
									<td><?php echo $rows->Password;?></td>
									<td><?php echo $rows->Pin_Password;?></td>
									<td><?php echo $rows->Bankname;?></td>
									<td><?php echo $rows->Branch;?></td>
									<td><?php echo $rows->Ifsc;?></td>
									<td><?php echo $rows->AcNo;?></td>
									<td><?php echo $rows->Pancard;?></td>
								</tr>   
							<?php $sn++; }?>                   
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>