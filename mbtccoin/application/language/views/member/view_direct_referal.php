<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('member/view_direct_referal',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Login Id <span class="required"> * </span></label>
							<div class="col-md-6">
								<input type="text" id="txtuserid" name="txtuserid"  class="form-control input-inline input-medium" placeholder="Enter login id.">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="submit">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Login Id</th>
								<th>Login Name</th>
								<th>Joining Date</th>
								<th>Position</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($rec))
								{
									$sn=1;
									foreach($rec->result() as $rows)
									{
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td><?php echo $rows->Login_Id;?></td>
										<td><?php echo $rows->Associate_Name;?></td>
										<td><?php echo date('d-m-y h:i:s',strtotime($rows->Joining_Date_1));?></td>
										<td><?php echo $rows->USER_POSITION_DESC;?></td>
									</tr>   
									<?php 
										$sn++;
									}
								}
							?>                    
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>