<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<form action="view_member_edit" method="post" class="cmxform horizontal-form" >
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
						<!--/row-->
						<div class="row">
							<!--/span-->
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Login Id<span class="required"> * </span></label>
									<input type="text" id="txtlogin" name="txtlogin" value="<?php echo ($uid=='')?'':$uid; ?>" class="form-control input-inline" placeholder="Enter login id.">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label"></label><br>
									<button class="btn btn-primary btn-sm input-inline" type="submit">Submit</button>
									<button type="button" class="btn btn-default btn-sm">Cancel</button>
								</div>
							</div>
						</div>
						
						</form>
					</div>
				</div>
			</div>
			<?php
			if(!empty($rec))
			{
			?>
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('member/update_member',array("class" => "cmxform form-horizontal", "id" => "signupForm", "enctype" => "multipart/form-data")); ?>
						
						
						<div class="row">
							
							<h3 class="form-section">Personal Details</h3>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Applicant Name <span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control empty"  name="txtassociate_name" value="<?php echo $rec->Associate_Name; ?>" id="txtassociate_name">
									<span id="divtxtassociate_name" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Father/Husband's Name</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtparent" id="txtparent" value="<?php echo $rec->Parent_Name; ?>">
									<span id="divtxtparent" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Gender</label>
								<div class="col-md-5">
									<div class="radio-list">
										<label class="radio-inline">
										<input type="radio" name="rbgender" id="rbgender1" <?php echo ($rec->Gender=='1')?'selected':''; ?> value="1">Male</label>
										<label class="radio-inline">
										<input type="radio" name="rbgender" id="rbgender2" <?php echo ($rec->Gender=='0')?'selected':''; ?> value="0">Female</label>
									</div>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Date of Birth</label>
								<div class="col-md-5">
									<input name="txtdob" id="datepicker" class="form-control empty" value="<?php echo $rec->DOB; ?>" data-date-end-date="+0d" data-date-format="yyyy-mm-dd" type="text"/>
									<span id="divdatepicker" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Profile Pic</label>
								<div class="col-md-5">
									<input type="hidden" name="txtpic" id="txtpic" value="<?php echo $rec->User_image; ?>" >
                                    <input type="file" class="filestyle" data-size="sm" name="userfile" id="userfile" accept="image/*">
								</div>
							</div>
							
							
							<h3 class="form-section">Contact Details</h3>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Mobile Number<span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtmobile" id="txtmobile" maxlength="10" value="<?php echo $rec->Mobile_No; ?>">
									<span id="divtxtmobile" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Email Address<span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtemail" id="txtemail" value="<?php echo $rec->Email; ?>">
									<span id="divtxtemail" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">State<span class="required"> * </span></label>
								<div class="col-md-5">
									<select id="ddstate" name="ddstate" class="form-control opt" onChange="get_city()">
										<option selected="selected" value="-1">Select State</option>
										<?php foreach($state->result() as $row)
											{
												if($rec->STATE_ID==$row->m_loc_id)
												{
												?>
												<option value="<?php echo $row->m_loc_id;?>" selected><?php echo $row->m_loc_name;?></option>
												<?php 
												}
												else
												{
												?>
												<option value="<?php echo $row->m_loc_id;?>"><?php echo $row->m_loc_name;?></option>
												<?php 
												}
											}
										?>
									</select>
									<span id="divddstate" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">City<span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="hidden" value="<?php echo $rec->City_ID; ?>" name="hdocity" id="hdocity" />
									<select id="ddcity" name="ddcity" class="form-control opt">
										<option selected="selected" value="">Select City</option>
									</select>
									<span id="divddcity" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Pincode</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtpincode" id="txtpincode" maxlength="6" value="<?php echo $rec->Pincode; ?>">
									<span id="divtxtpincode" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Address</label>
								<div class="col-md-5">
									<textarea class="form-control" rows="2" name="txtaddress" id="txtaddress"><?php echo $rec->Address; ?></textarea>
									<span id="divtxtaddress" style="color:red"></span>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Nominee's Name </label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtnoname" id="txtnoname" value="<?php echo $rec->or_m_n_name; ?>"/>
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Nominee's Age </label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtage" id="txtage" value="<?php echo $rec->or_m_n_age; ?>" />
								</div>
							</div>
							
							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Nominee's Relation</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtrelation" id="txtrelation" value="<?php echo $rec->or_m_n_relation; ?>"/>
								</div>
							</div>
						
						</div>
						
						<input type="hidden" name="txtproc" id="txtproc" value="2" >
						<input type="hidden" name="txtregid" id="txtregid" value="<?php echo $rec->regid; ?>">
						
						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>
	<script>
		$(window).load(function() {
		 // executes when complete page is fully loaded, including all frames, objects and images
		 get_city();
		});
	</script>