<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Name</th>
								<th>Package</th>
								<th>Mobile</th>
								<th>Address</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($info))
								{
									$sn=1;
									foreach($info->result() as $rows)
									{
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td><?php echo $rows->tr_mem_name;?></td>
										<td><?php echo $rows->m_pack_name;?></td>
										<td><?php echo $rows->tr_mem_mobile;?></td>
										<td><?php echo $rows->tr_mem_address;?></td>
										<td>
											<a href="#custom-modal" onclick="free_member_details(<?php echo $rows->tr_mem_id; ?>,'<?php echo $rows->or_m_user_id; ?>')" class="waves-effect waves-light" data-animation="sign" data-plugin="custommodal"  data-overlaySpeed="100" data-overlayColor="#36404a">Activate<a>
												/ 
												<a href="desable_free_register/<?php echo $rows->tr_mem_id; ?>">Deactivate<a>
												</td>
												</tr>   
												<?php 
													$sn++;
												}
											}
											?>                    
										</tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
					<div id="custom-modal" class="modal-demo">
						<button type="button" class="close" onclick="Custombox.close();">
							<span>&times;</span><span class="sr-only">Close</span>
						</button>
						<h4 class="custom-modal-title" id="headtxt">Join User</h4>
						<div class="custom-modal-text">
							<div class="form">
								
								<form action="" method="post" id="signupForm" class="form-horizontal" >
									
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
									
									<div class="form-group">
										<label class="col-md-4 control-label" id="chg_name">Introducer Associate Id <span class="required"> * </span></label>
										<div class="col-md-6">
											<input type="text" class="form-control empty" name="txtintroducer_id" readonly id="txtintroducer_id">
											<span id="divtxtintroducer_id" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label" id="chg_name">Leg</label>
										<div class="col-md-6">
											<div class="radio-list" id="leg">
												<label class="radio-inline">
												<input type="radio" name="rbjoin_leg" id="rbjoin_leg1" value="L" onClick="check_leg()">Left</label>
												<label class="radio-inline">
												<input type="radio" name="rbjoin_leg" id="rbjoin_leg2" value="R" onClick="check_leg()" >Right</label>
												<input type="hidden" class="form-control empty" id="txtjoin_leg" name="txtjoin_leg" />
												<span id="divtxtjoin_leg" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label" id="chg_name">Parent Id <span class="required"> * </span></label>
										<div class="col-md-6">
											<input type="text" class="form-control empty" name="txtparent_id" id="txtparent_id"  readonly>
											<span id="divtxtparent_id" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label" id="chg_name">Parent Name <span class="required"> * </span></label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="txtparent_name" id="txtparent_name" readonly >
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-md-offset-2 col-md-8">
											<button class="btn btn-primary" type="submit" onclick="return check('signupForm')">Submit</button>
										</div>
									</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
						<script>
							function free_member_details(id,userid)
							{
								$("#txtintroducer_id").val(userid);
								$("#signupForm").attr("action","free_candidate/"+id);
							}	
						</script>						