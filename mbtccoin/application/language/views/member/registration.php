<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>

					<div class="form">
						<?= form_open('member/register_candidate',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>

						<div class="row">
							<h3 class="form-section">Sponsor Details</h3>

							<!--/span-->
							<div class="form-group">
								<label class="col-md-4 control-label">Sponsor Associate Id <span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control empty" name="txtintroducer_id" id="txtintroducer_id" value="<?php echo $this->session->userdata('user_id'); ?>" onblur="get_intro_detail()">
									<span id="divtxtintroducer_id" style="color:red"></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Sponsor Associate Name <span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control empty" name="txtintroducer_name" id="txtintroducer_name"   value="<?php echo $this->session->userdata('name'); ?>" readonly >
									<span id="divtxtintroducer_name" style="color:red"></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Join Leg <span class="required"> * </span></label>
								<div class="col-md-5">
									<div class="radio-list" id="leg">
										<label class="radio-inline">
											<input type="radio" name="rbjoin_leg" id="rbjoin_leg1" value="L" onClick="check_leg()">Left</label>
										<label class="radio-inline">
											<input type="radio" name="rbjoin_leg" id="rbjoin_leg2" value="R" onClick="check_leg()" >Right</label>
										<input type="hidden" class="form-control empty" id="txtjoin_leg" name="txtjoin_leg" />
										<span id="divtxtjoin_leg" style="color:red"></span>
									</div>
								</div>
							</div>

							<input type="hidden" class="form-control empty" name="txtparent_id" id="txtparent_id"  readonly onblur="get_parent_detail()">
							<input type="hidden" class="form-control empty" name="txtparent_name" id="txtparent_name" readonly >


							<?php if(PIN_SYSTEM==1){ ?>
							<h3 class="form-section">Pin Details</h3>

							<div class="form-group">
								<label class="col-md-4 control-label">Package <span class="required"> * </span></label>
								<div class="col-md-5">
									<select id="ddpackid" name="ddpackid" class="form-control opt" onchange="fill_pin()">
										<option selected="selected" value="-1">Select Package</option>
										<?php 
										foreach($pack->result() as $p)
										{
										?>
										<option value="<?php echo $p->m_pack_id; ?>"><?php echo $p->m_pack_name; ?></option>
										<?php
										}
										?>
									</select>
									<span id="divddpackid" style="color:red"></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Pin <span class="required"> * </span></label>
								<div class="col-md-5">
									<select id="ddpin" name="ddpin" class="form-control opt">
										<option selected="selected" value="-1">Select Pin</option>
									</select>
									<span id="divddpin" style="color:red"></span>
								</div>
							</div>

							<?php } ?>

							<h3 class="form-section">Personal Details</h3>

							<div class="form-group">
								<label class="col-md-4 control-label">Applicant Name <span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control empty"  name="txtassociate_name" id="txtassociate_name">
									<span id="divtxtassociate_name" style="color:red"></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Father/Husband's Name</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtparent" id="txtparent">
									<span id="divtxtparent" style="color:red"></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Gender</label>
								<div class="col-md-5">
									<div class="radio-list">
										<label class="radio-inline">
											<input type="radio" name="rbgender" id="rbgender1" value="1">Male</label>
										<label class="radio-inline">
											<input type="radio" name="rbgender" id="rbgender2" value="0">Female</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Date of Birth</label>
								<div class="col-md-5">
									<input name="txtdob" id="txtdob" class="form-control"  placeholder="dd-mm-yyyy" type="text"/>
									<span id="divdatepicker" style="color:red"></span>
								</div>
							</div>

							<h3 class="form-section">Contact Details</h3>

							<div class="form-group">
								<label class="col-md-4 control-label">Mobile Number<span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtmobile" id="txtmobile" maxlength="10">
									<span id="divtxtmobile" style="color:red"></span>	
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Email Address</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtemail" id="txtemail">
									<span id="divtxtemail" style="color:red"></span>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">State <span class="required"> * </span></label>
								<div class="col-md-5">
									<select id="ddstate" name="ddstate" class="form-control opt" onChange="get_city()">
										<option selected="selected" value="-1">Select State</option>
										<?php foreach($state->result() as $row)
{
										?>
										<option value="<?php echo $row->m_loc_id;?>"><?php echo $row->m_loc_name;?></option>
										<?php
}
										?>
									</select>
									<span id="divddstate" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">City <span class="required"> * </span></label>
								<div class="col-md-5">
									<select id="ddcity" name="ddcity" class="form-control opt">
										<option selected="selected" value="">Select City</option>
									</select>
									<span id="divddcity" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Pincode</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtpincode" id="txtpincode" maxlength="6">
									<span id="divtxtpincode" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Address</label>
								<div class="col-md-5">
									<textarea class="form-control" name="txtaddress" id="txtaddress"></textarea>
									<span id="divtxtaddress" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Nominee's Name</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtnoname" id="txtnoname"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Nominee's Age</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtage" id="txtage"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Nominee's Relation</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtrelation" id="txtrelation"/>
								</div>
							</div>
							
							<h3 class="form-section">Bank Details</h3>
														
							<div class="form-group">
								<label class="col-md-4 control-label">Bank Name</label>
								<div class="col-md-5">
									<select class="form-control" name="ddbank_name" id="ddbank_name">
										<option value="">Select Bank</option>
										<?php
										foreach($bank->result() as $bank_row)
										{
										?>
										<option value="<?php echo $bank_row->m_bank_id;?>"><?php echo $bank_row->m_bank_name;?></option>
										<?php 
										}
										?>
									</select>
									<span id="divddbank_name" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Branch Name</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtbranch_name" id="txtbranch_name">	
									<span id="divtxtbranch_name" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Account Number</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtaccount" id="txtaccount">
									<span id="divtxtaccount" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">IFSCode</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtifscode" id="txtifscode"  maxlength="11">	
									<span id="divtxtifscode" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Pancard Number</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtpancard" id="txtpancard"  maxlength="10" onblur="validate_pancard()">
									<span id="divtxtpancard" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Adhar</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="txtadhar" id="txtadhar">
									<span id="divtxtadhar" style="color:red"></span>
								</div>
							</div>
							
							<h3 class="form-section">Password</h3>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Password <span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="password" class="form-control" name="txtpassword" id="txtpassword"  maxlength="10">
									<span id="divtxtpassword" style="color:red"></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Confirm Password <span class="required"> * </span></label>
								<div class="col-md-5">
									<input type="password" class="form-control" name="txtcpassword" id="txtcpassword">
									<span id="divtxtcpassword" style="color:red"></span>
									<span id="divtxtconfirm" style="color:red"></span>
								</div>
							</div>
												
						</div>

						<input type="hidden" name="txtproc" id="txtproc" value="1" >
						<input type="hidden" name="txtreg_from" id="txtreg_from" value="1" >

						<div class="row">
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button class="btn btn-primary" type="button" id="function" onclick="registration('signupForm')">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>


		</div>
	</div>