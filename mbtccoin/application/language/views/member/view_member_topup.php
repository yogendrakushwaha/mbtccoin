<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>

					<div class="form">
						<?= form_open('member/view_user_downline',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>

						<div class="form-group">
							<label class="col-md-2 control-label">User Id <span class="required"> * </span></label>
							<div class="col-md-3">
								<input type="text" id="txtuserid" name="txtuserid" class="form-control empty" placeholder="Enter login id." onblur="chaech_topup()">
							</div>

							<label class="col-md-2 control-label">User Name <span class="required"> * </span></label>
							<div class="col-md-3">
								<div id="divtxtuserid" style="color:red"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label">Package <span class="required"> * </span></label>
							<div class="col-md-3">
								<select id="ddpackid" name="ddpackid" class="form-control opt" onchange="fill_pin_id()">
									<option value='-1'>Select Pakcage</option>
									<?php 
									foreach($pack->result() as $p)
									{
									?>
									<option value="<?php echo $p->m_pack_id; ?>"><?php echo $p->m_pack_name; ?></option>
									<?php
									}
									?>
								</select>
								<span id="divddpack" style="color:red"></span>
							</div>

							<label class="col-md-2 control-label">Pin <span class="required"> * </span></label>
							<div class="col-md-3">
								<select id="ddpin" name="ddpin" class="form-control opt">
									<option value='-1'>Select Pin</option>
								</select>
								<span id="divddpin" style="color:red"></span>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="get_member_topup('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
	</div>