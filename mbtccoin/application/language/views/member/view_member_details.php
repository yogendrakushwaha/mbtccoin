<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<form action="view_member_details" method="post" class="cmxform horizontal-form" >
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Login Id<span class="required"> * </span></label>
										<input type="text" id="txtlogin" name="txtlogin" value="<?php echo ($uid=='')?'':$uid; ?>" class="form-control input-inline" placeholder="Enter login id.">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label"></label><br>
										<button class="btn btn-primary btn-sm input-inline" type="submit">Submit</button>
										<button type="button" class="btn btn-default btn-sm">Cancel</button>
									</div>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
			<?php
				if(!empty($rec))
				{
				?>
				<div class="col-md-4 col-lg-3">
					<div class="profile-detail card-box">
						<div>
							<?php
								if($rec->User_image!='')
								{
								?>
								<img src="<?php echo base_url(); ?>application/user_image/<?php echo $rec->User_image ?>" class="img-circle" alt="profile-image">
								<?php
								}
								else
								{
								?>
								<img src="<?php echo base_url(); ?>application/libraries/profile.png" class="img-circle" alt="profile-image">
								<?php
								}
							?>
							<hr>
							
							<div class="text-left">
								<p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15"><?php echo $rec->Associate_Name ?></span></p>
								
								<p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15"><?php echo $rec->Mobile_No ?></span></p>
								
								<p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15"><?php echo $rec->Email ?></span></p>
								
								<p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15"><?php echo $rec->State ?></span></p>
								
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="col-sm-9">
					<div class="card-box table-responsive">
						<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
						<table id="datatable-buttons" class="table table-striped table-bordered">
							<tbody>
								<tr>
									<td>Sponser Id</td>
									<td><?php echo $rec->Intro_Userid;?></td>
								</tr>
								<tr>
									<td>Sponser Name</td>
									<td><?php echo $rec->Intro_Name;?></td>
								</tr>
								<tr>
									<td>Postion</td>
									<td><?php echo $rec->USER_POSITION_DESC;?></td>
								</tr>
								<tr>
									<td>Upliner Id</td>
									<td><?php echo $rec->Upliner_Userid;?></td>
								</tr>
								<tr>
									<td>Upliner Name</td>
									<td><?php echo $rec->Upliner_Name;?></td>
								</tr>
								<tr>
									<td>DOB</td>
									<td><?php echo $rec->DOB;?></td>
								</tr>
								<tr>
									<td>Joining Date</td>
									<td><?php echo $rec->Joining_Date_1;?></td>
								</tr>
								<tr>
									<td>Password</td>
									<td><?php echo $rec->Password;?></td>
								</tr>
								<tr>
									<td>Pin Password</td>
									<td><?php echo $rec->Pin_Password;?></td>
								</tr>
								<tr>
									<td>Bank Name</td>
									<td><?php echo $rec->Bankname;?></td>
								</tr>
								<tr>
									<td>Branch Name</td>
									<td><?php echo $rec->Branch;?></td>
								</tr>
								<tr>
									<td>Account No.</td>
									<td><?php echo $rec->AcNo;?></td>
								</tr>
								<tr>
									<td>IFSC Code</td>
									<td><?php echo $rec->Ifsc;?></td>
								</tr>
								<tr>
									<td>Pancard</td>
									<td><?php echo $rec->Pancard;?></td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
				<?php
				}
			?>
		</div>
	</div>