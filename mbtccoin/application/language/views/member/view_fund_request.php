<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Date</th>
								<th>User Id</th>
								<th>Name</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sn=1;
								if(!empty($rid))
								{
									foreach($rid->result() as $row)
									{
									?>
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $row->m_top_reqdate; ?></td>
										<td><?php echo $row->or_m_user_id; ?></td>
										<td><?php echo $row->or_m_name; ?></td>
										<td><?php echo $row->tr09_req_amount; ?></td>
										<td><?php echo $row->m_top_status_desc; ?></td>
										<td>
											<?php
												if($row->m_top_status==2)
												{
												?>
												<a href="<?php echo base_url(); ?>member/fund_request_action/<?php echo $row->tr09_req_id; ?>/1"><span class="glyphicon glyphicon-ok"></span></a> / 
												<a href="<?php echo base_url(); ?>member/fund_request_action/<?php echo $row->tr09_req_id; ?>/0"><span class="glyphicon glyphicon-trash"></span></a>
												<?php
												}
											?>
										</td>
									</tr>
									<?php
										$sn++;
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>