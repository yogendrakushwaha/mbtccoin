<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('member/update_deactivate_member',array("class" => "cmxform horizontal-form", "id" => "signupForm")); ?>
						
						<!--/row-->
						<div class="row">
							<!--/span-->
							<div class="col-md-4 col-md-offset-4">
								<div class="form-group">
									<label class="control-label">Member Deactivate Here </label>
									<input type="hidden" id="txtquid" name="txtquid" class="form-control empty" >
									<button type="button" class="btn btn-primary" onclick="conwv('signupForm')">Deactivate</button>
								</div>
							</div>
						</div>
						
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>S No.</th>
								<th>Login Id</th>
								<th>Associate</th>
								<th>Joining Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="userid">
							<?php $sn=1;
								foreach($rec->result() as $rows)
								{
								?>
								<tr>
									<td><input type="checkbox" class="checkboxes" id="<?php echo $rows->regid;?>" name="<?php echo $rows->regid;?>" value="<?php echo $rows->regid;?>" onClick="chbchecksin();"/></td>
									<td><?php echo $sn;?></td>
									<td><?php echo $rows->Login_Id;?></td>
									<td><?php echo $rows->Associate_Name;?></td>
									<td><?php echo date('d-m-y h:i:s',strtotime($rows->Joining_Date));?></td>
									<td>Active Member</td>
								</tr>
								<?php 
									$sn++;
								}
							?>  
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>