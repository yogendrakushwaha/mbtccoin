<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
					<?= form_open('javascript:0;',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
							
							<div class="form-group">
								<label class="col-md-3 control-label">Message</label>
								<div class="col-md-9">
									<textarea id="txtdesc" name="txtdesc" rows="3" col="9" placeholder="Enter Message." class="form-control input-inline input-medium"></textarea>
									<input type="hidden"  value=""  id="txtquid" name="txtquid" />
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button id="b_send" class="btn btn-primary" type="button" onclick="send_sms_user()"">Send</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						<?php form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<!--<table id="datatable-buttons" class="table table-striped table-bordered">-->
					<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
						<thead>
							<tr>
								<th>
									<input type="checkbox" id="checkAll" name="checkAll" onclick="chbcheckall();" class="group-checkable" data-set="#sample_2 .checkboxes" />
								</th>
								<th>S.No</th>
								<th>Name</th>
								<th>Mobile No.</th>
								
							</tr>
						</thead>
						<tbody id="userid">
							<?php 
								$sn=0;
								foreach($rid->result() as $rows)
								{
									$sn++;
								?>												
								<tr>
									<td>
										<input type="checkbox" class="checkboxes" id="<?php echo $sn; ?>" name="<?php echo $rows->or_m_mobile_no; ?>" onClick="chbchecksin();" value="<?php echo $rows->or_m_mobile_no; ?>"/>
									</td>
									<td><?php echo $sn; ?></td>
									<td><?php echo $rows->or_m_name; ?></td>
									<td><?php echo $rows->or_m_mobile_no; ?></td>
								</tr>
							<?php } ?>            
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>
		
	