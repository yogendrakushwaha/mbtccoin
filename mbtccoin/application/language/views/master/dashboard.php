<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 style="float:left;" class="page-title"><?php echo $page; ?>&nbsp;&nbsp;&nbsp;|</h4><p class="text-muted page-title-alt">&nbsp;&nbsp; <?php echo $form_name; ?></p>
			</div>
		</div>
		
		
		<div class="row">
			
			<div class="col-lg-4">
				<div class="card-box">
					<div class="bar-widget">
						<div class="table-box">
							<div class="table-detail">
								<div class="iconbox bg-info">
									<i class="md md-add-circle-outline"></i>
								</div>
							</div>
							
							<div class="table-detail">
								<h4 class="m-t-0 m-b-5"><b><?php echo $rid->tot_id; ?></b></h4>
								<h5 class="text-muted m-b-0 m-t-0">Total User</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-lg-4">
				<div class="card-box">
					<div class="bar-widget">
						<div class="table-box">
							<div class="table-detail">
								<div class="iconbox bg-custom">
									<i class="glyphicon glyphicon-ok-circle"></i>
								</div>
							</div>
							
							<div class="table-detail">
								<h4 class="m-t-0 m-b-5"><b><?php echo $rid->active_id; ?></b></h4>
								<h5 class="text-muted m-b-0 m-t-0">Active Member</h5>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-lg-4">
				<div class="card-box">
					<div class="bar-widget">
						<div class="table-box">
							<div class="table-detail">
								<div class="iconbox bg-danger">
									<i class="glyphicon glyphicon-remove-circle"></i>
								</div>
							</div>
							
							<div class="table-detail">
								<h4 class="m-t-0 m-b-5"><b><?php echo $rid->deactive_id; ?></b></h4>
								<h5 class="text-muted m-b-0 m-t-0">Deactive Member</h5>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-4">
				<div class="card-box">
					<div class="bar-widget">
						<div class="table-box">
							<div class="table-detail">
								<div class="iconbox bg-info">
									<i class="icon-layers"></i>
								</div>
							</div>
							
							<div class="table-detail">
								<h4 class="m-t-0 m-b-5"><b><?php echo $rid->topup; ?></b></h4>
								<h5 class="text-muted m-b-0 m-t-0">Total TopUp</h5>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-4">
				<div class="card-box">
					<div class="bar-widget">
						<div class="table-box">
							<div class="table-detail">
								<div class="iconbox bg-danger">
									<i class="icon-layers"></i>
								</div>
							</div>
							
							<div class="table-detail">
								<h4 class="m-t-0 m-b-5"><b><?php echo $rid->nontopup; ?></b></h4>
								<h5 class="text-muted m-b-0 m-t-0">Total Non TopUp</h5>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="row">
				<div class=" col-md-6 card-box table-responsive">
					<h4 class="m-t-0 header-title"><b>Topup Users</b></h4>
					
					<table id="" class="table table-striped table-bordered dataTable no-footer">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Name</th>
								<th>Package</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($rec1))
								{
									$sn=1;
									foreach($rec1 as $rows)
									{
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td><?php echo $rows->or_m_name;?></td>
										<td><?php echo $rows->package;?></td>
										
									</tr>   
									<?php 
										$sn++;
									}
								}
							?>                    
						</tbody>
					</table>
				</div>
				
				<div class=" col-md-6 card-box table-responsive">
					<h4 class="m-t-0 header-title"><b>Non Topup Users
						
						</b></h4>
					
					<table id="" class="table table-striped table-bordered dataTable no-footer">
						<thead>
							<tr>
								<th>S No.</th>
								<th>Name</th>
								<th>Package</th>
								
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($rec2))
								{
									$sn=1;
									foreach($rec2 as $rows)
									{
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td><?php echo $rows->or_m_name;?></td>
										<td><?php echo $rows->package;?></td>
										
									</tr>   
									<?php 
										$sn++;
									}
								}
							?>                    
						</tbody>
					</table>
				</div>
		
		</div>
		<!--<div class="row">
			<div class="col-lg-4">
				<div class="card-box">
					<h4 class="m-t-0 m-b-20 header-title"><b><i class="md md-forum"></i> New Query</b></h4>
					
					<div class="nicescroll mx-box" style="overflow: hidden; outline: none;" tabindex="5000">
						<ul class="list-unstyled transaction-list m-r-5">
							<?php
							foreach($ticket->result() as $tic)
							{
							?>
							<li>
								<i class="md md-done-all text-success"></i>
								<span class="tran-text"><?php echo $tic->TICKET_TITLE; ?></span>
								<span class="pull-right text-muted"><?php echo $tic->TICKET_DATE; ?></span>
								<span class="clearfix"></span>
							</li>
							<?php
							}
							?>
						</ul>
					</div>
				</div>
				
			</div>
		</div>	-->			