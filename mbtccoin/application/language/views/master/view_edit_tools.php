<div class="wrapper">
	<div class="container">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 class="page-title"><?php echo $page; ?></h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>

					<div class="form">
						<?= form_open('master/view_update_tools/'.$info->m_tool_id,array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>

						<div class="form-group">
							<label class="col-md-2 control-label">Title</label>
							<div class="col-md-9">
								<input type="text" id="txttitle" name="txttitle" value="<?php echo $info->m_tool_title; ?>" class="form-control input-inline input-xlarge" placeholder="Enter Title." >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label">Video Url</label>
							<div class="col-md-9">
								<input type="text" id="txturl" name="txturl" value="<?php echo $info->m_tool_video; ?>" class="form-control input-inline input-xlarge" placeholder="Enter Url." >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label">Description</label>
							<div class="col-md-9">
								<textarea class="ckeditor form-control" name="txtdescription" id='txtdescription' placeholder="Enter Description" cols='50' rows='6'><?php echo $info->m_tool_description; ?></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-2 col-md-9">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
								<button type="reset" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>

		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				if($("#txtdescription").length > 0){
					tinymce.init({
						selector: "textarea#txtdescription",
						theme: "modern",
						height:300,
						plugins: [
							"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
							"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
							"save table contextmenu directionality emoticons template paste textcolor"
						],
						toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
						style_formats: [
							{title: 'Bold text', inline: 'b'},
							{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
							{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
							{title: 'Example 1', inline: 'span', classes: 'example1'},
							{title: 'Example 2', inline: 'span', classes: 'example2'},
							{title: 'Table styles'},
							{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
						]
					});
				}
			});
		</script>

