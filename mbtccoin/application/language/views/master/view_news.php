<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das" class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
					
					<a href="<?php echo base_url(); ?>index.php/master/add_news" class="btn btn-primary pull-right" style="margin-right:20px;">
						<i class="fa fa-plus"></i>
						<span class="hidden-480">
							Add News
						</span>
					</a>
				</ol>
				
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			
			<div class="col-sm-12">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>
									S No.
								</th>
								<th>
									Action
								</th>
								<th>
									News Title
								</th>
								<th>
									News Date
								</th>
								
								<th>
									Description
								</th>
								
							</tr>
						</thead>
						<tbody>
							<?php 
								$sn=0;
								foreach($content->result() as $row)
								{
									$sn++;
								?>
								<tr class="odd gradeX">
									<td>
										<?php echo $sn;?>
									</td>
									<td>
										<a href="<?php echo base_url(); ?>index.php/master/edit_news/<?php echo $row->m_news_id;?>" title="Edit" >
										<i class="fa fa-edit (alias)"></i></a>
										&nbsp;
										<?php
											if($row->m_news_status==1)
											{
											?>
											<a href="<?php echo base_url(); ?>index.php/master/delete_news/<?php echo $row->m_news_id;?>/0" title="Delete News" >
											<i class="fa fa-trash-o"></i></a>
											<?php
											}
											else
											{
											?>
											<a href="<?php echo base_url(); ?>index.php/master/delete_news/<?php echo $row->m_news_id;?>/1" title="Delete News" >
											<i class="fa fa-refresh"></i></a>
											<?php
											}
										?>
										
									</td>
									<td>
										<?php echo $row->m_news_title; ?>
										<td>
											<?php echo $row->m_entrydate; ?>
										</td>
										
										<td>
											<?php echo substr($row->m_news_des,0,100); ?>
										</td>
										
									</tr>
								<?php } ?>
							</tbody>
							
						</table>
					</div>
				</div>
				
				
			</div>
		</div>
		