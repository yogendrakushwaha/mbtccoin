<?php 
	foreach($content->result() as $row)
	{
		break;
	}
?>	
<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das" class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('master/update_news/'.$row->m_news_id,array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-2 control-label">Title</label>
							<div class="col-md-9">
								<input type="text" id="txttitle" name="txttitle"  class="form-control input-inline input-xlarge" placeholder="Enter News Title." value='<?php echo $row->m_news_title; ?>' >
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label">Description</label>
							<div class="col-md-9">
								<textarea class="form-control" name="txtdescription" id='txtdescription' placeholder="Enter User Name." cols='50' rows='6'><?php echo $row->m_news_des; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Status</label>
							<div class="col-md-9">
								<select class="bs-select form-control input-large" name="ddstatus" id="ddstatus">
									<?php
										if($row->m_news_status==1)
										{												
										?>
										<option value="1" selected>Active</option>
										<option value="0" >Inactive</option>
										<?php 
										}
										else
										{
										?>
										<option value="1">Active</option>
										<option value="0" selected>Inactive</option>
										<?php 
										} 
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Update</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>