<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('master/inser_bank_master',array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
						<div class="form-group">
							<label class="col-md-3 control-label">Bank Name</label>
							<div class="col-md-9">
								<input type="text" id="txtbank" name="txtbank" class="form-control input-inline input-medium" placeholder="Enter Bank Name.">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Submit</button>
								<button type="button" class="btn btn-default">Cancel</button>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="card-box table-responsive">
					<h4 class="m-t-0 header-title"><b><?php echo $table_name; ?></b></h4>
					
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>S.No</th>
								<th>Bank Name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$sn=0;
								foreach($info->result() as $unit)
								{
									$sn++;
								?>												
								<tr>
									<td><?php echo $sn; ?></td>
									<td><?php echo $unit->m_bank_name; ?></td>
									<td><a href="<?php echo base_url(); ?>master/view_edit_bank/<?php echo $unit->m_bank_id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
										<?php 
											if($unit->m_bank_status==1)
											{
											?>
											<a href="<?php echo base_url(); ?>master/bank_status/<?php echo $unit->m_bank_id; ?>/0"><span class='glyphicon glyphicon-trash'></span></a>
											<?php
											}
											else
											{
											?>
											<a href="<?php echo base_url(); ?>master/bank_status/<?php echo $unit->m_bank_id; ?>/1"><span class='glyphicon glyphicon-repeat'></span></a>
											<?php
											}
										?></td>
								</tr>
							<?php } ?>            
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
	</div>