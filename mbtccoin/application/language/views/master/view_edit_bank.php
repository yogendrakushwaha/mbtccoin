<?php 
	foreach($info->result() as $bank)
	{
		break;
	}
?>	
<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp;|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('master/view_update_bank/'.$bank->m_bank_id,array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
						
									<div class="form-group">
										<label class="col-md-3 control-label">Bank Name</label>
										<div class="col-md-9">
											<input type="text" id="txtbank" name="txtbank" class="form-control input-inline input-medium" placeholder="Enter Bank Name." value="<?php echo $bank->m_bank_name; ?>" />
										</div>
									</div>
						
								<div class="form-group">
									<div class="col-md-offset-4 col-md-8">
										<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Update</button>
										<button type="button" class="btn btn-default">Cancel</button>
									</div>
								</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>