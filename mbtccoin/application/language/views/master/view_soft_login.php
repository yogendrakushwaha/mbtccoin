<div class="wrapper">
	<div class="container">
		
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<h4 id="das"  class="page-title"><?php echo $page; ?>&nbsp;&nbsp|</h4>
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>master/index">&nbsp; Dashboard</a></li>
					<li class="active"><?php echo $form_name; ?></li>
				</ol>
			</div>
		</div>
		
		<?php
			foreach($config->result() as $row)
			{
				break;
			}
		?>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b><?php echo $form_name; ?></b></h4>
					<p class="text-muted font-13 m-b-30"></p>
					
					<div class="form">
						<?= form_open('master/update_mainconfig/'.$row->m00_login_id,array("class" => "cmxform form-horizontal", "id" => "signupForm")); ?>
							
							<div class="form-group ">
								<div class="row">
									<div class="col-lg-4">
										<label for="username" class="control-label col-lg-4">Login Id</label>
										<div class="col-lg-8">
											<input type="text" id="txtusername" name="txtusername" class="form-control empty" placeholder="Enter User Name." value="<?php echo $row->m00_username;?>">
										<span id="divtxtusername" style="color:red"></span></div>
									</div>
									
									<div class="col-lg-4">
										<label for="password" class="control-label col-lg-4">Login Password</label>
										<div class="col-lg-8">
											<input type="password" id="txtuserpass" name="txtuserpass" class="form-control" placeholder="Enter User Password." value="<?php echo $row->m00_password;?>">
											<span id="divtxtuserpass" style="color:red"></span>
										</div>
									</div>
									
									<div class="col-lg-4">
										<label for="confirm_password" class="control-label col-lg-4">Login Pin Pwd</label>
										<div class="col-lg-8">
											<input type="password" id="txtuserpinpass" name="txtuserpinpass" class="form-control" placeholder="Enter User Pin Password." value="<?php echo $row->m00_pinpassword;?>">
											<span id="divtxtuserpinpass" style="color:red"></span>
										</div>
									</div>
									
									<input type="hidden" id="txtusertype" name="txtusertype" value="1">
									<input type="hidden" id="txtuserstatus" name="txtuserstatus" value="1">
									
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-lg-offset-3 col-lg-6">
									<button class="btn btn-primary" type="button" onclick="conwv('signupForm')">Update</button>
									<button class="btn btn-default" type="button">Cancel</button>
								</div>
							</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
		