'use strict';
app.factory('userService',['$http','$location','jwtHelper','store',function($http,$location,jwtHelper,store){	
	var $base_url=BASE_URL;
	return{
		/////////////////   Get Margin for Operator according Slab Here ///////////////////////////


		getuser:function(data,scope){

			$http({
				url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_PersonalDetail',
				method: "POST",
				skipAuthorization:false,
				headers: { "Content-Type": 'application/x-www-form-urlencoded' },
				data: data
			})
				.then(function(msg) {
				var is_error=msg.data.error;
				if(!is_error)
				{

					scope.userInfo ={
						'UserId':msg.data.userDATA.RegId,
						'FirstName': msg.data.userDATA.FirstName,
						'LastName' : msg.data.userDATA.LastName,
						'Email' :  msg.data.userDATA.MemberEmail,
						'Role':msg.data.userDATA.MemberRole,
						'Mobile' : msg.data.userDATA.MemberContactNo,
						'MemberName' : msg.data.userDATA.MemberName,
						'MemberImage' : msg.data.userDATA.MemberImage,
						'Gender': msg.data.userDATA.MemberGender,
						'Location':msg.data.userDATA.MemberLocation,
						'Postal':msg.data.userDATA.MemberPostal,
						'City':msg.data.userDATA.MemberCity,
						'Country':msg.data.userDATA.MemberCountry,
						'AddressProofDoc':msg.data.userDATA.AddressProofDoc,
						'IdProofDoc':msg.data.userDATA.IdProofDoc,
						'Subcription':msg.data.userDATA.MemberSubcription,
						'AvailTOKEN':msg.data.userDATA.AvailTOKEN,
						'BonusTOKEN':'0.00000000',
						'QRCODE':msg.data.userDATA.QRCODE,
						'AddressProofDoc':msg.data.userDATA.AddressProofDoc,
						'IdProofDoc':msg.data.userDATA.IdProofDoc
					};
				}
				else
				{
					//scope.alerts[0].type='warning';
					//scope.alerts[0].msg=msg.data.message;
					$location.path('/login');
				}

			});




		},
		logout:function(){

			store.remove('token');
			$location.path('/login');	


		},
		getuserByPhone:function(data,form,scope){
			$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getuserByPhone',data).success(function(data){
				if(data.error){
					if($('.alert-error').hasClass('glyphicon-ok-circle')){
						$('.alert-error').removeClass('glyphicon-ok-circle');	
					}
					$('.alert-error').addClass('glyphicon glyphicon-remove-sign red-btn').show();
					form.$setValidity('nouserfound',false);

				}else{
					if($('.alert-error').hasClass('glyphicon-remove-sign')){
						$('.alert-error').removeClass('glyphicon-remove-sign');	
					}
					$('.alert-error').addClass('glyphicon glyphicon-ok-circle green-btn').show();
					form.$setValidity('nouserfound',true);

				}
			});

		},
		edituser:function(data,scope){
			var $loginurl=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/EnrollWebService/getUserByID',data);
			$loginurl.then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					$location.path('/app/home');

				}
				else
				{
					scope.alerts[0].type='warning';
					scope.alerts[0].msg=msg.data.message;
					$location.path('/access/signin');
				}
			});
		},
		forgotPasswod: function(data,scope)
		{
			$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/forgotPasswod',data).success(function(response){
				/* 			if(!response.error){
					console.log(response);
					$('.contact_title h2').html(response.data.title);
					$('.contact_content').html(response.data.content);
				} */
			});		
		},


		get_user_kyc: function(data,scope){
			$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_kyc',data).success(function(response){
				if(!response.error){
					console.log(response);
					scope.UpWalletInfo ={
						'IdproofType': response.data.kyc.Idtype,
						'UIDNUMBER': response.data.kyc.kYUnumber,
						'address': response.data.kyc.kyAddress, 
						'FirstName': response.data.user.FirstName,
						'LastName' : response.data.user.LastName,
						'Email' :   response.data.user.MemberEmail,
						'Mobile' :  response.data.user.MemberContactNo, 
						'Idtype' :  response.data.kyc.Idtype, 
						'Dob' :  response.data.user.MemberDOB, 
						'KyIdproofdoc' :  response.data.kyc.KyIdproofdoc, 
						'kyAddress' :  response.data.kyc.kyAddress, 
						'kYaddressproof' :  response.data.kyc.kYaddressproof, 	 
					}; 

				}
			});		
		},

		getaboutdata: function(scope)
		{

			$http.get($location.protocol() + '://' +$location.host() +'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},
		getContactUsdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getSiteContactUsdata').success(function(response){
				if(!response.error){
					console.log(response);
					$('.contact_title').html(response.data.title);
					$('.contact_content').html(response.data.content);
				}
			});		
		},
		getComingSoondata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getComingSoondata').success(function(response){
				if(!response.error){
					console.log(response);
					$('.comingsoon_title').html(response.data.title);
					$('.comingsoon_content').html(response.data.content);
				}
			});		
		},
		getCareerdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getCareerContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.contact_title h2').html(response.data.title);
					$('.contact_content').html(response.data.content);
				}
			});		
		},
		getOurTeamdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getTeamContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.contact_title h2').html(response.data.title);
					$('.contact_content').html(response.data.content);
				}
			});		
		},
		getTermandConditiondata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_TermOfUse').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},
		getPrivacyPolicydata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_PrivacyPolicy').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getRefundPolicydata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title h2').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getFAQdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title h2').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getPaymentOptionsdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title h2').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getSupportdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title h2').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getPressRealeasedata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error){
					console.log(response);
					$('.about_title h2').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getBlogdata: function(scope)
		{
			$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/getAboutContent').success(function(response){
				if(!response.error)
				{
					console.log(response);
					$('.about_title h2').html(response.data.title);
					$('.about_content').html(response.data.content);
				} 
			});		
		},

		getslottoken: function(scope)
		{
			$http({
				url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_slottoken',
				method: "POST",
				skipAuthorization:false,
				headers: { "Content-Type": 'application/x-www-form-urlencoded' },
				data: ''
			})
				.then(function(msg) {
				var is_error=msg.error;
				if(!is_error)
				{
					scope.icotokenata = msg.data.data;
				}
				else
				{
					$location.path('/login');
				}
			});		
		},
		
		getAdminBTCAdrress : function(scope)
		{
			$http({
				url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_admin_btcaddress',
				method: "POST",
				skipAuthorization:false,
				headers: { "Content-Type": 'application/x-www-form-urlencoded' },
				data: ''
			})
				.then(function(msg) {
				var is_error=msg.error;
				if(!is_error)
				{
					scope.user.btcAddress = msg.data.data[0].m_btc_address;
				}
				else
				{
					$location.path('/login');
				}
			});		
		},


		usdtobtc : function(data,scope)
		{
			$http({
				url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/get_usdtobtc',
				method: "POST",
				skipAuthorization:false,
				headers: { "Content-Type": 'application/x-www-form-urlencoded' },
				data: {usdAmount : data.usdAmount}
			})
				.then(function(msg) {
				var is_error=msg.error;
				if(!is_error)
				{
					scope.user.BTCAmount = msg.data.data;
				}
				else
				{
					$location.path('/login');
				}
			});		
		},

		buytoken : function(data,scope)
		{
			$http({
				url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/user_buytoken',
				method: "POST",
				skipAuthorization:false,
				headers: { "Content-Type": 'application/x-www-form-urlencoded' },
				data: data
			})
				.then(function(msg) {
				var is_error=msg.error;
				if(!is_error)
				{
					$location.path('/dashboard');
				}
				else
				{
					$location.path('/login');
				}
			});		
		},

		userOrder : function(data,scope)
		{
			$http({
				url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/UserprofileWebService/userOrder',
				method: "POST",
				skipAuthorization:false,
				headers: { "Content-Type": 'application/x-www-form-urlencoded' },
				data: {regid : data}
			})
				.then(function(msg) {
				var is_error=msg.error;
				if(!is_error)
				{
					scope.orderHistory = msg.data.data;
				}
				else
				{
					$location.path('/login');
				}
			});		
		},

		/////////////SERVICE MENU ////////////////////

		stripfun(html){
			var tmp = document.createElement('DIV');
			tmp.innerHTML = html;
			return tmp.textContent || tmp.innerText || "";
		}

	}
}]);