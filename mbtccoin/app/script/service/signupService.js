'use strict';
app.factory('signupService',function($http,$location,jwtHelper,store){
	var $base_url=BASE_URL;
	return{
	    
		signup:function(data,scope){
			console.log(data);
		    $http({
					url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/SignupWebService/user_signup",
					method: "POST",
					skipAuthorization:'true',
					headers: { "Content-Type": 'application/x-www-form-urlencoded' },
					data: data
				}).then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					 store.set('token',msg.data.data);
                     store.set('Mobileno',data.Mobile);
                     store.set('Email',data.Email);
				     scope.alerts[0].type='success';
					 scope.alerts[0].msg=msg.data.message;
					 $location.path('/verify_email');
				}
				else
				{
					$location.path('/signup');
				}
			});
		},
		fbsignup:function(data,scope){
			//console.log('Facebook Signup Here');
			var userdata={};
			var $signupurl=$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url+'/admin/users',data);
			$signupurl.then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					 userdata=msg.data.userDATA['0'];	
					 scope.data=userdata;
					//$location.path('/app/myservice');
				}
				else
				{
					scope.data.msgclass='alert alert-warning';
					scope.data.errormsg=msg.data.message;
					//console.log(msg.data.data);
					//$location.path('/admin/add_user');
				}
			});
		},
		gplussignup:function(){
			console.log('Google Plus Signup Here');
		},
		emailVerification:function(data,scope){
			console.log('Email Verification Here');
			console.log(data);
			var userdata={};
			 $http({
					url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/SignupWebService/verification",
					method: "POST",
					skipAuthorization:false,
					headers: { "Content-Type": 'application/x-www-form-urlencoded' },
					data: data
				}).then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					scope.verifyalerts[0].type="success";
					scope.verifyalerts[0].msg="Your Email has been verified.Your account has been activated please Login ";
					$location.path('/login');
				}
				else
				{
					scope.verifyalerts[0].type='warning';
					scope.verifyalerts[0].msg=msg.data.message;
					//console.log(msg.data.data);
					//$location.path('/admin/add_user');
				}
			});
		},
		mobilenoVerification:function(){
			console.log('Mobile No Verification Here');
		},
		smsOTPVerification:function(){
			console.log('SMS OTP Verification Here');
		},
		emailOTPVerification:function(){
			console.log('Email OTP Verification Here');
		}
	}
});