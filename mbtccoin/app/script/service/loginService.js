'use strict';
app.factory('loginService',['$http','$location','jwtHelper','store',function($http,$location,jwtHelper,store){
	var $base_url=BASE_URL;	
	return{
		
		login:function(data,scope){

				$http({
					url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/LoginWebService/email_login",
					method: "POST",
					skipAuthorization:'true',
					headers: { "Content-Type": 'application/x-www-form-urlencoded' },
					data: data
				})
				.then(function(msg) {
					var is_error=msg.data.error;
				if(!is_error)
				{
					scope.alerts[0].type='success';
					scope.alerts[0].msg=msg.data.message;
					store.set('token',msg.data.data);
					$location.path('/myprofile');
					//$location.path('/admin/dashboard');
				}
				else
				{
					scope.alerts[0].type='warning';
					scope.alerts[0].msg=msg.data.message;
					$location.path('/login');
				}
					
				});
				
			


/*
			var $loginurl=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/LoginWebService/email_login',data);
			$loginurl.then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					 
					scope.alerts[0].type='success';
					scope.alerts[0].msg=msg.data.message;
					
					$location.path('/app/upgrade-wallet');
					//$location.path('/admin/dashboard');
				}
				else
				{
					scope.alerts[0].type='warning';
					scope.alerts[0].msg=msg.data.message;
					$location.path('/access/signin');
				}
			});*/
		},
		adminlogin:function(data,scope)
		{
			var $loginurl=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/LoginWebService/admin_login',data);
			$loginurl.then(function(msg){
		    var is_error=msg.data.error;
				if(!is_error)
				{
				console.log(msg.data); 
				scope.alerts[0].type='success';
				scope.alerts[0].msg=msg.data.message;
				$location.path('/access/verifyotp');
				}
				else
				{
				scope.alerts[0].type='warning';
				scope.alerts[0].msg=msg.data.message;
				$location.path('/access/adminlogin');
				}
			});
		},
		verify_otp:function(data,scope)
		{
			console.log('OTP Verification Here');
			var userdata={};
			var $signupurl=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/LoginWebService/verify_otp',data);
			$signupurl.then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					$cookieStore.put('AID',msg.data.data);
				    $location.path('/admin/dashboard');
				}
				else
				{
				scope.alerts[0].type='warning';
				scope.alerts[0].msg=msg.data.message;
				$location.path('/access/adminlogin');
				}
			});
		},
        verify_userotp:function(data,scope)
		{
			console.log('OTP Verification Here');
			var userdata={};
			var $signupurl=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/LoginWebService/verify_userotp',data);
			$signupurl.then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
				    $location.path('/access/login');
				}
				else
				{
				scope.alerts[0].type='warning';
				scope.alerts[0].msg=msg.data.message;
				$location.path('/access/login');
				}
			});
		},
		authenticate_user:function(data,scope)
		{
			$('#resetPassword :input').prop('disabled', true);
			var $loginurl=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/LoginWebService/authenticate_forgetpass_user',data);
			$loginurl.then(function(msg){
			if(msg.error){
			$('#resetPassword :input').prop('disabled', true);	
			$('._con_load').show();	
			}else{
			$('#resetPassword :input').prop('disabled', false);	
			$('._con_load').hide();
			scope.RPauthToken = msg.data.data.oauthID;
			}
			});
		},
		logout:function(){
			store.get('token');
			
			store.remove('token');
			$location.path('/login');	
			
			
		},
		updatesession:function()
		{
		 	var $SessionData=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/LoginWebService/is_logged_in');
			$SessionData.then(function(session){
			if(session.data!='' && session.data!=0)
			{
				var $checkSession = $cookieStore.get('loginData');
				var $checkSession1 = $cookieStore.get('AID');
				if($checkSession && (!$checkSession1))
				{
					return $checkSession.RegId;	
				}
				else if((!$checkSession) && ($checkSession1))
				{
					return $checkSession1.user_id;
				}
				else
				{
				$cookieStore.remove('loginData');
				$cookieStore.remove('AID');
				}
			}
			else
			{
			$cookieStore.remove('loginData');
			$cookieStore.remove('AID');
			//$location.path('/access/front');
			}
			});
		},
		
		islogged:function()
		{
				var $checkSession = $cookieStore.get('loginData');
				var $checkSession1 = $cookieStore.get('AID');
				if($checkSession && (!$checkSession1))
				{
					return $checkSession.RegId;	
				}
				else if((!$checkSession) && ($checkSession1))
				{
					return $checkSession1.user_id;
				}
				else
				{
					$cookieStore.remove('loginData');
					$cookieStore.remove('AID');
					return 0;
				}		
		},
		forgetpassword:function(){
			console.log('Forget Password Here');
		},
		getRole:function(){
		var role = $cookieStore.get('loginData');
		if(role) return role.MemberRole;		
		},
		getMemberContactNo:function(){
		var role = $cookieStore.get('loginData');
		if(role) return role.MemberContactNo;		
		}
	}
}]);