'use strict';
app.factory('actionService',['$http','$location','jwtHelper','store',function($http,$location,jwtHelper,store){	
    var $base_url=BASE_URL;
    return{
        /////////////////   Get Margin for Operator according Slab Here ///////////////////////////


        userOrder : function(data,scope)
        {
            $http({
                url:$location.protocol() + '://' +$location.host()+'/'+$base_url+'/actionWebService/userOrder',
                method: "POST",
                skipAuthorization:false,
                headers: { "Content-Type": 'application/x-www-form-urlencoded' },
                data: {regid : data}
            })
                .then(function(msg) {
                var is_error=msg.error;
                if(!is_error)
                {
                    scope.orderHistory = msg.data.data;
                }
                else
                {
                    $location.path('/login');
                }
            });		
        },

        /////////////SERVICE MENU ////////////////////

        stripfun(html){
            var tmp = document.createElement('DIV');
            tmp.innerHTML = html;
            return tmp.textContent || tmp.innerText || "";
        }

    }
}]);