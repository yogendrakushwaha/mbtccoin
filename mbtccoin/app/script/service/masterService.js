'use strict';
app.factory('masterService',function($http,$location){
	var $base_url=BASE_URL;
	return{
		getAllRole:function(data,scope)
		{
			console.log('Get AllCategory Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/masterWebService/role',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		/////////////////--------------------   FOR MASTER SERVICE----------------------///////////////////////////
		getAllService:function(data,scope)
		{
			console.log('Get AllService Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/service',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data1=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		/////////////////--------------------   FOR MASTER SERVICE Type----------------------///////////////////////////
		getAllServiceType:function(data,scope)
		{
			console.log('Get AllService Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/servicetype',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
						
		/////////////////---------------   For MASTER OPERATOR------------------------///////////////////////////
		
		getAllOperator:function(data,scope)
		{
			console.log('Get AllOperator Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/operator',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.opdata=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
						
		/////////////////---------------   For MASTER OPERATOR------------------------///////////////////////////
		
		getOperatorById:function(data,scope)
		{
			console.log('Get op by id Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/operator',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					if(msg.data.is_edit){
					scope.operator.service = msg.data.userDATA[0].ServiceTypeId;
					scope.operator.operatorName = msg.data.userDATA[0].BrandName;
					scope.operator.operatorCode = msg.data.userDATA[0].BrandCode;
					scope.operator.status = msg.data.userDATA[0].BrandStatus;
					scope.operator.id = msg.data.userDATA[0].TelecomBrandId;
						console.log(scope.operator);
				 
				}
				}
				 
			});
		},
		
		/////////////////---------------   For MASTER CANCELLATION POLICY------------------------///////////////////////////
		
		getAllCancellationPolicy:function(data,scope)
		{
			console.log('Get AllOperator Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/cancellation_rates',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{	
					if(msg.data.is_edit){  
					userdata=msg.data.userDATA;	
					scope.cancelPolicy.name=userdata[0].CPolicyName;		
					scope.cancelPolicy.value=userdata[0].CPolicyPercent;		
					scope.cancelPolicy.status=userdata[0].CPolicyStatus;		
					scope.cancelPolicy.CPolicyId=userdata[0].CPolicyId;		
					}else{
					userdata=msg.data.userDATA;	
					scope.data=userdata;	
					}
					 
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		/////////////////---------------   For MASTER MESSAGES FOR ACTIVITY LIKE SERVICE'USE ,SIGN UP ,LOGIN /------------------------///////////////////////////
		getAllMessages:function(data,scope)
		{
			console.log('Get AllOperator Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/messages',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					if(msg.data.is_edit){  
					userdata=msg.data.userDATA;	
					scope.message.title=userdata[0].MessageTitle;		
					scope.message.subject=userdata[0].MessageSubject;		
					scope.message.status=userdata[0].MessageStatus;		
					scope.message.MessageId=userdata[0].MessageId;		
					tinymce.get('message_content').setContent(userdata[0].MessageContent);
					}else{
					userdata=msg.data.userDATA;	
					scope.data=userdata;	
					}
				}
				
				
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		
		
		/////////////////-----------------   For MASTER NEWS--------------------- ///////////////////////////
		
		getAllNews:function(data,scope)
		{
			console.log('Get AllNews Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/news',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
						
		/////////////////--------------------   For MASTER BANK-------------------------- ///////////////////////////
		
		getAllBank:function(data,scope)
		{
			console.log('Get AllBank Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/bank',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		
						
		/////////////////-------------------   For MASTER CITY------------------------ ///////////////////////////
		
		getAllCity:function(data,scope)
		{
			console.log('Get AllCity Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/city',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
												
		/////////////////-------------------   For MASTER LOCALITY------------------------  ///////////////////////////
		
		getAllLocality:function(data,scope)
		{
			console.log('Get AllLocality Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/locality',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
						
		/////////////////-------------------   For MASTER MENU----------------------  ///////////////////////////
		
		getAllMenu:function(data,scope)
		{
			console.log('Get AllMenu Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/menu',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		
		
		/////////////////-----------------   For MASTER SUBMENU-------------------  ///////////////////////////
	
	getAllSubMenu:function(data,scope)
		{
			console.log('Get AllMenu Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/submenu',data);
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(is_error=='success')
				{
					 userdata=msg.data.userDATA;	
					 scope.data=userdata;
					 //scope.catModel=userdata;
				}
				else
				{
					scope.actionInfo.errormsg=msg.data.message;
					scope.actionInfo.msgclass='alert alert-block alert-danger fade in';
					scope.actionInfo.errortype='Error!';
					scope.actionInfo.type='ti-close';
				}
			});
		},
		get_site_configuration:function(scope)
		{
			console.log('Get site config Here');
			var userdata={};
			var $url=$http.get($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_site_configuration');
			$url.then(function(msg){
				var is_error=msg.data.error;
				if(!is_error)
				{
					 scope.siteConfig=msg.data.data; 
				}
			});
		},
		getSiteConfigurationByID:function(data,scope)
		{
			console.log('Get site config By Id Here');
			var userdata={};
			var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/getSiteConfigurationByID',data);
			$url.then(function(msg){

				var is_error=msg.data.error;
				if(!is_error)
				{
					 scope.siteConfigData.sitename=msg.data.data.configName; 
					 scope.siteConfigData.sitevalue=msg.data.data.configValue; 
					 scope.siteConfigData.sitedesc=msg.data.data.configDesc; 
					 
				}
			});
		},
	    updateConfigureData:function(data,scope)
	    {
	    	console.log(data);
	    	var $url=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/updateConfigSiteData',data);
	    },
	    get_qrcode:function(data,scope)
        {
            var $get_qrcode_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_qrcode_data/');

            $get_qrcode_data.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                        console.log("success");
                        scope.qrcodeData=response.data;
                 

                }
                else{
                        console.log("failed");
                        //$location.path('/varify_otp');
                }
        });
        },
        generate_qrcode:function(data,scope)
        {
            var $generate_qrcode=$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/generateqrcode/',data);
             $generate_qrcode.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                         scope.alerts = [
                   { type: 'success', msg: response.data.message }
                    ];
                    scope.get_qrcode_data();
                       // scope.messageData=response.data;
                 

                }
                else{
                        scope.alerts = [
                   { type: 'danger', msg: response.data.message }
                    ];
                        //$location.path('/varify_otp');
                }
        });
        },
        assign_qrcode:function(data,scope)
        {   console.log(data);
            var $assign_qrcode= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/assign_qrcode/',data);
            $assign_qrcode.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                    scope.assignsmsg = [
                   { type: 'success', msg: response.data.message }
                    ];
                    scope.get_qrcode_data();
                        //scope.assign.assignName=response.data.assignName;

                }
                else{
                        scope.assigsmsg = [
                   { type: 'danger', msg: response.data.message }
                    ];
                        //$location.path('/varify_otp');
                }
        });
        },
        get_user_data:function(data,scope)
        {    console.log(data);
             var $get_user_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_user_data/',data);
             $get_user_data.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {console.log('usersuccess');
                        scope.assign.assignName=response.data.data;

                }
                else{
                    scope.alerts = [
                   { type: 'danger', msg: response.data.data.message }
                    ];
                        //$location.path('/varify_otp');
                }
        });
        },
        getConfig:function(data,scope){
		var $get_config_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_site_config/');

		$get_config_data.then(function(response){
        	var is_error=response.data.error;

        	if(!is_error)
        	{
        		console.log("success");
                scope.configData=response.data;
        	}
        	else{
        		console.log("failed");
        		//$location.path('/varify_otp');
        	}
        });
	},
	addConfig:function(data,scope){
	        var $add_config_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/add_site_config/',data);
	        
	        $add_config_data.then(function(response){
        	var is_error=response.data.error;
        	if(!is_error)
        	{
        		 scope.alerts = [
                       { type: 'success', msg: response.data.message }
                        ];
                        scope.get_config();
        		//$cookieStore.put('loginData',msg.data.data);
        		//$location.path('/home');

        	}
        	else{
        		 scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
        		//$location.path('/varify_otp');
        	}
        });
	},
	get_sellerStatus_count:function(data,scope)
        {
            var $get_sellerStatus_count=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_sellerStatus_count/');
            $get_sellerStatus_count.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                 scope.activeSeller= response.data.activeCount;
                 scope.inactiveSeller= response.data.inactiveCount;
                       //scope.sellers=response.data;

                }
                else{
                   
                        //$location.path('/varify_otp');
                }
        });
        },
        get_seller_data:function(data,scope)
        {
          var $get_seller_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_seller_data/');
          $get_seller_data.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {       
                        scope.sellers=response.data;
                }
                else{

                    scope.alerts = [
                   { type: 'danger', msg: response.data.message }
                    ];
                        //$location.path('/varify_otp');
                }
        });
        },
        change_seller_status:function(data,scope)
        {
            var $change_seller_status= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/change_seller_status/',data);
            $change_seller_status.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                  scope.alerts = [
                   { type: 'success', msg: response.data.message }
               ];
               scope.get_seller_data();
               scope.get_sellerStatus_count();
                       //scope.sellers=response.data;

                }
                else{
                        scope.alerts = [
                   { type: 'danger', msg: response.data.message }
               ];
                        //$location.path('/varify_otp');
                }
        });
        },
        get_userStatus_count:function(data,scope)
        {
             var $get_userStatus_count= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_userStatus_count/');
             $get_userStatus_count.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                 scope.activeUser= response.data.activeCount;
                 scope.inactiveUser= response.data.inactiveCount;
                       //scope.sellers=response.data;

                }
                else{
                   
                        //$location.path('/varify_otp');
                }
        });


        },
        get_alluser_data:function(data,scope)
        {
            var $get_alluser_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_alluser_data/');
             $get_alluser_data.then(function(response){
                var is_error=response.data.error;
                if(!is_error)
                    {        
                        scope.alluser=response.data;
                }
                else{
                        scope.alerts = [
                   { type: 'danger', msg: response.data.message }
                    ];
                        //$location.path('/varify_otp');
                }
        });
        },
        change_user_status:function(data,scope)
        {   
            var $change_user_status= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/change_user_status/',data);
            $change_user_status.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                        scope.alerts = [
                   { type: 'success', msg: response.data.message }
                    ];
                    scope.get_alluser_data();
                    scope.get_userStatus_count();
                        //scope.assign.assignName=response.data.assignName;

                }
                else{
                        scope.alerts = [
                   { type: 'danger', msg: response.data.message }
                    ];
                        //$location.path('/varify_otp');
                }
        });
        },
        add_user:function(data,scope)
        {
        var $add_user=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/add_user/',data);
        $add_user.then(function(response){
            var is_error=response.data.error;

                if(!is_error)
                {
                    scope.alerts = [
                   { type: 'success', msg: response.data.message }
               ];
                }
                else
                {
                   scope.alerts = [
                   { type: 'success', msg: response.data.message }
               ];
                }

        });
        },
        getmessage:function(data,scope)
        {
          var $get_message_data= $http.post($location.protocol() + '://' +$location.host()+'/'+$base_url+'/MasterWebService/get_message/');

          $get_message_data.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                        //$cookieStore.put('loginData',msg.data.data);
                        //$location.path('/home');
                        //console.log(response.data);
                        scope.messageData=response.data;
                        //$scope.configdata = msg;

                }
                else{
                        console.log("failed");
                        //$location.path('/varify_otp');
                }
        });
        },
        add_config_message:function(data,scope)
        {    
            var $add_message=$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/add_message/',data);

            $add_message.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                {
                        scope.alerts = [
                       { type: 'success', msg: response.data.message }
                        ];
                        scope.get_message();
                       // scope.messageData=response.data;
                 

                }
                else{
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
                        //$location.path('/varify_otp');
                }
        });
        },
        fetch_category:function(data,scope)
        {
        	var $fetch_category=$http.post($location.protocol() + '://' +$location.host()+'/'+$base_url + '/MasterWebService/fetch_category/');
        	$fetch_category.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                { 
                  scope.categoryData=response.data;
                  
                }
                else{
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
                        //$location.path('/varify_otp');
                }
        });
        },
        deleteCategory:function(data,scope)
        { console.log(data);
        	var $deleteCategory=$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/deleteCategory/',data);
        	$deleteCategory.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                { 
                   scope.alerts = [
                       { type: 'success', msg: 'successfully deleted!' }
                        ];
                }
                else{
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
                        //$location.path('/varify_otp');
                }
        });
        },
           updateseller:function(data,scope)
        {
        	var $updateseller =$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/updateseller/',data);
        	$updateseller.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                { 

                   scope.alerts = [
                       { type: 'success', msg: 'successfully deleted!' }
                        ];
                        scope.seller='';
                }
                else{
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
                        //$location.path('/varify_otp');
                }
        });

        },
        fetch_seller_data:function(data,scope)
        { 
        	var $fetch_seller_data =$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/fetch_seller_data/',data);
        	$fetch_seller_data.then(function(response){
                
                var is_error=response.data.error;

                if(!is_error)
                { 
               			scope.seller=response.data[0];
                }
                else
                {
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }];
                }
        });

        },
        fetch_subcategory:function(data,scope)
        {
        	var $fetch_subcategory =$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/fetch_subcategory/',data);
        	$fetch_subcategory.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                { 
                   scope.brand=response.data;
                  
                }
                else{
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
                        //$location.path('/varify_otp');
                }
        });
        },
        insert_brand:function(data,scope)
        {console.log(data);
        	var $insert_brand =$http.post($location.protocol() + '://' +$location.host() +'/'+$base_url + '/MasterWebService/insert_brand/',data);
        	$insert_brand.then(function(response){
                var is_error=response.data.error;

                if(!is_error)
                { 
                   console.log(response);
                  
                }
                else{
                        scope.alerts = [
                       { type: 'danger', msg: response.data.message }
                        ];
                        //$location.path('/varify_otp');
                }
        });
        }
	
	}

});