app.controller('AppCtrl', function ($scope,$timeout, $mdSidenav) {

 $scope.app = {
     facebook_link:facebook_link,
	 twitter_link:twitter_link,
	 gplus_link:gplus_link,
	 linkedin_link:linkedin_link,
	 copyright_link:copyright_link,
	 SITE_NAME 	:SITE_NAME,
     BASE_URL   :BASE_URL,
	 EMAIL:EMAIL,
	 CONTACT_NO:CONTACT_NO,
	 DEFAULT_PAGE:DEFAULT_PAGE
      }

    
  }).config(function($mdThemingProvider) {
    // Configure a dark theme with primary foreground yellow
    $mdThemingProvider.theme('docs-dark', 'default').primaryPalette('yellow').dark();
  

});
 