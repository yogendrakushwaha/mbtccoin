app.controller('catalogCtrl', function ($scope, $timeout, $mdSidenav,$mdToast,$mdDialog) {
	
	
	/*$scope.alerts = [
	    { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
	    { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
		];
		
		$scope.addAlert = function() {
	    $scope.alerts.push({msg: 'Another alert!'});
	};*/
	
	$scope.closeAlert = function(index) {
	    $scope.alerts.splice(index, 1);
	};
    $scope.add_product = function(form,ev) {
    	console.log(form);
		var firstError = null;
		if (form.$invalid) 
		{
			var field = null, firstError = null;
			for (field in form) 
			{
				if (field[0] != '$')
				{
					if (firstError === null && !form[field].$valid) 
					{
						firstError = form[field].$name;
					}
					if (form[field].$pristine) 
					{
						form[field].$dirty = true;
					}
				}
			}
			angular.element('.ng-invalid[name=' + firstError + ']').focus();
			return;
			}else {
			var confirm = $mdDialog.confirm()
			.title('Add New Category')
			.textContent('Would you like to add new categoty ?')
			.ariaLabel('Add Category')
			//.targetEvent(ev)
			.ok('YES')
			.cancel('NO');
			$mdDialog.show(confirm).then(function() {
				$scope.$watch('files.length',function(newVal,oldVal){
					console.log($scope.files);
					if(form.mrp <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid MRP !' }
						];
						return form.msp = "";
						}else if(form.mrp <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid MSP !' }
						];
						return form.msp = "";
					}
					else if(form.inventory <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid Inventory !' }
						];
						return form.msp = "";
					}
					else if(form.shippingtime <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid Shipping Time !' }
						];
						return form.msp = "";
					}
					else if(form.height <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid Height !' }
						];
						return form.msp = "";
					}
					else if(form.width <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid Width !' }
						];
						return form.msp = "";
					}
					else if(form.length <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid Length !' }
						];
						return form.msp = "";
					}
					else if(form.weight <= 0){
						$scope.alerts = [
							{ type: 'danger', msg: 'Please enter a valid Weight !' }
						];
						return form.msp = "";
					}
				});
				
			    }, function() {
				//$scope.status = 'You decided to keep your debt.';
				//console.log('not add new categoty');
			});
		}
		
	};
	
	$scope.check_mrp= function(data,ev){
		if(data.mrp < data.msp){
			$scope.alerts = [
				{ type: 'danger', msg: 'Maximum Selling Price must be less then Maximum Retail Price !' }
			];
		}
	}
	
	$scope.userOrder = function(){
		userService.userOrder($scope);
	};
});
