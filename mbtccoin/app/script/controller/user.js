'use strict';
/** 
	* controllers used for the 
*/
app.controller('UserCtrl',["$scope","$timeout",'$mdToast','$mdDialog','$http','$location','userService','$stateParams','$state','jwtHelper','store',function($scope,$timeout,$mdToast,$mdDialog,$http,$location,userService,$stateParams,$state,jwtHelper,store){

	var $base_url=BASE_URL;
	var token=store.get('token') || 0;
	if(!token)
	{

		$location.path("/login");
	}
	else
	{

		var tokenPayload=jwtHelper.decodeToken(token);
		$scope.MemberContactNo = tokenPayload.CONTACTNO; 
		$scope.Regid = tokenPayload.REGID; 
	}

	$scope.clickUpload = function(data){

		if(data =='MemberImage')
		{
			angular.element('#MemberImage').trigger('click');
		}
		else if(data == 'addressProof')
		{
			angular.element('#addressProof').trigger('click');
		}
		else{
			angular.element('#idProof').trigger('click');
		}

	};

	$scope.exportToExcel=function(tableId,name){ // ex: '#my-table'
		var exportHref=Excel.tableToExcel(tableId,name);
		$timeout(function(){location.href=exportHref;},100); // trigger download
	};



	// If logged in the user id will be return




	$scope.get_profile_data = function()
	{   
		$scope.user={'Mobileno': $scope.MemberContactNo };
		userService.getuser($scope.user,$scope);
	};

	$scope.data_bank = function()
	{
		console.log(userService.get_bankname());
	};
	//
	var formData = new FormData();
	$scope.files = [];
	$scope.LoadFileData = function (files)
	{  

		$scope.files.push(files[0]);
		console.log(files);
	};


	$scope.today = function() {
		$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function () {
		$scope.dt = null;
	};

	// Disable weekend selection
	$scope.disabled = function(date, mode) {
		return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.toggleMin = function() {
		$scope.minDate = $scope.minDate ? null : new Date();
	};
	$scope.toggleMin();
	$scope.open = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened = true;
	};
	$scope.open1 = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened1 = true;
	};
	$scope.dateOptions = {
		formatYear: 'yyyy',
		formatMonth: 'MM',
		startingDay: 0,
		class: 'datepicker'
	};
	$scope.dateOptions1 = {
		formatYear: 'yyyy',
		formatMonth: 'MM',
		startingDay: 0,
		class: 'datepicker'
	};
	$scope.initDate = new Date('20-06-2017');
	$scope.formats = ['dd-MM-yyyy'];
	$scope.format = $scope.formats[0];



	$scope.alerts={ 'profile' : { 
		'type' : 'info',
		'msg'   : 'Edit your profile!' 
	},
				   'cp' : { 
					   'type' : 'info',
					   'msg'   : 'Change your password!' 
				   },'setting' : { 
					   'type' : 'info',
					   'msg'   : 'Change your password!' 
				   },
				   'pm' : { 
					   'type' : 'info',
					   'msg'   : 'Add new primary number' 
				   },
				   'wallet' : { 
					   'type' : 'info',
					   'msg'   : 'TRANSFER TO ANOTHER WALLET' 
				   },
				   'dorecharge' : { 
					   'type' : 'info',
					   'msg'   : '' 
				   },
				   'dmtforgot' : { 
					   'type' : 'info',
					   'msg'   : 'Recover your password here!' 
				   },
				   'dmtlogin' : { 
					   'type' : 'info',
					   'msg'   : 'Domestic Money Transfer User Login Here!' 
				   },
				   'dmt' : { 
					   'type' : 'info',
					   'msg'   : 'Domestic Money Transfer User Registration Here!' 
				   },
				   'Surcharge' : { 
					   'type' : 'info',
					   'msg'   : 'Distribution Of Surcharge According To Designation Here!' 
				   },
				   'Pancardinfo' : { 
					   'type' : 'info',
					   'msg'   : 'Pan Card Registration Here!' 
				   },
				   'Bankdetails' : { 
					   'type' : 'info',
					   'msg'   : 'Update Your Bank Details!' 
				   }
				  };
	$scope.userInfo ={
		'FirstName' : '',
		'LastName' : '',
		'Email' : '',
		'oldPass' : '',
		'UserId' :$scope.userId,
	};
	$scope.user ={
		'balance' : '',
		'MaintainBalance':'',
		'MiscelleanousBalance':''
	};

	$scope.staticContent = {
		'Title' : '',
		'Content' : ''
	};

	$scope.newuserreg = [
		{ type: 'info', msg: 'New User Regsitration! Fill the basic Details of user ' }
	];
	$scope.closeAlert = function(index) 
	{
		$scope.newuserreg.splice(index, 1);
	};
	$scope.showToast = function(msg) {

		var toast = $mdToast.simple()
		.textContent(msg)
		.hideDelay('8000')
		.action('OK')
		.highlightAction(true)
		.highlightClass('md-accent')
		.position('top right' );
		$mdToast.show(toast).then(function(response) {
			if ( response == 'ok' ) {

			}
		});
	};
	$scope.closeToast = function() {
		$mdToast.hide();
	};

	//*********Slab Operator Margin  *************//
	$scope.get_SlabMargin=function(){
		$scope.pp={
			id:1,
			status:1
		};
		console.log($scope.pp);
		userService.get_SlabMargin($scope.pp,$scope);
	}; 



	/////////  New Registration Here
	$scope.regInfo = {
		IsProf:'Yes',
		Status:'Active',
		Referral:$scope.MemberContactNo
	};
	$scope.alert = '';
	// Add/Edit User
	$scope.add_user = function (form,ev) 
	{
		var firstError = null;
		if (form.$invalid) 
		{
			var field = null, firstError = null;
			for (field in form) 
			{
				if (field[0] != '$')
				{
					if (firstError === null && !form[field].$valid) 
					{
						firstError = form[field].$name;
					}
					if (form[field].$pristine) 
					{
						form[field].$dirty = true;
					}
				}
			}
			angular.element('.ng-invalid[name=' + firstError + ']').focus();
			return;
		} 
		else 
		{
			var confirm = $mdDialog.confirm()
			.title('Would you like to Add User?')
			.content('All of the details are correct.')
			.ariaLabel('New User')
			.ok('Save')
			.cancel('Cancel')
			.targetEvent(ev);
			$mdDialog.show(confirm).then(function() {
				$scope.regInfo.Id=0;
				$http({
					url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/EnrollWebService/user_regsitration",
					method: "POST",
					headers: { "Content-Type": undefined },
					transformRequest: function(data) {
						var formData = new FormData();
						formData.append("Enroll", angular.toJson($scope.regInfo));
						for (var i = 0; i < data.files.length; i++) {
							formData.append("img[" + i + "]", data.files[i]);
						}
						return formData;//NOTICE THIS RETURN WHICH WAS MISSING
					}, 
					data: { Enroll: $scope.regInfo, files: $scope.files }
				})
					.success(function(response) {

					$scope.newuserreg.type=response.error;
					$scope.newuserreg.msg=response.message;
					if(!response.error)
					{	
						$scope.regInfo.Agree= undefined;
						$scope.regInfo={};
						$scope.files={};
						form.$setPristine();
						form.$setUntouched();
						$location.path('/app/user');
					}

				});
			}, function() {
				$scope.alert = 'Cancel';
				$scope.newuserreg.type='alert';
				$scope.newuserreg.msg="User cancel the action.";
				$scope.regInfo.Agree = undefined;
				$scope.files={};
				form.$setPristine();
				form.$setUntouched();
			});
		}
	};






	////////////////******* Start Personal Details Here!  ********************************/////////////////////////////
	$scope.getbalance = function() {
		userService.getbalanceByID({'MemberId': $scope.userId},$scope);
	}
	// Get User 
	$scope.getUser = function(){
		$scope.user={'Mobileno': $scope.MemberContactNo}; 
		userService.getuser($scope.user,$scope);	

	};
	// Check User
	$scope.checkuser =function(form,phone){
		if(phone){
			userService.getuserByPhone({'phoneNumber': phone},form,$scope);	
		}
	};
	
	$scope.icotokenata = {};
	$scope.getslottoken = function(){
		userService.getslottoken($scope);
	};
	
	$scope.user={'btcAddress':'','BTCAmount':''};
	$scope.getAdminBTCAdrress = function(){
		userService.getAdminBTCAdrress($scope);
	};
	
	$scope.usdtobtc = function(){
		userService.usdtobtc($scope.user,$scope);
	};
	
	$scope.buytoken = function(){
		$scope.user.regid = $scope.Regid;
		userService.buytoken($scope.user,$scope);
	};
	
	$scope.userOrder = function(){
		userService.userOrder($scope.Regid,$scope);
	};

	//Edit User 
	$scope.editUser = function (form,ev) 
	{ 
		var confirm = $mdDialog.confirm()
		.title('Would you like edit profile?')
		.content('All of the details are correct.')
		.ariaLabel('Edit your profile')
		.ok('Update')
		.cancel('Cancel')
		.targetEvent(ev);
		$mdDialog.show(confirm).then(function() {

			$http({
				url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/UserprofileWebService/edit_user",
				method: "POST",
				headers: { "Content-Type": undefined },
				transformRequest: function(data) {
					var formData = new FormData();
					formData.append("UserData", angular.toJson($scope.userInfo));
					formData.append("img", data.files[0]);
					return formData;//NOTICE THIS RETURN WHICH WAS MISSING
				},
				data: { 'UserData': $scope.userInfo, files: $scope.files }
			})
				.success(function(response) {

				if(!response.error)
				{
					$scope.alerts.profile.type='success';
					$scope.alerts.profile.msg=response.message;
				}

			});
		}, function() {
			$scope.alert = 'Cancel';
			$scope.alerts[0].type='alert';
			$scope.alerts.profile.msg="User cancel the action.";
			$scope.regInfo.Agree = undefined;
			form.$setPristine();
			form.$setUntouched();
		});

	};	

	//Edit Password
	$scope.editpassword = function (form,ev) 
	{   
		var that = form;
		var firstError = null;

		var confirm = $mdDialog.confirm()
		.title('Would you like to change your password?')
		.content('All of the details are correct.')
		.ariaLabel('Change your password')
		.ok('Change')
		.cancel('Cancel')
		.targetEvent(ev);
		$mdDialog.show(confirm).then(function() {
			$scope.userPassInfo.UserId=$scope.MemberContactNo;
			$http({
				url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/UserprofileWebService/change_password",
				method: "POST",
				data: { 'UserPassData': $scope.userPassInfo}
			})
				.success(function(response) {
				console.log(response);
				$scope.alerts.setting.msg = response.message;	

				if(!response.error)
				{
					$scope.alerts.setting.msg = response.message;	
					$scope.alerts.setting.type = 'success';
				}
				else
				{
					$scope.alerts.setting.msg = response.message;
					$scope.alerts.setting.type = 'danger';	
				}

			});
		}, function() {
			$scope.alert = 'Cancel';
			$scope.alerts[0].type='alert';
			$scope.alerts.profile.msg="User cancel the action.";
			$scope.regInfo.Agree = undefined;
			form.$setPristine();
			form.$setUntouched();
		});
	};

	// Forget password

	$scope.setForgotPasswod = function (form,ev) 
	{  

		var firstError = null;
		if (form.$invalid) 
		{
			var field = null, firstError = null;
			for (field in form) 
			{
				if (field[0] != '$')
				{
					if (firstError === null && !form[field].$valid) 
					{
						firstError = form[field].$name;
					}
					if (form[field].$pristine) 
					{
						form[field].$dirty = true;
					}
				}
			}
			angular.element('.ng-invalid[name=' + firstError + ']').focus();
			return;
		} 
		else 
		{	

			var confirm = $mdDialog.confirm()
			.title('Would you like to retrive your password?')
			.content('All of the details are correct.')
			.ariaLabel('Forgot Passwod')
			.ok('Save')
			.cancel('Cancel')
			.targetEvent(ev);
			$mdDialog.show(confirm).then(function() {
				userService.forgotPasswod({ 'FormData': $scope.ForPassModal},$scope);
			}, function() {
				$scope.alert = 'Cancel';
				$scope.alerts[0].type='alert';
				$scope.alerts.profile.msg="User cancel the action.";
				$scope.regInfo.Agree = undefined;
				form.$setPristine();
				form.$setUntouched();
			});
		}
	};
	////////////////******* End Personal Details Here!  ********************************/////////////////////////////



	////////////////******* Start Wallet Transaction Here!  ********************************/////////////////////////////

	////////////////******* 
	//Wallet or Add Money
	//Upgarde Wallet
	//Transfer to any Wallet 
	//Transfer to Team Wallet
	////////////////*******

	// Get User KYC Details 
	$scope.get_kyc = function() {
		$scope.member={'MemberId': $scope.userId}; 
		userService.get_user_kyc($scope.member,$scope);
	};


	////////////////*******
	// Get All Static Data 
	//About Us,
	//Contact Us,
	//Term of Use,
	//Privacy Policy 
	//Help Desk 
	////////////////*********

	// Get About Us Data
	$scope.getaboutdata = function(){
		userService.getaboutdata($scope);
	};
	// Get Contact Us Data
	$scope.getContactUsdata = function(){
		userService.getContactUsdata($scope);
	};
	// Get Contact Us Data
	$scope.getComingSoondata = function(){
		userService.getComingSoondata($scope);
	};
	// Get Privacy Policy Data
	$scope.getPrivacyPolicydata = function(){
		userService.getPrivacyPolicydata($scope);
	};
	// Get Terms and Condition Data
	$scope.getTermandConditiondata = function(){
		userService.getTermandConditiondata($scope);
	};
	// Get Refund Policy Data
	$scope.getRefundPolicydata = function(){
		userService.getRefundPolicydata($scope);
	};
	// Get FAQ Data
	$scope.getFAQdata = function(){
		userService.getFAQdata($scope);
	};
	// Get Payment Options Data
	$scope.getPaymentOptionsdata = function(){
		userService.getPaymentOptionsdata($scope);
	};
	// Get Support Data
	$scope.getSupportdata = function(){
		userService.getSupportdata($scope);
	};
	// Get Press Realease Data
	$scope.getPressRealeasedata = function(){
		userService.getPressRealeasedata($scope);
	};
	// Get Press Realease Data
	$scope.getBlogdata = function(){
		userService.getBlogdata($scope);
	};

	// Get Career Data
	$scope.getCareerdata = function(){
		userService.getCareerdata($scope);
	};
	// Get OUR TeamData
	$scope.getOurTeamdata = function(){
		userService.getOurTeamdata($scope);
	};

	////////////////******* End Statc Data Here!  ********************************/////////////////////////////


	////////////////*******
	// CMS Admin And White Label 
	//About Us,
	//Contact Us,
	//Term of Use,
	//Privacy Policy 
	//Help Desk 
	////////////////*********




	$scope.get_cms_content = function() {
		$scope.contents={};
		$scope.member={'MemberId': $scope.userId}; 
		userService.get_cms_content_by_id($scope.member,$scope);
	};

	$scope.getContent = function() {
		$scope.member={'MemberId': $scope.userId,'SC_ID': $stateParams.ID}; 
		userService.getCmsContent($scope.member,$scope);
	};


	$scope.StaticcontentAlert =  
		{ 
		type: 'info', msg: 'Edit content..' };
	$scope.saveContent = function (form,ev) 
	{  

		var confirm = $mdDialog.confirm()
		.title('Would you like to edit content?')
		.content('All of the details are correct.')
		.ariaLabel('Update cms')
		.ok('Save')
		.cancel('Cancel')
		.targetEvent(ev);
		$mdDialog.show(confirm).then(function() {
			$scope.staticContent.MemberId= $scope.userId;
			$scope.staticContent.SC_ID= $stateParams.ID;
			$scope.staticContent.Content= tinyMCE.get('content').getContent();
			userService.saveStaticContent({ 'Content': $scope.staticContent},$scope);
		}, function() {
			$scope.alert = 'Cancel';
			$scope.alerts[0].type='alert';
			$scope.alerts.profile.msg="User cancel the action.";
			$scope.regInfo.Agree = undefined;
			form.$setPristine();
			form.$setUntouched();
		});
	};
	////////////////******* End CMS Here!  ********************************/////////////////////////////

	$scope.logout = function() {
		userService.logout($scope);
	};







	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.answer = function(answer) {
		$mdDialog.hide(answer);
	};


	$scope.showChangePassword = function(ev) {
		$mdDialog.show({
			templateUrl: 'app/Views/User/forget_pass.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
		})
			.then(function(answer) {
			$scope.status = 'You said the information was "' + answer + '".';
			console.log($scope.status);
		}, function() {
			$scope.status = 'You cancelled the dialog.';
			console.log($scope.status);
		});
	};

	$scope.showProfileSettings = function(ev) {
		$mdDialog.show({
			templateUrl: 'app/Views/User/profile_settings.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
		})
			.then(function(answer) {
			$scope.status = 'You said the information was "' + answer + '".';
			console.log($scope.status);
		}, function() {
			$scope.status = 'You cancelled the dialog.';
			console.log($scope.status);
		});
	};





}]);