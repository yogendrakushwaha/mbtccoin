'use strict';
/** 
	* controllers used for the 
*/
app.controller('actionCtrl',["$scope","$timeout",'$mdToast','$mdDialog','$http','$location','actionService','$stateParams','$state','jwtHelper','store',function($scope,$timeout,$mdToast,$mdDialog,$http,$location,actionService,$stateParams,$state,jwtHelper,store){

    var $base_url=BASE_URL;
    var token=store.get('token') || 0;
    if(!token)
    {

        $location.path("/login");
    }
    else
    {
        var tokenPayload=jwtHelper.decodeToken(token);
        $scope.MemberContactNo = tokenPayload.CONTACTNO; 
        $scope.Regid = tokenPayload.REGID; 
    }

    

    $scope.exportToExcel=function(tableId,name){ // ex: '#my-table'
        var exportHref=Excel.tableToExcel(tableId,name);
        $timeout(function(){location.href=exportHref;},100); // trigger download
    };



    // If logged in the user id will be return



    //
    var formData = new FormData();
    $scope.files = [];
    $scope.LoadFileData = function (files)
    {
        $scope.files.push(files[0]);
        console.log(files);
    };

    $scope.alerts={ 'profile' : { 
        'type' : 'info',
        'msg'   : 'Edit your Action!' 
    },
                   'cp' : { 
                       'type' : 'info',
                       'msg'   : 'Change your password!' 
                   },'setting' : { 
                       'type' : 'info',
                       'msg'   : 'Change your password!' 
                   },
                   'pm' : { 
                       'type' : 'info',
                       'msg'   : 'Add new primary number' 
                   },
                   'wallet' : { 
                       'type' : 'info',
                       'msg'   : 'TRANSFER TO ANOTHER WALLET' 
                   },
                   'dorecharge' : { 
                       'type' : 'info',
                       'msg'   : '' 
                   },
                   'dmtforgot' : { 
                       'type' : 'info',
                       'msg'   : 'Recover your password here!' 
                   },
                   'dmtlogin' : { 
                       'type' : 'info',
                       'msg'   : 'Domestic Money Transfer User Login Here!' 
                   },
                   'dmt' : { 
                       'type' : 'info',
                       'msg'   : 'Domestic Money Transfer User Registration Here!' 
                   },
                   'Surcharge' : { 
                       'type' : 'info',
                       'msg'   : 'Distribution Of Surcharge According To Designation Here!' 
                   },
                   'Pancardinfo' : { 
                       'type' : 'info',
                       'msg'   : 'Pan Card Registration Here!' 
                   },
                   'Bankdetails' : { 
                       'type' : 'info',
                       'msg'   : 'Update Your Bank Details!' 
                   }
                  };
   
    $scope.closeAlert = function(index) 
    {
        $scope.newuserreg.splice(index, 1);
    };
    $scope.showToast = function(msg) {
        var toast = $mdToast.simple()
        .textContent(msg)
        .hideDelay('8000')
        .action('OK')
        .highlightAction(true)
        .highlightClass('md-accent')
        .position('top right' );
        $mdToast.show(toast).then(function(response) {
            if ( response == 'ok' ) {

            }
        });
    };
    
    $scope.userOrder = function(){
        actionService.userOrder($scope);
    };
    
    $scope.closeToast = function() {
        $mdToast.hide();
    };


}]);