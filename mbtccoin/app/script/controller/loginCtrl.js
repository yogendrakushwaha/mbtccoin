app.controller('LoginCtrl', function($scope, $mdDialog,$rootScope,$state,loginService,jwtHelper,store) 
{
   $scope.alerts = [
        { 
            type: 'info', msg: 'User Login Here! Fill the login Details of user ' 
        }
    ];

        $scope.closeAlert = function(index) 
          {
            $scope.alerts.splice(index, 1);
          };


        $scope.resetpassalerts = [
        { 
            type: 'info', msg: 'Enter new password.' 
        }
    ];
$scope.for_password=
{
email:"",
mobileno:""
};
    $scope.res_password =
    {
        'oauthID' : ''
    }



    $scope.login=function(loginInfo){
        loginService.login($scope.loginInfo,$scope);
    };
    $scope.logout=function(){
        loginService.logout();
    };



    $scope.forget_password=function(form,ev){ 
        var firstError = null;
        if (form.$invalid) {
            var field = null, firstError = null;
            for (field in form) {
                if (field[0] != '$') {
                    if (firstError === null && !form[field].$valid) {
                        firstError = form[field].$name;
                    }
                    
                    if (form[field].$pristine) {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
            console.log("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            return;
            
            } 
            else 
            {
            var confirm = $mdDialog.confirm()
            .title('Would you like change your password?')
            .content('All of the details are correct.')
            .ariaLabel('Change your password')
            .ok('Change')
            .cancel('Cancel')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
                $http({
                    url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/LoginWebService/forget_password",
                    method: "POST",
                    data:$scope.for_password,
                })
                .success(function(response) {
                    var type ='';
                    if(response.error){
                        type = 'danger';                    
                        }else{
                        type = 'success';
                        $("#ForgetPassword")[0].reset();
                         $cookieStore.put('Mobileno',$scope.for_password.mobileno);
                         $location.path('/access/verifyuserotp');
                    }
                    $scope.forgetpassalerts = [
                        {       
                            type:type,
                            msg: response.message 
                        }
                    ];
                });
                }, function() {
                $scope.alert = 'Cancel';
                $scope.alerts[0].type='alert';
                $scope.alerts.profile.msg="User cancel the action.";
                $scope.regInfo.Agree = undefined;
                form.$setPristine();
                form.$setUntouched();
            });
        }
        
    } // function ends here
    
    $scope.reset_password=function(form,ev){ 
        var firstError = null;
        if (form.$invalid) {
            var field = null, firstError = null;
            for (field in form) {
                if (field[0] != '$') {
                    if (firstError === null && !form[field].$valid) {
                        firstError = form[field].$name;
                    }
                    
                    if (form[field].$pristine) {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
            console.log("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
            return;
            
            } else {
            var confirm = $mdDialog.confirm()
            .title('Would you like change your password?')
            .content('All of the details are correct.')
            .ariaLabel('Change your password')
            .ok('Change')
            .cancel('Cancel')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {
                $scope.res_password.oauthID = $scope.RPauthToken;
                $http({
                    url: $location.protocol() + '://' +$location.host() +"/"+$base_url+"/LoginWebService/reset_password",
                    method: "POST",
                    data:$scope.res_password,
                })
                .success(function(response) {
                    var type ='';
                    if(response.error){
                        type = 'danger';                    
                        }else{
                        type = 'success';
                        $("#resetPass")[0].reset();
                    }
                    $scope.resetpassalerts = [
                        {       
                            type:type,
                            msg: response.message 
                        }
                    ];
                });
                }, function() {
                $scope.alert = 'Cancel';
                $scope.alerts[0].type='alert';
                $scope.alerts.profile.msg="User cancel the action.";
                $scope.regInfo.Agree = undefined;
                form.$setPristine();
                form.$setUntouched();
            });
        }
    } // function ends here
    
    
    
    
    $scope.closeAlert = function(index) 
    {
        $scope.alerts.splice(index, 1);
    };
    $scope.loginInfo={
        'Username':'',
        'Password':''
    };
    $scope.form = {
        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {
                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }
                        
                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }
                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                console.log("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;
                
                } 
                else 
                {
                    
                loginService.login($scope.loginInfo,$scope);
            }
            
        },
        reset: function (form) {
            
            $scope.loginInfo = angular.copy($scope.loginInfo);
            form.$setPristine(true);
            
        }
    };
  
    
    $scope.masterlogin = function(form,ev){
        var firstError = null;
        if (form.$invalid) 
        {
            var field = null, firstError = null;
            for (field in form) 
            {
                if (field[0] != '$')
                {
                    if (firstError === null && !form[field].$valid) 
                    {
                        firstError = form[field].$name;
                    }
                    if (form[field].$pristine) 
                    {
                        form[field].$dirty = true;
                    }
                }
            }
            angular.element('.ng-invalid[name=' + firstError + ']').focus();
            return;
        } 
        else 
        { 
            loginService.adminlogin($scope.masterloginInfo,$scope);  
        }
        
    };
    $scope.verifyotp = function(form,ev){
         
            loginService.verify_otp($scope.verifyInfo,$scope);   
    };
    $scope.verifyuserotp = function(form,ev){
           $scope.verifyInfo.MobileNo = $cookieStore.get('Mobileno');
            loginService.verify_userotp($scope.verifyInfo,$scope);   
    };
    $scope.authenticate_user = function(){
        $scope.pp={
            oauthToken:$stateParams.oauth_token,
        };
        loginService.authenticate_user($scope.pp,$scope);
    }
});


app.controller('SignupCtrl' , function($scope,$mdDialog,$rootScope,$state,signupService,store) 
{

$scope.alerts = [
                { type: 'info', msg: 'Fill User Details for Sign Up !' }
            ];
$scope.verifyalerts = 
    [
        { 
            type: 'info', msg: 'User Email Verify Here! Fill the OTP Details sent to your account.' 
        }
    ];
            $scope.closeAlert = function(index) 
            {
                $scope.alerts.splice(index, 1);
            };
            $scope.regInfo={
                'IsProf':'No',
        'Status':'Pending',
        'Role':'B2C'
    };


            $scope.registration = function(form ,ev) {

            var firstError = null;
            if (form.$invalid) 
            {
                
                     var field = null, firstError = null;
                        for (field in form) {
                            if (field[0] != '$') 
                            {
                                if (firstError === null && !form[field].$valid) 
                                {
                                    firstError = form[field].$name;
                                }
                                        
                                if (form[field].$pristine)
                                {
                                    form[field].$dirty = true;
                                }
                            }
                    }
                    angular.element('.ng-invalid[name=' + firstError + ']').focus();
                    console.log("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                    return;
            } 
            else
            {
                        var confirm = $mdDialog.confirm()
                        .title('Confirm Registration')
                        .textContent('Make sure all details are correct.')
                        .ariaLabel('Lucky day')
                        //.targetEvent(ev)
                        .ok('Register')
                        .cancel('Cancel');

                        $mdDialog.show(confirm).then(function() {
                            signupService.signup($scope.regInfo,$scope);
                            $scope.alerts = [
                                { type: 'success', msg: 'User Register Successfully!' }
                                 ];
                        }, function() {
                        $scope.alerts = [
                            { type: 'warning', msg: 'You have decided to cancel sign up !' }
                        ];
                        
                        });

            }
        };
$scope.form = {
        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {
                
                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }
                        
                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }
                
                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                console.log("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                
                return;
                
                } else {
                //your code for submit
               
                signupService.signup($scope.signupInfo,$scope);

                
            }
            
        },
        mailverify: function (form) {
            var firstError = null;
            if (form.$invalid) {
                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }
                        
                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }
                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                console.log("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;
                } 
                else
                {
                $scope.verifyInfo.Email =store.get('Email');
                $scope.verifyInfo.MobileNo =store.get('Mobileno');                           
                signupService.emailVerification($scope.verifyInfo,$scope);
            }
            
        },
        reset: function (form) {
            
            $scope.loginInfo = angular.copy($scope.loginInfo);
            form.$setPristine(true);
            
        }
    };


});