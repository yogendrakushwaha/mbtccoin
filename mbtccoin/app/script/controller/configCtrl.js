//alert("config controller loaded"); 
app.controller('configCtrl', function($scope, $mdDialog,masterService,$http,$location,$state,$stateParams) {
	$scope.sortType     = 'username'; // set the default sort type
  	$scope.sortReverse  = false;  // set the default sort order
  	$scope.searchUser   = '';     // set the default search/filter term

var $base_url=BASE_URL;
 
 $scope.clickUpload = function(data){
  
  if(data =='profileImage')
  {
  	angular.element('#profileImage').trigger('click');
  }
  else if(data == 'shopImage')
  {
  	 angular.element('#shopImage').trigger('click');
  }
  else{
    angular.element('#catImage').trigger('click');
    }

};

var formData = new FormData();
  $scope.files = [];
  $scope.LoadFileData = function (files,data)
  { console.log(files);
      $scope.files.push(files[0]);
  },

$scope.closeAlert = function(index) {
       $scope.alerts.splice(index, 1);
   },
   $scope.closeAssignmsg = function(index) {
       $scope.assignsmsg.splice(index, 1);
   },

  	$scope.get_users = function(){
  		$scope.users = [
  			{ 
	    		username: 	'suraj1524', 
	    		name: 		'Suraj', 
	    		shopname: 	'shopname 1', 
	    		email : 	'email@gmail.com 1', 
	    		products: 	'254', 
	    		balance: 	'15245', 
	    		phone_no: 	'9458741254', 
	    		pan: 		'125ESDX144', 
	    		gst: 		'12HGDV25416KJNMD',
	    		status: 	'Active' 
	    	},
	    	{ 
	    		username: 	'Aka0422', 
	    		name: 		'Akash', 
	    		shopname: 	'shopname 2', 
	    		email : 	'email@gmail.com 2', 
	    		products: 	'152', 
	    		balance: 	'5245', 
	    		phone_no: 	'8558741254', 
	    		pan: 		'125ESDX148', 
	    		gst: 		'21HGDV25416KJNMD',
	    		status: 	'Deactive' 
	    	}
	    ];
	},
  	

    $scope.add_category = function(form,ev){
		var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
				
			    var confirm = $mdDialog.confirm()
			          .title('Add New Category')
			          .textContent('Would you like to add new categoty ?')
			          .ariaLabel('Add Category')
			          //.targetEvent(ev)
			          .ok('YES')
			          .cancel('NO');
			    $mdDialog.show(confirm).then(function() {
			    	console.log(form)
			    	$scope.$watch('files.length',function(newVal,oldVal){
				            console.log($scope.files);
				        });
			      //$scope.status = 'You decided to get rid of your debt.';
			      //console.log('add new categoty');
			    }, function() {
			      //$scope.status = 'You decided to keep your debt.';
			      //console.log('not add new categoty');
			    });
			}
    },
    $scope.get_profile = function(){
    	console.log('hello');
    	$scope.lists = [
			    { title: 'Company Name', value: 'AVIANCE SERVICES' },
			    { title: 'Company Display Name', value: 'AVIANCE SERVICES'},
			    { title: 'Mobile Number', value: '7705023330' },
			    { title: 'Primary Email', value: 'aviancesales@gmail.com' }
			  ];
   		},
   		$scope.get_qrcode_data = function()
    {
      masterService.get_qrcode($scope.data,$scope);
    },
    $scope.generate_qrcode = function(form,ev)
    {
       var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
				var confirm = $mdDialog.confirm().title('Generate QRCode').textContent('Would you like to Generate new qrcode ?').ariaLabel('Generate QRCode')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
               masterService.generate_qrcode($scope.qrcode,$scope);
               /*$scope.$watch('files.length',function(newVal,oldVal){
                           console.log($scope.files);
                   });*/
           },function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
				
			}
	},
			$scope.assign_qrcode= function(form,ev)
            {
    	 var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
				var confirm = $mdDialog.confirm().title('Assign QRCode').textContent('Would you like to Assign QRcode ?').ariaLabel('Assign QRCode')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
               masterService.assign_qrcode($scope.assign,$scope);
               /*$scope.$watch('files.length',function(newVal,oldVal){
                           console.log($scope.files);
                   });*/
           },function(){
           	$scope.assignsmsg = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });

			     }
          },

       $scope.fetch_qrcode= function(qrcode)
	    {
	      $scope.assign=qrcode;
	  },
	   $scope.get_user_detail= function(data)
    {
       if(data != '' || data != 'NULL')
       {
		masterService.get_user_data(data,$scope);
      }
      else{
      	console.log('failed');
      }
    },
    $scope.get_config =function(){
   		masterService.getConfig($scope.data,$scope);
    },
    $scope.edit_siteConfig = function(configData){
      $scope.config={
      	'configId':configData.m00_id,
      	'configName':configData.m00_name,
      	'configValue':configData.m00_value,
      	'configDescription':configData.m00_desc};
			$scope.disable_edit = {
            'display': 'none'
	        };
	        $scope.enable_edit = {
	            'display': 'inline'
	        };
    },
    $scope.showbutton = function()
    {
    	$scope.hideBtn= !$scope.hideBtn;
    },

    $scope.add_site_config = function(form,ev){
   			var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
                 
                 var confirm = $mdDialog.confirm().title('Add site configuration').textContent('Would you like to Add new site configuration ?').ariaLabel('Add site configuration')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
              $scope.data={
					'Site_Config_Name':$scope.config.configName,
					'Site_Config_Value':$scope.config.configValue,
					'Site_Config_Discription':$scope.config.configDescription
				}
				masterService.addConfig($scope.data,$scope);
               /*$scope.$watch('files.length',function(newVal,oldVal){
                           console.log($scope.files);
                   });*/
           },function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
				

			}
   		},
   		$scope.update_site_config = function(form,scope)
   		{
   				var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
				var confirm = $mdDialog.confirm().title('Update site configuration').textContent('Would you like to Update site configuration ?').ariaLabel('Update site configuration')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
               $mdDialog.show(confirm).then(function() {
              $scope.data={
					'Site_Id':$scope.config.configId,
					'Site_Config_Name':$scope.config.configName,
					'Site_Config_Value':$scope.config.configValue,
					'Site_Config_Discription':$scope.config.configDescription
				}

				masterService.addConfig($scope.data,$scope);
               /*$scope.$watch('files.length',function(newVal,oldVal){
                           console.log($scope.files);
                   });*/
           },function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
				
			}

   		},
   		$scope.get_sellerStatus_count = function()
	    {
	      masterService.get_sellerStatus_count($scope.data,$scope);
	    },
	    $scope.get_seller_data= function()
	    {
	    	masterService.get_seller_data($scope.data,$scope);
	    },
	    $scope.get_userStatus_count = function()
	    {
	       masterService.get_userStatus_count($scope.data,$scope);
	    },
	    $scope.change_seller_status= function(data)
	    {
	      if(data !='' || data !='NULL')
	      {
	      	var confirm = $mdDialog.confirm().title('Change Seller Status').textContent('Would you like to change the status of seller ?').ariaLabel('Change Seller status')
	               //.targetEvent(ev)
	               .ok('YES').cancel('NO');
	           $mdDialog.show(confirm).then(function() {
	               masterService.change_seller_status(data,$scope);
	               /*$scope.$watch('files.length',function(newVal,oldVal){
	                           console.log($scope.files);
	                   });*/
	           },function(){
	           	$scope.alerts = [
	                   { type: 'warning', msg: 'You have decline the process' }
	               ];
	                 
	           });
	          
	      }
	    },
	    $scope.get_alluser_data= function()
	    {
	    	masterService.get_alluser_data($scope.data,$scope);
	    },
	    $scope.change_user_status= function(data)
	    {
	      if(data !='' || data !='NULL')
	      {
	      	var confirm = $mdDialog.confirm().title('Change User Status').textContent('Would you like to change the status of user?').ariaLabel('Change User Status')
	               //.targetEvent(ev)
	               .ok('YES').cancel('NO');
	           $mdDialog.show(confirm).then(function() {
	               masterService.change_user_status(data,$scope);
	               /*$scope.$watch('files.length',function(newVal,oldVal){
	                           console.log($scope.files);
	                   });*/
	           },function(){
	           	$scope.alerts = [
	                   { type: 'warning', msg: 'You have decline the process' }
	               ];
	                 
	           });
	      	
	      	
	      }
	    },
	     $scope.add_user = function(form,ev)
	    {
	      var firstError = null;
	      if (form.$invalid) 
	      {
	        var field = null, firstError = null;
	        for (field in form) 
	        {
	          if (field[0] != '$')
	          {
	            if (firstError === null && !form[field].$valid) 
	            {
	              firstError = form[field].$name;
	            }
	            if (form[field].$pristine) 
	            {
	              form[field].$dirty = true;
	            }
	          }
	        }
	        angular.element('.ng-invalid[name=' + firstError + ']').focus();
	        return;
	      }else{
	      masterService.add_user($scope.user,$scope);
	    }
	    },
	    $scope.get_message = function()
		    {
		    	masterService.getmessage($scope.data,$scope);
		    },
	    $scope.edit_message = function(messagedata)
	    {
	    	$scope.message=messagedata;
	    	$scope.disable_edit = {
            'display': 'none'
	        };
	        $scope.enable_edit = {
	            'display': 'inline'
	        };
	    },
	    $scope.add_message = function(form,ev)
         {
    	var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
				var confirm = $mdDialog.confirm().title('Add Message').textContent('Would you like to Add new message ?').ariaLabel('Add Message')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
               $scope.data={
                   'messageTitle':$scope.message.messageTitle,
                   'messageSubject':$scope.message.messageSubject,
                   'messageContent':$scope.message.messageContent,
                   'messageStatus':$scope.message.messageStatus
				}
				masterService.add_config_message($scope.data,$scope);
				$scope.message='';
               /*$scope.$watch('files.length',function(newVal,oldVal){
                           console.log($scope.files);
                   });*/
           },function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
			}

    },
        $scope.update_message = function(form,ev)
    {
    	var firstError = null;
			if (form.$invalid) 
			{
				var field = null, firstError = null;
				for (field in form) 
				{
					if (field[0] != '$')
					{
						if (firstError === null && !form[field].$valid) 
						{
							firstError = form[field].$name;
						}
						if (form[field].$pristine) 
						{
							form[field].$dirty = true;
						}
					}
				}
				angular.element('.ng-invalid[name=' + firstError + ']').focus();
				return;
			}else{
				var confirm = $mdDialog.confirm().title('Update Message').textContent('Would you like to Update message ?').ariaLabel('Update Message')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
              masterService.add_config_message($scope.message,$scope);
              $scope.message='';
               /*$scope.$watch('files.length',function(newVal,oldVal){
                           console.log($scope.files);
                   });*/
           },function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
                  
			}

    },
    $scope.fetch_category= function()
    {
    	masterService.fetch_category($scope.data,$scope);
    },
    $scope.editCategory =function(data)
    {
    	$scope.category=data;
    	if(data.categoryStatus == 'Active')
    	{
    		$scope.category.categoryStatus =1;
    	}
    	else
    	{
    		$scope.category.categoryStatus =2;
    	}
    },

       $scope.add_category = function (form,scope) 
  { 
    var firstError = null;
    if (form.$invalid) 
    {
      var field = null, firstError = null;
      for (field in form) 
      {
        if (field[0] != '$')
        {
          if (firstError === null && !form[field].$valid) 
          {
            firstError = form[field].$name;
          }
          if (form[field].$pristine) 
          {
            form[field].$dirty = true;
          }
        }
      }
      angular.element('.ng-invalid[name=' + firstError + ']').focus();
      return;
    } 
    else 
    {
    	var confirm = $mdDialog.confirm().title('Update Message').textContent('Would you like to Update message ?').ariaLabel('Update Message')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {

        $http({
          url: $location.protocol() + '://' +$location.host() +'/'+$base_url+"/MasterWebService/add_category",
          method: "POST",
          headers: { "Content-Type": undefined },
          transformRequest: function(data) {
            var formData = new FormData();
            formData.append("categoryData", angular.toJson($scope.category));
            formData.append("img", data.files[0]);
          return formData;//NOTICE THIS RETURN WHICH WAS MISSING
        },
        data: { 'categoryData': $scope.category, files: $scope.files }
      })
      .success(function(response) {
        
        if(!response.error)
        {
          $scope.alerts = [
                   { type: 'success', msg: 'successfully Updated!' }
               ];
        }
        
      });
  },function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
     
  }
},
$scope.deleteCategory= function(data,scope)
{
	masterService.deleteCategory(data,scope);
},
$scope.updateseller = function(form,scope)
{
	var firstError = null;
    if (form.$invalid) 
    {
      var field = null, firstError = null;
      for (field in form) 
      {
        if (field[0] != '$')
        {
          if (firstError === null && !form[field].$valid) 
          {
            firstError = form[field].$name;
          }
          if (form[field].$pristine) 
          {
            form[field].$dirty = true;
          }
        }
      }
      angular.element('.ng-invalid[name=' + firstError + ']').focus();
      return;
    } 
    else 
    {
    	var confirm = $mdDialog.confirm().title('Update Message').textContent('Would you like to Update message ?').ariaLabel('Update Message')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
             
             masterService.updateseller($scope.seller,$scope);
           	},function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });

} 
}, 
//$scope.seller={};
$scope.fetch_seller_data=function(data)
{   $scope.seller={'sellerId':$stateParams.SellerId};
	masterService.fetch_seller_data($scope.seller,$scope);
},
$scope.fetch_subcategory= function(data)
{
	$scope.data={'sellerId': $stateParams.SellerId};
	masterService.fetch_subcategory($scope.data,$scope);
},
$scope.fetch_subcategory= function(data)
{
	$scope.data={'sellerId':$stateParams.SellerId};
	masterService.fetch_subcategory($scope.data,$scope);
},
$scope.insert_brand= function(form,scope)
{   
	var firstError = null;
    if (form.$invalid) 
    {
	 var field = null, firstError = null;
      for (field in form) 
      {
        if (field[0] != '$')
        {
          if (firstError === null && !form[field].$valid) 
          {
            firstError = form[field].$name;
          }
          if (form[field].$pristine) 
          {
            form[field].$dirty = true;
          }
        }
      }
      angular.element('.ng-invalid[name=' + firstError + ']').focus();
      return;
  }
      else 
    {
    	var confirm = $mdDialog.confirm().title('Add Brand').textContent('Would you like to Update message ?').ariaLabel('Add Brand')
               //.targetEvent(ev)
               .ok('YES').cancel('NO');
           $mdDialog.show(confirm).then(function() {
             
             masterService.insert_brand($scope.brand,$scope);
           	},function(){
           	$scope.alerts = [
                   { type: 'warning', msg: 'You have decline the process' }
               ];
                 
           });
    }
    } 



   		
});