var app = angular.module('app', [
  'ngMaterial',
  'lfNgMdFileInput',
  'ui.router',
  'angular-loading-bar',
  'oc.lazyLoad',
  'ngMessages',
  'material.svgAssetsCache',
  'ui.bootstrap',
  'angular-jwt',
  'angular-storage'

  ])
   .config(function($mdAriaProvider, $httpProvider, jwtInterceptorProvider) {
   $mdAriaProvider.disableWarnings();
   $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    jwtInterceptorProvider.tokenGetter = ['userService', function(userService) {
        return localStorage.getItem('token');
    }];
   $httpProvider.interceptors.push('jwtInterceptor');
});


   app.run(["$rootScope", 'jwtHelper', 'store', '$location', function($rootScope, jwtHelper, store, $location)
{
    $rootScope.$on('$routeChangeStart', function (event, next) 
    {
        var token = store.get("token") || null;
        if(!token)
            $location.path("/login");

        var bool = jwtHelper.isTokenExpired(token);
        if(bool === true)
            $location.path("/login");
    });
}]);