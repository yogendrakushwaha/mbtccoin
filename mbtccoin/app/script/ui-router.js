/***************      progress bar      ***************/
app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}])
/******************       routing        *******************/
app.config(function($stateProvider) {
    var base_url="app/";
    $stateProvider
        .state('home', {
        url: '/',
        views: {
            'navbar': {
                templateUrl: 'app/views/nav.html',
            },

            'main': {
                templateUrl: 'app/views/front.html'
            }
        }

    }).state('login',{
        url : "/login",
        views : {
            'navbar':{
                templateUrl: base_url+'views/nav.html',
            },

            'main':{
                templateUrl: base_url+'views/login.html',
            }
        }
    }).state('verify_email',{
        url : "/verify_email",
        views : {
            'navbar':{
                templateUrl: base_url+'views/nav.html',
            },

            'main':{
                templateUrl: base_url+'views/verify-email.html',
            }
        }
    }).state('sales',{
        url : "/sales",
        views : {
            'navbar':{
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar':{
                templateUrl: base_url+'views/sidebar.html',
            },
            'main':{
                templateUrl: base_url+'views/home.html',
            }
        }
    })
    /* //////////////// /////////////           Login Signup routes             ///////// ////////////*/


        .state('signup', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/nav.html',
            },
            'main': {
                templateUrl: base_url+"views/signup.html"
            }
        },
        url: "/signup"
    }).state('forget_password', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/nav.html',
            },
            'main': {
                templateUrl: base_url+"views/forget_password.html"
            }
        },
        url: "/forget_password"
    })


    /////  *****************    
    /////  Admin Route  Start Here 
    /////
    /////
    /////  *****************

        .state('siteconfig', {
        url: "/siteconfig",
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Config/site_config.html",

            }
        }
    }).state('addsiteconfig', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Config/add_site_config.html"
            }
        },
        url: "/addsiteconfig",
    }).state('message', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Config/message.html"
            }
        },
        url: "/message",
    }).state('editmessage', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Config/edit_message.html"
            }
        },
        url: "/edit_message",
    }).state('category', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Config/category.html"
            }
        },
        url: "/category",

    })
        .state('qrcode',{
        views: {
            'navbar':{
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar':{
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+'views/admin/config/qrcode.html'
            }
        },
        url:"/qrcodegenerator",
    })

    ///////////////////////////////////////////// Profiles Route Here        ///////////////////////////////////
        .state('sellers', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Profile/all_seller.html"
            }
        },
        url: "/sellers"
    })
        .state('users', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Profile/all_users.html"
            }
        },
        url: "/users"
    })
        .state('adduser', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Profile/add_user.html"
            }
        },
        url: "/add_user"
    })
        .state('viewuserinfo', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Profile/user_profile.html"
            }
        },
        url: "/view_userinfo"
    })
        .state('addsellerinfo', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Profile/add_seller.html"
            }
        },
        url: "/add_sellerinfo/:SellerId"
    })
        .state('viewsellerinfo', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Profile/seller_profile.html"
            }
        },
        url: "/view_sellerinfo"
    })
        .state('all_reports', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/all_reports"
    })
        .state('all_orders', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/all_orders"
    })
        .state('admindashboard', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/admin_dashboard"
    })
        .state('requestToken', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/Admin/Action/requestToken.html"
            }
        },
        url: "/requestToken"
    })

    /////  *****************    
    /////  Admin Route End Here 
    /////
    /////
    /////  *****************

    /////  *****************
    /////  Seller Route Start Here 
    /////
    /////
    /////  *****************




    /////  *****************
    /////  Seller Route End  Here 
    /////
    /////
    /////  *****************



    /////  *****************
    /////  User Route Start Here 
    /////
    /////
    /////  *****************


    /////  *****************
    /////  User Route End Here 
    /////
    /////
    /////  *****************

    /* //////////////// /////////////           Site Congiguration routes             ///////// ////////////*/




        .state('attribute', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/config/attribute.html"
            }
        },
        url: "/add_attribute"
    })



    /* //////////////// /////////////           catalog routes             ///////// ////////////*/


        .state('catalog', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/catalog/catalog.html",
                views:{

                    'catalog_in_stock': {
                        templateUrl: base_url+"views/catalog/in_stock.html"
                    },
                },
            }

        },
        url: "/catalog"
    }).state('add_product', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/catalog/add_product.html"
            }
        },
        url: "/add_product"
    })
        .state('add_single_product', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/admin_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/catalog/add_single_product.html"
            }
        },
        url: "/add_single_product"
    })


    /* //////////// ///////// ///////           user profile routes        /////////// ///////////// ///////// */


        .state('myprofile', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/profile.html"
            }
        },
        url: "/myprofile"
    })
        .state('settings', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/settings.html"
            }
        },
        url: "/settings"
    })
        .state('purchase', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/purchase.html"
            }
        },
        url: "/purchase"
    })
        .state('transfer', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/transfer.html"
            }
        },
        url: "/transfer"
    })
        .state('dashboard', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/dashboard.html"
            }
        },
        url: "/dashboard"
    })
        .state('mywallet', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/mywallet.html"
            }
        },
        url: "/mywallet"
    })

        .state('currentoffers', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/current_offers"
    })

        .state('nearbyoffers', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/nearby_offers"
    })
        .state('orders', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/orders"
    })
        .state('reports', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/reports"
    })
        .state('membership', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar': {
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+"views/User/coming-soon.html"
            }
        },
        url: "/membership"
    })
        .state('aboutUs',{
        views: {
            'navbar':{
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar':{
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+'views/User/about.html'
            }
        },
        url:"/aboutus",
    })
        .state('privacy_policy',{
        views: {
            'navbar':{
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar':{
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+'views/User/privacy_policy.html'
            }
        },
        url:"/privacy_policy",
    })
        .state('terms',{
        views: {
            'navbar':{
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar':{
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+'views/User/terms.html'
            }
        },
        url:"/terms",
    })
        .state('contactus',{
        views: {
            'navbar':{
                templateUrl: base_url+'views/user_nav.html',
            },
            'sidebar':{
                templateUrl: base_url+'views/user_sidebar.html',
            },
            'main': {
                templateUrl: base_url+'views/User/contact_us.html'
            }
        },
        url:"/contactus",
    })
        .state('notfound', {
        views: {
            'navbar': {
                templateUrl: base_url+'views/nav.html',
            },

            'main': {
                templateUrl: base_url+"views/404.html"
            }
        },
        url: "*path",
    });
});