 $(document).ready(function(){
      //  $('form').attr('autocomplete','off');
         $( ".mydate" ).datepicker({showAnim:'slideDown',dateFormat:'yy-mm-dd'});
        $('.div_service').hide();
        $("#back-top").hide();
        var showID = location.hash;
        if (showID) {
             $(showID).fadeIn();
             setTimeout(function(){
                 $("html, body").animate({ scrollTop: $(showID).offset().top });
             },500);
        }

        $(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
		});

                $('#module-1, #module-2').click(function () {
			$('body,html').animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 500);
			return false;
		});
                $('body input,body textarea, body select ').each(function(){
                if ($(this).next('div').html() == "") {
                    $(this).next('div').html('Key: '+$(this).attr('name'));
                }
            });
	});


        $('.list_1 > li > a').on('click',function(){
            $('#search').val($(this).text());

            $('.div_service').fadeOut(500);
            var showID = $(this).data('href');
            $('#search_id').val(showID);
            $(showID).fadeIn(1000);
            setTimeout(function(){
                $("html, body").animate({ scrollTop: $(showID).offset().top });
            },500);
            });

        $('#reg_type').on('change',function(){
            var regType = $(this).val();
            if (regType == 'normal') {
                $('.normal, #social').slideUp();
                $('#normal').slideDown();
            }else{
                $('.normal').slideDown();
                $('#normal').slideUp();
                $('#social').slideDown();
            }
        });

        $("#search").on('keyup change blur',function(e){
           if (e.type == "keyup") {
            $('#search_id').val('');
           }
           //   $('li a').trigger('click');

            $('.div_service').hide();
                // Retrieve the input field text and reset the count to zero
                var filter = $(this).val(), count = 0;

                // Loop through the comment list
                $(".list_1 li a").each(function(){
                    // If the list item does not contain the text phrase fade it out
                    var anchorText = $(this).text().search(new RegExp(filter, "i"));
                    var serviceUrl = $(this).next('p').text().search(new RegExp(filter, "i"))
                    if (anchorText < 0 && serviceUrl < 0) {
                        $(this).parents('li').fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                    } else {
                      $(this).parents('li').show();
                       $($('#search_id').val()).show();
                     // $(this).click();
                     // $($(this).data('href')).show();
                        count++;
                    }
                });
                //$('#search_id').val('');
        }).trigger('change');

        });
